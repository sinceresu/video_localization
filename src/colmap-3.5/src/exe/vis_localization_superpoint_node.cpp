#include <ros/ros.h>

#include "pre_local/colmap_io.hpp"

/*
工程功能：
1.读取colmap的db文件获取匹配信息，计算图像定位
2.读取superpoint的匹配信息，计算定位
./vis_localization_superpoint 
/media/zmc/zmc/fuwuqidata/mult_camera_down/sparse_BA_txt_txt_last 
/media/zmc/Teclast_S20/localization_ws/project/to_zhoumingchao/Re_02/match/localization/1635748770.820487500_rear.txt

使用是记得修改“auto image = *(reconstruction_map_.FindImageWithName("1635748770.820487500_rear.jpg"));”


2022.03.03:共能修改,读取的不在是单一的匹配文件，遍历所有匹配文件输出所有3d2d点，匹配文件的第一行信息不使用
./vis_localization_superpoint 
/media/zmc/zmc/fuwuqidata/mult_camera_down/sparse_BA_txt_txt_last
/media/zmc/Teclast_S20/localization_ws/project/to_zhoumingchao/Re_02/match/240

*/

int main (int argc, char** argv)
{
    ros::init(argc, argv, "online_localization");
    std::shared_ptr<ColmapFileReader> colmap_file_reader_ptr_ = std::make_shared<ColmapFileReader>();
    colmap_file_reader_ptr_->ReadMap(argv[1]);
    
    ros::NodeHandle nh;

    //colmape localization
    // colmap_file_reader_ptr_->ReadMatches(argv[2]);
    // colmap::Image ref_fram;
    // colmap_file_reader_ptr_->GetRefFrame(ref_fram);
    // colmap_file_reader_ptr_->FilterFrame3D(ref_fram);
    // colmap_file_reader_ptr_->Get3D2DMatches(ref_fram);
    // colmap_file_reader_ptr_->LocalizationColmapSift(argv[1]);

    //super point localization

    colmap_file_reader_ptr_->RunSuperPointLocalization(argv[2], argv[1]);


    return 1;
}