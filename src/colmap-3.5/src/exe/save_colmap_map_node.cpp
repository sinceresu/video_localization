#include <pcl/point_types.h> 
#include <pcl/io/pcd_io.h> 

#include "pre_local/colmap_io.hpp"

/*
程序功能：读取colmap建图，保存其中的3d点为点云，点云中点的I表示3D点的id。
./save_cloudpoint_index_node /media/zmc/Teclast_S20/localization_ws/project/colmap-3.5/python/superglue/images/sparse
*/

int main(int argc, char** argv)
{
    std::shared_ptr<ColmapFileReader> colmap_file_reader_ptr_ = std::make_shared<ColmapFileReader>();
    colmap_file_reader_ptr_->ReadMap(argv[1]);
    colmap_file_reader_ptr_->SaveMapWithImage();
    return 1;
}