#include <pcl/io/ply_io.h>
#include <pcl/point_types.h>
#include <opencv2/calib3d/calib3d.hpp>

#include "pre_local/colmap_io.hpp"
#include "../base/similarity_transform.h"
#include "tool/util.hpp"

/* 
利用一张图像在不同的地图中特征点的3d位置，计算两个地图的坐标转换。
./trans_two_cloud /home/zmc/Data/data/landengxia/huagui/0 /home/zmc/Data/data/landengxia/huagui2/0 vlcsnap-2022-01-04-11h21m58s302.png */

void trans_map(pcl::PointCloud<pcl::PointXYZRGB>& in_cloud, const colmap::SimilarityTransform3& tform)
{
    for(auto& pt : in_cloud)
    {
        auto pt_eigen = Eigen::Vector3d(pt.x, pt.y, pt.z);
        tform.TransformPoint(&pt_eigen);
        pt.x = pt_eigen[0];
        pt.y = pt_eigen[1];
        pt.z = pt_eigen[2];
    }
}

void trans_map(pcl::PointCloud<pcl::PointXYZRGB>& in_cloud, const Eigen::Isometry3d T)
{
    for(auto& pt : in_cloud)
    {
        auto pt_eigen = Eigen::Vector3d(pt.x, pt.y, pt.z);
        pt_eigen = T.linear() * pt_eigen + T.translation();
    
        // tform.TransformPoint(&pt_eigen);
        pt.x = pt_eigen[0];
        pt.y = pt_eigen[1];
        pt.z = pt_eigen[2];
    }
}
// 根据superglue匹配进行拼图
int main(int argc, char** argv)
{
    double pix_error = 1.0;
    std::shared_ptr<ColmapFileReader> colmap_file_reader1_ptr_ = std::make_shared<ColmapFileReader>(pix_error);
    colmap_file_reader1_ptr_->ReadTwoMap(argv[1], argv[2]);
    std::vector<Eigen::Vector3d> src;
    std::vector<Eigen::Vector3d> dst;
    
    // std::vector<std::pair<int, int>> matches;
    std::vector<std::pair<uint32_t, uint32_t>> matches;

    uint32_t id1, id2;
    std::string match_file_name = argv[3];
    std::vector<cv::Point2f> l_pts, pts2d;
    l_pts = readMatches(match_file_name.c_str(), matches, id1, id2);
    Eigen::Matrix4d Tmap2image2;
    pts2d = colmap_file_reader1_ptr_->GetFrame3DPoint3FromSuperGlue(argv[4], argv[5], matches, src, dst, l_pts, Tmap2image2);

    std::vector<cv::Point3f> pts1;
    std::vector<cv::Point3f> pts2;
    cv::Point3f pt;
    pcl::PointCloud<pcl::PointXYZ> features_cloud;
    pcl::PointXYZ cloud_pt;
    for (size_t i = 0; i < src.size(); i++)
    {
        pt = cv::Point3f(src[i][0], src[i][1], src[i][2]);
        pts1.push_back(pt);
        pt = cv::Point3f(dst[i][0], dst[i][1], dst[i][2]);
        pts2.push_back(pt);
        // cloud_pt.x = src[i][0];
        // cloud_pt.y = src[i][1];
        // cloud_pt.z = src[i][2];
        // features_cloud.push_back(cloud_pt);

        // std::cout << "src is" << src[i][0] << " " << src[i][1] << " " << src[i][2] << std::endl;
        // std::cout << "dst is" << dst[i][0] << " " << dst[i][1] << " " << dst[i][2] << std::endl;
    }
    // pcl::io::savePLYFile("features_cloud.ply", features_cloud);

/*     cv::Mat R;
    cv::Mat t;
    //computer pts2 to pts1
    pose_estimation_3d3d(pts1, pts2, R, t);
    std::cout <<"R is "<< R << std::endl;
    std::cout <<"t is "<< t << std::endl; */

    cv::Mat r, t, inliers, K;
    K = (cv::Mat_<double>(3, 3) << 2304 , 0 , 959 ,
              0 , 2304 , 539 ,
              0 , 0 , 1);
    
    float change_score = 3.0;
    bool computer_success;
    // std::cout << "85 init pose K is "<< std::endl << K_ << std::endl;
    std::cout << pts1.size() << std::endl;
    std::cout << pts2d.size() << std::endl;
    computer_success = cv::solvePnPRansac(pts1, pts2d, K, cv::Mat(), r, t, false, 300, change_score, 0.99, inliers, cv::SOLVEPNP_ITERATIVE);
    
/*     std::deque<std::pair<cv::Mat, cv::Mat>> pose_dq;
    while(computer_success && change_score > 0.2)
    {
        pose_dq.push_back(std::make_pair(r,t));
        change_score -= 0.2;
        // computer_success = cv::solvePnPRansac(match_pt3d_, match_pt2d_, K_, cv::Mat(), r, t, false, 300, change_score, 0.99, inliers, cv::SOLVEPNP_ITERATIVE);
        computer_success = cv::solvePnPRansac(pts1, pts2d, K, cv::Mat(), r, t, false, 300, change_score, 0.99, inliers, cv::SOLVEPNP_ITERATIVE);
    }

    if(pose_dq.size() == 1)
    {
        std::cout << " InitComputePose fail, please change solvePnPRansac score." << std::endl;
        return false;
    }

    r = pose_dq.back().first;
    t = pose_dq.back().second; */

    cv::Mat R;
    cv::Rodrigues(r, R);
    Eigen::Isometry3d T;
    T.setIdentity();
    pcl::PointCloud<pcl::PointXYZRGB> input_cloud;
    pcl::io::loadPLYFile(argv[6], input_cloud);
    Eigen::Matrix3d matrix_r;
    Eigen::Vector3d matrix_t;
    cv::cv2eigen(R, matrix_r);
    cv::cv2eigen(t, matrix_t);
    T.linear() = matrix_r;
    T.translation() = matrix_t;
    T = T.inverse() * Tmap2image2;
    trans_map(input_cloud, T);
    pcl::io::savePLYFile("after_trans.ply", input_cloud);
/*     pcl::PointCloud<pcl::PointXYZRGB> input_cloud;
    pcl::io::loadPLYFile(argv[6], input_cloud);

    Eigen::Isometry3d T;
    T.setIdentity();
    Eigen::Matrix3d R_matrix;
    Eigen::Vector3d t_matrix;
    cv2eigen(t, t_matrix);
    cv2eigen(R, R_matrix);
    T.linear() = R_matrix;
    T.translation() = t_matrix;
    trans_map(input_cloud, T);
    pcl::io::savePLYFile("after_trans.ply", input_cloud); */

/*     colmap::SimilarityTransform3 tform;
    //tform 表示 src 到 dst
    tform.Estimate(src, dst);
    std::cout << "sim3 is : " << std::endl << tform.Matrix() << std::endl;
    std::cout << "sim3 scale is : " << std::endl << tform.Scale() << std::endl;

    pcl::PointCloud<pcl::PointXYZRGB> input_cloud;
    pcl::io::loadPLYFile(argv[6], input_cloud);

    trans_map(input_cloud, tform);
    pcl::io::savePLYFile("after_trans.ply", input_cloud); */

}


//俩个地图包含同一张照片的拼接
/* int main(int argc, char** argv)
{
    double pix_error = 0.2;
    std::shared_ptr<ColmapFileReader> colmap_file_reader1_ptr_ = std::make_shared<ColmapFileReader>(pix_error);
    colmap_file_reader1_ptr_->ReadTwoMap(argv[1], argv[2]);
    std::vector<Eigen::Vector3d> src;
    std::vector<Eigen::Vector3d> dst;
    
    colmap_file_reader1_ptr_->GetFrame3DPoint3(argv[3], src, dst);

    colmap::SimilarityTransform3 tform;
    //tform 表示 src 到 dst
    tform.Estimate(src, dst);
    std::cout << "sim3 is : " << std::endl << tform.Matrix() << std::endl;
    std::cout << "sim3 scale is : " << std::endl << tform.Scale() << std::endl;

    pcl::PointCloud<pcl::PointXYZRGB> input_cloud;
    pcl::io::loadPLYFile(argv[4], input_cloud);

    trans_map(input_cloud, tform);
    pcl::io::savePLYFile("after_trans.ply", input_cloud);

} */