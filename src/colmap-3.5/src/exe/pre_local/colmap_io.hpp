#ifndef _COLMAP_IO_HPP_
#define _COLMAP_IO_HPP_


#include <pcl/kdtree/kdtree_flann.h>
#include <memory>
// #include <hash>
#include <unordered_map>
#include <geometry_msgs/Polygon.h>
#include <pcl/point_types.h> 
#include <pcl/io/pcd_io.h> 
#include <pcl/filters/frustum_culling.h>
#include <pcl/filters/passthrough.h>

#include "sophus/so3.h"
#include "../../base/reconstruction.h"
#include "../../base/database.h"
#include "../../base/database_cache.h"
#include "../../util/option_manager.h"
#include "../../util/endian.h"

#include "../tool/util.hpp"
#include "devel/include/COLMAP/PointMatch.h"
#include "../tool/params.hpp"

#include"cnpy.h"
#include<complex>


// #include "../localization/localization_flow.hpp"

using namespace colmap;
typedef std::vector<geometry_msgs::Point32> points;

struct hash_pair { 
    template <class T1, class T2> 
    size_t operator()(const std::pair<T1, T2>& p) const
    { 
        auto hash1 = std::hash<T1>{}(p.first); 
        auto hash2 = std::hash<T2>{}(p.second); 
        return hash1 ^ hash2; 
    } 
}; 

class ColmapFileReader : public Params
{
  public:
    ColmapFileReader(): Params()
    {

    };
    ColmapFileReader(const double pix_error);
    void ReadMap(const std::string& colmap_path);
    void ReadMatches(const std::string& match_file);
    Eigen::Matrix4d GetMapFramePose(const Image& image);
    void FilterFrame3D(Image& map_frame, bool draw_feature = false);
    void FilterMap3D(bool draw_feature = false);
    void Get3D2DMatches(const Image& image);//临时
    void GetSuperGlue3D2DMatches(const std::string& image_name, const points& pt_uv_id,
                            std::vector<cv::Point2f>& pts2d, std::vector<cv::Point3f>& pts3d);
    void LocalizationColmap(const std::string colmap_path);
    void LocalizationColmapSift(const std::string colmap_path);
    void GetRefFrame(Image& map_frame);
    Image GetImageWithName(const std::string image_name);

    // void RunColmapLocalization();
    void GetSuperGlue3D2DMatches(std::string& map_image_name);
    void RunSuperPointLocalization(const std::string& match_file, const std::string& colmap_file);

    //保存关键帧pose的XY值为点云，构建map pose和image name
    //根据定位pose查找最近的点
    //根据找到的最近pose，在map中找到image name
    std::string SearchNearImage(const Eigen::Matrix4f pose);

    //保存colmap重建点云， 点云中I表示其为地图中第I个3d点，
    // 根据3d点可以知道有多少图像观测到了这个点，
    // 遍历每个点，找到当前局部点云和哪个图像最相似
    void SavePointCloudWithIndex();
    void SaveMapWithImage();
    void CutPointIndexCloud(const Eigen::Matrix4f& pose);
    std::vector<std::string> FindLocalFrame();

    void ReadTwoMap(const std::string& colmap1_path, const std::string& colmap2_path);
    //获取指定关键帧的3D点
    void GetFrame3DPoint3(std::string frame_name, std::vector<Eigen::Vector3d>& src, std::vector<Eigen::Vector3d>& dst);
    //读取Superglue匹配txt文件， 分别从两个地图中选择3D点
    std::vector<cv::Point2f> GetFrame3DPoint3FromSuperGlue(const std::string& map1_fm, const std::string& map2_fm, 
                                                     const std::vector<std::pair<uint32_t, uint32_t>>& matches, 
                                                     std::vector<Eigen::Vector3d>& src, std::vector<Eigen::Vector3d>& dst,
                                                     const std::vector<cv::Point2f>& l_pts,
                                                     Eigen::Matrix4d& T_w2c);
    //读取带尺度定位，计算sim3
    void GetSim3Matched(std::vector<Eigen::Vector3d>& src, std::vector<Eigen::Vector3d>& dst, std::unordered_map<std::string, Eigen::Vector3d>& pose_map);
    //将尺度更新到图像定位，并保存
    void RecoverImageScale(const SimilarityTransform3& tform);

    void FilterByCloud();


  private:
    colmap::Reconstruction reconstruction_map_;
    colmap::Reconstruction reconstruction_map2_;
    DatabaseCache database_cache_;

    // colmap::Reconstruction reconstruction_match_;
    colmap::Image ref_frame_;
    colmap::Image ref_match_frame_;
    colmap::Image local_frame_;
    Eigen::Matrix3d map_K_;
    Eigen::Matrix3d local_K_;
    double map_projection_pix_uv_sum_error_;
    std::vector<Point2D> colmap_pt2d_;
    std::vector<Point3D> colmap_pt3d_;
    std::vector<cv::Point3f> pts3d_;
    std::vector<cv::Point2f> pts2d_;
    //为什么编译不通过？
    // std::shared_ptr<LocalizationFlow> localization_flow_ptr_;
    // LocalizationFlow localization;
    cv::Mat image_ref_;
    cv::Mat image_loc_;

    std::string match_file_;
    pcl::PointCloud<pcl::PointXYZI> cut_pc_;
    pcl::PointCloud<pcl::PointXYZI>::Ptr cloud_index_;

    pcl::PointCloud<pcl::PointXYZ> pose_xy_cloud_;
    std::unordered_map<std::pair<float, float>, std::string, hash_pair> un_map_pose_name_;
    std::unordered_map<std::string, Eigen::Quaterniond> un_map_name_R_;
    pcl::KdTreeFLANN<pcl::PointXYZ> pose_kdtree_;
    std::map<std::string, colmap::image_t> image_name_id_map_;

};








#endif