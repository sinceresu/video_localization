#include "colmap_io.hpp"
#include "../localization/localization.hpp"


using namespace colmap;

ColmapFileReader::ColmapFileReader(const double pix_error)
:Params()
{
    // std::shared_ptr<Localization> localization_ptr_ = std::make_shared<Localization>();
    // localization_ptr_ = std::make_shared<Localization>();
    //扫描车全景
    // map_K_ << 856.43 , 0 , 1440 ,
    //           0 , 856.43 , 1440 ,
    //           0 , 0 , 1;
    map_K_ << 2304 , 0 , 959 ,
              0 , 2304 , 539 ,
              0 , 0 , 1;

    // map_K_ << 1859.33 , 0 , 1280 ,
    //         0 , 1859.33 , 720 ,
    //         0 , 0 , 1;
    // std::cout << "map_k is " << std::endl << map_K_ << std::endl;

    //手机内参
    // local_K_ << 2.8139998692833274e+03, 0., 1.8371119520047309e+03, 
    //             0.,2.8177509464325831e+03, 1.3553898663009072e+03, 
    //             0., 0., 1.;
    //usb小相机1
    local_K_ << 6.9913438965345074e+02, 0., 5.4594832506454179e+02, 0.,
       6.9954891305755768e+02, 9.8866195362102337e+02, 0., 0., 1.;
    //
    // local_K_ << 856.43, 0., 1440, 0.,
    //    856.43, 1440, 0., 0., 1.;

            // 6265.05 3976 2652
    // map_K_ << 6265.05 , 0 , 3976 ,
    //           0 , 6265.05 , 2652 ,
    //           0 , 0 , 1;
    // local_K_ << 1.7989816263225610e+04, 0., 2.4563627434350524e+03, 
    //             0.,1.7996884058083324e+04, 1.8127859725369372e+03, 
    //             0., 0., 1.;
    map_projection_pix_uv_sum_error_ = pix_error;
    std::string WORKSPACE = ros::package::getPath("COLMAP");
    std::string param_file_path = WORKSPACE + "/config/map/cloud_with_index.pcd";

    pcl::PointCloud<pcl::PointXYZI> cloud_tmp;
    pcl::io::loadPCDFile(param_file_path, cloud_tmp);
    pcl::PointCloud<pcl::PointXYZI>::Ptr c2loud_index_(new pcl::PointCloud<pcl::PointXYZI>(cloud_tmp));
    cloud_index_ = c2loud_index_;

}

void ColmapFileReader::ReadMap(const std::string& colmap_path)
{
    reconstruction_map_.Read(colmap_path);
    
    auto images = reconstruction_map_.Images();
    LOG(INFO) <<"map contain image num is: "<< images.size();


    int i = 1;
    int image_left_num = images.size();
    // for (const auto& image : images )
    for (; i <= image_left_num; i++)
    {
        // if (i > image_left_num)
        //     break;
        auto image = reconstruction_map_.Image(i);
        auto t = image.ProjectionCenter();
        pcl::PointXYZ pt;
        pt.x = t[0];
        pt.y = t[1];
        pt.z = 0;
        pose_xy_cloud_.push_back(pt);
        un_map_pose_name_[std::make_pair(pt.x, pt.y)] = image.Name();


        un_map_name_R_[image.Name()] = Eigen::Quaterniond(image.RotationMatrix());
        
/*         auto T_cam_2_world = image.second.InverseProjectionMatrix();
        Sophus::SO3 R_c_2_w = Sophus::SO3(T_cam_2_world.block<3,3>(0,0));
        Eigen::Vector3d R_vec = R_c_2_w.log();
        un_map_name_R_[image.second.Name()] = R_vec; */

        /* // if (i > image_left_num)
        //     break;
        auto image = reconstruction_map_.Image(i);
        auto t = image.second.ProjectionCenter();
        pcl::PointXYZ pt;
        pt.x = t[0];
        pt.y = t[1];
        pt.z = 0;
        pose_xy_cloud_.push_back(pt);
        un_map_pose_name_[std::make_pair(pt.x, pt.y)] = image.second.Name();
        
        auto T_cam_2_world = image.second.InverseProjectionMatrix();
        Sophus::SO3 R_c_2_w = Sophus::SO3(T_cam_2_world.block<3,3>(0,0));
        Eigen::Vector3d R_vec = R_c_2_w.log();
        un_map_name_R_[image.second.Name()] = R_vec;
        // i++; */
    }
    pose_kdtree_.setInputCloud(pose_xy_cloud_.makeShared());
}

void ColmapFileReader::ReadTwoMap(const std::string& colmap1_path, const std::string& colmap2_path)
{
    reconstruction_map_.Read(colmap1_path);
    reconstruction_map2_.Read(colmap2_path);
}


std::string ColmapFileReader::SearchNearImage(const Eigen::Matrix4f pose)
{
    int K = loop_xy_kf_num_;
    // int K = 15;
    std::vector<int> pointIdxNKNSearch(K);  
	std::vector<float> pointNKNSquaredDistance(K);
    pcl::PointXYZ search_pose;
    search_pose.x = pose(0,3);
    search_pose.y = pose(1,3);
    search_pose.z = 0;

    Eigen::Quaterniond Q = Eigen::Quaterniond(pose.block<3,3>(0,0).cast<double>());

    // Sophus::SO3 pose_SO3 = Sophus::SO3(pose.block<3,3>(0,0).cast<double>());
    // Eigen::Vector3d pose_vec = pose_SO3.log();


    std::string loop_image_name;
    double min_error = 1.0;
    if (pose_kdtree_.nearestKSearch(search_pose, K, pointIdxNKNSearch, pointNKNSquaredDistance) > 0)
    {
        //add rpy
        for(const auto& point_index : pointIdxNKNSearch)
        {
            auto pt = pose_xy_cloud_.points[point_index];
            std::string image_name = un_map_pose_name_[std::make_pair(pt.x, pt.y)];
            double error = rotation_error(un_map_name_R_[image_name], Q);
            if (error < 0.2)
            {
                return image_name;
            }
            if (error < min_error)
            {
                min_error = error;
                loop_image_name = image_name;
            }
            std::cout << "image " << image_name << ", rotation error is = " << error << std::endl;

            // un_map_name_R_

        }
        return loop_image_name;
        //add rpy
        // auto pt = pose_xy_cloud_.points[pointIdxNKNSearch[0]];
        // // std::cout << "find key is " << pt.x << " " << pt.y << std::endl;
        // return un_map_pose_name_[std::make_pair(pt.x, pt.y)];
    } 

/*     if (pose_kdtree_.nearestKSearch(search_pose, K, pointIdxNKNSearch, pointNKNSquaredDistance) > 0)

        for (size_t i = 0; i < pointIdxNKNSearch.size(); ++i)
        {
            auto pt = pose_xy_cloud_.points[pointIdxNKNSearch[i]];
            // std::cout << "find key is " << pt.x << " " << pt.y << std::endl;

            std::string loop_image_name = un_map_pose_name_[std::make_pair(pt.x, pt.y)];
            Eigen::Vector3d near_R_vec = un_map_name_R_[loop_image_name];

            LOG(INFO) << "0 error is " << abs(pose_vec[0] - near_R_vec[0]);
            LOG(INFO) << "1 error is " << abs(pose_vec[1] - near_R_vec[1]);
            LOG(INFO) << "2 error is " << abs(pose_vec[2] - near_R_vec[2]);
            LOG(INFO) << "x error is " << abs(pt.x - search_pose.x);
            LOG(INFO) << "y error is " << abs(pt.y - search_pose.y);
            if ((abs(pose_vec[0] - near_R_vec[0]) + abs(pose_vec[1] - near_R_vec[1]) + abs(pose_vec[2] - near_R_vec[2])) < 0.8
            && abs(pt.x - search_pose.x) + abs(pt.y - search_pose.y) < 0.8)
            {
                LOG(INFO) << "loop image is " << loop_image_name ;    
                return loop_image_name;
            }


        }
        
        
    } */
    // if (pose_kdtree_.nearestKSearch(search_pose, K, pointIdxNKNSearch, pointNKNSquaredDistance) > 0)
    // {
    //     for (size_t i = 0; i < pointIdxNKNSearch.size(); ++i){
    //         if ((special_pose_cloud_.points[pointIdxNKNSearch[i]].YPR[0] - update_pose_ypr[0]) < 20)
    //         {
    //             loop_index.push_back(special_pose_cloud_.points[pointIdxNKNSearch[i]].frame_id + ".jpg");
    //         }
    //         if (loop_index.size() == 3)
    //         {
    //             break;
    //         }
    //     }
    // }
    return "wrong";
}

void ColmapFileReader::ReadMatches(const std::string& match_file)
{
    Database database(match_file + "/match.db");
    // DatabaseCache database_cache;
    size_t min_num_matches = 5;
    bool ignore_watermarks = false;
    std::set<std::string> image_names;
    
    if (!readImageNames(match_file, image_names))
    {
        std::cout << "match file " << match_file << " don't have images!!!" << std::endl;
        std::cout << "may be no match features." << std::endl;
        return;
    }

    database_cache_.Load(database, min_num_matches,
                        ignore_watermarks,
                        image_names);

    auto images = database_cache_.Images();
    if (images.size() != 2)
    {
        std::cout << "match file " << match_file + "/match.db" << " don't have match images!!!" << std::endl;

    }
    std::string map_frame_name, local_frame_name;

    //TODO 优化逻辑
    if (reconstruction_map_.FindImageWithName(*image_names.begin()) == nullptr)
    {
        map_frame_name = *(image_names.rbegin());
        local_frame_name = *(image_names.begin());
    }else{
        map_frame_name = *(image_names.begin());
        local_frame_name = *(image_names.rbegin());
    }

    ref_frame_ = *(reconstruction_map_.FindImageWithName(map_frame_name));
    // if (&ref_frame_ == nullptr)
    // {
    //     map_frame_name = *(++image_names.end());
    //     ref_frame_ = *(reconstruction_map_.FindImageWithName(*(++image_names.end())));
    // }else{
    //     map_frame_name = *image_names.begin();
    // }

    if (images[1].Name() == map_frame_name)
    {
      local_frame_ = images[2];
      ref_match_frame_ = images[1];
    }else{
      local_frame_ = images[1];
      ref_match_frame_ = images[2];

    }
    std::cout << "map image path is " << match_file + "/" + map_frame_name << std::endl;
    std::cout << "local image path is " << match_file + "/" + local_frame_name << std::endl;
    image_ref_ = cv::imread(match_file + "/" + map_frame_name, CV_LOAD_IMAGE_COLOR);   
    image_loc_ = cv::imread(match_file + "/" + local_frame_name, CV_LOAD_IMAGE_COLOR);   
}

//返回的是世界到相机坐标系pose
Eigen::Matrix4d ColmapFileReader::GetMapFramePose(const Image& image)
{
    Eigen::Vector4d vec_q = image.Qvec();
    Eigen::Quaterniond Q = Eigen::Quaterniond(vec_q[0], vec_q[1], vec_q[2], vec_q[3]);
    Eigen::Vector3d vec_t = image.Tvec();

    Eigen::Matrix4d rt_pose;
    rt_pose.setIdentity();
    rt_pose.block<3,3>(0,0) = Q.matrix();
    rt_pose.block<3,1>(0,3) = vec_t;
    return rt_pose;
}


void ColmapFileReader::FilterByCloud()
{
    pcl::PointCloud<pcl::PointXYZ> cloud_lidar, save_cloud;
    pcl::io::loadPCDFile("/home/zmc/Desktop/map.pcd",cloud_lidar);
    pcl::KdTreeFLANN<pcl::PointXYZ> lidar_cloud_kdtree;
    lidar_cloud_kdtree.setInputCloud(cloud_lidar.makeShared());

    auto pt3ds = reconstruction_map_.Points3D();
    double radius = 0.5;

    int delet_num = 0;
    for(const auto pt3d : pt3ds)
    {
        std::vector<int> kIndices;
        std::vector<float> kDistances;
        pcl::PointXYZ pt;
        pt.x = pt3d.second.X();
        pt.y = pt3d.second.Y();
        pt.z = pt3d.second.Z();


        if ((lidar_cloud_kdtree.radiusSearch(pt, radius, kIndices, kDistances)) > 0)
        {
            save_cloud.push_back(pt);
            continue;
        }else{
            auto id_3d = pt3d.first;
            reconstruction_map_.DeletePoint3D(id_3d);
            delet_num++;
        }

    }
    // pcl::io::savePCDFile("filter.pcd", save_cloud);
    // pcl::io::savePCDFile("cloud_little_error.pcd", *cloud_index);
}

void ColmapFileReader::FilterFrame3D(Image& map_frame, bool draw_feature)
{
    LOG(INFO) << "filter image " << map_frame.Name() << " 3d points";
    //3D点多的关键帧筛选严格
    colmap::point2D_t point_3d_num = 500;
    double map_projection_pix_uv_sum_error;
    if (map_frame.NumPoints3D() > point_3d_num)
    {
        map_projection_pix_uv_sum_error = map_projection_pix_uv_sum_error_;
    }else{
        map_projection_pix_uv_sum_error = 2 * map_projection_pix_uv_sum_error_;
    }
    std::vector<Eigen::Vector3d> eig_pt3d;
    std::vector<Eigen::Vector2d> eig_pt2d;

    Eigen::Matrix4d T_w2c = GetMapFramePose(map_frame);
    //获取一帧中所能观测的到3d点
    //计算投影点computerReprojectionErrorcomputerReprojectionErrorcomputerReprojectionError和特征点的误差
    //大于一定阈值，取消该3d点
    // auto& col_pt2ds = map_frame.Points2D();
    std::vector<cv::KeyPoint> org_kps, rej_kps; 
    // map_frame.
    int i = 0;
    for (auto& pt2 : (*(reconstruction_map_.FindImageWithName(map_frame.Name()))).Points2D())
    {
        point3D_t id_3d = pt2.Point3DId();
        if (reconstruction_map_.ExistsPoint3D(id_3d))
        {
            Eigen::Vector3d eig_pt3d_tmp = reconstruction_map_.Point3D(id_3d).XYZ();
            eig_pt3d.push_back(eig_pt3d_tmp);
            eig_pt2d.push_back(pt2.XY());
            
            //画图： 冲投影和原始特征点
            cv::KeyPoint pt2_comput;//useless
            Eigen::Matrix3d map_K;
            cv::cv2eigen(map_intri_matrix_, map_K);
            // Eigen::Vector2d error = computerReprojectionError(T_w2c, map_K, eig_pt3d_tmp, pt2.XY(), pt2_comput);
            Eigen::Vector2d error = computerReprojectionError(T_w2c, map_K_, eig_pt3d_tmp, pt2.XY(), pt2_comput);
            rej_kps.push_back(pt2_comput);
            cv::KeyPoint org_pt;
            org_pt.pt.x = pt2.X();
            org_pt.pt.y = pt2.Y();
            org_kps.push_back(org_pt);


            if(abs(error[0]) > 15)
            {
                std::cout << "Reprojection pix error is:" << error[0] << ", plesae check pose and camera param !!!" << std::endl;
                std::cout << "3d is= " << eig_pt3d_tmp[0] << eig_pt3d_tmp[1] << eig_pt3d_tmp[2] << std::endl; 
                std::cout << "map K is " << std::endl << map_intri_matrix_ << std::endl;
                return;
            }
            if (abs(error[0]) > map_projection_pix_uv_sum_error || abs(error[1]) > map_projection_pix_uv_sum_error)
            {
                reconstruction_map_.DeletePoint3D(id_3d);
            }else{
                // std::cout << "3d point " << i << " error is :" << error[0]<< " " << error[1] << std::endl;
            }
            i++;
        }
    }

    if (draw_feature)
    {
        // auto image_ref = image_ref_.clone();
        auto image_ref = cv::imread("/media/zmc/Teclast_S20/localization_ws/project/colmap-3.5/src/exe/data/images/" + map_frame.Name());
        std::cout << "image path is " << "/media/zmc/Teclast_S20/localization_ws/project/colmap-3.5/src/exe/data/images/" + map_frame.Name() << std::endl;
        cv::drawKeypoints(image_ref, rej_kps, image_ref, cv::Scalar(255,0,0));
        cv::drawKeypoints(image_ref, org_kps, image_ref, cv::Scalar(0,0,255));
        cv::imwrite(map_frame.Name(), image_ref);
    }
}

void ColmapFileReader::FilterMap3D(bool draw_feature)
{
    auto images_map = reconstruction_map_.Images();
    
    for (auto& map_frame : images_map)
    {
        // FilterFrame3D(map_frame.second, draw_feature);
        image_name_id_map_[map_frame.second.Name()] = map_frame.second.ImageId();
    }
}

void ColmapFileReader::Get3D2DMatches(const Image& image)
{
    // auto image_data = reconstruction_map_.FindImageWithName(ref_frame_.Name());
    auto image_data = &image;
    if (!image_data){
      std::cout << "image " << ref_frame_.Name() << " can't find in map !!!" << std::endl;
      return ;
    }

    //找到所有的match 2d点
    std::vector<cv::KeyPoint> pts1, pts2;

    uint32_t size = ref_frame_.NumPoints2D();
    uint32_t i = 0;//这里为什么从1开始
    int match_num = 0;


    //TODO 逻辑优化，遍历所有的2d点找match太费时间
    for (; i < size; i++)
    {
      if(database_cache_.CorrespondenceGraph().HasCorrespondences(ref_match_frame_.ImageId(),i))
      {
        auto a = database_cache_.CorrespondenceGraph().FindCorrespondences(ref_match_frame_.ImageId(), i);
        // std::cout << "corr is: image " << i << "  " << i << " match  image " << a[0] << "  " << a[0].point2D_idx << std::endl;
        match_num++;

        cv::KeyPoint pt1, pt2;
        pt1.pt.x = ref_frame_.Point2D(i).X();
        pt1.pt.y = ref_frame_.Point2D(i).Y();
        pts1.push_back(pt1);

        pt2.pt.x = local_frame_.Point2D(a[0].point2D_idx).X();
        pt2.pt.y = local_frame_.Point2D(a[0].point2D_idx).Y();
        pts2.push_back(pt2);

        auto pt_2d = image_data->Point2D(i);
        //查看所有match 2d点是否存在3d点
        // if (pt_2d.X() == -1 && pt_2d.Y() == -1)
        // {
        //     continue;
        // }
        // HasPoint3D()
        // if(reconstruction_map_.ExistsPoint3D(pt_2d.Point3DId()) && (match_num == 2 || match_num == 9 || match_num == 10 || match_num == 11 || match_num == 4 ||match_num == 17|| match_num == 20 || match_num == 22 || match_num == 23))
        if(reconstruction_map_.ExistsPoint3D(pt_2d.Point3DId()))
        {
            Eigen::Vector3d pt_3d = reconstruction_map_.Point3D(pt_2d.Point3DId()).XYZ();

            colmap_pt3d_.push_back(reconstruction_map_.Point3D(pt_2d.Point3DId()));
            colmap_pt2d_.push_back(local_frame_.Point2D(a[0].point2D_idx));

            cv::Point3f cv_pt_3f = cv::Point3f(pt_3d[0], pt_3d[1], pt_3d[2]);

            pts3d_.push_back(cv_pt_3f);
            pts2d_.push_back(pt2.pt);
        }
      }
    }

    auto image_loc = image_loc_.clone();
    auto image_ref = image_ref_.clone();
    for (size_t i = 0; i < pts2.size(); i++)
    {
      cv::Point2f point1 = pts1[i].pt;
      cv::Point2f point2 = pts2[i].pt;
      cv::putText(image_ref, std::to_string(i), point1, cv::FONT_HERSHEY_SIMPLEX,0.45, CV_RGB(255,0,0),1.8);
      cv::putText(image_loc, std::to_string(i), point2, cv::FONT_HERSHEY_SIMPLEX,0.45, CV_RGB(255,0,0),1.8);
    }
    cv::imwrite("image_ref_.jpg", image_ref);
    cv::imwrite("image_loc_.jpg", image_loc);

    std::cout << "feature match num is " << match_num << std::endl;
}

void ColmapFileReader::LocalizationColmap(const std::string colmap_path)
{
    cv::Mat cv_local_K;
    cv::eigen2cv(local_K_, cv_local_K);
    std::shared_ptr<Localization> localization_ptr_ = std::make_shared<Localization>(colmap_path);

    localization_ptr_->InitComputePose(pts3d_, pts2d_, cv_local_K);

    for (size_t i = 0; i < pts3d_.size(); i++)
    {

    }
    localization_ptr_->OptimizePose(colmap_pt3d_, colmap_pt2d_);
}

void ColmapFileReader::LocalizationColmapSift(const std::string colmap_path)
{
    cv::Mat cv_local_K;
    cv::eigen2cv(local_K_, cv_local_K);
    std::shared_ptr<Localization> localization_ptr_ = std::make_shared<Localization>(colmap_path);

    localization_ptr_->InitComputePose(pts3d_, pts2d_, cv_local_K);
    localization_ptr_->OptimizePose(colmap_pt3d_, colmap_pt2d_);
}

void ColmapFileReader::GetRefFrame(Image& map_frame)
{
    map_frame = ref_frame_;
}

void ColmapFileReader::GetSuperGlue3D2DMatches(std::string& map_image_name)
{
    // reconstruction_map_
    pts2d_.clear();
    pts3d_.clear();
    std::vector<std::pair<uint32_t, uint32_t>> matches_id;
    uint32_t image1_id;
    uint32_t image2_id;
    // std::cout << (match_file_ + "/" + map_image_name) << std::endl;
    auto local_2ds = readMatches((match_file_ + "/" + map_image_name).c_str(), matches_id, image1_id, image2_id);
    map_image_name = map_image_name.replace(map_image_name.find(".txt"), 4, ".jpg");
    auto image = *(reconstruction_map_.FindImageWithName(map_image_name));

    // auto image = reconstruction_map_.Image(image1_id);
    std::vector<cv::Point3f> ref_3ds;
    int index = 0;
    for (const auto& match_id : matches_id)
    {
        // if(database_cache_.CorrespondenceGraph().HasCorrespondences(image1_id, match_id.first))
        // {
        auto pt_2d = image.Point2D(match_id.first);
        if(reconstruction_map_.ExistsPoint3D(pt_2d.Point3DId()))
        {
            Eigen::Vector3d pt_3d = reconstruction_map_.Point3D(pt_2d.Point3DId()).XYZ();

            colmap_pt3d_.push_back(reconstruction_map_.Point3D(pt_2d.Point3DId()));

            colmap::Point2D pt2;
            pt2.SetXY(Eigen::Vector2d(local_2ds[index].x, local_2ds[index].y));
            colmap_pt2d_.push_back(pt2);

            cv::Point3f cv_pt_3f = cv::Point3f(pt_3d[0], pt_3d[1], pt_3d[2]);
            pts2d_.push_back(local_2ds[index]);
            pts3d_.push_back(cv_pt_3f);
            std::cout << pt_3d[0] << " " << pt_3d[1] << " " << pt_3d[2] << " " << local_2ds[index].x << " " << local_2ds[index].y << std::endl;
            // pts2d_.push_back(pt2.pt);
        }
        index++;
    }

}

void ColmapFileReader::GetSuperGlue3D2DMatches(const std::string& image_name, const points& pt_uv_id,
                                               std::vector<cv::Point2f>& pts2d, std::vector<cv::Point3f>& pts3d)
{ 
    pts2d.clear();
    pts3d.clear();
    std::cout << "1221 match image name is :" << image_name + ".jpg" << std::endl;
    auto image = (reconstruction_map_.Image(image_name_id_map_[image_name + ".jpg"]));
    // auto image = *(reconstruction_map_.FindImageWithName(image_name + ".jpg"));
    int num_3d = 0;
    for (const auto& match_id : pt_uv_id)
    {
        
        auto pt_2d = image.Point2D(match_id.z);
        // std::cout <<"map keypoint is :" << pt_2d.XY() << " id is: " << match_id.z << std::endl;
        // std::cout <<"local keypoint is :" << match_id.x << "  " << match_id.y << std::endl;

        if(reconstruction_map_.ExistsPoint3D(pt_2d.Point3DId()))
        {
            // std::cout << "pt_2d.Point3DId() is: " << pt_2d.Point3DId() << std::endl;
            Eigen::Vector3d pt_3d = reconstruction_map_.Point3D(pt_2d.Point3DId()).XYZ();
            // std::cout << "end pt_2d.Point3DId() is: " << pt_2d.Point3DId() << std::endl;

            cv::Point3f cv_pt_3f = cv::Point3f(pt_3d[0], pt_3d[1], pt_3d[2]);
            //TODO: 检查这里xy是否为原尺寸
            // pts2d.push_back(cv::Point2f(match_id.x, match_id.y));
            //临时
            // pts2d.push_back(cv::Point2f((match_id.x),( match_id.y + 420)));
            pts2d.push_back(cv::Point2f((match_id.x),(match_id.y)));
            pts3d.push_back(cv_pt_3f);
            // std::cout << "cv_pt_3f is :" << cv_pt_3f << std::endl;
            num_3d++;
        }
        // std::cout << "=================================" << std::endl;
    }
    // LOG(INFO) << "image " << image_name << " find match 2d point num is " << pt_uv_id.size();
    // LOG(INFO) << "image " << image_name << " find match 3d point num is " << num_3d;
}

Image ColmapFileReader::GetImageWithName(const std::string image_name)
{
    if (image_name_id_map_.count(image_name) > 0)
    {
        auto image = (reconstruction_map_.Image(image_name_id_map_[image_name]));
        return image;
    }else{
        LOG(ERROR) << "Cant find image " << image_name <<  ", please check colmap path !!!";
    }
}

void ColmapFileReader::RunSuperPointLocalization(const std::string& match_file, const std::string& colmap_file)
{
    match_file_ = match_file;

    // std::string id;
    // id = id.replace(id.find(".txt"), 4, ".jpg");
    
    boost::filesystem::path files_path = match_file;
    if (!boost::filesystem::exists(files_path))
    {
        std::cout << "file " << files_path << " not exist." << std::endl;
        return;
    }
    boost::filesystem::recursive_directory_iterator beg_iter(files_path);
    boost::filesystem::recursive_directory_iterator end_iter;

    for (; beg_iter != end_iter; beg_iter++)
    {
        std::string file_ext = beg_iter->path().extension().string();
        if(file_ext == ".txt")
        {
            std::string file_name = beg_iter->path().filename().string();
            // file_name.replace(file_name.find(".txt"), 4, ".jpg");
            std::cout << "======================" << file_name << "=======================" << std::endl;
            GetSuperGlue3D2DMatches(file_name);
            // LocalizationColmap(colmap_file);
        }
    }
    
    // std::string image_name = match_file.replace(match_file.find(".txt"), 4, ".jpg");
    // GetSuperGlue3D2DMatches(image_name);
}

void ColmapFileReader::SaveMapWithImage()
{
    //读取小吏存储的特征点描述子信息，
    std::string images_descri_path = "/media/zmc/Teclast_S20/localization_ws/project/colmap-3.5/python/superglue/out_put/image_messages";
    std::vector<std::pair<std::string, std::string> > descri_image_names;
    std::vector<std::pair<std::string, std::string> > score_image_names;
    if (!readDescripFiles(images_descri_path, "/descriptors.txt", descri_image_names))
    {
        return;
    }
    if (!readDescripFiles(images_descri_path, "/scores.txt", score_image_names))
    {
        return;
    }
    
    std::unordered_map<std::string, std::vector<std::vector<double> > > descri_map;
    std::unordered_map<std::string, std::vector<std::vector<double> > > score_map;

    //读取score
    for (const auto& path : score_image_names)
    {z
        std::vector<std::vector<double> > score;
        if (!readFeatureDescriptors(path.first.c_str(), score, false))
        {
            return;
        }
        score_map[path.second] = score;
        std::cout << path.second << " feature score num is " << score.size() << std::endl;
    }
    //读取描述子
    for (const auto& path : descri_image_names)
    {
        std::vector<std::vector<double> > descriptors;
        if (!readFeatureDescriptors(path.first.c_str(), descriptors, true))
        {
            return;
        }
        descri_map[path.second] = descriptors;
        std::cout << path.second << " feature descriptor num is " << descriptors.size() << std::endl;
    }
    
    auto images = reconstruction_map_.Images();
    std::unordered_map<std::string, YdFrame> image_map;
    for (const auto& image : images)
    {
        YdFrame frame;
        std::string image_name = image.second.Name();

        frame.transpose = image.second.ProjectionCenter();
        frame.rotation = image.second.RotationMatrix();

        auto pts_2d = image.second.Points2D();
        int i = 1;
        std::vector<YdPoint2d> points;
        for (const auto& pt_2d : pts_2d)
        {
            YdPoint2d pt;
            pt.pt.x = pt_2d.X();
            pt.pt.y = pt_2d.Y();
            if (reconstruction_map_.ExistsPoint3D(pt_2d.Point3DId()))
            {
                Eigen::Vector3d pt_3d = reconstruction_map_.Point3D(pt_2d.Point3DId()).XYZ();
                cv::Point3f cv_pt_3f = cv::Point3f(pt_3d[0], pt_3d[1], pt_3d[2]);
                pt.has_point_3d = true;
                pt.pt3d = cv_pt_3f;
                // std::cout << "id is: " << i << " has 3d" << std::endl;
            }else{
                pt.has_point_3d = false;
            }
            i++;
            points.push_back(pt);
        }
        std::cout << "image name is : " << image_name << ", 3d num is " << i << std::endl; 

        frame.points = points;
        // image_map[image_name] = frame;  
        image_map.insert(std::make_pair(image_name, frame));  
    }
    
    std::string image_map_name = "images_map.bin";
    std::ofstream file(image_map_name, std::ios::trunc | std::ios::binary);
    // CHECK(file.is_open()) << image_map_name;

    WriteBinaryLittleEndian<uint64_t>(&file, image_map.size());
    for (auto image : image_map)
    {
        const std::string name = image.first + '\0';
        file.write(name.c_str(), name.size());
        WriteBinaryLittleEndian<float>(&file, image.second.rotation.w());
        WriteBinaryLittleEndian<float>(&file, image.second.rotation.x());
        WriteBinaryLittleEndian<float>(&file, image.second.rotation.y());
        WriteBinaryLittleEndian<float>(&file, image.second.rotation.z());

        WriteBinaryLittleEndian<float>(&file, image.second.transpose.x());
        WriteBinaryLittleEndian<float>(&file, image.second.transpose.y());
        WriteBinaryLittleEndian<float>(&file, image.second.transpose.z());        
        // file.close();

        WriteBinaryLittleEndian<uint64_t>(&file, image.second.points.size());
        std::cout << "image "  << name << " pt is " << image.second.points.size() << std::endl;
        for (auto pt : image.second.points)
        {
            WriteBinaryLittleEndian<bool>(&file, pt.has_point_3d);
            WriteBinaryLittleEndian<float>(&file, pt.pt.x);
            WriteBinaryLittleEndian<float>(&file, pt.pt.y);
            WriteBinaryLittleEndian<float>(&file, pt.pt3d.x);
            WriteBinaryLittleEndian<float>(&file, pt.pt3d.y);
            WriteBinaryLittleEndian<float>(&file, pt.pt3d.z);
        }
    }
    std::cout <<"feature map save to " << image_map_name << std::endl;
}

void ColmapFileReader::SavePointCloudWithIndex()
{
    auto pt3ds = reconstruction_map_.Points3D();
    std::cout << " 3d point num is " << pt3ds.size() << std::endl;
    pcl::PointCloud<pcl::PointXYZI>::Ptr cloud_index(new pcl::PointCloud<pcl::PointXYZI>());
    for(const auto pt3d : pt3ds)
    {
        pcl::PointXYZI pti;
        pti.x = pt3d.second.X();
        pti.y = pt3d.second.Y();
        pti.z = pt3d.second.Z();
        pti.intensity = pt3d.first;
        cloud_index->push_back(pti);
    }
    pcl::io::savePCDFile("cloud_with_index.pcd", *cloud_index);
    std::cout << "cloud save to cloud_with_index.pcd. size is " << cloud_index->points.size() << std::endl;
}

void ColmapFileReader::CutPointIndexCloud(const Eigen::Matrix4f& pose)
{
    //临时
    // pcl::PointCloud<pcl::PointXYZI> cloud_tmp;
    // pcl::io::loadPCDFile("/home/zmc/Desktop/cloud_with_index_small.pcd", cloud_tmp);
    // pcl::PointCloud<pcl::PointXYZI>::Ptr c2loud_index_(new pcl::PointCloud<pcl::PointXYZI>(cloud_tmp));
    // cloud_index_ = c2loud_index_;

    pcl::FrustumCulling<pcl::PointXYZI> fc;
    float VerticalFov = 40;
    float HorizontalFov = 60;
    float NearPlanDistance = 0.0;
    float FarPlaneDistance = 4.0;
    fc.setInputCloud(cloud_index_);
    fc.setVerticalFOV(VerticalFov);
    fc.setHorizontalFOV(HorizontalFov);
    fc.setNearPlaneDistance(NearPlanDistance);
    fc.setFarPlaneDistance(FarPlaneDistance);
    Eigen::Matrix4f cam2robot;
    Eigen::Matrix4f rotation_Z;
    // pose << -0.0265384,-0.97662,-0.213337,-13.8441,
    // 0.0838577,-0.214837,0.973046,-3.36247,
    // -0.996126,0.0079311,0.0875957,-0.616713,
    // -0,-0,0,1;
    cam2robot << 0, 0, 1, 0,
                 0,-1, 0, 0,
                 1, 0, 0, 0,
                 0, 0, 0, 1;
    // rotation_Z << -1, 0, 0, 0,
    //               0,-1, 0, 0,
    //               0, 0, 1, 0,
    //               0, 0, 0, 1;
    
    fc.setCameraPose( pose * cam2robot);
    fc.filter(cut_pc_);
    pcl::io::savePCDFile("cut_cloud_with_index.pcd", cut_pc_);
}

bool comp( const std::pair<colmap::image_t,int>& a,  const std::pair<colmap::image_t,int> &b){
    return a.second > b.second;
}

std::vector<std::string> ColmapFileReader::FindLocalFrame()
{
    std::unordered_map<image_t, int> image_ids;
    for (const auto& pt : cut_pc_)
    {
        auto ima_id  = reconstruction_map_.Point3D(pt.intensity).Track().Elements();
        for (auto& ele : ima_id)
        {
            if (!image_ids.count(ele.image_id))
            {
                image_ids[ele.image_id] = 1;
            }else{
                image_ids[ele.image_id] += 1;
            }
        }
    }
    // int max_num = 0;
    std::vector<std::pair<colmap::image_t,int> > vec_image_ids;
    for (auto image_it_fetures : image_ids)
    {
        vec_image_ids.push_back(image_it_fetures);
    }

    std::sort(vec_image_ids.begin(), vec_image_ids.end(), comp);
    // image_t max_image_it;
    // for (auto image_it_fetures : image_ids)
    // {
    //     if(image_it_fetures.second > max_num)
    //     {
    //         max_num = image_it_fetures.second;
    //         max_image_it = image_it_fetures.first;
    //     }
    // }
    std::vector<std::string> max_image_names;
    std::string max_image_name;
    max_image_name = reconstruction_map_.Image((*(vec_image_ids.begin()+4)).first).Name();
    max_image_names.push_back(max_image_name);
    max_image_name = reconstruction_map_.Image((*(vec_image_ids.begin()+3)).first).Name();
    max_image_names.push_back(max_image_name);
    max_image_name = reconstruction_map_.Image((*(vec_image_ids.begin()+2)).first).Name();
    max_image_names.push_back(max_image_name);
    max_image_name = reconstruction_map_.Image((*(vec_image_ids.begin()+1)).first).Name();
    max_image_names.push_back(max_image_name);
    max_image_name = reconstruction_map_.Image((*vec_image_ids.begin()).first).Name();
    max_image_names.push_back(max_image_name);
    // std::cout << "max_image_name is " << max_image_name << std::endl;
    return max_image_names;
}
void ColmapFileReader::GetFrame3DPoint3(std::string frame_name, std::vector<Eigen::Vector3d>& src, std::vector<Eigen::Vector3d>& dst)
{
    const colmap::Image* image1 = reconstruction_map_.FindImageWithName(frame_name);
    auto image2 = reconstruction_map2_.FindImageWithName(frame_name);
    // reconstruction_map_.
    // FilterFrame3D(*image1);
    auto pts1 = image1->Points2D();
    auto pts2 = image2->Points2D();
    assert(pts1.size() == pts2.size());

    int i = 0;
    for (auto pt : pts1)
    {
        point3D_t id1_3d = pt.Point3DId();
        point3D_t id2_3d = pts2[i].Point3DId();
        if (reconstruction_map_.ExistsPoint3D(id1_3d) && reconstruction_map2_.ExistsPoint3D(id2_3d))
        {
            src.push_back(reconstruction_map_.Point3D(id1_3d).XYZ());
            dst.push_back(reconstruction_map2_.Point3D(id2_3d).XYZ());
        }
        i++;
    }
}

std::vector<cv::Point2f> ColmapFileReader::GetFrame3DPoint3FromSuperGlue(const std::string& map1_fm, const std::string& map2_fm, 
                                                     const std::vector<std::pair<uint32_t, uint32_t>>& matches, 
                                                     std::vector<Eigen::Vector3d>& src, std::vector<Eigen::Vector3d>& dst,
                                                     const std::vector<cv::Point2f>& l_pts,
                                                     Eigen::Matrix4d& T_w2c)
{
    FilterMap3D();
    std::vector<cv::Point2f> pts2d;
    auto image1 = reconstruction_map_.FindImageWithName(map1_fm);
    if (!image1)
    {
        std::cout << "map1 can't find frame " << map1_fm << std::endl;
    }
    auto image2 = reconstruction_map2_.FindImageWithName(map2_fm);
    if (!image2)
    {
        std::cout << "map2 can't find frame " << map2_fm << std::endl;
    }
    T_w2c = GetMapFramePose(*image2);
    // auto frame = reconstruction_map_.FindImageWithName(map1_fm);
    int i = 0;
    for (const auto& match : matches)
    {
        point3D_t id1_3d = image1->Point2D(match.first).Point3DId();
        point3D_t id2_3d = image2->Point2D(match.second).Point3DId();
        // if (reconstruction_map_.ExistsPoint3D(id1_3d) && reconstruction_map2_.ExistsPoint3D(id2_3d))
        if (reconstruction_map2_.ExistsPoint3D(id2_3d) &&reconstruction_map_.ExistsPoint3D(id1_3d))
        {
            src.push_back(reconstruction_map_.Point3D(id1_3d).XYZ());
            dst.push_back(reconstruction_map2_.Point3D(id2_3d).XYZ());
            pts2d.push_back(l_pts[i]);
        }
        i++;
    }
    return pts2d;
}

void ColmapFileReader::GetSim3Matched(std::vector<Eigen::Vector3d>& src, 
                                        std::vector<Eigen::Vector3d>& dst, 
                                        std::unordered_map<std::string, Eigen::Vector3d>& pose_map)
{
    auto images = reconstruction_map_.Images();
    for (const auto& image : images)
    {
        if(pose_map.count(image.second.Name()))
        {
            auto pose = GetMapFramePose(image.second);
            src.push_back(pose.block<3,1>(0,3));
            std::string name = image.second.Name();
/*             auto pose2 = GetMapFramePose(*reconstruction_map2_.FindImageWithName(name));
            std::cout << "image " << name << std::endl << pose << std::endl;
            std::cout << "image2 " << name << std::endl << pose2 << std::endl; */
            dst.push_back(pose_map[name]);

        }
    }
}

void ColmapFileReader::RecoverImageScale(const SimilarityTransform3& tform)
{
    Eigen::Quaterniond Q_4;
    Eigen::Isometry3d T;
    std::string pose_file = "colmap_scale_image.txt";
    for (auto& image : reconstruction_map_.Images()) {
        T.setIdentity();
        Eigen::Vector4d Q = image.second.Qvec();
        Eigen::Vector3d t = image.second.Tvec();
        std::cout << "position befor sim3 is:" << t << std::endl;

        tform.TransformPose(&Q, &t);
        std::cout << "position after sim3 is:" << t << std::endl;
        Q_4 = Eigen::Quaterniond(Q[0], Q[1], Q[2], Q[3]);

        T.linear() = Q_4.matrix();
        T.translation() = t;
        T = T.inverse();
        save_pose_image_name(pose_file.c_str(), image.second.Name(), T.matrix());



        // std::cout << "======================="<< image.second.ProjectionCenter() << std::endl;

  }
}