#include <ros/ros.h>

#include "pre_local/colmap_io.hpp"

/*
工程功能：
1.读取colmap的db文件获取匹配信息，计算图像定位
2.读取superpoint的匹配信息，计算定位

*/

int main (int argc, char** argv)
{
    std::shared_ptr<ColmapFileReader> colmap_file_reader_ptr_ = std::make_shared<ColmapFileReader>(atof(argv[3]));
    colmap_file_reader_ptr_->ReadMap(argv[1]);
    
    ros::init(argc, argv, "online_localization");
    ros::NodeHandle nh;

    //colmape localization
    colmap_file_reader_ptr_->ReadMatches(argv[2]);
    colmap::Image ref_fram;
    colmap_file_reader_ptr_->GetRefFrame(ref_fram);
    colmap_file_reader_ptr_->FilterFrame3D(ref_fram);
    colmap_file_reader_ptr_->Get3D2DMatches(ref_fram);
    colmap_file_reader_ptr_->LocalizationColmapSift(argv[1]);

    //super point localization

    // colmap_file_reader_ptr_->RunSuperPointLocalization(argv[2]);


    return 1;
}