#include "subscriber/match_subscriber.hpp"
#include "localization/localization_flow.hpp"
#include <iostream>
#include <ros/ros.h>
#include <glog/logging.h>


int main(int argc, char** argv)
{
    
    google::InitGoogleLogging(argv[0]);
    FLAGS_log_dir = "/home/zmc/Data/data/LOG";
    FLAGS_logtostderr = 1;
    FLAGS_colorlogtostderr = true;
    FLAGS_alsologtostderr = 1;
    ros::init(argc, argv, "a_online_superpoint_localization");
    ros::NodeHandle nh;
    std::string colmap_data_path, sub_match_topic_name, sub_img_topic_name, pub_pose_topic_name, pub_img_topic_name, pub_loop_topic_name, vis_pose_path;
    nh.param<std::string>("match_topic", sub_match_topic_name, "/vision_localization/match_result");
    nh.param<std::string>("pose_topic", pub_pose_topic_name, "/yd_camera_pose");
    nh.param<std::string>("loop_index_topic", pub_loop_topic_name, "/loop_index");
    nh.param<std::string>("colmap_data_path", colmap_data_path, "/media/zmc/Teclast_S20/localization_ws/project/to_zhoumingchao/SuperGluePretrainedNetwork-master/images/sparse");
    nh.param<std::string>("pose_path", vis_pose_path, "/vis_pose_path");
    // nh.param<std::string>("sub_img_topic", sub_img_name, "/image");
    // nh.param<std::string>("pub_img_topic", pub_img_name, "/localization_image");
    std::unique_ptr<LocalizationFlow> localization_flow_ = std::unique_ptr<LocalizationFlow>(new LocalizationFlow(colmap_data_path, nh, sub_match_topic_name,
                                                            pub_pose_topic_name, pub_loop_topic_name, pub_img_topic_name, sub_img_topic_name, vis_pose_path, 10000));
    ros::Rate rata(100);
    while(ros::ok())
    {   
        ros::spinOnce();
        localization_flow_->Run();
        rata.sleep();
    }
    google::ShutdownGoogleLogging();
    
    return 1;
}