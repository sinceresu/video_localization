#ifndef MATCH_SUBSCRIBER_HPP_
#define MATCH_SUBSCRIBER_HPP_


#include <ros/ros.h>
#include <deque>
#include <mutex>
#include <glog/logging.h>

#include "devel/include/COLMAP/PointMatch.h"


class MatchSubscriber
{
public:
    MatchSubscriber(const ros::NodeHandle& nh, const std::string topic_name, int buff_size);
    void msg_callback(const COLMAP::PointMatch& match_msg_ptr);
    void ParseData(std::deque<COLMAP::PointMatch>& deque_data);
private:
    ros::NodeHandle nh_;
    ros::Subscriber subscriber_;
    std::deque<COLMAP::PointMatch> match_data_;
    std::mutex buff_mutex_;
};
#endif