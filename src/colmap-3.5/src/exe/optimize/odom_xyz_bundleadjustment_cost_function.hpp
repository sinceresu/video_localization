#ifndef ODOM_XYZ_BUNDLEADJUSTMENT_COST_FUNCTION_HPP_
#define ODOM_XYZ_BUNDLEADJUSTMENT_COST_FUNCTION_HPP_
#include "sophus/se3.h"

enum OptimizeOdomXYZType
{
    OptimizeX = 0,
    OptimizeY,
    OptimizeZ,
};


class OdomXYZBundleAdjustmentCostFunction
{
 public:
  OdomXYZBundleAdjustmentCostFunction(const Sophus::Vector6d& pose_front)
  :pose_front_(pose_front)
  {}

  template <typename T>
  bool operator()(const T* qvec_data, const T* tvec_data, T* residuals) const {
    // Rotate and translate.
    // using Vec3T = Eigen::Matrix<T,3,1>;
    using Vec4T = Eigen::Matrix<T,4,1>;
    // Eigen::Map<const Vec3T> cam_t(&tvec_data[0]);
    Eigen::Map<const Vec4T> cam_R(&qvec_data[0]);
    const Vec4T cam_R_transpose(cam_R[0],cam_R[1],cam_R[2],cam_R[3]);


    Eigen::Matrix<T, 3, 3, Eigen::RowMajor> R;
    ceres::QuaternionToRotation(qvec_data, R.data());

    // Eigen::Isometry3d TT;
    // TT.translation() = R;
    Eigen::Matrix<T, 4, 4> T_curr;
    T_curr << R(0,0), R(0,1), R(0,2), tvec_data[0],
              R(1,0), R(1,1), R(1,2), tvec_data[1],
              R(2,0), R(2,1), R(2,2), tvec_data[2],
              T(0), T(0), T(0), T(1);
    Sophus::SE3 TSE3_front = Sophus::SE3::exp(pose_front_);

    auto T_relate = T_curr.inverse() * TSE3_front.matrix();
    
    residuals[0] = 0.1 * T_relate(1,3);
    residuals[1] = 0.1 * T_relate(2,3);
    std::cout << "OdomXYZBundleAdjustmentCostFunction residual1 is :" << residuals[0] << std::endl;
    std::cout << "OdomXYZBundleAdjustmentCostFunction residual2 is :" << residuals[1] << std::endl;
    return true;
  }


private:
    Sophus::Vector6d pose_front_;

};


























#endif