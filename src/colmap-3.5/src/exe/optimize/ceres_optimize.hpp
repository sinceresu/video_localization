#ifndef CERES_OPTIMIZE_HPP_
#define CERES_OPTIMIZE_HPP_


#include "optim/bundle_adjustment.h"
#include "base/camera_models.h"
#include "base/cost_functions.h"
#include "sophus/se3.h"
#include "odom_xyz_bundleadjustment_cost_function.hpp"
using namespace colmap;

class BundleAdjustPose3d2d
{
  public:
    // BundleAdjustPose3d2d() = default;
    BundleAdjustPose3d2d();
    // void AddImageToProblem(Eigen::Matrix4d& T,const std::vector<Point3D>& pt3ds, const std::vector<Point2D>& pt2ds, const std::vector<double>& camera_params, const ceres::LossFunction* loss_function);
    Eigen::Matrix4d AddImageToProblem3d2d(Eigen::Matrix4d& T,
                                          std::vector<Point3D>& pt3ds,
                                          const std::vector<Point2D>& pt2ds, 
                                          std::vector<double>& camera_params, 
                                          ceres::LossFunction* loss_function,
                                          Eigen::Matrix4d& T_front,
                                          bool optmize_xyz = false);
    void AddOdomXYZProblem(Eigen::Matrix4d& T_curr, Eigen::Matrix4d& T_front);
    void Solve();
  private:
    std::unique_ptr<ceres::Problem> problem_;
    ceres::Solver::Summary summary_;
};

#endif 