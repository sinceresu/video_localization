#include "ceres_optimize.hpp"

//camera_params fx fy cx cy
BundleAdjustPose3d2d::BundleAdjustPose3d2d()
{
    problem_.reset(new ceres::Problem());

}

Eigen::Matrix4d BundleAdjustPose3d2d::AddImageToProblem3d2d(Eigen::Matrix4d& T,std::vector<Point3D>& pt3ds,
                                                            const std::vector<Point2D>& pt2ds, 
                                                            std::vector<double>& camera_params,
                                                            ceres::LossFunction* loss_function,
                                                            Eigen::Matrix4d& T_front,
                                                            bool optmize_xyz)
{
    // problem_.reset(new ceres::Problem());
    
    Eigen::Vector3d t = T.block<3,1>(0,3);
    Eigen::Quaterniond tmp_q = Eigen::Quaterniond(T.block<3,3>(0,0));
    Eigen::Vector4d q = Eigen::Vector4d(tmp_q.w(), tmp_q.x(), tmp_q.y(), tmp_q.z());

    double* qvec_data = q.data();
    double* tvec_data = t.data();
    double* camera_params_data = camera_params.data();
    ceres::CostFunction* cost_function = nullptr;
    for (size_t i = 0; i < pt3ds.size(); i++)
    {
        // PinholeCameraModel
        // RadialCameraModel
        cost_function = colmap::BundleAdjustmentCostFunction<PinholeCameraModel>::Create(pt2ds[i].XY());
        problem_->AddResidualBlock(cost_function, loss_function, qvec_data,
                                 tvec_data, pt3ds[i].XYZ().data(),
                                 camera_params_data);
        problem_->SetParameterBlockConstant(camera_params_data);
    }
    //添加Z向和横向约束
    if (optmize_xyz)
    {
        Sophus::SE3 front_SE3(T_front.block<3,3>(0,0), T_front.block<3,1>(0,3));
        Sophus::Vector6d front_se3_v6 = front_SE3.log();
        // Eigen::Quaterniond Q_T(T.block<3,3>(0,0));

        // Eigen::Vector4d Q_curr = Q_T.coeffs();
        // Eigen::Vector3d t_curr = T.block<3,1>(0,3);

        // double* q_data = Q_curr.data();
        // double* t_data = t_curr.data();
        cost_function = new ceres::AutoDiffCostFunction<OdomXYZBundleAdjustmentCostFunction, 2, 4, 3>
                                            (new OdomXYZBundleAdjustmentCostFunction(front_se3_v6));
        problem_->AddResidualBlock(cost_function, nullptr, qvec_data, tvec_data);

    }
// 
    ceres::Solver::Options solver_options;
    solver_options.linear_solver_type = ceres::SPARSE_SCHUR;
    solver_options.function_tolerance = 0.0;
    solver_options.gradient_tolerance = 10.0;
    // solver_options.parameter_tolerance = 0.0;
    // options.solver_options.max_num_iterations = ba_local_max_num_iterations;
    solver_options.max_linear_solver_iterations = 100;
    solver_options.minimizer_progress_to_stdout = false;
    // options.solver_options.num_threads = num_threads;
    ceres::Solve(solver_options, problem_.get(), &summary_);
    Eigen::Quaterniond optimizer_q(*qvec_data,*(qvec_data+1),*(qvec_data+2),*(qvec_data+3));
    Eigen::Vector3d optimize_t(*tvec_data,*(tvec_data+1),*(tvec_data+2));

    Eigen::Matrix4d optimizer_rt;
    optimizer_rt.setIdentity();
    optimizer_rt.block<3,3>(0,0) = optimizer_q.matrix();
    optimizer_rt.block<3,1>(0,3) = optimize_t;
    Eigen::Matrix4d cam2world_rt = optimizer_rt.inverse();
    optimizer_rt = cam2world_rt;

    std::cout << summary_.BriefReport() << std::endl;

/*     std::cout << "optimizer matrix is:" << std::endl 
             << optimizer_rt(0,0) << ","<< optimizer_rt(0,1)<< "," << optimizer_rt(0,2) << ","<< optimizer_rt(0,3) << "," 
             << optimizer_rt(1,0) << ","<< optimizer_rt(1,1)<< "," << optimizer_rt(1,2) << ","<< optimizer_rt(1,3) << "," 
             << optimizer_rt(2,0) << ","<< optimizer_rt(2,1)<< "," << optimizer_rt(2,2) << ","<< optimizer_rt(2,3) << "," 
             << optimizer_rt(3,0) << ","<< optimizer_rt(3,1)<< "," << optimizer_rt(3,2) << ","<< optimizer_rt(3,3) << "," 
             << std::endl; */

    // std::cout << "camera param is " << *camera_params_data << " " << *(camera_params_data+1) << " " << *(camera_params_data+2) << " " << *(camera_params_data+3) << std::endl;
    return optimizer_rt;
}

void BundleAdjustPose3d2d::AddOdomXYZProblem(Eigen::Matrix4d& T_curr, Eigen::Matrix4d& T_front)
{
    
/*     Eigen::Vector3d t = T_curr.block<3,1>(0,3);
    Eigen::Quaterniond tmp_q = Eigen::Quaterniond(T_curr.block<3,3>(0,0));
    Eigen::Vector4d q = Eigen::Vector4d(tmp_q.w(), tmp_q.x(), tmp_q.y(), tmp_q.z());
    std::cout << "AddOdomXYZProblem q is " << q << std::endl;

    double* qvec_data = q.data();
    double* tvec_data = t.data();
    
    Sophus::SE3 front_SE3(T_front.block<3,3>(0,0), T_front.block<3,1>(0,3));
    Sophus::Vector6d front_se3_v6 = front_SE3.log();
    // Eigen::Quaterniond Q_T(T_curr.block<3,3>(0,0));
    // Eigen::Vector4d Q_curr = Q_T.coeffs();
    // Eigen::Vector3d t_curr = T_curr.block<3,1>(0,3);

    // double* q_data = Q_curr.data();
    // double* t_data = t_curr.data();
    ceres::CostFunction* cost_function = new ceres::AutoDiffCostFunction<OdomXYZBundleAdjustmentCostFunction, 2, 4, 3>
                                          (new OdomXYZBundleAdjustmentCostFunction(front_se3_v6));
    problem_->AddResidualBlock(cost_function, nullptr, qvec_data, tvec_data); */
//     problem_->AddResidualBlock(cost_function, loss_function, Q_curr.data(), t_curr.data());
}

void BundleAdjustPose3d2d::Solve()
{
    ceres::Solver::Options solver_options;
    solver_options.linear_solver_type = ceres::DENSE_SCHUR;
    ceres::Solve(solver_options, problem_.get(), &summary_);
    // problem_->GetParameterBlocks

}

