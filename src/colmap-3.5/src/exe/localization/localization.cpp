#include "localization.hpp"
// #include "../pre_local/colmap_io.hpp"

Localization::Localization(const std::string load_map_path)
: Params()
{
    
    // WORKSPACE = ros::package::getPath("COLMAP");
    // param_file_path_ = WORKSPACE + "/config/localization.yaml";
    // ReadConfig();
    
    colmap_file_reader_ptr_ = std::make_shared<ColmapFileReader>(pix_error_);
    LOG(INFO) << "==========Loading map=====================";
    colmap_file_reader_ptr_->ReadMap(load_map_path);
    LOG(INFO) << "==========Load map complete===============";

    // LOG(INFO) << "==========Filter map=====================";
    colmap_file_reader_ptr_->FilterMap3D();
    // LOG(INFO) << "==========Filter map complete=====================";

    colmap_file_reader_ptr_->FilterByCloud();
    LOG(INFO) << "==========Filter map by cloud complete=====================";


    //TODO: 读参数
}

void Localization::ReadConfig()
{
    // YAML::Node pnp_node = YAML::LoadFile(param_file_path_);
    cv::FileStorage fs;
    fs.open(param_file_path_, cv::FileStorage::READ);
    if (!fs.isOpened())
    {
        LOG(ERROR) << param_file_path_ << "  param filepath is empty,please check out!" << std::endl;
        return;
    }
    fs["RansacInitScore"] >> change_score_;
    fs["CameraMat"] >> K_;
    fs["DistCoeff"] >> distCoeffs_;
    fs["PixError"] >> pix_error_;

    std::cout <<"RansacInitScore is: " << change_score_ << std::endl;
    std::cout <<"distCoeffs_ is: " << std::endl << distCoeffs_ << std::endl;
    std::cout <<"CameraMat is: " << K_ << std::endl;
    std::cout <<"PixError is: " << pix_error_ << std::endl;

}

bool Localization::InitComputePose(const std::vector<cv::Point3f>& match_pt3d,
                         const std::vector<cv::Point2f>& match_pt2d,
                          const cv::Mat& K)
{
    match_pt3d_ = match_pt3d;
    match_pt2d_ = match_pt2d;
    bool computer_success = true;
    float change_score = 25.0;

    // std::cout <<"match size is " << match_pt3d.size() << std::endl;
    std::deque<std::pair<cv::Mat, cv::Mat>> pose_dq;
    cv::Mat r, t, inliers;
    // std::cout << "46 init pose K is "<< std::endl << K_ << std::endl;
    // std::cout << "distCoeffs_ is "<< std::endl << distCoeffs_ << std::endl;
/*     for (size_t i = 0; i < match_pt3d_.size(); i++)
    {
        std::cout << match_pt3d_[i].x << " " << match_pt3d_[i].y << " " << match_pt3d_[i].z << " "
                  << match_pt2d_[i].x << " " << match_pt2d_[i].y << std::endl;
    } */
    while(computer_success && change_score > 0.2)
    {
        pose_dq.push_back(std::make_pair(r,t));
        change_score -= 0.2;
        computer_success = cv::solvePnPRansac(match_pt3d, match_pt2d, K_, distCoeffs_, r, t, false, 300, change_score, 0.99, inliers, cv::SOLVEPNP_ITERATIVE);
    }

    if(pose_dq.size() == 1)
    {
        std::cout << " InitComputePose fail, please change solvePnPRansac score." << std::endl;
        return false;
    }

    r = pose_dq.back().first;
    t = pose_dq.back().second;

    cv::Mat R;
    cv::Rodrigues(r, R);
    Eigen::Matrix3d matrix_r;
    Eigen::Vector3d matrix_t;
    cv::cv2eigen(R, matrix_r);
    cv::cv2eigen(t, matrix_t);
    // Eigen::Matrix4f tmp_pose;
    // tmp_pose.setIdentity();
    // tmp_pose.block<3,3>(0,0) = matrix_r;
    // tmp_pose.block<3,1>(0,3) = matrix_t;

    // init_pose_ = tmp_pose.inverse();

    Eigen::Matrix4d pose_tmp;
    pose_tmp.setIdentity();
    pose_tmp.block<3,3>(0,0) = matrix_r;
    pose_tmp.block<3,1>(0,3) = matrix_t;
    init_pose_ = pose_tmp.inverse();
    std::cout << "init pose is " << std::endl << init_pose_ << std::endl;
    return true;
}

bool Localization::GetFeature3dCloud(pcl::PointCloud<pcl::PointXYZ>& pub_feature_cloud)
{
    if (save_feature_)
    {
        pub_feature_cloud = pub_feature_cloud_;
        return true;
    }
    return false;
}

bool Localization::InitComputePose()
{
    // bool computer_success = true;
    float change_score = ransac_init_score_;

    // std::deque<std::pair<cv::Mat, cv::Mat>> pose_dq;
    cv::Mat r, t, inliers;
    // LOG(INFO) << "K_ is:" << std::endl << projection_matrix_;
    // LOG(INFO) << "distCoeffs_ is:" << std::endl << distortion_matrix_;
    std::vector<cv::Point2f> match_undistor_2d;
    
    int i = 0;
    for (auto d3 : match_pt3d_)
    {
        std::cout << d3 << "==============" << match_pt2d_[i] << std::endl;
        i++;
    }


    if (undistortion_)
    {
        // LOG(INFO) << "projection_matrix is:" << std::endl << projection_matrix_;
        // LOG(INFO) << "distortion_matrix is:" << std::endl << distortion_matrix_;
        cv::fisheye::undistortPoints(match_pt2d_, match_undistor_2d, projection_matrix_, distortion_matrix_, cv::Mat(), projection_matrix_);
        cv::solvePnPRansac(match_pt3d_, match_undistor_2d, projection_matrix_, cv::Mat(), r, t, false, 300, ransac_init_score_, 0.99, inliers, cv::SOLVEPNP_ITERATIVE);
    }else{
        cv::solvePnPRansac(match_pt3d_, match_pt2d_, projection_matrix_, distortion_matrix_, r, t, false, 300, ransac_init_score_, 0.99, inliers, cv::SOLVEPNP_ITERATIVE);
    }
    // bool computer_success = cv::solvePnPRansac(match_pt3d_, match_pt2d_, K_, distCoeffs_, r, t, false, 300, change_score, 0.99, inliers, cv::SOLVEPNP_ITERATIVE);
    if (save_feature_)
    {
        pub_feature_cloud_.clear();

        for (auto pt_3d : match_pt3d_)
        {   
            pcl::PointXYZ pt;
            pt.x = pt_3d.x;
            pt.y = pt_3d.y;
            pt.z = pt_3d.z;
            pub_feature_cloud_.push_back(pt);
        }
        // pcl::io::savePCDFileBinary("feature.pcd", cloud_3d);
        // std::cout << "match feature 3d cloud save in feature.pcd" << std::endl;
    }
    // computer_success = cv::solvePnPRansac(match_pt3d_, match_undistor_2d, projection_matrix_, cv::Mat(), r, t, false, 300, change_score, 0.99, inliers, cv::SOLVEPNP_ITERATIVE);

   /*  while(computer_success && change_score > 0.2)
    {
        pose_dq.push_back(std::make_pair(r,t));
        change_score -= 0.2;
        // computer_success = cv::solvePnPRansac(match_pt3d_, match_pt2d_, K_, cv::Mat(), r, t, false, 300, change_score, 0.99, inliers, cv::SOLVEPNP_ITERATIVE);
        computer_success = cv::solvePnPRansac(match_pt3d_, match_pt2d_, K_, distCoeffs_, r, t, false, 300, change_score, 0.99, inliers, cv::SOLVEPNP_ITERATIVE);
    }

    if(pose_dq.size() == 1)
    {
        std::cout << " InitComputePose fail, please change solvePnPRansac score." << std::endl;
        return false;
    }

    r = pose_dq.back().first;
    t = pose_dq.back().second; */

    cv::Mat R;
    cv::Rodrigues(r, R);
    Eigen::Matrix3d matrix_r;
    Eigen::Vector3d matrix_t;
    cv::cv2eigen(R, matrix_r);
    cv::cv2eigen(t, matrix_t);
    Eigen::Matrix4d pose_tmp;
    pose_tmp.setIdentity();
    pose_tmp.block<3,3>(0,0) = matrix_r;
    pose_tmp.block<3,1>(0,3) = matrix_t;
    init_pose_ = pose_tmp.inverse();
    if (pose_buff_.size() > 1)
    {
        auto last_pose = pose_buff_.back();
        double distance = abs(init_pose_(0,3) - last_pose(0,3)) 
                        + abs(init_pose_(1,3) - last_pose(1,3));
        if (distance > 1.5)
        {
            LOG(ERROR) <<"current pose error is too large";
            return false;
        }
    }



    // std::cout << "updata pose is " << std::endl << init_pose_ << std::endl;

/*     std::vector<Point3D> colmap_3d;
    std::vector<Point2D> colmap_2d;
    Point3D tmp_p3d;
    Point2D tmp_p2d;
    for (size_t i = 0; i < match_pt3d_.size(); i++)
    {
        Eigen::Vector3d tmp_v3d(match_pt3d_[i].x, match_pt3d_[i].y, match_pt3d_[i].z);
        tmp_p3d.SetXYZ(tmp_v3d);
        colmap_3d.push_back(tmp_p3d);

        Eigen::Vector2d tmp_v2d(match_pt2d_[i].x, match_pt2d_[i].y);
        tmp_p2d.SetXY(tmp_v2d);
        colmap_2d.push_back(tmp_p2d);

    }
    OptimizePose(colmap_3d, colmap_2d); */
    pose_buff_.push_back(init_pose_);
    return true;
}

void Localization::OptimizePose(std::vector<Point3D>& colmap_3d, const std::vector<Point2D>& colmap_2d)
{
    BundleAdjustPose3d2d bundleadjust;
    std::vector<double> camera_param;

    // std::cout  << K_.at<float>(0, 0) << std::endl;
    camera_param.push_back(K_.at<float>(0, 0));
    camera_param.push_back(K_.at<float>(1, 1));
    camera_param.push_back(K_.at<float>(0, 2));
    camera_param.push_back(K_.at<float>(1, 2));
    camera_param.push_back(0.0);
    // std::cout <<"optimize K is " << camera_param[0] << " " << camera_param[1] << " "<< camera_param[2] << " "<< camera_param[3] << " " << std::endl;

    ceres::LossFunction* loss_function = new ceres::CauchyLoss(1.0);
    // std::cout << "init_pose_is  " << init_pose_ << std::endl;

    Eigen::Matrix4d X = init_pose_.inverse();
    // std::cout << "pose_buff_.size is " << pose_buff_.size() << std::endl;
    // if (pose_buff_.size() > 4)
    // {
    //     auto pose_front = pose_buff_.back();
    //     std::cout << "X pose_front :" << std::endl << X.inverse() * pose_front << std::endl;
    //     init_pose_ = bundleadjust.AddImageToProblem3d2d(X, colmap_3d, colmap_2d, camera_param, loss_function, pose_front, true);

    //     // bundleadjust.AddOdomXYZProblem(X, pose_front);
    //     std::cout << "add AddOdomXYZProblem" << std::endl;
    // }else{
        Eigen::Matrix4d pose_front = Eigen::Matrix4d::Identity();
        init_pose_ = bundleadjust.AddImageToProblem3d2d(X, colmap_3d, colmap_2d, camera_param, loss_function, pose_front);
    // }
    // Eigen::Matrix4d T_r_c;
    // T_r_c << 1.0009264299254783e+00, -6.3760797234911374e-03,
    //    -3.6412217106308389e-03, -8.2306003280063766e-03,
    //    5.7404705025172701e-03, 9.9910623405121868e-01,
    //    3.7155780203881376e-02, 3.2207062434066727e-03,
    //    -2.5306818625789543e-03, -3.9831681350535456e-02,
    //    9.8551346156874331e-01, 1.0017158034678583e-01, 0., 0., 0., 1.;
    
    // auto T_r = T_r_c * optim_T;


    // std::cout << "r T is:" << std::endl 
    //          << T_r(0,0) << ","<< T_r(0,1)<< "," << T_r(0,2) << ","<< T_r(0,3) << "," 
    //          << T_r(1,0) << ","<< T_r(1,1)<< "," << T_r(1,2) << ","<< T_r(1,3) << "," 
    //          << T_r(2,0) << ","<< T_r(2,1)<< "," << T_r(2,2) << ","<< T_r(2,3) << "," 
    //          << T_r(3,0) << ","<< T_r(3,1)<< "," << T_r(3,2) << ","<< T_r(3,3) << "," 
    //          << std::endl;
}

void Localization::GetUpdatePose(Eigen::Matrix4d& update_pose)
{
    update_pose = init_pose_;
}

bool Localization::AnalysisMatch3d2d(const COLMAP::PointMatch& cur_data)
{
    int length = cur_data.img_ids.size();
    if (length == 0)
    {
        LOG(ERROR)<< "match image size is 0";
        return false;
    }
    size_t max_num = 0;
    int max_id = 0;
    for (int i = 0; i < length; i++)
    {
        auto points = cur_data.polygons[i].points;
        if (max_num < points.size())
        {
            max_num = points.size();
            max_id = i;
        }
    }
    std::string img_ids = cur_data.img_ids[max_id];
    auto image = colmap_file_reader_ptr_->GetImageWithName(img_ids);
    img_ids = img_ids.replace(img_ids.find(".jpg"), 4, "");
    // colmap_file_reader_ptr_->FilterFrame3D(image);
    colmap_file_reader_ptr_->GetSuperGlue3D2DMatches(img_ids, cur_data.polygons[max_id].points, match_pt2d_, match_pt3d_);


    if (match_pt2d_.size() < 5)
    {
        LOG(ERROR) << "match size is " << match_pt2d_.size() << " not enough to computer pose !!!";
        return false;
    }
    return true;
}

std::vector<std::string> Localization::FindRelatedImage(const Eigen::Matrix4f pose)
{
    colmap_file_reader_ptr_->CutPointIndexCloud(pose);
    std::vector<std::string> related_image_name = colmap_file_reader_ptr_->FindLocalFrame();
    return related_image_name;
}

std::string Localization::FindRelatedImageDependXY(const Eigen::Matrix4f pose)
{
    std::string image_name = colmap_file_reader_ptr_->SearchNearImage(pose);
    return image_name;
}
