#ifndef _LOCALIZATION_FLOW_HPP_
#define _LOCALIZATION_FLOW_HPP_

#include <g2o/core/base_vertex.h>
#include <g2o/core/base_unary_edge.h>
#include <g2o/core/sparse_optimizer.h>
#include <g2o/core/block_solver.h>
#include <g2o/core/solver.h>
#include <g2o/core/optimization_algorithm_gauss_newton.h>
#include <g2o/solvers/dense/linear_solver_dense.h>

#include <opencv2/core/core.hpp> 
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/core/eigen.hpp>

#include <ros/package.h>
#include <ros/time.h>

#include "../optimize/ceres_optimize.hpp"
#include "../pre_local/colmap_io.hpp"
#include "devel/include/COLMAP/PointMatch.h"
#include "../tool/params.hpp"

class Localization : public Params
{
  public:
    Localization() = default;
    Localization(const std::string load_map_path);
    bool InitComputePose();
    bool InitComputePose(const std::vector<cv::Point3f>& match_pt3d,
                         const std::vector<cv::Point2f>& match_pt2d,
                         const cv::Mat& K);
    void OptimizePose(std::vector<Point3D>& colmap_3d, const std::vector<Point2D>& colmap_2d);
    bool AnalysisMatch3d2d(const COLMAP::PointMatch& cur_data);
    void GetUpdatePose(Eigen::Matrix4d& update_pose);
    void ReadConfig();
    std::vector<std::string> FindRelatedImage(const Eigen::Matrix4f pose);
    std::string FindRelatedImageDependXY(const Eigen::Matrix4f pose);
    bool GetFeature3dCloud(pcl::PointCloud<pcl::PointXYZ>& pub_feature_cloud);


  private:
    std::string param_file_path_;
    std::string WORKSPACE;
    std::shared_ptr<ColmapFileReader> colmap_file_reader_ptr_;
    std::vector<cv::Point3f> match_pt3d_;
    std::vector<cv::Point2f> match_pt2d_;
    Eigen::Matrix4d init_pose_;
    Eigen::Matrix4d optim_pose_;
    double change_score_;
    cv::Mat K_;
    cv::Mat distCoeffs_;
    double pix_error_;
    
    std::deque<Eigen::Matrix4d> pose_buff_;
    ros::Time loc_match_time_;
    pcl::PointCloud<pcl::PointXYZ> pub_feature_cloud_;


};








#endif