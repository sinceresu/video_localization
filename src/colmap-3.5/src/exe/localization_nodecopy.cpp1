#include "../base/reconstruction.h"
#include "../base/database.h"
#include "../base/database_cache.h"
#include "../util/option_manager.h"
#include "optimize/ceres_optimize.hpp"

#include "sophus/se3.h"

#include <opencv2/core/core.hpp> 
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/imgcodecs/imgcodecs_c.h>
#include <opencv2/core/eigen.hpp>

#include <pcl/point_types.h> 
#include <pcl/io/pcd_io.h> 

#include <g2o/core/base_vertex.h>
#include <g2o/core/base_unary_edge.h>
#include <g2o/core/sparse_optimizer.h>
#include <g2o/core/block_solver.h>
#include <g2o/core/solver.h>
#include <g2o/core/optimization_algorithm_gauss_newton.h>
#include <g2o/solvers/dense/linear_solver_dense.h>

#include <chrono>

using namespace colmap;
using namespace cv;
using namespace std;



typedef vector<Eigen::Vector2d, Eigen::aligned_allocator<Eigen::Vector2d>> VecVector2d;
typedef vector<Eigen::Vector3d, Eigen::aligned_allocator<Eigen::Vector3d>> VecVector3d;


/// vertex and edges used in g2o ba
class VertexPose : public g2o::BaseVertex<6, Sophus::SE3> {
public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

  virtual void setToOriginImpl() override {
    _estimate = Sophus::SE3();
  }

  /// left multiplication on SE3
  virtual void oplusImpl(const double *update) override {
    Eigen::Matrix<double, 6, 1> update_eigen;
    update_eigen << update[0], update[1], update[2], update[3], update[4], update[5];
    _estimate = Sophus::SE3::exp(update_eigen) * _estimate;
  }

  virtual bool read(istream &in) override {}

  virtual bool write(ostream &out) const override {}
};

class EdgeProjection : public g2o::BaseUnaryEdge<2, Eigen::Vector2d, VertexPose> {
public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

  EdgeProjection(const Eigen::Vector3d &pos, const Eigen::Matrix3d &K) : _pos3d(pos), _K(K) {}

  virtual void computeError() override {
    const VertexPose *v = static_cast<VertexPose *> (_vertices[0]);
    Sophus::SE3 T = v->estimate();
    Eigen::Vector3d pos_pixel = _K * (T * _pos3d);
    pos_pixel /= pos_pixel[2];
    _error = _measurement - pos_pixel.head<2>();
  }

  virtual void linearizeOplus() override {
    const VertexPose *v = static_cast<VertexPose *> (_vertices[0]);
    Sophus::SE3 T = v->estimate();
    Eigen::Vector3d pos_cam = T * _pos3d;
    double fx = _K(0, 0);
    double fy = _K(1, 1);
    double cx = _K(0, 2);
    double cy = _K(1, 2);
    double X = pos_cam[0];
    double Y = pos_cam[1];
    double Z = pos_cam[2];
    double Z2 = Z * Z;
    _jacobianOplusXi
      << -fx / Z, 0, fx * X / Z2, fx * X * Y / Z2, -fx - fx * X * X / Z2, fx * Y / Z,
      0, -fy / Z, fy * Y / (Z * Z), fy + fy * Y * Y / Z2, -fy * X * Y / Z2, -fy * X / Z;
  }

  virtual bool read(istream &in) override {}

  virtual bool write(ostream &out) const override {}

private:
  Eigen::Vector3d _pos3d;
  Eigen::Matrix3d _K;
};

void bundleAdjustmentG2O(
  const VecVector3d &points_3d,
  const VecVector2d &points_2d,
  const Mat &K,
  Sophus::SE3 &pose) {

  // 构建图优化，先设定g2o
  typedef g2o::BlockSolver<g2o::BlockSolverTraits<6, 3>> BlockSolverType;  // pose is 6, landmark is 3
  typedef g2o::LinearSolverDense<BlockSolverType::PoseMatrixType> LinearSolverType; // 线性求解器类型
  // 梯度下降方法，可以从GN, LM, DogLeg 中选
  auto solver = new g2o::OptimizationAlgorithmGaussNewton(
    g2o::make_unique<BlockSolverType>(g2o::make_unique<LinearSolverType>()));
  g2o::SparseOptimizer optimizer;     // 图模型
  optimizer.setAlgorithm(solver);   // 设置求解器
  optimizer.setVerbose(true);       // 打开调试输出

  // vertex
  VertexPose *vertex_pose = new VertexPose(); // camera vertex_pose
  vertex_pose->setId(0);
  vertex_pose->setEstimate(Sophus::SE3());
  optimizer.addVertex(vertex_pose);

  // K
  Eigen::Matrix3d K_eigen;
  K_eigen <<
          K.at<double>(0, 0), K.at<double>(0, 1), K.at<double>(0, 2),
    K.at<double>(1, 0), K.at<double>(1, 1), K.at<double>(1, 2),
    K.at<double>(2, 0), K.at<double>(2, 1), K.at<double>(2, 2);

  // edges
  int index = 1;
  for (size_t i = 0; i < points_2d.size(); ++i) {
    auto p2d = points_2d[i];
    auto p3d = points_3d[i];
    EdgeProjection *edge = new EdgeProjection(p3d, K_eigen);
    edge->setId(index);
    edge->setVertex(0, vertex_pose);
    edge->setMeasurement(p2d);
    edge->setInformation(Eigen::Matrix2d::Identity());
    optimizer.addEdge(edge);
    index++;
  }

  chrono::steady_clock::time_point t1 = chrono::steady_clock::now();
  optimizer.setVerbose(true);
  optimizer.initializeOptimization();
  optimizer.optimize(10);
  chrono::steady_clock::time_point t2 = chrono::steady_clock::now();
  chrono::duration<double> time_used = chrono::duration_cast<chrono::duration<double>>(t2 - t1);
  cout << "optimization costs time: " << time_used.count() << " seconds." << endl;
  cout << "pose estimated by g2o =\n" << vertex_pose->estimate().matrix() << endl;
  pose = vertex_pose->estimate();
}


int RunDatabaseCreator(int argc, char** argv) {
  OptionManager options;

  options.AddDatabaseOptions();
  options.Parse(argc, argv);

  Database database(*options.database_path);

  return EXIT_SUCCESS;
}


Eigen::Vector2d computerReprojectionError(const Eigen::Matrix4d& T, const Eigen::Matrix3d& K, const Eigen::Vector3d& pt3, const Eigen::Vector2d& pt2, cv::KeyPoint& pt)
{
  Eigen::Vector4d p_cam_world =  T.inverse() * pt3.homogeneous();
  // Eigen::Vector3d p_cam_world =  T.block<3,3>(0,0) * pt3 + T.block<3,1>(0,3);

  // std::cout << "p_cam_world[0] is " << p_cam_world[0] << std::endl;
  // std::cout << "p_cam_world[1] is " << p_cam_world[1] << std::endl;
  // std::cout << "p_cam_world[2] is " << p_cam_world[2] << std::endl;
  // std::cout << "p_cam_world[0] / p_cam_world[2] is " << p_cam_world[0]/p_cam_world[2] << std::endl;
  // std::cout << "p_cam_world[1] / p_cam_world[2] is " << p_cam_world[1]/p_cam_world[2] << std::endl;
  Eigen::Vector3d p_came_world_norm = Eigen::Vector3d(p_cam_world[0]/p_cam_world[2], p_cam_world[1]/p_cam_world[2], 1);
  // Eigen::Vector3d p_came_world_norm = Eigen::Vector3d(p_cam_world[0]/p_cam_world[2], p_cam_world[1]/p_cam_world[2], 1);
  auto p_cam_norm = K * p_came_world_norm;
  // std::cout << "p_cam_norm is " << p_cam_norm << std::endl;
  pt.pt.x = p_cam_norm[0];
  pt.pt.y = p_cam_norm[1];
  Eigen::Vector2d error_uv = Eigen::Vector2d(p_cam_norm[0] - pt2[0], p_cam_norm[1] - pt2[1]);
  return error_uv;
}

int main(int argc, char** argv)
{
    
    Eigen::Quaterniond A(-0.480768,-0.505976,0.498273,0.514367);
    Eigen::Matrix4d B, C;
    B.setIdentity();
    // B << 0.0493994,-0.0151813,-0.998663,0.2004119825357251,0.308498,-0.950759,0.0297129,-1.191073700157925,-0.949941,-0.309554,-0.0422823,0.03761419923373222,0,0,0,1;
    // C << 0.07558182891994192,-0.008786707961736168,-0.9971008880250345,0.2004119825357251,0.3087989406303363,-0.9505961127871496,0.03178434551069004,-1.191073700157925,-0.94811950797497,-0.3103060168884194,-0.06913446665842737,0.03761419923373222,0,0,0,1;
    B.block<3,3>(0,0) = A.matrix();
    B.block<3,1>(0,3) = Eigen::Vector3d(0.138126 ,-2.359357 ,-0.207764);
    std::cout << B.inverse() << std::endl; 
    // return 1;

    if (argc != 3)
    {
        std::cout << "input error, ./localization *.txt(bin)path *.db" << std::endl;
        std::cout << "example ./localization /media/zmc/0053-2C75/localization/sparse_BA_txt_txt_last /media/zmc/0053-2C75/localization/match2/match2.db" << std::endl;
    }

    // RunFeatureExtractor();
    // RunSequentialMatcher();



    colmap::Reconstruction reconstruction_map,reconstruction;
    //读txt 或者 bin
    // reconstruction_map.Read("/media/zmc/0053-2C75/localization/sparse_BA_txt_txt_last");
    reconstruction_map.Read(argv[1]);

    Database database(argv[2]);
    // Database database("/media/zmc/0053-2C75/localization/match2/match2.db");
    DatabaseCache database_cache;
    size_t min_num_matches = 5;
    bool ignore_watermarks = false;
    std::set<std::string> image_names;

    std::string map_frame_name = "1635748820.220487600_rear.jpg";
    image_names.emplace(map_frame_name);
    // image_names.emplace("1635748729.260487600_rear.jpg");
    image_names.emplace("IMG_20211103_165649.jpg");
    database_cache.Load(database, min_num_matches,
                        ignore_watermarks,
                        image_names);

    
    reconstruction.Load(database_cache);
     // uint32_t image_id = 1;
  // for (const auto& image : database_cache.Images())
  // {
    uint32_t i = 1;
    int num = 0;
    colmap::Image image, image2;
    auto images = database_cache.Images();
    if (images[1].Name() == map_frame_name)
    {
      image = images[1];
      image2 = images[2];
    }else{
      image = images[2];
      image2 = images[1];
    }
    std::vector<cv::KeyPoint> pts1, pts2;
    std::vector<cv::Point3f> pts3d;
    VecVector3d pts3d_eigen;
    std::vector<cv::Point2f> pts2d;
    std::vector<cv::Point2f> pts1d;
    std::vector<Point2D> colmap_pt2d;
    std::vector<Point3D> colmap_pt3d;
    VecVector2d pts2d_eigen;
    uint32_t size = image.NumPoints2D();
    // vector<cv::DMatch> matches;

    auto image_data = reconstruction_map.FindImageWithName(map_frame_name);
    if (!image_data){
      std::cout << "image " << map_frame_name << " can't find in map !!!" << std::endl;
      return 1;
    }
    std::cout << image_data->Name() << " has 3d point num is " << image_data->NumPoints3D() << std::endl;


    cv::KeyPoint pt1, pt2;
    pcl::PointCloud<pcl::PointXYZ> cloud_3d;
    for (; i < size; i++)
    {
      
      if(database_cache.CorrespondenceGraph().HasCorrespondences(image.ImageId(),i))
      {
        auto a = database_cache.CorrespondenceGraph().FindCorrespondences(image.ImageId(), i);
        std::cout << "corr is: image" << 1 << "  " << i << " match  image" << a[0].image_id << "  " << a[0].point2D_idx << std::endl;
        pt1.pt.x = image.Point2D(i).X();
        pt1.pt.y = image.Point2D(i).Y();
        pts1.push_back(pt1);

        pt2.pt.x = image2.Point2D(a[0].point2D_idx).X();
        pt2.pt.y = image2.Point2D(a[0].point2D_idx).Y();
        pts2.push_back(pt2);


        // if (image_data->IsPoint3DVisible(i))
        // {
        auto pt_2d = image_data->Point2D(i);
        //是否存在3d点
        if(reconstruction_map.ExistsPoint3D(pt_2d.Point3DId()))
        {
            pcl::PointXYZ pt;
            Eigen::Vector3d pt_3d = reconstruction_map.Point3D(pt_2d.Point3DId()).XYZ();

            colmap_pt3d.push_back(reconstruction_map.Point3D(pt_2d.Point3DId()));
            colmap_pt2d.push_back(image2.Point2D(a[0].point2D_idx));

            cv::Point3f cv_pt_3f = cv::Point3f(pt_3d[0], pt_3d[1], pt_3d[2]);

            pts3d_eigen.push_back(pt_3d);
            pts2d_eigen.push_back(Eigen::Vector2d(pt2.pt.x, pt2.pt.y));

            pts3d.push_back(cv_pt_3f);
            pts2d.push_back(pt2.pt);

            pts1d.push_back(pt1.pt);

            pt.x = pt_3d[0];
            pt.y = pt_3d[1];
            pt.z = pt_3d[2];
            cloud_3d.push_back(pt);
            num++;
        }
        // }
      }
    }
    std::cout <<"pts3d size is " << pts3d.size() << std::endl;
    cv::Mat r, t, inliers;
    //xiao xiang ji
    // cv::Mat K = (cv::Mat_<double>(3, 3) << 6.9913438965345074e+02, 0.,5.4594832506454179e+02 , 0.,
    //    6.9954891305755768e+02, 9.8866195362102337e+02, 0., 0., 1.);
    // 华为手机 mata20 
    cv::Mat K = (cv::Mat_<double>(3, 3) << 2.8139998692833274e+03, 0., 1.8371119520047309e+03, 0.,
       2.8177509464325831e+03, 1.3553898663009072e+03, 0., 0., 1.);
    //H20T 彩色相机
    // cv::Mat K = (cv::Mat_<double>(3, 3) << 1.7989816263225610e+04, 0., 2.4563627434350524e+03, 0.,
    //    1.7996884058083324e+04, 1.8127859725369372e+03, 0., 0., 1.);
    // cv::Mat K = (cv::Mat_<double>(3, 3) << 6.9913438965345074e+02, 0., 9.8866195362102337e+02, 0.,
      //  6.9954891305755768e+02, 5.4594832506454179e+02, 0., 0., 1.);
    // cv::Mat K = (cv::Mat_<double>(3, 3) << 856.43, 0, 1440, 0, 856.43, 1440, 0, 0, 1);
    float change_score = 20.0;
    std::deque<std::pair<cv::Mat, cv::Mat>> pose_dq;



    std::vector<cv::Point3f> pts3d1;
    std::vector<cv::Point2f> pts2d1;

    pts3d1.push_back(pts3d[164]);
    pts2d1.push_back(pts2d[164]);

    pts3d1.push_back(pts3d[163]);
    pts2d1.push_back(pts2d[163]);

    pts3d1.push_back(pts3d[168]);
    pts2d1.push_back(pts2d[168]);

    pts3d1.push_back(pts3d[180]);
    pts2d1.push_back(pts2d[180]);

    pts3d1.push_back(pts3d[126]);
    pts2d1.push_back(pts2d[126]);

    pts3d1.push_back(pts3d[125]);
    pts2d1.push_back(pts2d[125]);

    pts3d1.push_back(pts3d[119]);
    pts2d1.push_back(pts2d[119]);

    pts3d1.push_back(pts3d[109]);
    pts2d1.push_back(pts2d[109]);




    //=============good 3d point =================

    // pts3d1.push_back(pts3d[161]);
    // pts2d1.push_back(pts2d[161]);

    // pts3d1.push_back(pts3d[73]);
    // pts2d1.push_back(pts2d[73]);

    // pts3d1.push_back(pts3d[135]);
    // pts2d1.push_back(pts2d[135]);

    // pts3d1.push_back(pts3d[184]);
    // pts2d1.push_back(pts2d[184]);

    // pts3d1.push_back(pts3d[27]);
    // pts2d1.push_back(pts2d[27]);

    // pts3d1.push_back(pts3d[170]);
    // pts2d1.push_back(pts2d[170]);

    // pts3d1.push_back(pts3d[158]);
    // pts2d1.push_back(pts2d[158]);

    // pts3d1.push_back(pts3d[15]);
    // pts2d1.push_back(pts2d[15]);

    // pts3d1.push_back(pts3d[16]);
    // pts2d1.push_back(pts2d[16]);

    // pts3d1.push_back(pts3d[133]);
    // pts2d1.push_back(pts2d[133]);

    // pts3d1.push_back(pts3d[199]);
    // pts2d1.push_back(pts2d[199]);

    // pts3d1.push_back(pts3d[24]);
    // pts2d1.push_back(pts2d[24]);

    bool computer_success = true;
    //逻辑要改，万一第一次就false了呢
    while(computer_success && change_score > 0.2)
    {
        pose_dq.push_back(std::make_pair(r,t));
        change_score -= 0.2;
        // computer_success = cv::solvePnPRansac(pts3d, pts2d, K, cv::Mat(), r, t, false, 300, change_score, 0.99, inliers, cv::SOLVEPNP_ITERATIVE);
        computer_success = cv::solvePnPRansac(pts3d1, pts2d1, K, cv::Mat(), r, t, false, 300, change_score, 0.99, inliers, cv::SOLVEPNP_ITERATIVE);
    }
    r = pose_dq.back().first;
    t = pose_dq.back().second;

    // cv::solvePnP(pts3d1, pts2d1, K, cv::Mat(), r, t, false);

    cv::Mat R;
    cv::Rodrigues(r, R); // r为旋转向量形式，用Rodrigues公式转换为矩阵
    std::cout << "inliers is " << inliers << std::endl;

    // Sophus::SE3 pose;
    // bundleAdjustmentG2O(pts3d_eigen, pts2d_eigen, K, pose);
    // std::cout << "after optimize pose is " << std::endl << pose.matrix().inverse() << std::endl;


    // std::cout << "R=" << std::endl << R << std::endl;
    // std::cout << "t=" << std::endl << t << std::endl;

    Eigen::Matrix3d matrix_r;
    Eigen::Vector3d matrix_t;
    cv::cv2eigen(R, matrix_r);
    cv::cv2eigen(t, matrix_t);
    Eigen::Matrix4d matrix_tt;
    matrix_tt.setIdentity();
    matrix_tt.block<3,3>(0,0) = matrix_r;
    matrix_tt.block<3,1>(0,3) = matrix_t;

    std::cout << "T is:" << matrix_tt.inverse() << std::endl;
    Eigen::Matrix4d T_r_c;
    T_r_c << 1.0009264299254783e+00, -6.3760797234911374e-03,
       -3.6412217106308389e-03, -8.2306003280063766e-03,
       5.7404705025172701e-03, 9.9910623405121868e-01,
       3.7155780203881376e-02, 3.2207062434066727e-03,
       -2.5306818625789543e-03, -3.9831681350535456e-02,
       9.8551346156874331e-01, 1.0017158034678583e-01, 0., 0., 0., 1.;
    
    // std::cout << "T 红外 is:" << T_r_c * matrix_tt.inverse() << std::endl;


    // Sophus::SE3 pose;
    // bundleAdjustmentG2O(pts3d_eigen, pts2d_eigen, K, pose);
    // std::cout << "after optimize pose is " << std::endl << pose.matrix() << std::endl;
    // std::cout << "after optimize pose is " << std::endl << pose.matrix().inverse() << std::endl;

    Eigen::Matrix3d K_eigen, K_eigen_map;
    K_eigen_map << 856.43 , 0 , 1440 
                , 0 , 856.43 , 1440 
                , 0 , 0 , 1;
    cv::cv2eigen(K, K_eigen);

    std::vector<cv::Point2f> projectedPoints;
      cv::Mat distCoeffs(5, 1, cv::DataType<float>::type);   // Distortion vector
    distCoeffs.at<float>(0) = 0;
    distCoeffs.at<float>(1) = 0;
    distCoeffs.at<float>(2) = 0;
    distCoeffs.at<float>(3) = 0;
    distCoeffs.at<float>(4) = 0;

    // cv::projectPoints(pts3d, r, t, K, distCoeffs, projectedPoints);
    for (size_t i = 0; i < pts3d.size(); i++)
    {
      
      Eigen::Vector3d pt3 = Eigen::Vector3d(pts3d[i].x, pts3d[i].y, pts3d[i].z);
      Eigen::Vector2d pt2 = Eigen::Vector2d(pts2d[i].x, pts2d[i].y);
      // auto error = computerReprojectionError(pose.matrix().inverse(), K_eigen, pt3, pt2);
      cv::KeyPoint pt_result;
      auto error = computerReprojectionError(matrix_tt.inverse(), K_eigen, pt3, pt2, pt_result);
      // std::cout << i <<"init error is: " << error[0]  << "  " << error[1] << std::endl;
    }
    // return 1;

    auto pic1 = cv::imread("/media/zmc/0053-2C75/localization/match14/1635748820.220487600_rear.jpg",CV_LOAD_IMAGE_COLOR);
    auto pic2 = cv::imread("/media/zmc/0053-2C75/localization/match14/IMG_20211103_165649.jpg",CV_LOAD_IMAGE_COLOR);
    

    
    for (size_t i = 0; i < pts2.size(); i++)
    {
      cv::Point2f point1 = pts1[i].pt;
      cv::Point2f point2 = pts2[i].pt;
      cv::putText(pic1, std::to_string(i), point1, cv::FONT_HERSHEY_SIMPLEX,0.45, CV_RGB(255,0,0),1.8);
      cv::putText(pic2, std::to_string(i), point2, cv::FONT_HERSHEY_SIMPLEX,0.45, CV_RGB(255,0,0),1.8);
    }
    cv::imwrite("image1.jpg", pic1);
    cv::imwrite("image2.jpg", pic2);
    pcl::io::savePCDFileBinary("cloud_3d.pcd", cloud_3d);
    // cv::waitKey(0);
    // image_id++;
    std::cout << "match size is " << num << std::endl;
  // }
    


    BundleAdjustPose3d2d bundleadjust;
    std::vector<double> camera_param;
    // camera_param.push_back(1.7989816263225610e+04);
    // camera_param.push_back(1.7996884058083324e+04);
    // camera_param.push_back(2.4563627434350524e+03);
    // camera_param.push_back(1.8127859725369372e+03);

    camera_param.push_back(2.8139998692833274e+03);
    camera_param.push_back(2.8177509464325831e+03);
    camera_param.push_back(1.8371119520047309e+03);
    camera_param.push_back(1.3553898663009072e+03);
    camera_param.push_back(0.0);

    ceres::LossFunction* loss_function = new ceres::CauchyLoss(1.0);
    Eigen::Matrix4d optim_T = bundleadjust.AddImageToProblem3d2d(matrix_tt, colmap_pt3d, colmap_pt2d, camera_param, loss_function);
    // 0.458901 -0.5205 -0.468557 0.546758 0.19965 42.2661 -5.6381

    Eigen::Matrix4d map_pose;
    map_pose.setIdentity();
    map_pose.block<3,3>(0,0) = Eigen::Quaterniond(0.458901,-0.5205,-0.468557,0.546758).matrix();
    map_pose.block<3,1>(0,3) = Eigen::Vector3d(0.19965,42.2661,-5.6381);
    
    std::vector<cv::KeyPoint> pts_result, pts_result1, pts_result2;
    for (size_t i = 0; i < pts3d.size(); i++)
    {
      
      Eigen::Vector3d pt3 = Eigen::Vector3d(pts3d[i].x, pts3d[i].y, pts3d[i].z);
      Eigen::Vector2d pt2 = Eigen::Vector2d(pts2d[i].x, pts2d[i].y);
      // auto error = computerReprojectionError(pose.matrix().inverse(), K_eigen, pt3, pt2);
      
      cv::KeyPoint pt_result, pt_result1;
      auto error = computerReprojectionError(optim_T, K_eigen, pt3, pt2, pt_result);

      //3d 点投影到自己图像上的point
      auto error1 = computerReprojectionError(map_pose.inverse(), K_eigen_map, pt3, pt2, pt_result1);

      pts_result.push_back(pt_result);
      pts_result1.push_back(pt_result1);
      // std::cout << i <<" error is: " << error[0]  << "  " << error[1] << std::endl;
    }

    for (size_t i = 0; i < pts_result1.size(); i++)
    {
      std::cout << i <<" pix error is :" << pts_result1[i].pt - pts1d[i] << std::endl;
    }



    cv::Mat key_image2;
    cv::Mat key_image1, key_image3;
    auto pic22 = cv::imread("/media/zmc/0053-2C75/localization/match14/IMG_20211103_165649.jpg",CV_LOAD_IMAGE_COLOR);
    auto pic11 = cv::imread("/media/zmc/0053-2C75/localization/match14/1635748820.220487600_rear.jpg",CV_LOAD_IMAGE_COLOR);

    cv::drawKeypoints(pic11, pts_result1, key_image3, cv::Scalar(0,255,0));
    for (int i = 0; i < pts1.size(); i++)
    {
      cv::KeyPoint point1 = pts1[i];
      pts_result2.push_back(point1);
    }
    cv::drawKeypoints(key_image3, pts_result2, key_image3, cv::Scalar(0,0,255));
    



    cv::drawKeypoints(pic22, pts2, key_image1, cv::Scalar(255,0,0));
    // cv::imwrite("key_image1.jpg",key_image1);
    // cv::Mat imag3_org = cv::imread("/media/zmc/Teclast_S20/localization_ws/project/colmap-3.5/build/key_image1.jpg", CV_LOAD_IMAGE_COLOR);
    cv::drawKeypoints(key_image1, pts_result, key_image2, cv::Scalar(0,0,255));
    cv::imwrite("key_image2.jpg",key_image2);
    cv::imwrite("org_key_image2.jpg",key_image3);
    // bundleadjust.Solve();
    return 1;
}