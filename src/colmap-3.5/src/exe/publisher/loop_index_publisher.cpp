#include "loop_index_publisher.hpp"


LoopIndexPublisher::LoopIndexPublisher(ros::NodeHandle& nh, const std::string topic_name, const int buff_size)
{
    publisher_ = nh.advertise<COLMAP::PointMatch>(topic_name, buff_size);
}

void LoopIndexPublisher::Publish(const COLMAP::PointMatch& search_indexs)
{
    publisher_.publish(search_indexs);
}   