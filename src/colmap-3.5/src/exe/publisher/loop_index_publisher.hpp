#ifndef LOOP_INDEX_PUBLISHER_
#define LOOP_INDEX_PUBLISHER_

#include "devel/include/COLMAP/PointMatch.h"
#include <nav_msgs/Odometry.h>
#include <ros/ros.h>
class LoopIndexPublisher    
{
public:
    LoopIndexPublisher(ros::NodeHandle& nh, const std::string topic_name, const int buff_size);
    void Publish(const COLMAP::PointMatch& search_indexs);
    bool HasSubscribers() { return publisher_.getNumSubscribers() != 0; }

private:
    ros::Publisher publisher_;
};























#endif