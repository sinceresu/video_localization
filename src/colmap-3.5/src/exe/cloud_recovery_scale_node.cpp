#include <pcl/io/ply_io.h>
#include <pcl/point_types.h>
#include <opencv2/calib3d/calib3d.hpp>

#include "pre_local/colmap_io.hpp"
#include "../base/similarity_transform.h"
#include "tool/util.hpp"

/* 
蓝灯侠滑轨建图，用colmap建立无尺度点云，再根据滑轨的固定位置，进行尺度恢复。
./trans_two_cloud /home/zmc/Data/data/landengxia/huagui/0 /home/zmc/Data/data/landengxia/huagui2/0 vlcsnap-2022-01-04-11h21m58s302.png */

void trans_map(pcl::PointCloud<pcl::PointXYZRGB>& in_cloud, const colmap::SimilarityTransform3& tform)
{
    for(auto& pt : in_cloud)
    {
        auto pt_eigen = Eigen::Vector3d(pt.x, pt.y, pt.z);
        tform.TransformPoint(&pt_eigen);
        pt.x = pt_eigen[0];
        pt.y = pt_eigen[1];
        pt.z = pt_eigen[2];
    }
}

void trans_map(pcl::PointCloud<pcl::PointXYZRGB>& in_cloud, const Eigen::Isometry3d T)
{
    for(auto& pt : in_cloud)
    {
        auto pt_eigen = Eigen::Vector3d(pt.x, pt.y, pt.z);
        pt_eigen = T.linear() * pt_eigen + T.translation();
    
        // tform.TransformPoint(&pt_eigen);
        pt.x = pt_eigen[0];
        pt.y = pt_eigen[1];
        pt.z = pt_eigen[2];
    }
}

int main(int argc, char** argv)
{
    double pix_error = 1.0;
    std::shared_ptr<ColmapFileReader> colmap_file_reader1_ptr_ = std::make_shared<ColmapFileReader>(pix_error);
    // colmap_file_reader1_ptr_->ReadMap(argv[1]);
    colmap_file_reader1_ptr_->ReadTwoMap(argv[1]);
    std::string pose_file_name = argv[2];
    std::unordered_map<std::string, Eigen::Vector3d> pose_map;
    read_pose_image_name(pose_map, pose_file_name.c_str());
    
    
    std::vector<Eigen::Vector3d> src;
    std::vector<Eigen::Vector3d> dst;
    colmap_file_reader1_ptr_->GetSim3Matched(src, dst, pose_map);

    colmap::SimilarityTransform3 tform;
    //tform 表示 src 到 dst 
    tform.Estimate(src, dst);
    std::cout << "sim3 is : " << std::endl << tform.Matrix() << std::endl;
    std::cout << "sim3 scale is : " << std::endl << tform.Scale() << std::endl;

    colmap_file_reader1_ptr_->RecoverImageScale(tform);

    pcl::PointCloud<pcl::PointXYZRGB> input_cloud;
    pcl::io::loadPLYFile(argv[3], input_cloud);

    trans_map(input_cloud, tform);
    pcl::io::savePLYFile("after_trans.ply", input_cloud);
    

}
