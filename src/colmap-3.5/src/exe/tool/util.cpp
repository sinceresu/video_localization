#include <iostream>
#include <fstream>


#include "util.hpp"

bool readImageNames(const std::string& path, std::set<std::string>& image_names)
{
    boost::filesystem::path files_path = path;
    if (!boost::filesystem::exists(files_path))
    {
        return false;
    }
    boost::filesystem::recursive_directory_iterator beg_iter(path);
    boost::filesystem::recursive_directory_iterator end_iter;

    for (; beg_iter != end_iter; beg_iter++)
    {
        std::string file_ext = beg_iter->path().extension().string();
        if(file_ext == ".jpg" || file_ext == ".JPG" || file_ext == ".png")
        {
            image_names.emplace(beg_iter->path().filename().string());
        }
    }
    return true;
}

Eigen::Vector2d computerReprojectionError(const Eigen::Matrix4d& T, const Eigen::Matrix3d& K, const Eigen::Vector3d& pt3, const Eigen::Vector2d& pt2, cv::KeyPoint& pt)
{
    Eigen::Vector4d p_cam_world =  T * pt3.homogeneous();
    Eigen::Vector3d p_came_world_norm = Eigen::Vector3d(p_cam_world[0]/p_cam_world[2], p_cam_world[1]/p_cam_world[2], 1);
    auto p_cam_norm = K * p_came_world_norm;
    pt.pt.x = p_cam_norm[0];
    pt.pt.y = p_cam_norm[1];
    Eigen::Vector2d error_uv = Eigen::Vector2d(p_cam_norm[0] - pt2[0], p_cam_norm[1] - pt2[1]);
    return error_uv;
}

std::vector<cv::Point2f> readMatches(const char* file_path, std::vector<std::pair<uint32_t, uint32_t>>& matches_id, uint32_t& image1_id, uint32_t& image2_id)
{
    std::ifstream in(file_path, std::ios::out);
    if(!in.good())
    {
        std::vector<cv::Point2f> empty;
        std::cout << "file " << file_path << "read fail!!!" << std::endl;
        return empty;
    }

    std::string line;
    int line_num = 1;
    std::vector<cv::Point2f> local_kps;
    while(std::getline(in, line))
    {
        std::istringstream iss(line);
        if (line_num == 1)
        {
            if(!(iss >> image1_id >> image2_id));
            // std::cout << "image 1 id is:  " << image1_id << std::endl;
            // std::cout << "image 2 id is:  " << image2_id << std::endl;
        }else{
            uint32_t pt1_id, pt2_id;
            double x, y;
            cv::Point2f xy;
            Eigen::Matrix4d pose_data;
            if(!(iss >> pt1_id >> x >> y >> pt2_id >> xy.x >> xy.y));
            local_kps.push_back(xy);

            matches_id.push_back(std::make_pair(pt1_id, pt2_id));
            // std::cout << pt1_id << "   " << pt2_id << std::endl;
        }
        line_num++;
    }
    return local_kps;
}

bool readMatchFiles(const std::string& path, std::vector<std::string>& match_names)
{
    boost::filesystem::path files_path = path;
    if (!boost::filesystem::exists(files_path))
    {
        return false;
    }
    boost::filesystem::recursive_directory_iterator beg_iter(path);
    boost::filesystem::recursive_directory_iterator end_iter;

    for (; beg_iter != end_iter; beg_iter++)
    {
        std::string file_ext = beg_iter->path().extension().string();
        if(file_ext == ".txt")
        {
            match_names.push_back(beg_iter->path().filename().string());
        }
    }
    return true;
}

bool readDescripFiles(const std::string& path, const std::string& file_name, std::vector<std::pair<std::string, std::string> >& descrip_image_names)
{
    boost::filesystem::path files_path = path;
    if (!boost::filesystem::exists(files_path))
    {
        return false;
    }
    boost::filesystem::recursive_directory_iterator beg_iter(path);
    boost::filesystem::recursive_directory_iterator end_iter;

    for (; beg_iter != end_iter; beg_iter++)
    {
        std::string file_ext = beg_iter->path().extension().string();
        if(file_ext == ".txt" || file_ext == ".npy")
        {
          continue;
        }
        descrip_image_names.push_back(std::make_pair(beg_iter->path().string() + file_name, beg_iter->path().filename().string()));
    }
    return true;
}



bool readImageNameId(const char* file_path, std::map<uint32_t, std::string>& image_name_id)
{
    std::ifstream in(file_path, std::ios::out);
    if(!in.good())
    {
        std::cout << "readImageNameId fail in:" << file_path << std::endl;
        return false;
    }
    std::string line;
    while(std::getline(in, line))
    {
        std::istringstream iss(line);
        uint32_t image_id;
        std::string image_name;
        if(!(iss >> image_id >> image_name));
        std::cout << "image_id is:  " << image_id << std::endl;
        std::cout << "image_name is:  " << image_name << std::endl;
        image_name_id[(image_id + 1)] = image_name;
    }
}

bool readImageKeypoints(const char* file_path, std::vector<std::pair<float, float>>& keypoints)
{
    std::ifstream in(file_path, std::ios::out);
    if(!in.good())
    {
        std::cout << "readImageKeypoints fail in:" << file_path << std::endl;
        return false;
    }
    std::string line;
    while(std::getline(in, line))
    {
        std::istringstream iss(line);
        int x, y;
        if(!(iss >> x >> y));
        // std::cout << "keypoint x is:  " << x << std::endl;
        // std::cout << "keypoint y is:  " << y << std::endl;
        keypoints.push_back(std::make_pair(x, y));
    }
    return true;
}

bool readFeatureDescriptors(const char* file_path, std::vector<std::vector<double>>& data, bool if_descri)
{   
    std::vector<std::vector<double>> res;
    std::ifstream in(file_path, std::ios::out);
    if(!in.good())
    {
        std::cout << "readFeatureDescriptors fail in:" << file_path << std::endl;
        return false;
    }
    std::string line;
    std::vector<double> tmp_des;
    double s;
    while(std::getline(in, line))
    {
        int line_size = 0;
        std::istringstream iss(line);
        while((iss >> s))
        {
          tmp_des.push_back(s);
          line_size++;
          // std::cout << "line size = " << line_size << std::endl;
        }
        res.push_back(tmp_des);
        tmp_des.clear();
    }
    if (if_descri)
    {
      tmp_des.clear();
      for (int i = 0; i < res[0].size(); i++)
      {
        for(int j = 0; j < res.size(); j++)
        {
          tmp_des.push_back(res[j][i]);
        }
        data.push_back(tmp_des);
        tmp_des.clear();
      }
    }else{
      data = res;
    }
    return true;
}

using namespace cv;
using namespace std;

void pose_estimation_3d3d(const vector<Point3f> &pts1,
                          const vector<Point3f> &pts2,
                          Mat &R, Mat &t) {
  Point3f p1, p2;     // center of mass
  int N = pts1.size();
  for (int i = 0; i < N; i++) {
    p1 += pts1[i];
    p2 += pts2[i];
  }
  p1 = Point3f(Vec3f(p1) / N);
  p2 = Point3f(Vec3f(p2) / N);
  vector<Point3f> q1(N), q2(N); // remove the center
  for (int i = 0; i < N; i++) {
    q1[i] = pts1[i] - p1;
    q2[i] = pts2[i] - p2;
  }

  // compute q1*q2^T
  Eigen::Matrix3d W = Eigen::Matrix3d::Zero();
  for (int i = 0; i < N; i++) {
    W += Eigen::Vector3d(q1[i].x, q1[i].y, q1[i].z) * Eigen::Vector3d(q2[i].x, q2[i].y, q2[i].z).transpose();
  }
  cout << "W=" << W << endl;

  // SVD on W
  Eigen::JacobiSVD<Eigen::Matrix3d> svd(W, Eigen::ComputeFullU | Eigen::ComputeFullV);
  Eigen::Matrix3d U = svd.matrixU();
  Eigen::Matrix3d V = svd.matrixV();

  cout << "U=" << U << endl;
  cout << "V=" << V << endl;

  Eigen::Matrix3d R_ = U * (V.transpose());
  if (R_.determinant() < 0) {
    R_ = -R_;
  }
  Eigen::Vector3d t_ = Eigen::Vector3d(p1.x, p1.y, p1.z) - R_ * Eigen::Vector3d(p2.x, p2.y, p2.z);

  // convert to cv::Mat
  R = (Mat_<double>(3, 3) <<
    R_(0, 0), R_(0, 1), R_(0, 2),
    R_(1, 0), R_(1, 1), R_(1, 2),
    R_(2, 0), R_(2, 1), R_(2, 2)
  );
  t = (Mat_<double>(3, 1) << t_(0, 0), t_(1, 0), t_(2, 0));
}

bool read_pose_image_name(std::unordered_map<std::string, Eigen::Vector3d>& pose_datas, const char* file_path)
{
    std::ifstream in(file_path, std::ios::out);
    if (!in.good())
    {
        return false;
    }
    std::string line, image_name;
    Eigen::Quaterniond Q;
    Eigen::Vector3d t;
    int id, camera_id;
    while(std::getline(in, line))
    {
        std::istringstream iss(line);
        if (!(iss >> id >> Q.w() >> Q.x() >> Q.y() >> Q.z() >> t[0] >> t[1] >> t[2] >> camera_id >> image_name))
        {
            break;
        }
        pose_datas[image_name] = t;
    }
    return true;
}

void save_pose_image_name(const char* file_path, std::string image_name, Eigen::Matrix4d T)
{
    FILE *pFile_3d2d;
    pFile_3d2d = fopen(file_path,"a");

    std::cout << "name is: "<< image_name << std::endl; 
    fprintf (pFile_3d2d, "%s %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n", image_name.c_str(), T(0,0), T(0,1), T(0,2), T(0,3), 
                                                                                                    T(1,0), T(1,1), T(1,2), T(1,3),
                                                                                                    T(2,0), T(2,1), T(2,2), T(2,3),
                                                                                                    T(3,0), T(3,1), T(3,2), T(3,3));
    fprintf(pFile_3d2d, "\n");
    fclose (pFile_3d2d);

}


// template <typename std::string>
bool save_image_name(const int32_t& image_name, const char* file_path)
{
/*     std::ofstream out(file_path, std::ios::out | std::ios::binary);
    out.write((char*)(image_name.data()), sizeof(int));
    out.close(); */
    return true;
}

// template <typename std::string>
bool load_image_name(int& image_name, const char* file_path)
{
/*     std::ifstream in(file_path, std::ios::in | std::ios::binary);
    if(!in.good())
    {
        std::cout <<"can't read messages from " << *file_path << std::endl;
        return false;
    }
    in.read((char*)(image_name.data()), sizeof(int)); */
    return true;
}

double rotation_error(const Eigen::Quaterniond& w2c, const Eigen::Quaterniond& c2w)
{
    auto error_vec = (w2c * c2w).vec();
    return std::abs(error_vec(0) + error_vec(1) + error_vec(2));
}

template <typename T>
void WriteBinaryLittleEndian1(std::ostream* stream, const T& data) {
  const T data_little_endian = NativeToLittleEndian(data);
  stream->write(reinterpret_cast<const char*>(&data_little_endian), sizeof(T));
}

template <typename T>
void WriteBinaryLittleEndian1(std::ostream* stream, const std::vector<T>& data) {
  for (const auto& elem : data) {
    WriteBinaryLittleEndian1<T>(stream, elem);
  }
}

template <typename T>
T NativeToLittleEndian(const T x) {
  if (IsLittleEndian()) {
    return x;
  } else {
    return ReverseBytes(x);
  }
}

inline bool IsLittleEndian() {
#ifdef BOOST_BIG_ENDIAN
  return false;
#else
  return true;
#endif
}

template <typename T>
T ReverseBytes(const T& data) {
  T data_reversed = data;
  std::reverse(reinterpret_cast<char*>(&data_reversed),
               reinterpret_cast<char*>(&data_reversed) + sizeof(T));
  return data_reversed;
}


