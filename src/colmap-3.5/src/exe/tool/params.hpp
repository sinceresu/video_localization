#ifndef COLMAP_PARAMS_
#define COLMAP_PARAMS_

#include <ros/ros.h>
#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>
#include <vector>
#include <Eigen/Core>

struct YdPoint2d
{
    bool has_point_3d;
    cv::Point2f pt;
    cv::Point3f pt3d;
    int id;
};

struct YdFrame
{
    Eigen::Matrix4d T;
    Eigen::Vector3d transpose;
    Eigen::Quaterniond rotation;
    std::vector<YdPoint2d> points;
};



class Params
{
public:
    Params()
    {
        if(!ros::ok())
        {
            return;
        }
        ros::NodeHandle nh;
        nh.param<bool>("undistortion", undistortion_, true);
        nh.param<bool>("save_feature", save_feature_, false);
        // nh.param<int>("undistortion_param_size", undistortion_param_size_, 4);
        std::vector<double> m_intri_matrix;
        std::vector<double> m_distor_matrix;
        nh.param<std::vector<double>>("m_intri_matrix", m_intri_matrix, std::vector<double>());
        nh.param<std::vector<double>>("m_distor_matrix", m_distor_matrix, std::vector<double>());

        // int index = 0;

        projection_matrix_ = ( cv::Mat_<double> ( 3,3 ) << m_intri_matrix[0], m_intri_matrix[1], m_intri_matrix[2], 
                                                       m_intri_matrix[3],m_intri_matrix[4],m_intri_matrix[5],
                                                       m_intri_matrix[6],m_intri_matrix[7],m_intri_matrix[8] );
        distortion_matrix_ = ( cv::Mat_<double> ( 4,1 ) << m_distor_matrix[0], m_distor_matrix[1], m_distor_matrix[2], m_distor_matrix[3]);

        nh.param<int>("loop_xy_kf_num", loop_xy_kf_num_, 10);

        std::vector<double> map_intri_matrix;
        nh.param<std::vector<double>>("map_intri_matrix", map_intri_matrix, std::vector<double>());
        map_intri_matrix_= ( cv::Mat_<double> ( 3,3 ) << map_intri_matrix[0], map_intri_matrix[1], map_intri_matrix[2], 
                                                     map_intri_matrix[3],map_intri_matrix[4],map_intri_matrix[5],
                                                     map_intri_matrix[6],map_intri_matrix[7],map_intri_matrix[8]); 
        nh.param<double>("RansacInitScore", ransac_init_score_, 5.0);
        nh.param<std::string>("publish_feature_cloud_topic", publish_feature_cloud_topic_, "/feature_cloud");
    }




    //订阅的 superglue2d点是否需要去畸变。
    bool undistortion_;
    int undistortion_param_size_;
    cv::Mat projection_matrix_;
    cv::Mat distortion_matrix_;
    //回环中第一次只用xy筛选关键帧的个数
    int loop_xy_kf_num_;
    cv::Mat map_intri_matrix_;
    bool save_feature_;
    double ransac_init_score_;

    std::string publish_feature_cloud_topic_;



};




#endif