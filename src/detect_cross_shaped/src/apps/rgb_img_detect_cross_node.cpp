/*
 * @Description: 
 * @Version: 2.0
 * @Autor: ZhouMingchao
 * @Date: 2021-03-05 22:42:36
 * @LastEditors: ZhouMingchao
 * @LastEditTime: 2021-03-06 21:29:08
 */
#include "detect_cross_shaped/detect_cross/detect_cross_flow.hpp"

int main(int argc, char** argv)
{
    ros::init(argc, argv, "rgb_detec_cross_node");
    ros::NodeHandle nh;
    std::string img_sub_topic, center_pose_pub_topic, yaml_path;
    nh.param<std::string>("img_sub_topic", img_sub_topic, "/imx219_camera/image");
    nh.param<std::string>("center_pose_pub_topic", center_pose_pub_topic, "/center_pose");
    nh.param<std::string>("yaml_path", yaml_path, "/home/ubuntu/workspace/src/detect_cross_shaped/config/rgb_detect_cross.yaml");
    yaml_path = "/home/zmc/practice/findcentor_ws/src/detect_cross_shaped/config/rgb_detect_cross.yaml";
    std::shared_ptr<DetectCrossFlow> detect_cross_flow_ptr = std::make_shared<DetectCrossFlow>(nh, img_sub_topic, center_pose_pub_topic, yaml_path);

    ros::Rate rate(10);
    while (ros::ok()) {
        ros::spinOnce();

        detect_cross_flow_ptr->Run();

        rate.sleep();
    }
    return 0;
}