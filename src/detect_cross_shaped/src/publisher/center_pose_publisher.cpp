/*
 * @Description: 
 * @Version: 2.0
 * @Autor: ZhouMingchao
 * @Date: 2021-03-05 22:42:36
 * @LastEditors: ZhouMingchao
 * @LastEditTime: 2021-03-06 00:05:07
 */
#include "detect_cross_shaped/publisher/center_pose_publisher.hpp"
CenterPosePublisher::CenterPosePublisher(ros::NodeHandle& nh, std::string& topic_name, size_t buff_size)
:nh_(nh)
{
    publisher_= nh_.advertise<geometry_msgs::Pose2D>(topic_name, buff_size);
}

void CenterPosePublisher::PublishData(cv::Point2f& centor_pose)
{
    geometry_msgs::Pose2D geometry_msgs_center_pose;
    geometry_msgs_center_pose.x = centor_pose.x - 640.0;
    geometry_msgs_center_pose.y = centor_pose.y - 360.0;
    publisher_.publish(geometry_msgs_center_pose);
}