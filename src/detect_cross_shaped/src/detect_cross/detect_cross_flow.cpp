/*
 * @Description: 
 * @Version: 2.0
 * @Autor: ZhouMingchao
 * @Date: 2021-03-05 22:42:36
 * @LastEditors: ZhouMingchao
 * @LastEditTime: 2021-03-06 23:02:47
 */
#include "detect_cross_shaped/detect_cross/detect_cross_flow.hpp"

DetectCrossFlow::DetectCrossFlow(ros::NodeHandle& nh, std::string img_sub_topic_name, std::string center_pub_topic_name, std::string yaml_path)
// :yaml_path_(yaml_path)
{
    yaml_path_ = yaml_path;
    img_subscriber_ptr_ = std::make_shared<ImgSubscriber>(nh, img_sub_topic_name, 10000);
    center_pose_publisher_ptr_ = std::make_shared<CenterPosePublisher>(nh, center_pub_topic_name, 10000);
    detect_cross_ptr = std::make_shared<DetectCross>();
    init_finish_ = false;
}

bool DetectCrossFlow::ReadeData()
{
    img_subscriber_ptr_->ParseData(img_ptr_buff_);
    return true;
}

bool DetectCrossFlow::HasData()
{
    return img_ptr_buff_.size() > 0;
}

bool DetectCrossFlow::ValiData()
{
    current_img_ptr_ = img_ptr_buff_.front();
    img_ptr_buff_.pop_front();

    return true;
}

bool DetectCrossFlow::ProcessCross()
{
    if (!init_finish_)
    {
        detect_cross_ptr->InitParam(yaml_path_);
        init_finish_ = true;
    }
    if (detect_cross_ptr->FilterRed(current_img_ptr_))
    {   
        detect_cross_ptr->DetetcLineAndGetCenter(center_);
        std::cout << "center_ is " << center_ << std::endl;
        return true;
    }
    return false;
}

void DetectCrossFlow::PublishData()
{
    center_pose_publisher_ptr_->PublishData(center_);
}


bool DetectCrossFlow::Run()
{
    if (!ReadeData())
        return false;
    while(HasData())
    {
        if(!ValiData())
            continue;
        if(ProcessCross())
        {
            PublishData();
        }
    }
}