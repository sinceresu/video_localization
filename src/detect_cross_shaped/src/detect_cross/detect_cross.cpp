#include "detect_cross_shaped/detect_cross/detec_cross.hpp"

bool DetectCross::Init()
{
    int width = origin_img_.cols;
    int hight = origin_img_.rows;
    cv::Size imageSize(width, hight);
    cv::fisheye::initUndistortRectifyMap(intrinsic_mat_, distortion_coeffs_, cv::Mat(), intrinsic_mat_, imageSize, CV_16SC2, map_x_, map_y_);
    return true;
}

void DetectCross::InitParam(const std::string& param_path)
{
    cv::FileStorage fs;
    fs.open(param_path, cv::FileStorage::READ);
    if (!fs.isOpened())
    {
        std::cout << "param filepath is empty,please check out!" << std::endl;
        return;
    }
    fs["houghLines_5"] >> houghLines_5_;
    fs["houghLines_6"] >> houghLines_6_;
    fs["houghLines_7"] >> houghLines_7_;
    fs["canny_3"] >> canny_3_;
    fs["canny_4"] >> canny_4_;
    fs["canny_5"] >> canny_5_;

    fs["CameraMat"] >> intrinsic_mat_;
    fs["DistCoeff"] >> distortion_coeffs_;
    fs["max_point_distance"] >> max_point_distance_;
    fs["min_cloud_num"] >> min_cloud_num_;
    fs["Brightness_threshold"] >> bright_threshold_;
    fs.release();
}

bool DetectCross::FishUndistor()
{
    remap(origin_img_, Undistor_img_, map_x_, map_y_, cv::INTER_LINEAR);
    return true;
}

bool DetectCross::FilterRed(const cv_bridge::CvImageConstPtr& img_msg_ptr)
{
    std::cout << "============filterRed=======" << std::endl;
    origin_img_ = img_msg_ptr->image;
    filter_red_img_ = cv::Mat::zeros(origin_img_.size(), CV_8UC1);
    // cv::imshow("origin_img_", origin_img_);
    // cv::waitKey();
    int height = origin_img_.rows;
    int width = origin_img_.cols;
    int x, y;
	for (x = 0; x < height; x++)
	{
		for (y = 0; y < width; y++)
		{
            rgb_and_hsv_.red = origin_img_.at<cv::Vec3b>(x, y)[2];
            rgb_and_hsv_.gre = origin_img_.at<cv::Vec3b>(x, y)[1];
            rgb_and_hsv_.blu = origin_img_.at<cv::Vec3b>(x, y)[0];
            rgb_and_hsv_.intensity = 0;
            rgb_and_hsv_.saturation = 0;
            rgb_and_hsv_.hue = 0;
			rgb2Hsv();
            // std::cout << "rgb_and_hsv_.hue is " << rgb_and_hsv_.hue <<std::endl;
			if (( rgb_and_hsv_.hue >= 200 &&  rgb_and_hsv_.hue <= 360) 
                && ( rgb_and_hsv_.saturation >= 0 &&  rgb_and_hsv_.saturation <= 100) 
                && ( rgb_and_hsv_.intensity > 18 &&  rgb_and_hsv_.intensity < 100))
            {
                filter_red_img_.at<uchar>(x, y) = 255;
            }else{
                // std::cout << "rgb_and_hsv_.saturation is " << rgb_and_hsv_.saturation << std::endl;
            }
		}
	}
    std::cout << "0000" << std::endl;
    // cv::imshow("filter_red_img_", filter_red_img_);
    // cv::waitKey();
    return true;
}

void DetectCross::rgb2Hsv()
{
    RgbAndHsv tmp_RH;
    tmp_RH.red =  rgb_and_hsv_.red / 255;
    tmp_RH.gre =  rgb_and_hsv_.gre / 255;
    tmp_RH.blu =  rgb_and_hsv_.blu / 255;

    double sum;
	double minRGB, maxRGB;
	double theta;

	minRGB = ((tmp_RH.red < tmp_RH.gre) ? (tmp_RH.red) : (tmp_RH.gre));
	minRGB = (minRGB < tmp_RH.blu) ? (minRGB) : (tmp_RH.blu);
 
	maxRGB = ((tmp_RH.red > tmp_RH.gre) ? (tmp_RH.red ) : (tmp_RH.gre));
	maxRGB = (maxRGB>tmp_RH.blu) ? (maxRGB) : (tmp_RH.blu);
 
	sum = tmp_RH.red + tmp_RH.gre+ tmp_RH.blu;
	tmp_RH.intensity  = sum / 3.0;
 
	if (tmp_RH.intensity < 0.001 || maxRGB - minRGB<0.001)
	{
		tmp_RH.hue = 0.0;
		tmp_RH.saturation = 0.0;
	}
	else
	{
		tmp_RH.saturation = 1.0 - 3.0*minRGB / sum;
		theta = sqrt((tmp_RH.red - tmp_RH.gre)*(tmp_RH.red - tmp_RH.gre) + (tmp_RH.red - tmp_RH.blu)*(tmp_RH.gre- tmp_RH.blu));
		theta = acos((tmp_RH.red - tmp_RH.gre+ tmp_RH.red - tmp_RH.blu)*0.5 / theta);
		if (tmp_RH.blu <= tmp_RH.gre)
			tmp_RH.hue = theta;
		else
			tmp_RH.hue = 2 * M_PI - theta;
		if (tmp_RH.saturation <= 0.01)
			tmp_RH.hue = 0;
	}
	 rgb_and_hsv_.hue = (int)(tmp_RH.hue * 180 / M_PI);
	 rgb_and_hsv_.saturation = (int)(tmp_RH.saturation * 100);
	 rgb_and_hsv_.intensity = (int)(tmp_RH.intensity  * 100);
}

cv::Point2f DetectCross::GetCrossPoint(cv::Vec4i LineA, cv::Vec4i LineB)
{
    double ka, kb;
    ka = (double)(LineA[3] - LineA[1]) / (double)(LineA[2] - LineA[0]); 
    kb = (double)(LineB[3] - LineB[1]) / (double)(LineB[2] - LineB[0]); 

    cv::Point2f crossPoint;
    crossPoint.x = (ka*LineA[0] - LineA[1] - kb*LineB[0] + LineB[1]) / (ka - kb);
    crossPoint.y = (ka*kb*(LineA[0] - LineB[0]) + ka*LineB[1] - kb*LineA[1]) / (ka - kb);
    if ((crossPoint.x > LineA[0] &&  crossPoint.x > LineA[2]) 
     || (crossPoint.x < LineA[0] &&  crossPoint.x < LineA[2])
     || (crossPoint.y > LineA[1] &&  crossPoint.y > LineA[3])
     || (crossPoint.y < LineA[1] &&  crossPoint.y < LineA[3]))
     {
        return cv::Point2f(-1, -1);
     }else{
        return crossPoint;
     }
}

float DetectCross::ComputerPonitDistance(const cv::Point2f& point1, const cv::Point2f& point2)
{
    float distance;
    distance = sqrtf(powf((point1.x - point2.x), 2) + powf((point1.y - point2.y), 2));
    return distance;
}

bool DetectCross::FilterPoints(const std::vector<cv::Point2f>& centers)
{
     filter_centres_.clear();
    int point_size = centers.size();
    if (point_size == 0 || point_size == 1) return false;
    if (point_size == 2)
    {
        std::cout << " 2 " << std::endl;
        if (ComputerPonitDistance(centers[0], centers[1]) < max_point_distance_)
        {
             filter_centres_ = centers;
            return true;
        }else{
            return false;
        }
    }
    bool find_cloud_center = false;
    cv::Point2f cloud_middle;
    for (int i = 0; i < point_size; i++)
    {
        if (!find_cloud_center)
        {
            cv::Point2f tmp_center = centers[i];
            int cloud_num = 0;
            for (int j = 0; j < point_size; j++)
            {
                if (ComputerPonitDistance(tmp_center, centers[j]) < max_point_distance_)
                {
                    cloud_num++;
                }
                if (cloud_num >= min_cloud_num_)
                {
                    find_cloud_center = true;
                    cloud_middle = tmp_center;
                    break;
                }
            }
        }else{
            break;
        }
    }
    for (int k = 0; k < point_size; k++)
    {
        if (ComputerPonitDistance(cloud_middle, centers[k]) < max_point_distance_)
        {
             filter_centres_.push_back(centers[k]);
        }
    }
    if( filter_centres_.size() > 0)
    {
        return true;
    }else{
        std::cout << "filter center is empty!" << std::endl;
        return false;
    }
}

cv::Point2f DetectCross::ComputeMidllePoint(const std::vector<cv::Point2f> filter_centres)
{
    float sum_x = 0;
    float sum_y = 0;
    int point_size = filter_centres.size();
    for(size_t i = 0; i < point_size; i++)
    {
        sum_x += filter_centres[i].x;
        sum_y += filter_centres[i].y;
    }
    return cv::Point2f(sum_x / point_size, sum_y / point_size);
}

void DetectCross::DetetcLineAndGetCenter(cv::Point2f& center)
{
    cv::Mat srcImage = filter_red_img_.clone();
    cv::Mat midImage, th_srcImage;
    if (!srcImage.empty())
    {
        th_srcImage = srcImage > bright_threshold_; 
        cv::Canny(th_srcImage, midImage, canny_3_, canny_4_, canny_5_);
        // cv::imshow("midImage", midImage);
        // cv::waitKey();
        std::vector<cv::Vec4i> lines;
        cv::HoughLinesP(midImage, lines, 1, CV_PI / 180, houghLines_5_, houghLines_6_, houghLines_7_);
        if(lines.size() >= 2)
        {
            for (size_t i = 0; i < lines.size(); i++)
            {
                cv::Vec4i l = lines[i];
                line(srcImage, cv::Point(l[0], l[1]), cv::Point(l[2], l[3]), cv::Scalar(0, 0, 255), 1, cv::LINE_AA);
            }
            std::vector<cv::Point2f> corners; 
            for (unsigned int i = 0; i<lines.size(); i++)
            {
                for (unsigned int j = i + 1; j<lines.size(); j++)
                {
                    cv::Point2f pt = GetCrossPoint(lines[i], lines[j]);
                    if (pt.x >= 0 && pt.y >= 0)
                    {
                        corners.push_back(pt);
                    }
                }
            }
            if (!corners.empty())
            {
                if(FilterPoints(corners))
                {
                    center = ComputeMidllePoint(filter_centres_);
                    std::cout << "======outputCenter===center is " << center << std::endl;
                    // circle(_line_cross_img, _center, (20 + 5), cv::Scalar(255, 255, 0), 2);
                }else{
                    center = cv::Point2f(640.0, 360.0);
                }
            }
        }
    }else{
        std::cout << "line detec wrong image name !!! " << std::endl; 
    }
    // line_cross_img = midImage;
}
