/*
 * @Description: 
 * @Version: 2.0
 * @Autor: ZhouMingchao
 * @Date: 2021-03-05 22:42:36
 * @LastEditors: ZhouMingchao
 * @LastEditTime: 2021-03-06 21:57:58
 */
#include <detect_cross_shaped/subscriber/img_subscriber.hpp>

ImgSubscriber::ImgSubscriber(ros::NodeHandle& nh, std::string topic_name, size_t buff_size)
: nh_(nh)
{
    subscriber_ = nh_.subscribe(topic_name, buff_size, &ImgSubscriber::msg_callback, this);
}

void ImgSubscriber::msg_callback(const sensor_msgs::ImageConstPtr &img_msg)
{
    buff_mutex_.lock();
    cv_bridge::CvImageConstPtr ptr;
    if (img_msg->encoding == "8UC1")
    {
        sensor_msgs::Image img;
        img.header = img_msg->header;
        img.height = img_msg->height;
        img.width = img_msg->width;
        img.is_bigendian = img_msg->is_bigendian;
        img.step = img_msg->step;
        img.data = img_msg->data;
        img.encoding = "mono8";
        ptr = cv_bridge::toCvCopy(img, sensor_msgs::image_encodings::MONO8);
    }
    else{
        ptr = cv_bridge::toCvCopy(img_msg, sensor_msgs::image_encodings::BGR8);
    }
    new_img_data_.push_back(ptr);
    std::cout << "get a pic ptr" << std::endl;
    buff_mutex_.unlock();
}

void ImgSubscriber::ParseData(std::deque<cv_bridge::CvImageConstPtr>& img_data_buff)
{
    buff_mutex_.lock();
    if(new_img_data_.size() > 0)
    {
        std::cout << "new_img_data_ is not empty" << std::endl;
        img_data_buff.insert(img_data_buff.end(), new_img_data_.begin(), new_img_data_.end());
        new_img_data_.clear();
        std::cout <<"!!!!! img_data_buff size is "<< img_data_buff.size() << std::endl;
    }
    buff_mutex_.unlock();
}