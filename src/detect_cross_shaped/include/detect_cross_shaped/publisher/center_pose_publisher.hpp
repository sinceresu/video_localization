/*
 * @Description: 
 * @Version: 2.0
 * @Autor: ZhouMingchao
 * @Date: 2021-03-05 22:42:36
 * @LastEditors: ZhouMingchao
 * @LastEditTime: 2021-03-06 00:02:27
 */
#ifndef DETECT_CROSS_SHAPED_CENTER_POSE_PUBLISHER_
#define DETECT_CROSS_SHAPED_CENTER_POSE_PUBLISHER_

#include <opencv2/opencv.hpp>
#include <ros/ros.h>
#include <geometry_msgs/Pose2D.h>

class CenterPosePublisher
{
  public:
    CenterPosePublisher(ros::NodeHandle& nh, std::string& topic_name, size_t buff_size);
    // CenterPosePublisher(ros::NodeHandle& nh, std::string& topic_name, size_t buff_size);
    CenterPosePublisher() = default;
    void PublishData(cv::Point2f& centor_pose);
  
  private:
    ros::NodeHandle nh_;
    ros::Publisher publisher_;
};



#endif