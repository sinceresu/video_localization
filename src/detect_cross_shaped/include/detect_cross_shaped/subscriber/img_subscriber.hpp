#ifndef DETECT_CROSS_SHAPED_IMG_SUBSCRIBER_
#define DETECT_CROSS_SHAPED_IMG_SUBSCRIBER_

#include <ros/ros.h>
#include <deque>
#include <mutex>
#include <thread>
#include <cv_bridge/cv_bridge.h>


class ImgSubscriber
{
  public:
    ImgSubscriber() = default;
    ImgSubscriber(ros::NodeHandle& nh, std::string topic_name, size_t buff_size);
    void ParseData(std::deque<cv_bridge::CvImageConstPtr>& img_data_buff);
  private:
    void msg_callback(const sensor_msgs::ImageConstPtr &img_msg);

  private:
    ros::NodeHandle nh_;
    ros::Subscriber subscriber_;

    std::deque<cv_bridge::CvImageConstPtr> new_img_data_;
    std::mutex buff_mutex_;
};




#endif