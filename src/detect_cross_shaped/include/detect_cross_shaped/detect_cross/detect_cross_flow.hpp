/*
 * @Description: 
 * @Version: 2.0
 * @Autor: ZhouMingchao
 * @Date: 2021-03-05 22:42:36
 * @LastEditors: ZhouMingchao
 * @LastEditTime: 2021-03-05 23:55:14
 */
#ifndef DETECT_CROSS_SHAPED_DETECT_CROSS_FLOW_HPP_
#define DETECT_CROSS_SHAPED_DETECT_CROSS_FLOW_HPP_


#include "detect_cross_shaped/subscriber/img_subscriber.hpp"
#include "detect_cross_shaped/publisher/center_pose_publisher.hpp"
#include "detect_cross_shaped/detect_cross/detec_cross.hpp"

class DetectCrossFlow
{
  public:
    DetectCrossFlow(ros::NodeHandle& nh, std::string img_sub_topic_name, std::string center_pub_topic_name, std::string yaml_path);
    DetectCrossFlow() = default;
    bool Run();
    bool ReadeData();
    bool HasData();
    bool ValiData();
    bool ProcessCross();
    void PublishData();
  
  private:
    std::deque<cv_bridge::CvImageConstPtr> img_ptr_buff_;
    cv_bridge::CvImageConstPtr current_img_ptr_;
    std::shared_ptr<DetectCross> detect_cross_ptr;
    std::shared_ptr<ImgSubscriber> img_subscriber_ptr_;
    std::shared_ptr<CenterPosePublisher> center_pose_publisher_ptr_;
    cv::Point2f center_;
    std::string yaml_path_;
    bool init_finish_;
};

#endif