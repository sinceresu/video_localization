/*
 * @Description: 
 * @Version: 2.0
 * @Autor: ZhouMingchao
 * @Date: 2021-03-05 22:42:36
 * @LastEditors: ZhouMingchao
 * @LastEditTime: 2021-03-06 22:56:43
 */
#ifndef DETECT_CROSS_SHAPED_DETECT_CROSS_HPP_
#define DETECT_CROSS_SHAPED_DETECT_CROSS_HPP_

#include <opencv2/opencv.hpp>
#include <cv_bridge/cv_bridge.h>
// #include "detect_cross_shaped/subscriber/img_subscriber.hpp"

struct RgbAndHsv
{
    double red;
    double gre;
    double blu;
    double hue;
    double saturation;
    double intensity;
};

class DetectCross
{
  public:
    DetectCross() = default;
    bool Init();
    void InitParam(const std::string& param_path);
    bool FishUndistor();
    void rgb2Hsv();
    bool FilterRed(const cv_bridge::CvImageConstPtr& img_msg_ptr);
    cv::Point2f GetCrossPoint(cv::Vec4i LineA, cv::Vec4i LineB);
    bool FilterPoints(const std::vector<cv::Point2f>& centers);
    cv::Point2f ComputeMidllePoint(const std::vector<cv::Point2f> filter_centres);
    float ComputerPonitDistance(const cv::Point2f& point1, const cv::Point2f& point2);
    void DetetcLineAndGetCenter(cv::Point2f& center);
    // void FilterPoints();
    
    
    
  private:
    cv::Mat origin_img_;
    cv::Mat Undistor_img_;
    cv::Mat intrinsic_mat_;
    cv::Mat distortion_coeffs_;
    cv::Mat map_x_, map_y_;
    cv::Mat filter_red_img_;
    RgbAndHsv rgb_and_hsv_;
    int houghLines_5_;
    int houghLines_6_;
    int houghLines_7_;
    int canny_3_;
    int canny_4_;
    int canny_5_;
    float max_point_distance_;
    int min_cloud_num_;
    int bright_threshold_;
    std::vector<cv::Point2f> filter_centres_;
};

#endif