#include <Eigen/Core>
#include <ros/package.h>
#include <cv_bridge/cv_bridge.h>
// #include <tf_conversions/tf_eigen.h>
// #include <eigen_conversions/eigen_msg.h>

#include <pcl_conversions/pcl_conversions.h> 
#include <sensor_msgs/PointCloud2.h>
#include <pcl/point_types.h> 
#include <pcl/io/pcd_io.h> 
#include <pcl/filters/frustum_culling.h>
#include <pcl/filters/passthrough.h>

#include "vis_angle_show/reprojection/reprojection.hpp"
#include "vis_angle_show/subscriber/pose_subscriber.hpp"
#include "vis_angle_show/subscriber/image_subscriber.hpp"
#include "vis_angle_show/publisher/cloud_publisher.hpp"
#include "vis_angle_show/publisher/image_publisher.hpp"
#include "vis_angle_show/publisher/publisher.hpp"

using namespace vis_angle_show;
using namespace cv;
std::shared_ptr<Subscriber<nav_msgs::Odometry>> pose_subscriber_ptr;
std::shared_ptr<Subscriber<nav_msgs::Odometry>> image_pose_subscriber_ptr;
// std::shared_ptr<PoseSubscriber> pose_subscriber_ptr;
// std::shared_ptr<PoseSubscriber> image_pose_subscriber_ptr;
// std::shared_ptr<ImgSubscriber> image_subscriber_ptr;




std::shared_ptr<Subscriber<sensor_msgs::CompressedImage::ConstPtr> > image_subscriber_ptr;
std::deque<sensor_msgs::CompressedImage::ConstPtr> deque_image_data;
// std::shared_ptr<Subscriber<sensor_msgs::Image::ConstPtr> > image_subscriber_ptr;
// std::deque<sensor_msgs::Image::ConstPtr> deque_image_data;

std::shared_ptr<ImgPublisher<sensor_msgs::Image, sensor_msgs::Image::ConstPtr>> image_publisher_ptr;
std::shared_ptr<CloudPublisher<sensor_msgs::PointCloud2, sensor_msgs::PointCloud2>> cloud_publisher_ptr;
std::shared_ptr<CloudPublisher<sensor_msgs::PointCloud2, sensor_msgs::PointCloud2>> map_cloud_publisher_ptr;

// std::shared_ptr<CloudPublisher> cloud_publisher_ptr;
// std::shared_ptr<CloudPublisher> map_cloud_publisher_ptr;

std::shared_ptr<Reprojection> reprojection_ptr_;

std::deque<nav_msgs::Odometry> deque_data;
std::deque<nav_msgs::Odometry> deque_image_pose_data;
pcl::PointCloud<pcl::PointXYZ>::Ptr feature_map_cloud_ptr;
// pcl::PointCloud<pcl::PointXYZI>::Ptr 

// Eigen::Matrix4f imu_2_measure_T;


void cut_cloud(const Eigen::Matrix4f& pose, pcl::PointCloud<pcl::PointXYZ>& cut_pc)
{
    pcl::FrustumCulling<pcl::PointXYZ> fc;
    float VerticalFov = 80;
    float HorizontalFov = 60;
    float NearPlanDistance = 0.0;
    float FarPlaneDistance = 16.0;
    fc.setInputCloud(feature_map_cloud_ptr);
    fc.setVerticalFOV(VerticalFov);
    fc.setHorizontalFOV(HorizontalFov);
    fc.setNearPlaneDistance(NearPlanDistance);
    fc.setFarPlaneDistance(FarPlaneDistance);
    Eigen::Matrix4f cam2robot;
    cam2robot << 0, 0, 1, 0,
                 0,-1, 0, 0,
                 1, 0, 0, 0,
                 0, 0, 0, 1;
    
    fc.setCameraPose(pose * cam2robot);
    fc.filter(cut_pc);
    // pcl::io::savePCDFile("cut_cloud_with_index.pcd", cut_pc);
}

void odomMsgToEigen(const nav_msgs::Odometry& odom_pose, Eigen::Matrix4f& result_pose)
{
    result_pose.setIdentity();
    result_pose(0, 3) = odom_pose.pose.pose.position.x;
    result_pose(1, 3) = odom_pose.pose.pose.position.y;
    result_pose(2, 3) = odom_pose.pose.pose.position.z;
    Eigen::Quaterniond Q = Eigen::Quaterniond(odom_pose.pose.pose.orientation.w,
                                              odom_pose.pose.pose.orientation.x,
                                              odom_pose.pose.pose.orientation.y,
                                              odom_pose.pose.pose.orientation.z);
    result_pose.block<3,3>(0,0) = Q.matrix().cast<float>();
}

void imageRotationAngle(cv::Mat& input, cv::Mat& output, const float angle)
{
    std::cout << "input " << input.cols << " " << input.rows << std::endl;
    float radian = (float) (angle /180.0 * CV_PI);

    int maxBorder =(int) (max(input.cols, input.rows)* 1.414 ); //即为sqrt(2)*max

    int dx = (maxBorder - input.cols)/2;
    int dy = (maxBorder - input.rows)/2;
    copyMakeBorder(input, output, dy, dy, dx, dx, BORDER_CONSTANT);
 
    //旋转
    Point2f center( (float)(output.cols/2) , (float) (output.rows/2));
    Mat affine_matrix = getRotationMatrix2D( center, angle, 1.0 );//求得旋转矩阵
    warpAffine(output, output, affine_matrix, output.size());
 
    //计算图像旋转之后包含图像的最大的矩形
    float sinVal = abs(sin(radian));
    float cosVal = abs(cos(radian));
    // Size targetSize( (int)(input.cols * cosVal +input.rows * sinVal),
    //                  (int)(input.cols * sinVal + input.rows * cosVal) );
    Size targetSize((int)(input.rows),
                     (int)(input.cols) );
 
    //剪掉多余边框
    int x = (output.cols - targetSize.width) / 2;
    int y = (output.rows - targetSize.height) / 2;
    Rect rect(x, y, targetSize.width, targetSize.height);
    output = Mat(output,rect);
}

bool run()
{
    nav_msgs::Odometry curent_odom;
    nav_msgs::Odometry curent_image_odom;
    // sensor_msgs::ImageConstPtr current_image;
    sensor_msgs::CompressedImageConstPtr current_image;
    pose_subscriber_ptr->ParseData(deque_data);
    image_pose_subscriber_ptr->ParseData(deque_image_pose_data);
    image_subscriber_ptr->ParseData(deque_image_data);

    if(deque_image_data.size() > 0 && deque_image_pose_data.size() > 0)
    {
        while(deque_image_pose_data.front().header.stamp < deque_image_data.front()->header.stamp)
        {
            LOG(INFO) << "pose time is early " << deque_image_pose_data.front().header.stamp <<"  " << deque_image_data.front()->header.stamp;
            deque_image_pose_data.pop_front();
            if(deque_image_pose_data.empty())
            {
                return false;
            }

        }
        while(std::abs((deque_image_pose_data.front().header.stamp - deque_image_data.front()->header.stamp).toSec()) > 1e-3)
        {
            LOG(INFO) << "image time is early";
            LOG(INFO) << "pose time is early " << deque_image_pose_data.front().header.stamp <<"  " << deque_image_data.front()->header.stamp;
            LOG(INFO) << "error " << std::abs((deque_image_pose_data.front().header.stamp - deque_image_data.front()->header.stamp).toSec());
            deque_image_data.pop_front();
            if(deque_image_data.empty())
            {
                return false;
            }

        }
        LOG(INFO) << "image  pose time is same";

        current_image = deque_image_data.front();
        curent_image_odom = deque_image_pose_data.front();

        deque_image_data.pop_front();
        deque_image_pose_data.pop_front();
        cv_bridge::CvImagePtr cv_ptr;
        try
        {
            cv_ptr = cv_bridge::toCvCopy(current_image, sensor_msgs::image_encodings::BGR8);
        }
        catch (cv_bridge::Exception& e)
        {
            ROS_ERROR("cv_bridge exception: %s", e.what());
            return false;
        }
        Eigen::Matrix4f image_pose;

        odomMsgToEigen(curent_image_odom, image_pose);
        // image_pose = image_pose * imu_2_measure_T.inverse();
        // std::cout << "image pose = " << std::endl << image_pose << std::endl;
    
        std::vector<cv::KeyPoint> kps;
        kps = reprojection_ptr_->GetPixUV(image_pose.inverse().cast<double>());

        cv::drawKeypoints(cv_ptr->image, kps, cv_ptr->image, cv::Scalar(255,255,255));

        int i = 0;
        std::vector<std::string> blocks_name;
        blocks_name.push_back("FU");
        blocks_name.push_back("XFS");
        blocks_name.push_back("XF");
        blocks_name.push_back("MF");
        blocks_name.push_back("LL");
        blocks_name.push_back("SHY");
        cv::Mat roatated_image;
        for(auto& kp : kps)
        {
            cv::Point2f pt;
            pt.x = kp.pt.x;
            pt.y = kp.pt.y;
            if (pt.x > (cv_ptr->image.cols * 1) || pt.y > (cv_ptr->image.rows * 2))
	   {
		i++;		
		continue;
           }
            cv::putText(cv_ptr->image, blocks_name[i], pt, cv::FONT_HERSHEY_SIMPLEX,1.0, CV_RGB(100,100,255),3.5);
            i++;
        }
        image_publisher_ptr->Publish(cv_ptr->toImageMsg());
    }else{
        // LOG(INFO) << deque_image_data.size() << "  " << deque_image_pose_data.size();
    }


    if (deque_data.size() > 0)
    {
        curent_odom = deque_data.front();
        deque_data.pop_front();
        Eigen::Matrix4f eigen_pose;
        odomMsgToEigen(curent_odom, eigen_pose);

        pcl::PointCloud<pcl::PointXYZ> pub_cloud;
        cut_cloud(eigen_pose, pub_cloud);
        if (pub_cloud.points.size() > 0)
        {
            sensor_msgs::PointCloud2 pub_vis_angle_cloud;

            pcl::toROSMsg(pub_cloud, pub_vis_angle_cloud);
            pub_vis_angle_cloud.header.frame_id = "map";
            cloud_publisher_ptr->Publish(pub_vis_angle_cloud);
            std::cout <<"pub cloud data !!!" << std::endl;
        }
        return true;

    }else{
        // std::cout <<"pose data is empty !!!" << std::endl;
        return false; 
    }


}

int main (int argc, char** argv)
{
    ros::init(argc, argv, "vis_angle_show_node");
    ros::NodeHandle nh;

    google::InitGoogleLogging(argv[0]);
    FLAGS_log_dir = "/home/zmc/Data/data/LOG";
    FLAGS_logtostderr = 1;
    FLAGS_alsologtostderr = 1;

    std::vector<double> m_intri_matrix;
    std::vector<double> m_distor_matrix;
    nh.param<std::vector<double>>("intri_matrix", m_intri_matrix, std::vector<double>());
    nh.param<std::vector<double>>("distor_matrix", m_distor_matrix, std::vector<double>());

    // int index = 0;

    cv::Mat projection_matrix = ( cv::Mat_<double> ( 3,3 ) << m_intri_matrix[0], m_intri_matrix[1], m_intri_matrix[2], 
                                                    m_intri_matrix[3],m_intri_matrix[4],m_intri_matrix[5],
                                                    m_intri_matrix[6],m_intri_matrix[7],m_intri_matrix[8] );
    cv::Mat distortion_matrix = ( cv::Mat_<double> ( 4,1 ) << m_distor_matrix[0], m_distor_matrix[1], m_distor_matrix[2], m_distor_matrix[3]);

    // std::vector<double> imu_2_measure_sensor_R, imu_2_measure_sensor_t;
    // nh.param<std::vector<double>>("imu_2_measure_sensor_R", imu_2_measure_sensor_R, std::vector<double>());
    // nh.param<std::vector<double>>("imu_2_measure_sensor_t", imu_2_measure_sensor_t, std::vector<double>());
    // Eigen::Matrix3d imu_2_measure_sensor_R_eigen;
    // Eigen::Vector3d imu_2_measure_sensor_t_eigen;
    // imu_2_measure_sensor_R_eigen << imu_2_measure_sensor_R[0], imu_2_measure_sensor_R[1], imu_2_measure_sensor_R[2],
    //                                 imu_2_measure_sensor_R[3], imu_2_measure_sensor_R[4], imu_2_measure_sensor_R[5],
    //                                 imu_2_measure_sensor_R[6], imu_2_measure_sensor_R[7], imu_2_measure_sensor_R[8];
    // imu_2_measure_sensor_t_eigen << imu_2_measure_sensor_t[0], imu_2_measure_sensor_t[1], imu_2_measure_sensor_t[2];
    // imu_2_measure_T.setIdentity();
    // imu_2_measure_T.block<3,3>(0, 0) = imu_2_measure_sensor_R_eigen.cast<float>();
    // imu_2_measure_T.block<3,1>(0, 3) = imu_2_measure_sensor_t_eigen.cast<float>();

    std::string sub_pose_topic, pub_cut_cloud_topic, pub_global_cloud_topic, sub_image_topic, pub_image_topic;
    nh.param<std::string>("sub_pose_topic", sub_pose_topic, "/orb_slam3/pose");
    nh.param<std::string>("sub_image_topic", sub_image_topic, "/camera/fisheye2/image_raw/compressed");
    nh.param<std::string>("pub_cut_cloud_topic", pub_cut_cloud_topic, "/vis_angle_clouds");
    nh.param<std::string>("pub_global_cloud_topic", pub_global_cloud_topic, "/global_map");
    nh.param<std::string>("pub_image_topic", pub_image_topic, "/block_image");
    pose_subscriber_ptr = std::make_shared<Subscriber<nav_msgs::Odometry>>(nh, sub_pose_topic, 1000);
    image_pose_subscriber_ptr = std::make_shared<Subscriber<nav_msgs::Odometry>>(nh, sub_pose_topic, 1000);
    cloud_publisher_ptr =  std::make_shared<CloudPublisher<sensor_msgs::PointCloud2, sensor_msgs::PointCloud2>>(nh, pub_cut_cloud_topic, 100);
    map_cloud_publisher_ptr = std::make_shared<CloudPublisher<sensor_msgs::PointCloud2, sensor_msgs::PointCloud2>>(nh, pub_global_cloud_topic, 100);
    // image_subscriber_ptr = std::make_shared<ImgSubscriber>(nh, sub_image_topic, 100);

    // image_subscriber_ptr = std::make_shared<Subscriber<sensor_msgs::Image::ConstPtr>>(nh, sub_image_topic, 100);
    image_subscriber_ptr = std::make_shared<Subscriber<sensor_msgs::CompressedImageConstPtr>>(nh, sub_image_topic, 100);
    // image_subscriber_ptr = std::make_shared<Subscriber<sensor_msgs::CompressedImage::ConstPtr>>(nh, sub_image_topic, 100);

    // auto image_pub = ImgPublisher(nh, pub_image_topic, 100);
    image_publisher_ptr = std::make_shared<ImgPublisher<sensor_msgs::Image, sensor_msgs::Image::ConstPtr>>(nh, pub_image_topic, 100);
    
    std::vector<Eigen::Vector3d> blocks;
    int block_size;
    nh.param<int>("block_size", block_size, 2);
    for (int i = 0; i < block_size; i++)
    {
        Eigen::Vector3d block;
        std::vector<double> read_block;
        nh.param<std::vector<double>>("block_" + std::to_string(i), read_block, std::vector<double>());
        if(!read_block.data())
        {
            std::cout << "block_" + std::to_string(i) << " did't exist ！！！" << std::endl;
        }
        block[0] = read_block[0];
        block[1] = read_block[1];
        block[2] = read_block[2];
        blocks.push_back(block);
    }

    reprojection_ptr_ = std::make_shared<Reprojection>(projection_matrix, blocks, distortion_matrix);
    std::string WORKSPACE = ros::package::getPath("vis_angle_show");
    
    //加载特征点地图
    std::string feature_map_name, map_name;
    nh.param<std::string>("feature_map", feature_map_name, "cloud_with_index.pcd");
    nh.param<std::string>("map", map_name, "map.pcd");
    std::string feature_map_path = WORKSPACE + "/config/map/" + feature_map_name;
    
    pcl::PointCloud<pcl::PointXYZ> cloud_tmp;
    pcl::io::loadPCDFile(feature_map_path, cloud_tmp);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_feature(new pcl::PointCloud<pcl::PointXYZ>(cloud_tmp));
    feature_map_cloud_ptr = cloud_feature;
    //加载彩色点云地图
    std::string global_map_path = WORKSPACE + "/config/map/" + map_name;

    pcl::PointCloud<pcl::PointXYZRGB> map_cloud;
    pcl::io::loadPCDFile(global_map_path, map_cloud);
    sensor_msgs::PointCloud2 pub_global_cloud;
    pcl::toROSMsg(map_cloud, pub_global_cloud);

    ros::Rate rata(10);
    while(ros::ok())
    {
        ros::spinOnce();
        if (run())
        {
            pub_global_cloud.header.frame_id = "map";
            map_cloud_publisher_ptr->Publish(pub_global_cloud);
        }
        rata.sleep();
    }
    return 1;
}
