# vis_angle_show (block展示)
提供视觉反定位可视化功能


##订阅话题
```json
/camera/fisheye2/image_raw/compressed [sensor_msgs::Image::ConstPtr] #用于画block的原图
/camera_pose [nav_msgs::Odometry] #相机的位姿
```

##发布话题
```json
/global_map [sensor_msgs::PointCloud2] #整个点云地图
/block_image [sensor_msgs::Image::ConstPtr] #带有block的图像
```

##参数
|名称|类型|描述|缺省
|--------------------- |-----------|----------------------|------------------|
intri_matrix|double|执行solvePnPRansac中的reprojectionError值|
distor_matrix|int|搜索最近关键帧时，先用距离找到最近的帧数|在最近距离的几帧中再找到姿态角最相似的一帧|
block_|数组|相机内参|
m_distor_matrix|数组|相机畸变参数|
pub_feature|bool|是否 pub feature点云|

## 启动
```
roslaunch vis_angle_show vis_angle_show.launch
```