#ifndef VIS_ANGLE_SHOW_REPROJECTION_HPP_
#define VIS_ANGLE_SHOW_REPROJECTION_HPP_

#include <Eigen/Core>
#include <Eigen/Dense>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/core/core.hpp> 
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/eigen.hpp>
#include <opencv2/opencv.hpp>
#include <glog/logging.h>
#include <vector>



namespace vis_angle_show
{
class Reprojection
{
public:
    Reprojection() = default;
    Reprojection(const cv::Mat& K, const std::vector<Eigen::Vector3d>& blocks, const cv::Mat& distortion);
    std::vector<cv::KeyPoint> GetPixUV(const Eigen::Matrix4d& image_pose);

private:
    cv::KeyPoint WorldToPix(const Eigen::Vector3d& world3d, const Eigen::Matrix3d& K, const Eigen::Matrix4d& pose);



private:
    cv::Mat K_;
    std::vector<Eigen::Vector3d> blocks_;
    cv::Mat distortion_;


};




}



















#endif