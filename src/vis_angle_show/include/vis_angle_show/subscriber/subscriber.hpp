#ifndef SUBSCRIBER_HPP_
#define SUBSCRIBER_HPP_
#include <ros/ros.h>
#include <deque>
#include <mutex>

namespace vis_angle_show{

template <typename T>
class Subscriber{
public:
    Subscriber(ros::NodeHandle &nh, const std::string topic_name, int buff_size = 1000)
    :nh_(nh)
    {
        subscriber_ = nh_.subscribe(topic_name, buff_size, &Subscriber::msg_callback, this);

    }
    void msg_callback(const T msgs)
    {
        buff_mutex_.lock();
        data.push_back(msgs);
        buff_mutex_.unlock();
    }
    void ParseData(std::deque<T> &deuqe_msgs)
    {
        buff_mutex_.lock();
        if (data.size() > 0)
        {
            deuqe_msgs.insert(deuqe_msgs.end(), data.begin(), data.end());
            data.clear();
        }
        buff_mutex_.unlock();
    }
    // virtual int type();
private:
    ros::NodeHandle nh_;
    std::deque<T> data;
    ros::Subscriber subscriber_;
    std::mutex buff_mutex_;

};

}












#endif