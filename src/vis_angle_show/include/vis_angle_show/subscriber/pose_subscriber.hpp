#ifndef POSE_SUBSCRIBER_
#define POSE_SUBSCRIBER_

#include <ros/ros.h>
#include <deque>
#include <nav_msgs/Odometry.h>
#include <mutex>

namespace vis_angle_show
{
    class PoseSubscriber
    {
    public:
        PoseSubscriber() = default;
        PoseSubscriber(ros::NodeHandle &nh, std::string topic_name, int buff_size);
        void ParseData(std::deque<nav_msgs::Odometry> &deque_data);
        void msg_callback(const nav_msgs::Odometry& pose_msg);

    private:
        ros::NodeHandle nh_;
        ros::Subscriber subscriber_;
        std::deque<nav_msgs::Odometry> data_buff_;
        std::mutex buff_mutex_;
    };



}

















#endif