// /**
//  * @file cloud_publisher.hpp
//  * @author Zhang Songpeng
//  * @version 1.0
//  * @date 2020-5-29
//  * @brief cloud publisher
//  */

// #ifndef CLOUD_PUBLISHER_HPP_
// #define CLOUD_PUBLISHER_HPP_

// #include <pcl_conversions/pcl_conversions.h> 
// #include <sensor_msgs/PointCloud2.h>
// #include <pcl/point_types.h> 
// #include <pcl/io/pcd_io.h> 
// #include <pcl/filters/frustum_culling.h>
// #include <pcl/filters/passthrough.h>

// #include "vis_angle_show/subscriber/pose_subscriber.hpp"

// namespace vis_angle_show
// {
//     class CloudPublisher
//     {
//     public:
//         CloudPublisher(ros::NodeHandle &nh,
//                        const std::string &topic_name,
//                        int buff_size);

//         void Publish(sensor_msgs::PointCloud2 &data);
//     private:
//         ros::Publisher publisher_;
//     };
// } 

// #endif