#ifndef PUBLISHER_
#define PUBLISHER_

#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/PointCloud2.h>

namespace vis_angle_show{

template <class pub_type, class sensor_type>
class BasePublisher
{
public:
    BasePublisher(ros::NodeHandle &nh, const std::string topic_name, int buff_size)
    :nh_(nh), topic_name_(topic_name), buff_size_(buff_size)
    {
        publisher_ = nh.advertise<pub_type>(topic_name, buff_size);
    }


    virtual void Publish(const sensor_type& pub_data) = 0;
// private:
    ros::Publisher publisher_;
    std::string topic_name_;
    int buff_size_;
    ros::NodeHandle nh_;

};


template <class pub_type, class sensor_type>
class ImgPublisher : BasePublisher<pub_type, sensor_type>
{
public:
    ImgPublisher(ros::NodeHandle &nh, const std::string topic_name, int buff_size) : BasePublisher<pub_type, sensor_type>(nh, topic_name, buff_size)
    {

    }

    virtual void Publish(const sensor_type& pub_data)
    {
        // 模板子类使用模板父类函数和变量需要用：this->
        this->publisher_.publish(pub_data);
        LOG(INFO) << "pub image data";
    }
};

template <class pub_type, class sensor_type>
class CloudPublisher : BasePublisher<pub_type, sensor_type>
{
public:
    CloudPublisher(ros::NodeHandle &nh, const std::string topic_name, int buff_size) : BasePublisher<pub_type, sensor_type>(nh, topic_name, buff_size)
    {
        
    }
    virtual void Publish(const sensor_type& pub_data)
    {
        this->publisher_.publish(pub_data);
    }
};

}












#endif