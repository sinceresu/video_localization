// #ifndef IMAGE_PUBLISHER_HPP_
// #define IMAGE_PUBLISHER_HPP_

// #include <ros/ros.h>
// #include <sensor_msgs/Image.h>
// #include <glog/logging.h>

// namespace vis_angle_show
// {
//     class ImgPublisher
//     {
//     public:
//         ImgPublisher() = default;
//         ImgPublisher(ros::NodeHandle &nh, const std::string topic_name, int buff_size = 1000);
//         void Publish(const sensor_msgs::Image::ConstPtr img_data);

//     private:
//         ros::Publisher publisher_;
//         ros::NodeHandle nh_;
//     };

// }

// #endif