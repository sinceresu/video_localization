#include "vis_angle_show/reprojection/reprojection.hpp"

namespace vis_angle_show
{

Reprojection::Reprojection(const cv::Mat& K, const std::vector<Eigen::Vector3d>& blocks, const cv::Mat& distortion)
:K_(K), blocks_(blocks), distortion_(distortion)
{
    LOG(INFO) << "K is "<< K_;
    LOG(INFO) << "blocks is "<< blocks[0];
}

std::vector<cv::KeyPoint> Reprojection::GetPixUV(const Eigen::Matrix4d& image_pose)
{
    std::vector<cv::KeyPoint> kps;
    std::vector<cv::Point2f> projectedPoints;
    std::vector<cv::Point3f> pts, out_pts; 
    if (blocks_.size() > 0)
    {
        for(const auto& block : blocks_)
        {
            // cv::KeyPoint uv = WorldToPix(block, K_, image_pose);
            // kps.push_back(uv);
            
            cv::Point3f pt;
            Eigen::Vector3d camera_block = image_pose.block(0,0,3,3) * block +  image_pose.block(0,3,3,1);

            if(camera_block[2] < 0)
            {
                pt.x = 0;
                pt.y = 1000;
                pt.z = 0;
                pts.push_back(pt);
            }else{
                pt.x = camera_block[0];
                pt.y = camera_block[1];
                pt.z = camera_block[2];
                pts.push_back(pt);
            }

        }
    }else{
        // LOG(ERROR) <<"BLOCK is empty !!!";
    }
    cv::Mat R = cv::Mat::zeros(1, 3, CV_32F);
    cv::Mat t = cv::Mat::zeros(1, 3, CV_32F);
    // cv::eigen2cv(K_, projection);
/*     Eigen::Matrix3d R_matrix = image_pose.block(0,0,3,3).cast<double>();
    cv::eigen2cv(R_matrix, R);
    Eigen::Vector3d t_matrix = image_pose.block(0,3,3,1).cast<double>();
    cv::eigen2cv(t_matrix, t); */
    cv::fisheye::projectPoints(pts, projectedPoints, R, t, K_, distortion_);
    // cv::projectPoints(pts, R, t, K_, distortion_, projectedPoints);
    for (auto& pP : projectedPoints)
    {
        // pP.y = 800 - pP.y;
        cv::KeyPoint kp;
        kp.pt.x = pP.x;
        kp.pt.y = pP.y;
        kps.push_back(kp);
    }
    // return kps;
    return kps;
}

cv::KeyPoint Reprojection::WorldToPix(const Eigen::Vector3d& world3d, const Eigen::Matrix3d& K, const Eigen::Matrix4d& pose)
{
    cv::KeyPoint res;
    // Eigen::Matrix4d w2c = pose;
    // auto w2cc = w2c.inverse();
    Eigen::Vector3d cam3d = pose.block<3,3>(0,0) * world3d + pose.block<3,1>(0,3);
    if(cam3d[2] < 0)
    {
        res.pt.x = -100;
        res.pt.x = -100;
        return res;
    }

    LOG(INFO) << "befor cam3d is " << cam3d ;
    Eigen::Vector3d normal_cam3d = Eigen::Vector3d(cam3d[0]/cam3d[2], cam3d[1]/cam3d[2], 1.0);
    LOG(INFO) << "cam3d is " << normal_cam3d ;
    Eigen::Vector3d image3d = K * normal_cam3d;
    // res.pt.x = image3d[0];
    // res.pt.y = image3d[1];
    res.pt.x = image3d[1];
    res.pt.y = 1080 - image3d[0];

    return res;
}

}