#include "vis_angle_show/subscriber/pose_subscriber.hpp"

namespace vis_angle_show
{
    PoseSubscriber::PoseSubscriber(ros::NodeHandle &nh, std::string topic_name, int buff_size)
    :nh_(nh)
    {
        subscriber_ = nh_.subscribe(topic_name, buff_size, &PoseSubscriber::msg_callback, this);
    }

    void PoseSubscriber::msg_callback(const nav_msgs::Odometry& pose_msg)
    {
        buff_mutex_.lock();
        data_buff_.push_back(pose_msg);
        buff_mutex_.unlock();
    }

    void PoseSubscriber::ParseData(std::deque<nav_msgs::Odometry> &deque_data)
    {
        buff_mutex_.lock();
        if (data_buff_.size() > 0) {
            deque_data.insert(deque_data.end(), data_buff_.begin(), data_buff_.end());
            data_buff_.clear();
        }
        buff_mutex_.unlock();
    }
}