// /**
//  * @file cloud_publisher.cpp
//  * @author Zhang Songpeng
//  * @version 1.0
//  * @date 2020-5-29
//  * @brief cloud publisher
//  */

// #include "vis_angle_show/publisher/cloud_publisher.hpp"


// namespace vis_angle_show
// {
//     CloudPublisher::CloudPublisher(ros::NodeHandle &nh,
//                                  const std::string &topic_name,
//                                  int buff_size) 
//     {
//         publisher_ = nh.advertise<sensor_msgs::PointCloud2>(topic_name, buff_size);
//     }

//     void CloudPublisher::Publish(sensor_msgs::PointCloud2 &data)
//     {
//         // sensor_msgs::PointCloud2 output;
//         // pcl::toROSMsg(data, output);
//         data.header.frame_id = "map";
//         publisher_.publish(data);
//     }
// } // namespace vis_angle_show