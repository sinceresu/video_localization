#ifndef TOOL_HPP_
#define TOOL_HPP_

#include <iostream>
#include <fstream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/viz.hpp>
#include <Eigen/Geometry> 
#include <boost/format.hpp>  // for formating strings
#include <boost/filesystem.hpp>
#include <set>


struct pose_name
{
    std::string name;
    Eigen::Matrix4d pose;
    bool left;
    double east;
    double north;
    double high;
};


template <typename datatype>
bool save_pose(const char* file_path, datatype& data, Eigen::Vector3d center_t, std::string name, int id)
{
    FILE *pFile_3d2d;
    pFile_3d2d = fopen(file_path,"a");
    // auto pose = data.pose;
    int camera_id = 1;
    std::cout << "name is: "<< name << std::endl; 
    // fprintf(pFile_3d2d, "%s\n", name);
    fprintf (pFile_3d2d, "%d %f %f %f %f %f %f %f %d %s\n", id, data.w(), data.x(), data.y(), data.z(), center_t[0], center_t[1], center_t[2], camera_id, name.c_str());
    fprintf(pFile_3d2d, "\n");
    fclose (pFile_3d2d);
}

template <typename T>
bool read_pose_and_save(std::vector<T>& pose_datas, const char* file_path, const char* save_path)
{
    std::ifstream in(file_path, std::ios::out);
    if(!in.good())
    {
        return false;
    }
    std::string line;
    T pose_name;
    int id = 1;

    // Eigen::Matrix4d Tcam2lidar;
    // Tcam2lidar.setIdentity();
    Eigen::Matrix3d Rcam2lidar;
    // Rcam2lidar << -1,0,0,
    //                0,0,-1,
    //                0,-1,0;
    Rcam2lidar << 9.9987981775838564e-01, -2.0302048422644070e-03,-1.5369720483289185e-02,
                  1.5355080758123429e-02, -7.0397829366975560e-03,9.9985732129694838e-01,
                  -2.1381146712708588e-03, -9.9997315950203736e-01,-7.0077629204988515e-03;

    // Tcam2lidar.block<3,3>(0,0) = Rcam2lidar;
    while(std::getline(in, line))
    {
        std::istringstream iss(line);
        std::string image_name;
        int id, camera_id;
        Eigen::Matrix4d pose_data;
        pose_data.setIdentity();
        // Eigen::Quaterniond Q;
        Eigen::Vector4d Q_v;
        Eigen::Vector3d p_v;
        if(!(iss >> id >> Q_v[3] >> Q_v[0] >> Q_v[1] >> Q_v[2] >> p_v[0] >> p_v[1] >> p_v[2] >> camera_id >> image_name));
        Eigen::Quaterniond Q = Eigen::Quaterniond(Q_v);
        std::cout << "Q is " << Q.w() << Q.x() << Q.y() << Q.z() << std::endl;
        Q = Q * Rcam2lidar;
        
        pose_data.block<3,3>(0,0) = Q.matrix();
        pose_data.block<3,1>(0,3) = p_v;
        std::cout << "pose data is " << std::endl << pose_data << std::endl;
        pose_data = pose_data.inverse();
        std::cout << "pose data inverse is " << std::endl << pose_data << std::endl;

        Q = Eigen::Quaterniond(pose_data.block<3,3>(0,0));
        p_v = pose_data.block<3,1>(0,3);
        save_pose(save_path, Q, p_v, image_name, id);
    }
}

template <typename T>
bool read_pose(std::vector<T>& pose_datas, const char* file_path, int data_form)
{
    std::ifstream in(file_path, std::ios::out);
    if(!in.good())
    {
        return false;
    }
    std::string line;
    // std::string save_path = "center_pose.txt";
    T pose_name;
    int id = 1;
    while(std::getline(in, line))
    {
        std::istringstream iss(line);
        std::string image_time_name, image_type_name, image_name;
        Eigen::Matrix4d pose_data;
        if(!(iss >> image_time_name >> image_type_name >> pose_data(0,0) >> pose_data(0,1) >> pose_data(0,2) >> pose_data(0,3) 
                                                       >> pose_data(1,0) >> pose_data(1,1) >> pose_data(1,2) >> pose_data(1,3)
                                                       >> pose_data(2,0) >> pose_data(2,1) >> pose_data(2,2) >> pose_data(2,3)
                                                       >> pose_data(3,0) >> pose_data(3,1) >> pose_data(3,2) >> pose_data(3,3)));
        image_name = image_time_name + "_" + image_type_name + ".jpg";
        if(image_type_name == "rear")
        {
            pose_name.left = true;
        }else{
            pose_name.left = false;
        }

        pose_name.pose = pose_data;
        pose_name.name = image_name;
        pose_datas.push_back(pose_name);
        // Eigen::Quaterniond center_Q = Eigen::Quaterniond(pose_data.block<3,3>(0,0).inverse());
        // Eigen::Vector3d center_t = Eigen::Vector3d(pose_data.block<3,1>(0,3));
        // save_pose(save_path.c_str(), center_Q, center_t, image_name, id);
        id++;
    }
    return true;
}

template <typename datatype>
bool save_gps_pose(const char* file_path, datatype& data)
{
    FILE *pFile_3d2d;
    pFile_3d2d = fopen(file_path,"a");
    std::cout << data.name << std::endl;
    fprintf (pFile_3d2d, "%s %f %f %f \n", data.name.c_str(), data.east, data.north, data.high);
    fclose (pFile_3d2d);
}

template <typename T>
bool read_bkth_pose_save(const char* file_path, std::vector<T>& pose_datas)
{
    std::ifstream in(file_path, std::ios::out);
    if(!in.good())
    {
        return false;
    }
    std::string line;
    std::string save_path = "bkth_pose.txt";
    T pose_name;
    int id = 1;
    while(std::getline(in, line))
    {
        std::istringstream iss(line);
        std::string image_time_name, image_type_name, image_name;
        Eigen::Matrix4d pose_data;
        double esating, northing, high, time;
        double W, X, Y, Z, east, north;

        int id;
        if(!(iss >> image_name >>id >> time >> esating >> northing >> high >> X >> Y >> Z >> east >> north));
        image_name = image_name + ".JPG";
        pose_name.name = image_name;
        pose_name.east = east;
        pose_name.north = north;
        pose_name.high = high;
        save_gps_pose(save_path.c_str(), pose_name);


        id++;
    }
    return true;
}

bool readImageNames(const std::string& path, std::set<std::string>& image_names)
{
    boost::filesystem::path files_path = path;
    if (!boost::filesystem::exists(files_path))
    {
        return false;
    }
    boost::filesystem::recursive_directory_iterator beg_iter(path);
    boost::filesystem::recursive_directory_iterator end_iter;

    for (; beg_iter != end_iter; beg_iter++)
    {
        std::string file_ext = beg_iter->path().extension().string();
        if(file_ext == ".jpg" || file_ext == ".JPG" || file_ext == ".png")
        {
            image_names.emplace(beg_iter->path().filename().string());
        }
    }
    return true;
}

template <typename T>
bool read_pixloc_pose(std::vector<T>& pose_datas, const char* file_path)
{
    std::ifstream in(file_path, std::ios::out);
    if(!in.good())
    {
        return false;
    }
    std::string line;
    T pose_name;
    while(std::getline(in, line))
    {
        std::istringstream iss(line);
        std::string image_name;
        Eigen::Matrix4d pose_data;
        Eigen::Quaterniond pose_Q;
        Eigen::Vector3d pose_t;
        if(!(iss >> image_name  >> pose_Q.w() >> pose_Q.x()>> pose_Q.y()>> pose_Q.z()
        >> pose_t[0] >> pose_t[1] >> pose_t[2]))
        {
        std::cout << "read_pixloc_pose read wrong !!!!" << std::endl;
        }
        std::cout << "pt is " << pose_t << std::endl;
        Eigen::Matrix4d pose_i;
        pose_i.setIdentity();
        pose_i.block<3,3>(0,0) = pose_Q.matrix();
        pose_i.block<3,1>(0,3) = pose_t;
        pose_name.pose = pose_i;
        pose_name.name = image_name;
        pose_datas.push_back(pose_name);
    }
    return true;
}




#endif