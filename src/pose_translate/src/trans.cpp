#include "tool.hpp"
#include <opencv2/core/eigen.hpp>
/* 
功能：将扫描车的pose文件转为colmap使用的文件格式，并转换坐标系到相机

./trans /media/zmc/Teclast_S20/bag/vis_视觉反定位/1314D435/rear.txt /media/zmc/Teclast_S20/data/易达/标定参数文件/Fw_白色机头fisheye_omni模型参数/rear_fisheye_lidar_calibration.yaml /media/zmc/Teclast_S20/data/易达/标定参数文件/Fw_白色机头fisheye_omni模型参数/front_fisheye_lidar_calibration.yaml
 */

int main (int argc, char**argv)
{
    if(argc != 4)
    {
        std::cout << "Error! ./trans  image_traj.txt rear_came.txt front_came.txt" << std::endl;
        return 0;
    }
    std::vector<pose_name> pose_data;
    read_pose(pose_data, argv[1], 1);
    std::string save_path = "images.txt";

    cv::FileStorage fsl(argv[2], cv::FileStorage::READ);
    cv::FileStorage fsr(argv[3], cv::FileStorage::READ);
    cv::Mat ex_left_cv, ex_right_cv;
    fsl["CameraExtrinsicMat"] >> ex_left_cv;
    fsr["CameraExtrinsicMat"] >> ex_right_cv;

    Eigen::Matrix4d ex_left;//rear
    Eigen::Matrix4d ex_right;

    cv::cv2eigen(ex_left_cv, ex_left);
    cv::cv2eigen(ex_right_cv, ex_right);
    std::cout << "ex_left = " << std::endl
              << ex_left << std::endl;
    
    std::cout << "ex_right = " << std::endl
              << ex_right << std::endl;

    //照片向右旋转90度数 rear
    Eigen::AngleAxisd rotation_vector (-M_PI/2, Eigen::Vector3d ( 0,0,1 ) );
    Eigen::Matrix4d pic_rot_rear;
    pic_rot_rear.setIdentity();
    pic_rot_rear.block<3,3>(0,0) = rotation_vector.matrix();

    //照片向左旋转90度数
    Eigen::AngleAxisd rotation_vector_front (M_PI/2, Eigen::Vector3d ( 0,0,1 ) );
    Eigen::Matrix4d pic_rot_front;
    pic_rot_front.setIdentity();
    pic_rot_front.block<3,3>(0,0) = rotation_vector_front.matrix();

    int id = 1;
    for(auto& ele : pose_data)
    {
        if(ele.left)
        {
            // std::cout << "left " << std::endl;
            ele.pose = (ele.pose * ex_left * pic_rot_rear).inverse();
        }else{
            // std::cout << "right " << std::endl;
            ele.pose = (ele.pose * ex_right * pic_rot_front).inverse();
        }
        std::cout << ele.name << " pose is =" << std::endl 
                  << ele.pose.inverse() << std::endl;
        Eigen::Quaterniond center_Q = Eigen::Quaterniond(ele.pose.block<3,3>(0,0));
        Eigen::Vector3d center_t = Eigen::Vector3d(ele.pose.block<3,1>(0,3));

        save_pose(save_path.c_str(), center_Q, center_t, ele.name, id);
        id ++;
    }
    std::cout << "transform finish, save to images.txt" << std::endl;
    return 1;
}