#include "localization_flow.hpp"

namespace yd_vis_localization{

LocalizationFlow::LocalizationFlow(std::string load_map_path,
                   ros::NodeHandle& nh, const std::string sub_match_topic_name,
                   const std::string pub_pose_topic_name,  const std::string pub_loop_topic_name, 
                   const std::string pub_img_topic_name,  const std::string sub_img_topic_name, 
                   const std::string pub_path_topic_name, int buff_size)
: Params()
{
    localization_ptr_ = std::make_shared<Localization>(load_map_path);
    match_sub_ptr_ = std::make_shared<MatchSubscriber>(nh, sub_match_topic_name, buff_size);
    pose_pub_ptr_ = std::make_shared<PosePublisher>(nh, pub_pose_topic_name, buff_size); 
    path_pub_ptr_ = std::make_shared<PathPublisher>(nh, pub_path_topic_name, buff_size); 
    loop_index_pub_ptr_ = std::make_shared<LoopIndexPublisher>(nh, pub_loop_topic_name, buff_size);
    cur_pose_sub_ptr_ = std::make_shared<Subscriber<nav_msgs::Odometry>>(nh, sub_current_camera_pose_topic_, buff_size);

    feature_cloud_pub_ptr_ = std::make_shared<CloudPublisher<sensor_msgs::PointCloud2, sensor_msgs::PointCloud2>>(nh, publish_feature_cloud_topic_,buff_size);
    data_init_ = false;
}

void LocalizationFlow::ReadData()
{
    match_sub_ptr_->ParseData(match_data_);
    cur_pose_sub_ptr_->ParseData(pose_data_);
}

bool LocalizationFlow::ValidData()
{
    if (match_data_.size() == 0)
    {
        return false;
    }
    cur_match_data_ = match_data_.front();
    match_data_.pop_front();
    if (localization_ptr_->AnalysisMatch3d2d(cur_match_data_))
    {
        return true;
    }
    return false;
}

void LocalizationFlow::PublishLoopData()
{
    Eigen::Matrix4d cur_pose;
    if (!pose_data_.empty())
    {
        // cur_pose = pose_data_
        auto last_pose = pose_data_.back();
        odomMsgToEigen(last_pose, cur_pose);
    }else{
        localization_ptr_->GetUpdatePose(cur_pose);
    }

    std::vector<std::string> relate_images;
    // relate_images = localization_ptr_->FindRelatedImage(cur_pose.cast<float>());
    std::string relate_image_name = localization_ptr_->FindRelatedImageDependXY(cur_pose.cast<float>());
    if (relate_image_name == "wrong")
    {
        LOG(INFO) << "Din't find loop image !";
        return;
    }
    relate_images.push_back(relate_image_name);

    yd_vis_localization::PointMatch loop_index_msg;
    loop_index_msg.img_ids = relate_images;
    if (loop_index_pub_ptr_->HasSubscribers())
    {
        loop_index_pub_ptr_->Publish(loop_index_msg);
    }
}

void LocalizationFlow::PublishData()
{
    Eigen::Matrix4d cur_pose;
    localization_ptr_->GetUpdatePose(cur_pose);
    nav_msgs::Odometry pose_msg;
    pose_msg.header.stamp = cur_match_data_.header.stamp;
    pose_msg.pose.pose.position.x = cur_pose(0, 3);
    pose_msg.pose.pose.position.y = cur_pose(1, 3);
    pose_msg.pose.pose.position.z = cur_pose(2, 3);
    Eigen::Quaterniond cur_Q = Eigen::Quaterniond(cur_pose.block<3,3>(0,0));
    pose_msg.pose.pose.orientation.w = cur_Q.w();
    pose_msg.pose.pose.orientation.x = cur_Q.x();
    pose_msg.pose.pose.orientation.y = cur_Q.y();
    pose_msg.pose.pose.orientation.z = cur_Q.z();
    pose_msg.header.frame_id = "map";
    if (pose_pub_ptr_->HasSubscribers())
    {
        pose_pub_ptr_->Publish(pose_msg); 
    }
    if (path_pub_ptr_->HasSubscribers())
    {
        path_pub_ptr_->Publish(cur_pose); 
    }
    std::vector<std::string> relate_images;
    // relate_images = localization_ptr_->FindRelatedImage(cur_pose.cast<float>());
    std::string relate_image_name = localization_ptr_->FindRelatedImageDependXY(cur_pose.cast<float>());
    relate_images.push_back(relate_image_name);

    yd_vis_localization::PointMatch loop_index_msg;
    loop_index_msg.img_ids = relate_images;
    if (loop_index_pub_ptr_->HasSubscribers())
    {
        loop_index_pub_ptr_->Publish(loop_index_msg);
    }

    pcl::PointCloud<pcl::PointXYZ> featrue_cloud;
    if (localization_ptr_->GetFeature3dCloud(featrue_cloud))
    {
        sensor_msgs::PointCloud2 pub_feature_cloud;
        pcl::toROSMsg(featrue_cloud, pub_feature_cloud);
        
        pub_feature_cloud.header.stamp = cur_match_data_.header.stamp;
        pub_feature_cloud.header.frame_id = "map";
        if(feature_cloud_pub_ptr_->HasSubscribers())
        {
            feature_cloud_pub_ptr_->Publish(pub_feature_cloud);
        }
    }
}

void LocalizationFlow::Run()
{
    std::chrono::steady_clock::time_point t_start = std::chrono::steady_clock::now();
    ReadData();
    if (ValidData())
    {
        std::chrono::steady_clock::time_point t_end_readdata = std::chrono::steady_clock::now();
        std::chrono::duration<double, std::milli> time_cost_read_match = std::chrono::duration<double, std::milli>(t_end_readdata - t_start);
        LOG(INFO) << "read match cost time is " << time_cost_read_match.count();
        
        if (localization_ptr_->InitComputePose())
        {
            std::chrono::steady_clock::time_point t_end_c_pose = std::chrono::steady_clock::now();
            std::chrono::duration<double, std::milli> time_cost_c_pose = std::chrono::duration<double, std::milli>(t_end_c_pose - t_start);
            LOG(INFO) << "computer pose cost time is " << time_cost_c_pose.count();
            PublishData();
            data_init_ = true;
        }
    std::chrono::steady_clock::time_point t_end = std::chrono::steady_clock::now();
    std::chrono::duration<double, std::milli> time_cost = std::chrono::duration<double, std::milli>(t_end - t_start);
    LOG(INFO) << "localization cost time is " << time_cost.count();
    }
    if (data_init_){
        PublishLoopData();
    }

}
}