#include "localization.hpp"

namespace yd_vis_localization{
Localization::Localization(const std::string load_map_path)
: Params()
{
    
    colmap_file_reader_ptr_ = std::make_shared<ColmapFileReader>(pix_error_);
    LOG(INFO) << "==========Loading map=====================";
    images_map_ = std::make_shared<Map>(load_map_path);

    LOG(INFO) << "==========Load map complete===============";

    // LOG(INFO) << "==========Filter map=====================";
    colmap_file_reader_ptr_->FilterMap3D();
    // LOG(INFO) << "==========Filter map complete=====================";

    colmap_file_reader_ptr_->FilterByCloud();
    LOG(INFO) << "==========Filter map by cloud complete=====================";
}

bool Localization::GetFeature3dCloud(pcl::PointCloud<pcl::PointXYZ>& pub_feature_cloud)
{
    if (pub_feature_)
    {
        pub_feature_cloud = pub_feature_cloud_;
        return true;
    }
    return false;
}

bool Localization::InitComputePose()
{
    float change_score = ransac_init_score_;
    cv::Mat r, t, inliers;
    std::vector<cv::Point2f> match_undistor_2d;
    if (undistortion_)
    {
        // LOG(INFO) << "projection_matrix is:" << std::endl << projection_matrix_;
        // LOG(INFO) << "distortion_matrix is:" << std::endl << distortion_matrix_;
        cv::fisheye::undistortPoints(match_pt2d_, match_undistor_2d, projection_matrix_, distortion_matrix_, cv::Mat(), projection_matrix_);
        cv::solvePnPRansac(match_pt3d_, match_undistor_2d, projection_matrix_, cv::Mat(), r, t, false, 300, ransac_init_score_, 0.99, inliers, cv::SOLVEPNP_ITERATIVE);
    }else{
        cv::solvePnPRansac(match_pt3d_, match_pt2d_, projection_matrix_, distortion_matrix_, r, t, false, 300, ransac_init_score_, 0.99, inliers, cv::SOLVEPNP_ITERATIVE);
    }
    if (pub_feature_)
    {
        pub_feature_cloud_.clear();

        for (auto pt_3d : match_pt3d_)
        {   
            pcl::PointXYZ pt;
            pt.x = pt_3d.x;
            pt.y = pt_3d.y;
            pt.z = pt_3d.z;
            pub_feature_cloud_.push_back(pt);
        }
        // pcl::io::savePCDFileBinary("feature.pcd", cloud_3d);
        // std::cout << "match feature 3d cloud save in feature.pcd" << std::endl;
    }

    cv::Mat R;
    cv::Rodrigues(r, R);
    Eigen::Matrix3d matrix_r;
    Eigen::Vector3d matrix_t;
    cv::cv2eigen(R, matrix_r);
    cv::cv2eigen(t, matrix_t);
    Eigen::Matrix4d pose_tmp;
    pose_tmp.setIdentity();
    pose_tmp.block<3,3>(0,0) = matrix_r;
    pose_tmp.block<3,1>(0,3) = matrix_t;
    init_pose_ = pose_tmp.inverse();
    if (pose_buff_.size() > 1)
    {
        auto last_pose = pose_buff_.back();
        double distance = abs(init_pose_(0,3) - last_pose(0,3)) 
                        + abs(init_pose_(1,3) - last_pose(1,3));
        if (distance > 1.5)
        {
            LOG(ERROR) <<"current pose error is too large";
            return false;
        }
    }
    // std::cout << "curr pose id " << std::endl << init_pose_ << std::endl;
    pose_buff_.push_back(init_pose_);
    return true;
}

// void Localization::OptimizePose(std::vector<Point3D>& colmap_3d, const std::vector<Point2D>& colmap_2d)
// {
// /*     BundleAdjustPose3d2d bundleadjust;
//     std::vector<double> camera_param;

//     // std::cout  << K_.at<float>(0, 0) << std::endl;
//     camera_param.push_back(K_.at<float>(0, 0));
//     camera_param.push_back(K_.at<float>(1, 1));
//     camera_param.push_back(K_.at<float>(0, 2));
//     camera_param.push_back(K_.at<float>(1, 2));
//     camera_param.push_back(0.0);
//     // std::cout <<"optimize K is " << camera_param[0] << " " << camera_param[1] << " "<< camera_param[2] << " "<< camera_param[3] << " " << std::endl;

//     ceres::LossFunction* loss_function = new ceres::CauchyLoss(1.0);

//     Eigen::Matrix4d X = init_pose_.inverse();
//     Eigen::Matrix4d pose_front = Eigen::Matrix4d::Identity();
//     init_pose_ = bundleadjust.AddImageToProblem3d2d(X, colmap_3d, colmap_2d, camera_param, loss_function, pose_front); */
// }

void Localization::GetUpdatePose(Eigen::Matrix4d& update_pose)
{
    update_pose = init_pose_;
}

bool Localization::AnalysisMatch3d2d(const yd_vis_localization::PointMatch& cur_data)
{
    int length = cur_data.img_ids.size();
    if (length == 0)
    {
        LOG(ERROR)<< "match image size is 0";
        return false;
    }
    size_t max_num = 0;
    int max_id = 0;
    for (int i = 0; i < length; i++)
    {
        auto points = cur_data.polygons[i].points;
        if (max_num < points.size())
        {
            max_num = points.size();
            max_id = i;
        }
    }
    std::string img_ids = cur_data.img_ids[max_id];
    
    images_map_->GetSuperGlue3D2DMatched(img_ids, cur_data.polygons[max_id].points, match_pt2d_, match_pt3d_);


    if (match_pt2d_.size() < 5)
    {
        LOG(ERROR) << "match size is " << match_pt2d_.size() << " not enough to computer pose !!!";
        return false;
    }
    return true;
}

std::vector<std::string> Localization::FindRelatedImage(const Eigen::Matrix4f pose)
{
/*     colmap_file_reader_ptr_->CutPointIndexCloud(pose);
    std::vector<std::string> related_image_name = colmap_file_reader_ptr_->FindLocalFrame();
    return related_image_name; */
}

std::string Localization::FindRelatedImageDependXY(const Eigen::Matrix4f pose)
{
    std::string image_name = images_map_->SearchNearImage(pose);
    return image_name;
}

}
