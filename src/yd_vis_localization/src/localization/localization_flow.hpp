#ifndef YD_VIS_LOCALIZATION_LOCALIZATION_HPP_
#define YD_VIS_LOCALIZATION_LOCALIZATION_HPP_



#include "../subscriber/match_subscriber.hpp"
#include "../publisher/pose_publisher.hpp"
#include "../publisher/path_publisher.hpp"
#include "../publisher/loop_index_publisher.hpp"
#include "../publisher/publisher.hpp"
#include "../subscriber/subscriber.hpp"
#include "../pre_local/colmap_io.hpp"
#include "../tool/data_conversions.hpp"
#include "../tool/params.hpp"
#include "localization.hpp"

#include <pcl_conversions/pcl_conversions.h> 
#include <chrono>

namespace yd_vis_localization{

class LocalizationFlow : public Params
{
public:
  LocalizationFlow() = default;
  LocalizationFlow(std::string load_map_path,
                   ros::NodeHandle& nh, const std::string sub_match_topic_name,
                   const std::string pub_pose_topic_name,  const std::string pub_loop_topic_name, 
                   const std::string pub_img_topic_name,  const std::string sub_img_topic_name,
                   const std::string pub_path_topic_name, int buff_size);
  void ReadData();
  bool ValidData();
  void Run();
  bool HasData();
  bool UpdatePose();
  void PublishData();
  void PublishLoopData();
private:
  std::shared_ptr<MatchSubscriber> match_sub_ptr_;
  std::shared_ptr<PosePublisher> pose_pub_ptr_;
  std::shared_ptr<PathPublisher> path_pub_ptr_;
  std::shared_ptr<Subscriber<nav_msgs::Odometry>> cur_pose_sub_ptr_;
  std::shared_ptr<LoopIndexPublisher> loop_index_pub_ptr_;
  std::shared_ptr<Localization> localization_ptr_;
  std::shared_ptr<CloudPublisher<sensor_msgs::PointCloud2, sensor_msgs::PointCloud2>> feature_cloud_pub_ptr_;
  std::deque<yd_vis_localization::PointMatch> match_data_;
  std::deque<nav_msgs::Odometry> pose_data_;
  

  
  yd_vis_localization::PointMatch cur_match_data_;

  bool data_init_;
  
  
};
}



















#endif