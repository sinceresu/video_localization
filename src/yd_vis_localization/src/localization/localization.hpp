#ifndef _LOCALIZATION_FLOW_HPP_
#define _LOCALIZATION_FLOW_HPP_

// #include <g2o/core/base_vertex.h>
// #include <g2o/core/base_unary_edge.h>
// #include <g2o/core/sparse_optimizer.h>
// #include <g2o/core/block_solver.h>
// #include <g2o/core/solver.h>
// #include <g2o/core/optimization_algorithm_gauss_newton.h>
// #include <g2o/solvers/dense/linear_solver_dense.h>
#include <Eigen/Core>

#include <opencv2/core/core.hpp> 
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/core/eigen.hpp>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include <ros/package.h>
#include <ros/time.h>
#include <glog/logging.h>

#include "yd_vis_localization/PointMatch.h"
#include "../tool/params.hpp"
#include "../pre_local/colmap_io.hpp"
#include "../base/map_io.hpp"
#include "../base/map.hpp"


namespace yd_vis_localization{
  
class Localization : public Params
{
  public:
    Localization() = default;
    Localization(const std::string load_map_path);
    bool InitComputePose();
    // void OptimizePose(std::vector<Point3D>& colmap_3d, const std::vector<Point2D>& colmap_2d);
    bool AnalysisMatch3d2d(const yd_vis_localization::PointMatch& cur_data);
    void GetUpdatePose(Eigen::Matrix4d& update_pose);
    std::vector<std::string> FindRelatedImage(const Eigen::Matrix4f pose);
    std::string FindRelatedImageDependXY(const Eigen::Matrix4f pose);
    bool GetFeature3dCloud(pcl::PointCloud<pcl::PointXYZ>& pub_feature_cloud);


  private:
    std::string param_file_path_;
    std::string WORKSPACE;
    std::shared_ptr<ColmapFileReader> colmap_file_reader_ptr_;
    std::vector<cv::Point3f> match_pt3d_;
    std::vector<cv::Point2f> match_pt2d_;
    Eigen::Matrix4d init_pose_;
    Eigen::Matrix4d optim_pose_;
    double change_score_;
    cv::Mat K_;
    cv::Mat distCoeffs_;
    double pix_error_;
    
    std::deque<Eigen::Matrix4d> pose_buff_;
    ros::Time loc_match_time_;
    pcl::PointCloud<pcl::PointXYZ> pub_feature_cloud_;

    std::shared_ptr<Map> images_map_;



};

}






#endif