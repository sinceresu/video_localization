#include <opencv2/core/core.hpp> 
#include <boost/filesystem.hpp>
#include <Eigen/Core>
#include <Eigen/Geometry>
#include <opencv2/core/eigen.hpp>

#include <set>
#include <map>
#include <unordered_map>

namespace yd_vis_localization{
bool readImageNames(const std::string& path, std::set<std::string>& image_names);

Eigen::Vector2d computerReprojectionError(const Eigen::Matrix4d& T, 
                                          const Eigen::Matrix3d& K, 
                                          const Eigen::Vector3d& pt3, 
                                          const Eigen::Vector2d& pt2, cv::KeyPoint& pt);
//获取匹配两帧图像的image id
std::vector<cv::Point2f> readMatches(const char* file_path, std::vector<std::pair<uint32_t, uint32_t>>& matches_id, uint32_t& image1_id, uint32_t& image2_id);
//获取匹配文件夹下的的所有文件名字
bool readMatchFiles(const std::string& path, std::vector<std::string>& match_names);
//获取图像名字和id的对应关系
// bool readImageNameId(const char* file_path, std::vector<std::pair<uint32_t, std::string>>& image_name_id);
bool readImageNameId(const char* file_path, std::map<uint32_t, std::string>& image_name_id);
//读取图像keypoints
bool readImageKeypoints(const char* file_path, std::vector<std::pair<float, float>>& keypoints);
using namespace cv;
using namespace std;
void pose_estimation_3d3d(const vector<Point3f> &pts1,
                          const vector<Point3f> &pts2,
                          Mat &R, Mat &t);


bool read_pose_image_name(std::unordered_map<std::string, Eigen::Vector3d>& pose_datas, const char* file_path);

void save_pose_image_name(const char* file_path, std::string image_name, Eigen::Matrix4d T);

double rotation_error(const Eigen::Quaterniond& w2c, const Eigen::Quaterniond& c2w);

bool IsLittleEndian();

template <typename T>
void WriteBinaryLittleEndian1(std::ostream* stream, const T& data);

template <typename T>
void WriteBinaryLittleEndian1(std::ostream* stream, const std::vector<T>& data);

template <typename T>
T ReverseBytes(const T& data);


// template <typename datatype>
bool save_image_name(const std::string image_name, const char* file_path);

// template <typename datatype>
bool load_image_name(std::string& image_name, const char* file_path);
}