#ifndef FUSION_OPTIMIZER_DATA_CONVERSION_HPP_
#define FUSION_OPTIMIZER_DATA_CONVERSION_HPP_

// #include "utility.h"
#include <Eigen/Core>
#include <Eigen/Geometry>
#include <nav_msgs/Odometry.h>
#include <iostream>

namespace yd_vis_localization {

void odomMsgToEigen(const nav_msgs::Odometry& odom_pose, Eigen::Matrix4d& result_pose);


}



#endif