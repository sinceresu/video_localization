#include "data_conversions.hpp"

namespace yd_vis_localization{
void odomMsgToEigen(const nav_msgs::Odometry& odom_pose, Eigen::Matrix4d& result_pose)
{
    result_pose.setIdentity();
    result_pose(0, 3) = odom_pose.pose.pose.position.x;
    result_pose(1, 3) = odom_pose.pose.pose.position.y;
    result_pose(2, 3) = odom_pose.pose.pose.position.z;
    Eigen::Quaterniond Q = Eigen::Quaterniond(odom_pose.pose.pose.orientation.w,
                                              odom_pose.pose.pose.orientation.x,
                                              odom_pose.pose.pose.orientation.y,
                                              odom_pose.pose.pose.orientation.z);
    result_pose.block<3,3>(0,0) = Q.matrix().cast<double>();
}

}