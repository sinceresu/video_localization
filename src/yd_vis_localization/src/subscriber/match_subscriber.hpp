#ifndef MATCH_SUBSCRIBER_HPP_
#define MATCH_SUBSCRIBER_HPP_


#include <ros/ros.h>
#include <deque>
#include <mutex>
#include <glog/logging.h>

#include "yd_vis_localization/PointMatch.h"

namespace yd_vis_localization{
class MatchSubscriber
{
public:
    MatchSubscriber(const ros::NodeHandle& nh, const std::string topic_name, int buff_size);
    void msg_callback(const yd_vis_localization::PointMatch& match_msg_ptr);
    void ParseData(std::deque<yd_vis_localization::PointMatch>& deque_data);
private:
    ros::NodeHandle nh_;
    ros::Subscriber subscriber_;
    std::deque<yd_vis_localization::PointMatch> match_data_;
    std::mutex buff_mutex_;
};
}
#endif