#include <ros/ros.h>
#include "match_subscriber.hpp"

namespace yd_vis_localization{
MatchSubscriber::MatchSubscriber(const ros::NodeHandle& nh, const std::string topic_name, int buff_size)
:nh_(nh)
{
    subscriber_ = nh_.subscribe(topic_name, buff_size, &MatchSubscriber::msg_callback, this);
}

void MatchSubscriber::msg_callback(const yd_vis_localization::PointMatch& match_msg_ptr)
{
    buff_mutex_.lock();
    match_data_.push_back(match_msg_ptr);
    buff_mutex_.unlock();
}

void MatchSubscriber::ParseData(std::deque<yd_vis_localization::PointMatch>& deque_data)
{
    buff_mutex_.lock();
    if (match_data_.size() > 0) {
        deque_data.insert(deque_data.end(), match_data_.begin(), match_data_.end());
        match_data_.clear();
    }
    buff_mutex_.unlock();
}

}

