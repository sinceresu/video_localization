#ifndef _COLMAP_IO_HPP_
#define _COLMAP_IO_HPP_


#include <pcl/kdtree/kdtree_flann.h>
#include <memory>
// #include <hash>
#include <unordered_map>
#include <geometry_msgs/Polygon.h>
#include <pcl/point_types.h> 
#include <pcl/io/pcd_io.h> 
#include <pcl/filters/frustum_culling.h>
#include <pcl/filters/passthrough.h>

// #include "sophus/so3.h"

#include "../tool/util.hpp"
#include "yd_vis_localization/PointMatch.h"
#include "../tool/params.hpp"

namespace yd_vis_localization{

typedef std::vector<geometry_msgs::Point32> points;


// struct hash_pair { 
//     template <class T1, class T2> 
//     size_t operator()(const std::pair<T1, T2>& p) const
//     { 
//         auto hash1 = std::hash<T1>{}(p.first); 
//         auto hash2 = std::hash<T2>{}(p.second); 
//         return hash1 ^ hash2; 
//     } 
// }; 

class ColmapFileReader : public Params
{
  public:
    ColmapFileReader(): Params()
    {

    };
    ColmapFileReader(const double pix_error);
    void FilterMap3D(bool draw_feature = false);
    void GetSuperGlue3D2DMatches(const std::string& image_name, const points& pt_uv_id,
                            std::vector<cv::Point2f>& pts2d, std::vector<cv::Point3f>& pts3d);

    // Image GetImageWithName(const std::string image_name);

    // void RunColmapLocalization();
    void RunSuperPointLocalization(const std::string& match_file, const std::string& colmap_file);

    //保存关键帧pose的XY值为点云，构建map pose和image name
    //根据定位pose查找最近的点
    //根据找到的最近pose，在map中找到image name
    std::string SearchNearImage(const Eigen::Matrix4f pose);

    //保存colmap重建点云， 点云中I表示其为地图中第I个3d点，
    // 根据3d点可以知道有多少图像观测到了这个点，
    // 遍历每个点，找到当前局部点云和哪个图像最相似


    //获取指定关键帧的3D点
    void GetFrame3DPoint3(std::string frame_name, std::vector<Eigen::Vector3d>& src, std::vector<Eigen::Vector3d>& dst);
    //读取Superglue匹配txt文件， 分别从两个地图中选择3D点

  

    void FilterByCloud();


  private:
    // yd_vis_localization::Reconstruction reconstruction_map_;
    // yd_vis_localization::Reconstruction reconstruction_map2_;
    // DatabaseCache database_cache_;

    // yd_vis_localization::Reconstruction reconstruction_match_;
    // yd_vis_localization::Image ref_frame_;
    // yd_vis_localization::Image ref_match_frame_;
    // yd_vis_localization::Image local_frame_;
    Eigen::Matrix3d map_K_;
    Eigen::Matrix3d local_K_;
    double map_projection_pix_uv_sum_error_;
    // std::vector<Point2D> colmap_pt2d_;
    // std::vector<Point3D> colmap_pt3d_;
    std::vector<cv::Point3f> pts3d_;
    std::vector<cv::Point2f> pts2d_;
    //为什么编译不通过？
    // std::shared_ptr<LocalizationFlow> localization_flow_ptr_;
    // LocalizationFlow localization;
    cv::Mat image_ref_;
    cv::Mat image_loc_;

    std::string match_file_;
    pcl::PointCloud<pcl::PointXYZI> cut_pc_;
    pcl::PointCloud<pcl::PointXYZI>::Ptr cloud_index_;

    pcl::PointCloud<pcl::PointXYZ> pose_xy_cloud_;
    // std::unordered_map<std::pair<float, float>, std::string, hash_pair> un_map_pose_name_;
    std::unordered_map<std::string, Eigen::Quaterniond> un_map_name_R_;
    pcl::KdTreeFLANN<pcl::PointXYZ> pose_kdtree_;
    // std::map<std::string, yd_vis_localization::image_t> image_name_id_map_;

};

}

#endif