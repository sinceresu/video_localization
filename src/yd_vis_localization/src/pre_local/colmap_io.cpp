#include "colmap_io.hpp"
#include "../localization/localization.hpp"


// using namespace colmap;
namespace yd_vis_localization{
ColmapFileReader::ColmapFileReader(const double pix_error)
:Params()
{


}

// void ColmapFileReader::ReadMap(const std::string& colmap_path)
// {
   /*  reconstruction_map_.Read(colmap_path);
    
    auto images = reconstruction_map_.Images();
    LOG(INFO) <<"map contain image num is: "<< images.size();


    int i = 1;
    int image_left_num = images.size();
    // for (const auto& image : images )
    for (; i <= image_left_num; i++)
    {
        // if (i > image_left_num)
        //     break;
        auto image = reconstruction_map_.Image(i);
        auto t = image.ProjectionCenter();
        pcl::PointXYZ pt;
        pt.x = t[0];
        pt.y = t[1];
        pt.z = 0;
        pose_xy_cloud_.push_back(pt);
        un_map_pose_name_[std::make_pair(pt.x, pt.y)] = image.Name();


        un_map_name_R_[image.Name()] = Eigen::Quaterniond(image.RotationMatrix());
        
    }
    pose_kdtree_.setInputCloud(pose_xy_cloud_.makeShared()); */
// }




std::string ColmapFileReader::SearchNearImage(const Eigen::Matrix4f pose)
{
    /* int K = loop_xy_kf_num_;
    // int K = 15;
    std::vector<int> pointIdxNKNSearch(K);  
	std::vector<float> pointNKNSquaredDistance(K);
    pcl::PointXYZ search_pose;
    search_pose.x = pose(0,3);
    search_pose.y = pose(1,3);
    search_pose.z = 0;

    Eigen::Quaterniond Q = Eigen::Quaterniond(pose.block<3,3>(0,0).cast<double>());

    // Sophus::SO3 pose_SO3 = Sophus::SO3(pose.block<3,3>(0,0).cast<double>());
    // Eigen::Vector3d pose_vec = pose_SO3.log();


    std::string loop_image_name;
    double min_error = 1.0;
    if (pose_kdtree_.nearestKSearch(search_pose, K, pointIdxNKNSearch, pointNKNSquaredDistance) > 0)
    {
        //add rpy
        for(const auto& point_index : pointIdxNKNSearch)
        {
            auto pt = pose_xy_cloud_.points[point_index];
            std::string image_name = un_map_pose_name_[std::make_pair(pt.x, pt.y)];
            double error = rotation_error(un_map_name_R_[image_name], Q);
            if (error < 0.2)
            {
                return image_name;
            }
            if (error < min_error)
            {
                min_error = error;
                loop_image_name = image_name;
            }
            std::cout << "image " << image_name << ", rotation error is = " << error << std::endl;

            // un_map_name_R_

        }
        return loop_image_name;
        //add rpy
        // auto pt = pose_xy_cloud_.points[pointIdxNKNSearch[0]];
        // // std::cout << "find key is " << pt.x << " " << pt.y << std::endl;
        // return un_map_pose_name_[std::make_pair(pt.x, pt.y)]; */
    // } 

/*     if (pose_kdtree_.nearestKSearch(search_pose, K, pointIdxNKNSearch, pointNKNSquaredDistance) > 0)

        for (size_t i = 0; i < pointIdxNKNSearch.size(); ++i)
        {
            auto pt = pose_xy_cloud_.points[pointIdxNKNSearch[i]];
            // std::cout << "find key is " << pt.x << " " << pt.y << std::endl;

            std::string loop_image_name = un_map_pose_name_[std::make_pair(pt.x, pt.y)];
            Eigen::Vector3d near_R_vec = un_map_name_R_[loop_image_name];

            LOG(INFO) << "0 error is " << abs(pose_vec[0] - near_R_vec[0]);
            LOG(INFO) << "1 error is " << abs(pose_vec[1] - near_R_vec[1]);
            LOG(INFO) << "2 error is " << abs(pose_vec[2] - near_R_vec[2]);
            LOG(INFO) << "x error is " << abs(pt.x - search_pose.x);
            LOG(INFO) << "y error is " << abs(pt.y - search_pose.y);
            if ((abs(pose_vec[0] - near_R_vec[0]) + abs(pose_vec[1] - near_R_vec[1]) + abs(pose_vec[2] - near_R_vec[2])) < 0.8
            && abs(pt.x - search_pose.x) + abs(pt.y - search_pose.y) < 0.8)
            {
                LOG(INFO) << "loop image is " << loop_image_name ;    
                return loop_image_name;
            }


        }
        
        
    } */
    // if (pose_kdtree_.nearestKSearch(search_pose, K, pointIdxNKNSearch, pointNKNSquaredDistance) > 0)
    // {
    //     for (size_t i = 0; i < pointIdxNKNSearch.size(); ++i){
    //         if ((special_pose_cloud_.points[pointIdxNKNSearch[i]].YPR[0] - update_pose_ypr[0]) < 20)
    //         {
    //             loop_index.push_back(special_pose_cloud_.points[pointIdxNKNSearch[i]].frame_id + ".jpg");
    //         }
    //         if (loop_index.size() == 3)
    //         {
    //             break;
    //         }
    //     }
    // }
//     return "wrong";
}


void ColmapFileReader::FilterByCloud()
{
    /* pcl::PointCloud<pcl::PointXYZ> cloud_lidar, save_cloud;
    pcl::io::loadPCDFile("/home/zmc/Desktop/map.pcd",cloud_lidar);
    pcl::KdTreeFLANN<pcl::PointXYZ> lidar_cloud_kdtree;
    lidar_cloud_kdtree.setInputCloud(cloud_lidar.makeShared());

    auto pt3ds = reconstruction_map_.Points3D();
    double radius = 0.5;

    int delet_num = 0;
    for(const auto pt3d : pt3ds)
    {
        std::vector<int> kIndices;
        std::vector<float> kDistances;
        pcl::PointXYZ pt;
        pt.x = pt3d.second.X();
        pt.y = pt3d.second.Y();
        pt.z = pt3d.second.Z();


        if ((lidar_cloud_kdtree.radiusSearch(pt, radius, kIndices, kDistances)) > 0)
        {
            save_cloud.push_back(pt);
            continue;
        }else{
            auto id_3d = pt3d.first;
            reconstruction_map_.DeletePoint3D(id_3d);
            delet_num++;
        }

    } */
    // pcl::io::savePCDFile("filter.pcd", save_cloud);
    // pcl::io::savePCDFile("cloud_little_error.pcd", *cloud_index);
}

// void ColmapFileReader::FilterFrame3D(Image& map_frame, bool draw_feature)
// {
/*     LOG(INFO) << "filter image " << map_frame.Name() << " 3d points";
    //3D点多的关键帧筛选严格
    yd_vis_localization::point2D_t point_3d_num = 500;
    double map_projection_pix_uv_sum_error;
    if (map_frame.NumPoints3D() > point_3d_num)
    {
        map_projection_pix_uv_sum_error = map_projection_pix_uv_sum_error_;
    }else{
        map_projection_pix_uv_sum_error = 2 * map_projection_pix_uv_sum_error_;
    }
    std::vector<Eigen::Vector3d> eig_pt3d;
    std::vector<Eigen::Vector2d> eig_pt2d;

    Eigen::Matrix4d T_w2c = GetMapFramePose(map_frame);
    //获取一帧中所能观测的到3d点
    //计算投影点computerReprojectionErrorcomputerReprojectionErrorcomputerReprojectionError和特征点的误差
    //大于一定阈值，取消该3d点
    // auto& col_pt2ds = map_frame.Points2D();
    std::vector<cv::KeyPoint> org_kps, rej_kps; 
    // map_frame.
    int i = 0;
    for (auto& pt2 : (*(reconstruction_map_.FindImageWithName(map_frame.Name()))).Points2D())
    {
        point3D_t id_3d = pt2.Point3DId();
        if (reconstruction_map_.ExistsPoint3D(id_3d))
        {
            Eigen::Vector3d eig_pt3d_tmp = reconstruction_map_.Point3D(id_3d).XYZ();
            eig_pt3d.push_back(eig_pt3d_tmp);
            eig_pt2d.push_back(pt2.XY());
            
            //画图： 冲投影和原始特征点
            cv::KeyPoint pt2_comput;//useless
            Eigen::Matrix3d map_K;
            cv::cv2eigen(map_intri_matrix_, map_K);
            // Eigen::Vector2d error = computerReprojectionError(T_w2c, map_K, eig_pt3d_tmp, pt2.XY(), pt2_comput);
            Eigen::Vector2d error = computerReprojectionError(T_w2c, map_K_, eig_pt3d_tmp, pt2.XY(), pt2_comput);
            rej_kps.push_back(pt2_comput);
            cv::KeyPoint org_pt;
            org_pt.pt.x = pt2.X();
            org_pt.pt.y = pt2.Y();
            org_kps.push_back(org_pt);


            if(abs(error[0]) > 15)
            {
                std::cout << "Reprojection pix error is:" << error[0] << ", plesae check pose and camera param !!!" << std::endl;
                std::cout << "3d is= " << eig_pt3d_tmp[0] << eig_pt3d_tmp[1] << eig_pt3d_tmp[2] << std::endl; 
                std::cout << "map K is " << std::endl << map_intri_matrix_ << std::endl;
                return;
            }
            if (abs(error[0]) > map_projection_pix_uv_sum_error || abs(error[1]) > map_projection_pix_uv_sum_error)
            {
                reconstruction_map_.DeletePoint3D(id_3d);
            }else{
                // std::cout << "3d point " << i << " error is :" << error[0]<< " " << error[1] << std::endl;
            }
            i++;
        }
    }

    if (draw_feature)
    {
        // auto image_ref = image_ref_.clone();
        auto image_ref = cv::imread("/media/zmc/Teclast_S20/localization_ws/project/colmap-3.5/src/exe/data/images/" + map_frame.Name());
        std::cout << "image path is " << "/media/zmc/Teclast_S20/localization_ws/project/colmap-3.5/src/exe/data/images/" + map_frame.Name() << std::endl;
        cv::drawKeypoints(image_ref, rej_kps, image_ref, cv::Scalar(255,0,0));
        cv::drawKeypoints(image_ref, org_kps, image_ref, cv::Scalar(0,0,255));
        cv::imwrite(map_frame.Name(), image_ref);
    } */
// }

void ColmapFileReader::FilterMap3D(bool draw_feature)
{
/*     auto images_map = reconstruction_map_.Images();
    
    for (auto& map_frame : images_map)
    {
        // FilterFrame3D(map_frame.second, draw_feature);
        // image_name_id_map_[map_frame.second.Name()] = map_frame.second.ImageId();
    } */
}








void ColmapFileReader::GetSuperGlue3D2DMatches(const std::string& image_name, const points& pt_uv_id,
                                               std::vector<cv::Point2f>& pts2d, std::vector<cv::Point3f>& pts3d)
{ 
    /* pts2d.clear();
    pts3d.clear();
    std::cout << "1221 match image name is :" << image_name + ".jpg" << std::endl;
    auto image = (reconstruction_map_.Image(image_name_id_map_[image_name + ".jpg"]));
    // auto image = *(reconstruction_map_.FindImageWithName(image_name + ".jpg"));
    int num_3d = 0;
    for (const auto& match_id : pt_uv_id)
    {
        
        auto pt_2d = image.Point2D(match_id.z);
        // std::cout <<"map keypoint is :" << pt_2d.XY() << " id is: " << match_id.z << std::endl;
        // std::cout <<"local keypoint is :" << match_id.x << "  " << match_id.y << std::endl;

        if(reconstruction_map_.ExistsPoint3D(pt_2d.Point3DId()))
        {
            // std::cout << "pt_2d.Point3DId() is: " << pt_2d.Point3DId() << std::endl;
            Eigen::Vector3d pt_3d = reconstruction_map_.Point3D(pt_2d.Point3DId()).XYZ();
            // std::cout << "end pt_2d.Point3DId() is: " << pt_2d.Point3DId() << std::endl;

            cv::Point3f cv_pt_3f = cv::Point3f(pt_3d[0], pt_3d[1], pt_3d[2]);
            //TODO: 检查这里xy是否为原尺寸
            // pts2d.push_back(cv::Point2f(match_id.x, match_id.y));
            //临时
            // pts2d.push_back(cv::Point2f((match_id.x),( match_id.y + 420)));
            pts2d.push_back(cv::Point2f((match_id.x),(match_id.y)));
            pts3d.push_back(cv_pt_3f);
            // std::cout << "cv_pt_3f is :" << cv_pt_3f << std::endl;
            num_3d++;
        }
        // std::cout << "=================================" << std::endl;
    }
    // LOG(INFO) << "image " << image_name << " find match 2d point num is " << pt_uv_id.size();
    // LOG(INFO) << "image " << image_name << " find match 3d point num is " << num_3d; */
}

// Image ColmapFileReader::GetImageWithName(const std::string image_name)
// {
// /*     if (image_name_id_map_.count(image_name) > 0)
//     {
//         auto image = (reconstruction_map_.Image(image_name_id_map_[image_name]));
//         return image;
//     }else{
//         LOG(ERROR) << "Cant find image " << image_name <<  ", please check colmap path !!!";
//     } */
// }








void ColmapFileReader::GetFrame3DPoint3(std::string frame_name, std::vector<Eigen::Vector3d>& src, std::vector<Eigen::Vector3d>& dst)
{
/*     const yd_vis_localization::Image* image1 = reconstruction_map_.FindImageWithName(frame_name);
    auto image2 = reconstruction_map2_.FindImageWithName(frame_name);
    // reconstruction_map_.
    // FilterFrame3D(*image1);
    auto pts1 = image1->Points2D();
    auto pts2 = image2->Points2D();
    assert(pts1.size() == pts2.size());

    int i = 0;
    for (auto pt : pts1)
    {
        point3D_t id1_3d = pt.Point3DId();
        point3D_t id2_3d = pts2[i].Point3DId();
        if (reconstruction_map_.ExistsPoint3D(id1_3d) && reconstruction_map2_.ExistsPoint3D(id2_3d))
        {
            src.push_back(reconstruction_map_.Point3D(id1_3d).XYZ());
            dst.push_back(reconstruction_map2_.Point3D(id2_3d).XYZ());
        }
        i++;
    } */
}





}