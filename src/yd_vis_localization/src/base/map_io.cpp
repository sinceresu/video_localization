#include "map_io.hpp"

namespace yd_vis_localization
{
std::string ReadString(std::istream* read_stream)
{
    std::string image_name;
    char name_char;
    do {
    read_stream->read(&name_char, 1);
    if (name_char != '\0') {
        image_name += name_char;
    }
    } while (name_char != '\0');
    return image_name;
}

}