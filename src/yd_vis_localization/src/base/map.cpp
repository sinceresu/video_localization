#include "map.hpp"
#include "iostream"

namespace yd_vis_localization
{

Map::Map(const std::string& map_file_path)
: Params()
{
    std::ifstream file_in(map_file_path,  std::ios::binary);
    const size_t num_image = ReadBinaryLittleEndian<uint64_t>(&file_in);
    for (size_t i = 0; i < num_image; i++)
    {
        std::string image_name = ReadString(&file_in);
        YdFrame frame;
        //读 图像pose
        frame.rotation.w() = ReadBinaryLittleEndian<float>(&file_in);
        frame.rotation.x() = ReadBinaryLittleEndian<float>(&file_in);
        frame.rotation.y() = ReadBinaryLittleEndian<float>(&file_in);
        frame.rotation.z() = ReadBinaryLittleEndian<float>(&file_in);

        frame.transpose.x() = ReadBinaryLittleEndian<float>(&file_in);
        frame.transpose.y() = ReadBinaryLittleEndian<float>(&file_in);
        frame.transpose.z() = ReadBinaryLittleEndian<float>(&file_in);

        //读图像特征点个数
        const size_t num_pt = ReadBinaryLittleEndian<uint64_t>(&file_in);
        std::cout << image_name <<" point size is " << num_pt << std::endl;
        std::vector<YdPoint2d> points;
        //读图像2d3d点 TODO:没有3d点的就不要读了
        for (size_t j = 0; j < num_pt; j++)
        {   
            YdPoint2d pt;
            pt.has_point_3d = ReadBinaryLittleEndian<bool>(&file_in);
            pt.pt.x = ReadBinaryLittleEndian<float>(&file_in);
            pt.pt.y = ReadBinaryLittleEndian<float>(&file_in);
            pt.pt3d.x = ReadBinaryLittleEndian<float>(&file_in);
            pt.pt3d.y = ReadBinaryLittleEndian<float>(&file_in);
            pt.pt3d.z = ReadBinaryLittleEndian<float>(&file_in);
            points.push_back(pt);
        }
        frame.points = points;


        images_map_[image_name] = frame;
        pcl::PointXYZ pose_pt;
        pose_pt.x = frame.transpose[0];
        pose_pt.y = frame.transpose[1];
        pose_pt.z = 0;

        pose_xy_cloud_.push_back(pose_pt);
        un_map_name_R_[image_name] = frame.rotation;
        un_map_pose_name_[std::make_pair(frame.transpose[0], frame.transpose[1])] = image_name;
    }
    pose_kdtree_.setInputCloud(pose_xy_cloud_.makeShared());


    for (auto ele : un_map_pose_name_)
    {
        std::cout << ele.first.first << " "<< ele.first.second << "-------------------" << ele.second << std::endl;
    }

    for(auto image : images_map_)
    {
        int i = 0;
        for (auto pt : image.second.points)
        {
            if (pt.has_point_3d)
            {
                i++;
            }
        }
        std::cout << "image " << image.first << " has 3d point num is " << i << std::endl;
    }








}

bool Map::GetSuperGlue3D2DMatched(const std::string& image_name, const points& pt_uv_id,
                                 std::vector<cv::Point2f>& pts2d, std::vector<cv::Point3f>& pts3d)
{
    pts2d.clear();
    pts3d.clear();
    if (!(images_map_.count(image_name) > 0))
    {
        LOG(ERROR) << "map din't contain image " << image_name;
        return false;
    }
    YdFrame match_frame = images_map_[image_name];
    
    for(const auto match : pt_uv_id)
    {
        int pt_id = match.z;
        if (match_frame.points[pt_id].has_point_3d)
        {
            pts2d.push_back(cv::Point2f(match.x, match.y));
            pts3d.push_back(match_frame.points[pt_id].pt3d);
            // std::cout << match_frame.points[pt_id].pt3d << "-----------" << match.x << ", " << match.y << std::endl;
        }
    }
    return true;
}

std::string Map::SearchNearImage(const Eigen::Matrix4f pose)
{
    int K = loop_xy_kf_num_;
    std::vector<int> pointIdxNKNSearch(K);  
	std::vector<float> pointNKNSquaredDistance(K);
    pcl::PointXYZ search_pose;
    search_pose.x = pose(0,3);
    search_pose.y = pose(1,3);
    search_pose.z = 0;

    Eigen::Quaterniond Q = Eigen::Quaterniond(pose.block<3,3>(0,0).cast<double>());

    std::string loop_image_name;
    double min_error = 1.0;
    if (pose_kdtree_.nearestKSearch(search_pose, K, pointIdxNKNSearch, pointNKNSquaredDistance) > 0)
    {
        //add rpy
        for(const auto& point_index : pointIdxNKNSearch)
        {
            auto pt = pose_xy_cloud_.points[point_index];
            std::string image_name;
            if(un_map_pose_name_.count(std::make_pair(pt.x, pt.y)) > 0)
            {
                image_name = un_map_pose_name_[std::make_pair(pt.x, pt.y)];
            }else{
                std::cout << "pt x: " << pt.x << " pt.y: " << pt.y << " not find" << std::endl; 
                continue;
            }

            double error = rotation_error(un_map_name_R_[image_name], Q);
            if (error < 0.2)
            {
                return image_name;
            }
            if (error < min_error)
            {
                min_error = error;
                loop_image_name = image_name;
            }
            std::cout << "image " << image_name << ", rotation error is = " << error << std::endl;
        }
        if(min_error > max_loop_rotation_error_)
        {
            int new_K = 2 * K;
            pointIdxNKNSearch.resize(new_K);
            pointNKNSquaredDistance.resize(new_K);
            if (pose_kdtree_.nearestKSearch(search_pose, new_K, pointIdxNKNSearch, pointNKNSquaredDistance) > 0)
            {
                //add rpy
                for(const auto& point_index : pointIdxNKNSearch)
                {
                    auto pt = pose_xy_cloud_.points[point_index];
                    std::string image_name = un_map_pose_name_[std::make_pair(pt.x, pt.y)];
                    double error = rotation_error(un_map_name_R_[image_name], Q);
                    if (error < 0.2)
                    {
                        return image_name;
                    }
                    if (error < min_error)
                    {
                        min_error = error;
                        loop_image_name = image_name;
                    }
                    // std::cout << "image " << image_name << ", rotation error is = " << error << std::endl;
                }
                if(min_error > max_loop_rotation_error_)
                {
                    return "wrong";
                }
            }
        }
        std::cout << "loop final rotation error = " << min_error << std::endl;
        return loop_image_name;
    }
    return "wrong";
}

}
