#ifndef YD_VIS_LOCALIZATION_MAP_
#define YD_VIS_LOCALIZATION_MAP_


#include "map_io.hpp"
#include "../tool/params.hpp"
#include "../tool/util.hpp"


#include <geometry_msgs/Polygon.h>
#include <unordered_map>
#include <glog/logging.h>
#include <pcl/point_types.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <Eigen/Core>

namespace yd_vis_localization
{
typedef std::vector<geometry_msgs::Point32> points;

struct hash_pair { 
    template <class T1, class T2> 
    size_t operator()(const std::pair<T1, T2>& p) const
    { 
        auto hash1 = std::hash<T1>{}(p.first); 
        auto hash2 = std::hash<T2>{}(p.second); 
        return hash1 ^ hash2; 
    } 
}; 


class Map : public Params
{
public:
    Map() = default;
    Map(const std::string& map_file_path);
    bool GetSuperGlue3D2DMatched(const std::string& image_name, const points& pt_uv_id,
                                 std::vector<cv::Point2f>& pts2d, std::vector<cv::Point3f>& pts3d);
    std::string SearchNearImage(const Eigen::Matrix4f pose);
private:
    std::unordered_map<std::string, YdFrame> images_map_;
    pcl::KdTreeFLANN<pcl::PointXYZ> pose_kdtree_;
    std::unordered_map<std::pair<float, float>, std::string, hash_pair> un_map_pose_name_;
    std::unordered_map<std::string, Eigen::Quaterniond> un_map_name_R_;
    pcl::PointCloud<pcl::PointXYZ> pose_xy_cloud_;




};










}















#endif