#include "loop_index_publisher.hpp"

namespace yd_vis_localization{
LoopIndexPublisher::LoopIndexPublisher(ros::NodeHandle& nh, const std::string topic_name, const int buff_size)
{
    publisher_ = nh.advertise<yd_vis_localization::PointMatch>(topic_name, buff_size);
}

void LoopIndexPublisher::Publish(const yd_vis_localization::PointMatch& search_indexs)
{
    publisher_.publish(search_indexs);
}
   
}