#ifndef     PATH_PUBLISHER_CPP_
#define     PATH_PUBLISHER_CPP_
#include "pose_publisher.hpp"

#include <Eigen/Core>
#include <Eigen/Geometry>
#include <nav_msgs/Path.h>

namespace yd_vis_localization{
class PathPublisher
{
public:
    PathPublisher() = default;
    PathPublisher(ros::NodeHandle &nh,
                  const std::string &topic_name,
                  int buff_size);
    void Publish(const Eigen::Matrix4d& path_pose);
    bool HasSubscribers() { return publisher_.getNumSubscribers() != 0; }
private:
    nav_msgs::Path path_;
    ros::Publisher publisher_;

};
}














#endif