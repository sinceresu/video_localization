#ifndef POSE_PUBLISHER_
#define POSE_PUBLISHER_

#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
namespace yd_vis_localization{

class PosePublisher
{
public:
    PosePublisher(ros::NodeHandle& nh, std::string topic_name, int buff_size);
    void Publish(const nav_msgs::Odometry& pose_data);
    bool HasSubscribers() { return publisher_.getNumSubscribers() != 0; }

private:
    ros::Publisher publisher_;
};
}
#endif