#include "path_publisher.hpp"

namespace yd_vis_localization{
PathPublisher::PathPublisher(ros::NodeHandle &nh,
                             const std::string &topic_name,
                             int buff_size)
{
    publisher_ = nh.advertise<nav_msgs::Path>(topic_name, buff_size);
    path_.header.frame_id = "map";

}

void PathPublisher::Publish(const Eigen::Matrix4d& path_pose)
{
    geometry_msgs::PoseStamped pose;
    pose.header.frame_id = "map";

    pose.pose.position.x = path_pose(0,3);
    pose.pose.position.y = path_pose(1,3);
    pose.pose.position.z = path_pose(2,3);

    Eigen::Quaterniond Q = Eigen::Quaterniond(path_pose.block<3,3>(0,0));

    pose.pose.orientation.x = Q.x();
    pose.pose.orientation.y = Q.y();
    pose.pose.orientation.z = Q.z();
    pose.pose.orientation.w = Q.w();

    path_.poses.push_back(pose);
    publisher_.publish(path_);

}
}