#include "pose_publisher.hpp"

namespace yd_vis_localization{
PosePublisher::PosePublisher(ros::NodeHandle& nh, std::string topic_name, const int buff_size)
{
    publisher_ = nh.advertise<nav_msgs::Odometry>(topic_name, buff_size); 
}

void PosePublisher::Publish(const nav_msgs::Odometry& pose_data)
{
    publisher_.publish(pose_data);
}
}