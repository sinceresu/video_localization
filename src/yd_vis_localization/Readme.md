# yd_vis_localization (视觉反定位前端)
提供3hz左右的定位数据


##  订阅话题
```
/vision_localization/match_result [yd_vis_localization::PointMatch] #匹配点对信息
```


## 发布话题
```
/loop_index [yd_vis_localization::PointMatch] #下一需要匹配的关键帧的名字
/vis_pose_path [nav_msgs::Path] #纯视觉定位camera的轨迹
/yd_camera_pose [nav_msgs::Odometry] #纯视觉定位camera的定位
/feature_cloud [sensor_msgs::PointCloud2] #当前帧定位中使用的3d点

```

## 配置参数

|名称|类型|描述|缺省
|--------------------- |-----------|----------------------|------------------|
RansacInitScore|double|执行solvePnPRansac中的reprojectionError值|
loop_xy_kf_num_|int|搜索最近关键帧时，先用距离找到最近的帧数|在最近距离的几帧中再找到姿态角最相似的一帧|
m_intri_matrix|数组|相机内参|
m_distor_matrix|数组|相机畸变参数|
pub_feature|bool|是否发feature点云|


## 启动
```
roslaunch yd_vis_localization online_vis_localization.launch
```