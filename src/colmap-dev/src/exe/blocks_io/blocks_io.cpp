#include "blocks_io.hpp"

namespace blocks_io
{
    BlockIo::BlockIo(const std::string data_path)
    {
        ydBlockDB::setDBPath(data_path);
    }

    void BlockIo::PushData(const std::unordered_map<std::string, YdFrame>& images_map, const std::string las_path, const std::string image_path)
    {
        // ydBlockDB::upload(las_path);
        std::vector<Eigen::Vector3d> pose_datas_save;
        for (const auto& image : images_map)
        {
            int size_3d = 0;
            pose_datas_save.push_back(image.second.transpose);
            for (auto pt : image.second.points)
            {
                if (pt.has_point_3d)
                {
                    size_3d++;
                }
            }
            std::cout << image.second.image_name << " has 3d size " << size_3d << std::endl;
        }

        save_translate("images.txt", pose_datas_save);
        std::vector<Eigen::Vector3d> pose_datas;
        read_translate(pose_datas, "images.txt");
        int i = 0;
        ydUtil::DataReadWriter drw;
        for (const auto& image : images_map)
        {
            auto pose = pose_datas[i];
            ydUtil::BlockIndex bi = ydBlockDB::get(pose[0], pose[1], pose[2]);
            std::string key = "2022_4_19";
            std::string tmp = image.second.toString();
            ydBlockDB::set( bi, key, image.second.toString() );
            //save image file
            std::string image_path = "/media/zmc/Teclast_S20/localization_ws/project/colmap-3.5/python/superglue/images/";
            image_path += image.second.image_name;
            size_t fs = ydUtil::fileSize( image_path.c_str() );
            std::ifstream ifs( image_path, std::ios::in | std::ios::binary );
            std::string s;
            s.resize( fs );
            drw.read(ifs, (unsigned char*)&s[0], fs );
            ydBlockDB::uploadFile(bi, image.second.image_name, s);
            
            //test
            YdFrame test;
            std::string value = ydBlockDB::get(bi, key);
            test.fromString( value );
            //break;
            i++;
        }
        ydBlockDB::flushDB();
        //ydBlockDB::unloadDB();
    }
    void BlockIo::LoadData(std::unordered_map<std::string, YdFrame>& images_map, const std::string key)
    {
        std::vector<Eigen::Vector3d> pose_datas;
        read_translate(pose_datas, "images.txt");
        ydUtil::DataReadWriter drw;
        if (pose_datas.size() == 0)
        {
            std::cout << "please check images.txt path " << std::endl;
        }
        for(const auto& pose_data : pose_datas)
        {
            ydUtil::BlockIndex bi = ydBlockDB::get(pose_data[0], pose_data[1], pose_data[2]);
            YdFrame frame;
            std::string value = ydBlockDB::get(bi, key);
            frame.fromString( value );
            images_map.insert(std::make_pair(frame.image_name, frame)); 
            // load images
            std::string result = ydBlockDB::downloadFile(bi, frame.image_name);
            std::string simpleFileName = osgDB::getSimpleFileName( frame.image_name );
            std::ofstream ofs( simpleFileName, std::ios::out | std::ios::binary );
            drw.write( ofs, (unsigned char*)&result[0], result.size() );
            std::cout << "save image " << simpleFileName << std::endl;

        }
    }
}