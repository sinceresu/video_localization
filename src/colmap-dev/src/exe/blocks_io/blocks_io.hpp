#ifndef COLMAP_BLOCK_IO_
#define COLMAP_BLOCK_IO_


#include <osg/ref_ptr>
#include <osgDB/FileNameUtils>
#include <ydBlockDB/BlockDB.h>
#include <ydUtil/DataReadWriter.h>
#include <ydUtil/Param.h>
#include <ydUtil/console.h>
#include <ydNet/server.h>
#include <ydNet/client.h>
#include <sole.hpp>
#include <unordered_map>

#include "../pre_local/colmap_io.hpp"



namespace blocks_io
{
class BlockIo{
public:
    BlockIo() = default;
    // 设置数据库目录
    BlockIo(const std::string dataset);
    void PushData(const std::unordered_map<std::string, YdFrame>& images_map, const std::string las_path, const std::string image_path);
    void LoadData(std::unordered_map<std::string, YdFrame>& images_map, const std::string key);

};


    
} // namespace blocks_io

#endif