#include "colmap_io.hpp"


using namespace colmap;


void ColmapFileReader::ReadMap(const std::string& colmap_path)
{
    reconstruction_map_.Read(colmap_path);
}

void ColmapFileReader::SaveImagesMapToLmap(std::unordered_map<std::string, YdFrame>& image_map, const std::string& save_path)
{
    std::string image_map_name = "images_map.bin";
    std::ofstream file(image_map_name, std::ios::trunc | std::ios::binary);
    // CHECK(file.is_open()) << image_map_name;

    WriteBinaryLittleEndian<uint64_t>(&file, image_map.size());
    for (auto image : image_map)
    {
        const std::string name = image.first + '\0';
        file.write(name.c_str(), name.size());
        WriteBinaryLittleEndian<float>(&file, image.second.rotation.w());
        WriteBinaryLittleEndian<float>(&file, image.second.rotation.x());
        WriteBinaryLittleEndian<float>(&file, image.second.rotation.y());
        WriteBinaryLittleEndian<float>(&file, image.second.rotation.z());

        WriteBinaryLittleEndian<float>(&file, image.second.transpose.x());
        WriteBinaryLittleEndian<float>(&file, image.second.transpose.y());
        WriteBinaryLittleEndian<float>(&file, image.second.transpose.z());        
        // file.close();

        WriteBinaryLittleEndian<uint64_t>(&file, image.second.points.size());
        int pt_3d_size = 0;
        for (auto pt : image.second.points)
        {
            if (pt.has_point_3d)
            {
                pt_3d_size++;
            }
            WriteBinaryLittleEndian<bool>(&file, pt.has_point_3d);
            WriteBinaryLittleEndian<float>(&file, pt.pt.x);
            WriteBinaryLittleEndian<float>(&file, pt.pt.y);
            WriteBinaryLittleEndian<float>(&file, pt.pt3d.x);
            WriteBinaryLittleEndian<float>(&file, pt.pt3d.y);
            WriteBinaryLittleEndian<float>(&file, pt.pt3d.z);
        }
        std::cout << "image "  << name << " pt is " << image.second.points.size() << std::endl;
        std::cout <<"3d  pt is " << pt_3d_size << std::endl;

        if (image.second.descriptors.size() > 0)
        {
            int descri_size = image.second.descriptors[0].size();
            std::vector<std::vector<double> >describer_ij;
            std::vector<std::vector<double> >scores;
            std::vector<std::vector<double> >points;

            for(size_t i = 0; i < image.second.scores.size(); i++)
            {
                std::vector<double> score;
                std::vector<double> point;
                score.push_back(image.second.scores[i][0]);
                scores.push_back(score);

                point.push_back(image.second.points[i].pt.x);
                point.push_back(image.second.points[i].pt.y);
                points.push_back(point);
            }

            for (int i = 0; i < descri_size; i++)
            {
                std::vector<double> col_descri;
                for (size_t j = 0; j < image.second.descriptors.size(); j++)
                {
                    col_descri.push_back(image.second.descriptors[j][i]);
                }
                describer_ij.push_back(col_descri);
            }
            std::string file_path = image.second.image_name.replace(image.second.image_name.find(".jpg"), 4, "");
            file_path =  save_path + file_path;
            std::string save_descri_path = file_path + "/descriptors.txt";
            std::string save_score_path = file_path + "/scores.txt";
            std::string save_keypoint_path = file_path + "/keypoint.txt";
            if(!boost::filesystem::exists(file_path))
            {
                boost::filesystem::create_directory(file_path);
            }
            save_describer(save_descri_path.c_str(), describer_ij);
            save_describer(save_score_path.c_str(), scores);
            save_describer(save_keypoint_path.c_str(), points);
        }

    }
    std::cout <<"feature map save to " << image_map_name << std::endl;
}

void ColmapFileReader::GetImagesMap(std::unordered_map<std::string, YdFrame>& image_map, const std::string images_descri_path, const bool read_describer)
{
    //读取小吏存储的特征点描述子信息，
    // std::string images_descri_path = "/media/zmc/Teclast_S20/localization_ws/project/colmap-3.5/python/superglue/out_put/image_messages";
    std::unordered_map<std::string, std::vector<std::vector<double> > > descri_map;
    std::unordered_map<std::string, std::vector<std::vector<double> > > score_map;
    if(read_describer)
    {

        std::vector<std::pair<std::string, std::string> > descri_image_names;
        std::vector<std::pair<std::string, std::string> > score_image_names;
        if (!readDescripFiles(images_descri_path, "/descriptors.txt", descri_image_names))
        {
            return;
        }
        if (!readDescripFiles(images_descri_path, "/scores.txt", score_image_names))
        {
            return;
        }
        

        //读取score
        for (const auto& path : score_image_names)
        {
            std::vector<std::vector<double> > score;
            if (!readFeatureDescriptors(path.first.c_str(), score, false))
            {
                return;
            }
            score_map[path.second + ".jpg"] = score;
            std::cout << path.second << " feature score num is " << score.size() << std::endl;
        }
        //读取描述子
        for (const auto& path : descri_image_names)
        {
            std::vector<std::vector<double> > descriptors;
            if (!readFeatureDescriptors(path.first.c_str(), descriptors, true))
            {
                return;
            }
            descri_map[path.second + ".jpg"] = descriptors;
            std::cout << path.second << " feature descriptor num is " << descriptors.size() << std::endl;
        }
    }
    
    auto images = reconstruction_map_.Images();
    // std::unordered_map<std::string, YdFrame> image_map;
    for (const auto& image : images)
    {
        YdFrame frame;
        std::string image_name = image.second.Name();
        frame.image_name = image_name;

        frame.transpose = image.second.ProjectionCenter();
        frame.rotation = image.second.RotationMatrix();
        if(read_describer)
        {
            if (descri_map.count(image_name) > 0)
            {
                frame.descriptors = descri_map[image_name];
            }
            if (score_map.count(image_name) > 0)
            {
                frame.scores = score_map[image_name];
            }
        }

        auto pts_2d = image.second.Points2D();
        int i = 1;
        std::vector<YdPoint2d> points;
        for (const auto& pt_2d : pts_2d)
        {
            YdPoint2d pt;
            pt.pt.x = pt_2d.X();
            pt.pt.y = pt_2d.Y();
            if (reconstruction_map_.ExistsPoint3D(pt_2d.Point3DId()))
            {
                Eigen::Vector3d pt_3d = reconstruction_map_.Point3D(pt_2d.Point3DId()).XYZ();
                cv::Point3f cv_pt_3f = cv::Point3f(pt_3d[0], pt_3d[1], pt_3d[2]);
                pt.has_point_3d = true;
                pt.pt3d = cv_pt_3f;
                // std::cout << "id is: " << i << " has 3d" << std::endl;
            }else{
                pt.has_point_3d = false;
            }
            i++;
            points.push_back(pt);
        }
        std::cout << "image name is : " << image_name << ", 3d num is " << i << std::endl; 

        frame.points = points;
        // image_map[image_name] = frame;  
        image_map.insert(std::make_pair(image_name, frame));  
    }
}