#ifndef _COLMAP_IO_HPP_
#define _COLMAP_IO_HPP_


#include <pcl/kdtree/kdtree_flann.h>
#include <memory>
// #include <hash>
#include <unordered_map>
// #include <geometry_msgs/Polygon.h>
#include <pcl/point_types.h> 
#include <pcl/io/pcd_io.h> 
#include <pcl/filters/frustum_culling.h>
#include <pcl/filters/passthrough.h>

#include "sophus/so3.h"
#include "../../base/reconstruction.h"
#include "../../base/database.h"
#include "../../base/database_cache.h"
#include "../../util/option_manager.h"
#include "../../util/endian.h"

#include "../tool/util.hpp"

struct YdPoint2d
{

    cv::Point2f pt;
    cv::Point3f pt3d;
    bool has_point_3d;
    // double score;
    // int id;

    std::string toString()const
    {
        std::string s;
        s.insert( s.end(), (char*)&has_point_3d, ((char*)&has_point_3d) + sizeof( has_point_3d ) );
        s.insert( s.end(), (char*)&pt, ((char*)&pt) + sizeof( pt ));
        s.insert( s.end(), (char*)&pt3d, ((char*)&pt3d) + sizeof( pt3d ));
        // s.insert( s.end(), (char*)&score, ((char*)&score) + sizeof( score ));
        // s.insert( s.end(), (char*)&id, ((char*)&id) + sizeof( id ) );
        return s;
    }

    void fromString( std::string const& s)
    {
        unsigned int index = 0;

        memcpy( &has_point_3d, &s[index], sizeof( has_point_3d ) );
        index += sizeof( has_point_3d );
        memcpy( &pt, &s[index], sizeof( pt ) );
        index += sizeof( pt );
        memcpy( &pt3d, &s[index], sizeof( pt3d ) );
        index += sizeof( pt3d );
        // memcpy( &score, &s[index], sizeof( score ) );
        // index += sizeof( score );
        // memcpy( &id, &s[index], sizeof( id ) );
        // index += sizeof( id );
    }

    int fromString(const char* s, int strSize)
    {
        unsigned int index = 0;

        memcpy( &has_point_3d, &s[index], sizeof( has_point_3d ) );
        index += sizeof( has_point_3d );
        memcpy( &pt, &s[index], sizeof( pt ) );
        index += sizeof( pt );
        memcpy( &pt3d, &s[index], sizeof( pt3d ) );
        index += sizeof( pt3d );
        return index;
        // memcpy( &score, &s[index], sizeof( score ) );
        // index += sizeof( score );
        // memcpy( &id, &s[index], sizeof( id ) );
        // index += sizeof( id );
    }

};

struct YdFrame
{
    Eigen::Vector3d transpose;
    Eigen::Quaterniond rotation;
    std::vector<YdPoint2d> points;
    std::vector<std::vector<double> > descriptors;
    std::vector<std::vector<double> > scores;
    std::string image_name;

    std::string toString()const
    {
        std::string s;
        s.insert( s.end(), (char*)&transpose, ((char*)&transpose) + sizeof( transpose ));
        s.insert( s.end(), (char*)&rotation, ((char*)&rotation) + sizeof( rotation ) );
        size_t name_len = image_name.length();
        s.insert(s.end(), (char*)&name_len, ((char*)&name_len) + sizeof( name_len ) );
        s.insert( s.end(), (char*)&image_name[0], ((char*)&image_name[0]) +  image_name.length() );
        int descri_length = descriptors.size();
        s.insert( s.end(), (char*)&descri_length, ((char*)&descri_length) + sizeof( descri_length ) );
        for (int i = 0; i < descri_length; i++)
        {
            int descri_i_length = descriptors[i].size();
            s.insert( s.end(), (char*)&descri_i_length, ((char*)&descri_i_length) + sizeof( descri_i_length ) );
            for(int j = 0; j < descri_i_length; j++)
            {
                s.insert( s.end(), (char*)&descriptors[i][j], ((char*)&descriptors[i][j]) + sizeof( descriptors[i][j] ) );
            }
        }

        int scores_length = scores.size();
        s.insert( s.end(), (char*)&scores_length, ((char*)&scores_length) + sizeof( scores_length ) );
        for (int i = 0; i < scores_length; i++)
        {
            s.insert( s.end(), (char*)&scores[i][0], ((char*)&scores[i][0]) + sizeof( scores[i][0] ) );
        }

        s.insert( s.end(), (char*)&descri_length, ((char*)&descri_length) + sizeof( descri_length ) );
        for (int i = 0; i < descri_length; i++)
        {
            s += (points[i].toString() );
        }

        return s;
    }

    void fromString( std::string const& s )
    {
        unsigned int index = 0;

        memcpy( &transpose, &s[index], sizeof( transpose ) );
        index += sizeof( transpose );
        memcpy( &rotation, &s[index], sizeof( rotation ) );
        index += sizeof( rotation );
        size_t name_len = 0;
        memcpy( &name_len, &s[index], sizeof( name_len ) );
        image_name.resize( name_len );
        index += sizeof( name_len );
        memcpy( &image_name[0], &s[index], name_len );
        index += name_len;

        int descri_length;
        memcpy( &descri_length, &s[index], sizeof( descri_length ) );
        index += sizeof( descri_length );
        for(int i = 0; i < descri_length; i++)
        {
            int descri_i_length;
            memcpy( &descri_i_length, &s[index], sizeof( descri_i_length ) );
            index += sizeof( descri_i_length );
            std::vector<double> descri_i;
            for(int j = 0; j < descri_i_length; j++)
            {
                double descri_ij;
                memcpy( &descri_ij, &s[index], sizeof( descri_ij ) );
                index += sizeof( descri_ij );
                descri_i.push_back(descri_ij);
            }
            descriptors.push_back(descri_i);
        }


        int scores_length;
        memcpy( &scores_length, &s[index], sizeof( scores_length ) );
        index += sizeof( scores_length );
        for(int i = 0; i < scores_length; i++)
        {
            double score_i;
            std::vector<double> score_i_vec;
            memcpy( &score_i, &s[index], sizeof( score_i ) );
            index += sizeof( score_i );
            score_i_vec.push_back(score_i);
            scores.push_back(score_i_vec);
        }
        int points_length;
        memcpy( &points_length, &s[index], sizeof( points_length ) );
        index += sizeof( points_length );
        for(int i = 0; i < points_length; i++)
        {
            YdPoint2d tmp_2d;
            index += tmp_2d.fromString(&s[index], s.size() - index);
            // tmp_2d.fromString(&s[index]);
            //index += sizeof( tmp_2d );
            points.push_back(tmp_2d);
        }
        
    }

};


using namespace colmap;

struct hash_pair { 
    template <class T1, class T2> 
    size_t operator()(const std::pair<T1, T2>& p) const
    { 
        auto hash1 = std::hash<T1>{}(p.first); 
        auto hash2 = std::hash<T2>{}(p.second); 
        return hash1 ^ hash2; 
    } 
}; 

class ColmapFileReader
{
  public:
    ColmapFileReader()
    {

    };
    void ReadMap(const std::string& colmap_path);

    void GetImagesMap(std::unordered_map<std::string, YdFrame>& image_map, const std::string images_descri_path, const bool read_describer = true);
    void SaveImagesMapToLmap(std::unordered_map<std::string, YdFrame>& image_map, const std::string& save_path);
    void CutPointIndexCloud(const Eigen::Matrix4f& pose);
    std::vector<std::string> FindLocalFrame();

    void ReadTwoMap(const std::string& colmap1_path, const std::string& colmap2_path);
    //获取指定关键帧的3D点
    void GetFrame3DPoint3(std::string frame_name, std::vector<Eigen::Vector3d>& src, std::vector<Eigen::Vector3d>& dst);


  private:
    colmap::Reconstruction reconstruction_map_;

    pcl::PointCloud<pcl::PointXYZ> pose_xy_cloud_;
    std::unordered_map<std::pair<float, float>, std::string, hash_pair> un_map_pose_name_;
    std::unordered_map<std::string, Eigen::Quaterniond> un_map_name_R_;
    pcl::KdTreeFLANN<pcl::PointXYZ> pose_kdtree_;
};








#endif