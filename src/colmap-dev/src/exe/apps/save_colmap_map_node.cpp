#include "../pre_local/colmap_io.hpp"
#include "../blocks_io/blocks_io.hpp"

/*
程序功能：读取colmap建图结果和super point相关信息，上传至block。
./save_colmap_map_node colmap地图地址 superpoint信息地址 block存储地址  las文件 要保存的图像地址

./save_colmap_map_node 
/media/zmc/Teclast_S20/localization_ws/project/colmap-3.5/python/superglue/images/sparse 
/media/zmc/Teclast_S20/localization_ws/project/colmap-3.5/python/superglue/out_put/image_messages
*/

int main(int argc, char** argv)  
{
    std::shared_ptr<ColmapFileReader> colmap_file_reader_ptr_ = std::make_shared<ColmapFileReader>();
    colmap_file_reader_ptr_->ReadMap(argv[1]);
    std::unordered_map<std::string, YdFrame> image_map;
    colmap_file_reader_ptr_->GetImagesMap(image_map, argv[2], false);
    colmap_file_reader_ptr_->SaveImagesMapToLmap(image_map, argv[3]);
    return 1;
}