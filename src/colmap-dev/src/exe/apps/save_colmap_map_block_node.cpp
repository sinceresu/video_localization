#include "../pre_local/colmap_io.hpp"
#include "../blocks_io/blocks_io.hpp"

/*
程序功能：读取colmap建图结果和super point相关信息，上传至block。
./save_colmap_map_node colmap地图地址 superpoint信息地址 block存储地址  las文件 要保存的图像地址

./save_colmap_map_node 
/media/zmc/Teclast_S20/localization_ws/project/colmap-3.5/python/superglue/images/sparse 
/media/zmc/Teclast_S20/localization_ws/project/colmap-3.5/python/superglue/out_put/image_messages
 /home/zmc/Desktop/match0225 
/home/zmc/Desktop/map.las 
 /media/zmc/Teclast_S20/localization_ws/project/colmap-3.5/python/superglue/images/
*/

int main(int argc, char** argv)  
{
    std::shared_ptr<ColmapFileReader> colmap_file_reader_ptr_ = std::make_shared<ColmapFileReader>();
    colmap_file_reader_ptr_->ReadMap(argv[1]);
    std::unordered_map<std::string, YdFrame> image_map;
    colmap_file_reader_ptr_->GetImagesMap(image_map, argv[2]);
    // colmap_file_reader_ptr_->SaveImagesMapToLmap(image_map);
    std::shared_ptr<blocks_io::BlockIo> block_io_ptr = std::make_shared<blocks_io::BlockIo>(argv[3]);
    block_io_ptr->PushData(image_map, argv[4], argv[5]);    
    return 1;
}