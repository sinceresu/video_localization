#include "../../base/database.h"
#include "../tool/util.hpp"
#include "../../estimators/two_view_geometry.h"

#include <map>
/*
程序功能： 读取通过super glue产生的匹配对，把匹配对信息写入到db文件中。
整个替换sift到superpoint过程分两步， python(readkeypoint_match.py)执行两部分，分别是写入特征点和match，
本文件负责写入two_view_geometry，image name 和 image_id

后面参数为superglue文件目录
./write_match_node /media/zmc/Teclast_S20/localization_ws/project/colmap-dev/python
*/

using namespace colmap;

class WriteFeatureData
{
  public:
    WriteFeatureData(const std::string db_file_path, const std::string image_name_id_file_path,
                     const std::string matches_path, const std::string keypoints_path)
    :db_file_path_(db_file_path), image_name_id_file_path_(image_name_id_file_path),
     matches_path_(matches_path), keypoints_path_(keypoints_path)
    {
      database_.Open(db_file_path_);
    }
    void WriteImages();
    void WriteDataKeypoints();
    void WriteDataMatchesAndTwoViewGeometry();
    void RunSave();
    // void WriteTwoViewGeometrys();
  
  private:
    Database database_;
    std::string db_file_path_;
    std::string image_name_id_file_path_;
    std::string matches_path_;
    std::string keypoints_path_;
    std::map<uint32_t, std::string> image_name_id_map_;
    std::map<std::string, uint32_t> image_name_id_map2_;

};

void WriteFeatureData::RunSave()
{
  std::cout << "====start writr images......====" << std::endl;
  WriteImages();
  std::cout << "====finish writr images ====" << std::endl;
  //不可用和python的有区别
  // std::cout << "====start writr Keypoints......====" << std::endl;
  // WriteDataKeypoints();
  // std::cout << "====finish writr Keypoints ====" << std::endl;

  std::cout << "====start writr match and TwoViewGeometry......====" << std::endl;
  WriteDataMatchesAndTwoViewGeometry();
  std::cout << "====finish writr match and TwoViewGeometry====" << std::endl;
}

void WriteFeatureData::WriteImages()
{
  readImageNameId(image_name_id_file_path_.c_str(), image_name_id_map_);
  Camera camera;
  camera.InitializeWithName("SIMPLE_PINHOLE", 1.0, 1, 1);
  // std::cout <<"database.WriteCamera(camera) is " << database_.WriteCamera(camera) << std::endl;
  camera.SetCameraId(database_.WriteCamera(camera));
  for (const auto& name_id : image_name_id_map_)
  {
/*     Image image;
    image.SetName(name_id.second + ".jpg");
    image.SetCameraId(camera.CameraId());
    // image.SetQvecPrior(Eigen::Vector4d(0.1, 0.2, 0.3, 0.4));
    // image.SetTvecPrior(Eigen::Vector3d(0.1, 0.2, 0.3));
    image.SetImageId(name_id.first);
    std::cout << name_id.first << std::endl;

    // image2.SetName("test2");
    // image2.SetImageId(image.ImageId() + 1);
    // image.SetImageId(database_.WriteImage(image));
    database_.WriteImage(image, true); */
    
    
    
    
    Image image;
    image.SetName(name_id.second + ".jpg");
    image.SetCameraId(camera.CameraId());
    image.SetImageId(name_id.first);
    // std::cout << image.CameraId() << "  " << image.ImageId() << " " << image.Name() << std::endl;
    database_.WriteImage(image, true);
    image_name_id_map2_[name_id.second + ".txt"] = name_id.first;
  }
}

void WriteFeatureData::WriteDataKeypoints()
{
  std::vector<std::string> keypoints_names;
  readMatchFiles(keypoints_path_.c_str(), keypoints_names);
  for (const auto& keypoints_name : keypoints_names)
  {
    uint32_t image_id = image_name_id_map2_[keypoints_name];
    std::vector<std::pair<float, float>> tmp_keypoints;
    FeatureKeypoints fkps;
    if(!readImageKeypoints(((keypoints_path_ + "/" + keypoints_name)).c_str(), tmp_keypoints))
      return;
    for (const auto& kp : tmp_keypoints)
    {
      FeatureKeypoint fkp(kp.first, kp.second);
      fkps.push_back(fkp);
    }
    database_.WriteKeypoints(image_id, fkps);
  }
}

void WriteFeatureData::WriteDataMatchesAndTwoViewGeometry()
{
  std::vector<std::string> matchs_names;
  readMatchFiles(matches_path_.c_str(), matchs_names);
  for (const auto& matchs_name : matchs_names)
  {
    image_t image_id1;
    image_t image_id2;
    std::vector<std::pair<uint32_t, uint32_t>> matches;
    std::vector<FeatureMatch> FeatureMatches;
    readMatches((matches_path_ + "/" + matchs_name).c_str(), matches, image_id1, image_id2);

    for (const auto& match : matches)
    {
        FeatureMatch fea_match(match.first, match.second);
        FeatureMatches.push_back(fea_match);
    }
    //写入match
    // database_.WriteMatches(image_id1, image_id2, FeatureMatches);

    //写入two_view_geometry
    TwoViewGeometry two_view_geometry;
    
    two_view_geometry.inlier_matches = FeatureMatches;
    two_view_geometry.config = 2;
    two_view_geometry.F = Eigen::Matrix3d::Identity();
    two_view_geometry.E = Eigen::Matrix3d::Identity();
    two_view_geometry.H = Eigen::Matrix3d::Identity();
    database_.WriteTwoViewGeometry(image_id1, image_id2, two_view_geometry);
  }
}


void writeImages(Database& database, std::string image_name_id_map_file)
{
/*   std::vector<std::pair<uint32_t, std::string>> image_name_id_map;
  readImageNameId(image_name_id_map_file.c_str(), image_name_id_map);
  Camera camera;
  camera.InitializeWithName("SIMPLE_PINHOLE", 1.0, 1, 1);
  std::cout <<"database.WriteCamera(camera) is " << database.WriteCamera(camera) << std::endl;
  camera.SetCameraId(database.WriteCamera(camera));
  for (const auto& name_id : image_name_id_map)
  {
    Image image;
    // const uint32_t cam_id = 1;
    // camera.SetCameraId(database.WriteCamera(camera, true));
    // camera.SetCameraId(cam_id);

    image.SetName(name_id.second + ".jpg");
    image.SetImageId(name_id.first);
    image.SetCameraId(camera.CameraId());
    database.WriteImage(image);
  }
  // image.SetCameraId(camera.CameraId());
  // image.SetQvecPrior(Eigen::Vector4d(0.1, 0.2, 0.3, 0.4));
  // image.SetTvecPrior(Eigen::Vector3d(0.1, 0.2, 0.3)); */
}

int main(int argc, char** argv) 
{
  // std::string image_name_id_file = "/media/zmc/zmc/workspace/other/to_zhoumingchao/Re_02/frame_index_name.txt";
  // // std::string db_path = "/media/zmc/zmc/fuwuqidata/mult_camera_down/rear_image/writesuper.db";
  // std::string db_path = argv[1];
  // Database database(db_path);
  // std::string match_path = argv[2];
  // std::vector<std::string> match_names;
  // readMatchFiles(match_path.c_str(), match_names);

  // writeImages(database,image_name_id_file);

  // for (const auto& match_name : match_names)
  // {
  //   image_t image_id1;
  //   image_t image_id2;
  //   std::vector<std::pair<uint32_t, uint32_t>> matches;
  //   std::vector<FeatureMatch> FeatureMatches;
  //   std::cout << "match txt file name is: " << (match_path + match_name) << std::endl;
  //   readMatches((match_path + "/" + match_name).c_str(), matches, image_id1, image_id2);
  //   for (const auto& match : matches)
  //   {
  //       FeatureMatch fea_match(match.first, match.second);
  //       FeatureMatches.push_back(fea_match);
  //   }
  //   TwoViewGeometry two_view_geometry;
    
  // //   FeatureMatch 
  //   two_view_geometry.inlier_matches = FeatureMatches;
  //   two_view_geometry.config = 2;
  //   two_view_geometry.F = Eigen::Matrix3d::Identity();
  //   two_view_geometry.E = Eigen::Matrix3d::Identity();
  //   two_view_geometry.H = Eigen::Matrix3d::Identity();
  //   database.WriteTwoViewGeometry(image_id1, image_id2, two_view_geometry);
  // }
  std::string super_glue_path = argv[1];
  std::string db_file_path = super_glue_path + "/superglue/images/write_super.db";
  std::string image_name_id_file_path = super_glue_path + "/superglue/out_put/frame_index_name.txt";
  std::string matches_path = super_glue_path + "/superglue/out_put/match";
  std::string keypoints_path = super_glue_path + "/superglue/out_put/keypoints";
  std::unique_ptr<WriteFeatureData> write_feature_data_ptr_(new WriteFeatureData(db_file_path, image_name_id_file_path, matches_path, keypoints_path));
  write_feature_data_ptr_->RunSave();

  return 1;
}