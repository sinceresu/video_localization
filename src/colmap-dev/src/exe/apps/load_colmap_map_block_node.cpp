#include "../pre_local/colmap_io.hpp"
#include "../blocks_io/blocks_io.hpp"
#include <unordered_map>
#include <ydBlockDB/BlockDB.h>

/*
程序功能：读取blocks中的地图信息，将定位需要信息保存为二进制地图，将匹配需要信息保存为txt。
./load_colmap_map_node block地址 图像地址 block_key 
./load_colmap_map_node /home/zmc/Desktop/match0225 "2022_4_19" /media/zmc/Teclast_S20/localization_ws/project/colmap-3.5/python/superglue/out_put/test
*/

int main(int argc, char** argv)
{
    std::shared_ptr<ColmapFileReader> colmap_file_reader_ptr_ = std::make_shared<ColmapFileReader>();
    std::shared_ptr<blocks_io::BlockIo> block_io_ptr = std::make_shared<blocks_io::BlockIo>(argv[1]);
    std::unordered_map<std::string, YdFrame> images_map;
    block_io_ptr->LoadData(images_map, argv[2]);
    colmap_file_reader_ptr_->SaveImagesMapToLmap(images_map, argv[3]);


    return 1;
}