#coding:utf-8
from mpl_toolkits.mplot3d import axes3d
import matplotlib.pyplot as plt
import numpy as np


def deale_data():
	x = []
	y = []
	z = []
	xx = []
	yy = []
	zz = []
	f = open("pic_pose.txt")
	ff = open("lidar.txt")
	line = f.readline()
	line_lidar = ff.readline()
	while line:
		c,d,e = line.split()
		x.append(c)
		y.append(d)
		z.append(e)
		line = f.readline()
	f.close()

	while line_lidar:
		c,d,e = line_lidar.split()
		xx.append(c)
		yy.append(d)
		zz.append(e)
		line_lidar = ff.readline()
	ff.close()
	
	#string型转int型
	x = [ float( x ) for x in x if x ]
	y = [ float( y ) for y in y if y ]
	z = [ float( z ) for z in z if z ]

	xx = [ float( xx ) for xx in xx if xx ]
	yy = [ float( yy ) for yy in yy if yy ]
	zz = [ float( zz ) for zz in zz if zz ]

	i = 0
	all_error = 0
	all_error_x = 0
	all_error_y = 0
	all_error_z = 0
	for x_ele in x:
		error = 0
		error_x = abs(x_ele - xx[i])
		error_y = abs(y[i] - yy[i])
		error_z = abs(z[i] - zz[i])
		error = pow((x_ele - xx[i]),2) + pow((y[i] - yy[i]),2) + pow((z[i] - zz[i]),2)
		error = np.sqrt(error)
		print (error)
		print("================================")
		print (abs(error_x))
		print (abs(error_y))
		print (abs(error_z))
		print("================================")
		i = i + 1
		all_error = error +  all_error
		all_error_x = all_error_x + error_x
		all_error_y = all_error_y + error_y
		all_error_z = all_error_z + error_z
	average_error = all_error / i
	print ("average error is %f" %average_error)
	print ("average x error is %f" %(all_error_x/i))
	print ("average y error is %f" %(all_error_y/i))
	print ("average z error is %f" %(all_error_z/i))



def test(input_num):
	print("input_num")
	return input_num

if __name__ == '__main__':
	test(6)
        deale_data()


# print x[1]
# fig=plt.figure()
# ax = fig.gca(projection = '3d')
 
# ax.plot(x,y,z)
# ax.plot(xx,yy,zz)
 
# ax.set_xlabel('x')
# ax.set_ylabel('y')
# ax.set_zlabel('z')
# plt.show()
