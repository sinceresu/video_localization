#coding:utf-8
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d.axes3d import Axes3D
 
x = []
y = []
z = []

xx = []
yy = []
zz = []

f = open("pic_pose.txt")
# ff = open("lidar.txt")
line = f.readline()
while line:
	c,d,e = line.split()
	x.append(c)
	y.append(d)
	z.append(e)
	
	
	line = f.readline()
	
f.close()
 
#string型转int型
x = [ float( x ) for x in x if x ]
y = [ float( y ) for y in y if y ]
z = [ float( z ) for z in z if z ]
 
print x
fig=plt.figure()
ax=Axes3D(fig)
ax.scatter3D(x, y, z)
ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_zlabel('z')
plt.show()
