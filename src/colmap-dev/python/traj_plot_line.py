#coding:utf-8
from mpl_toolkits.mplot3d import axes3d
import matplotlib.pyplot as plt
 
x = []
y = []
z = []
xx = []
yy = []
zz = []
f = open("image_pose.txt_centor_pose.txt")
ff = open("images_yd.txt_centor_pose.txt")
line = f.readline()
line_lidar = ff.readline()
while line:
	c,d,e = line.split()
	x.append(c)
	y.append(d)
	z.append(e)
	
	line = f.readline()
	
f.close()

while line_lidar:
	c,d,e = line_lidar.split()
	xx.append(c)
	yy.append(d)
	zz.append(e)
	
	line_lidar = ff.readline()
	
ff.close()
 
#string型转int型
x = [ float( x ) for x in x if x ]
y = [ float( y ) for y in y if y ]
z = [ float( z ) for z in z if z ]

xx = [ float( xx ) for xx in xx if xx ]
yy = [ float( yy ) for yy in yy if yy ]
zz = [ float( zz ) for zz in zz if zz ]

for x_ele in x:
        print(x_ele) 
 
#print x
fig=plt.figure()
ax = fig.gca(projection = '3d')
 
ax.plot(x,y,z)
ax.plot(xx,yy,zz)
 
ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_zlabel('z')
plt.show()
