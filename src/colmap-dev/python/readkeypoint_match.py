# -*- coding:utf-8 -*-
import numpy as np
import sqlite3
import cv2
import sys
import os, os.path as osp

'''
文件分为两个功能：
1，(import_feature)读取superglue提取的特征点，写入到DB文件
   注意几个文件：frame_index_name.txt存储提前的时候imagename和imageid的对应关系，之后所有数据统一此数据关系
               keypoints_path是superpoint提取的所有特征点文件集合
               db文件 = db_path
2,add_match函数读取匹配信息，match文件夹下放的是所有顺序匹配的txt文件，文件第一行是对应imageid

注意： scale = 4.0 是superglue提取特征点时候对图像的缩放比例的倒数

执行：python readkeypoint_match.py *.db
'''



# def import_feature(images, paths):
def import_feature(db_path, keypoints_path):
    scale = 1.0
    print("DB file is:", db_path)
    
    #读imageid和imagename 映射
    # f = open("/media/zmc/Teclast_S20/localization_ws/project/superglue/out_put/frame_index_name.txt")
    f = open("superglue/out_put/frame_index_name.txt")
    line = f.readline()
    map_id_name = {}
    while line:
        image_id, image_name = line.split()
        # map_id_name[(int)(image_id)+1] = image_name + ".txt"
        map_id_name[image_name + ".txt"] = (int)(image_id)+1
        line = f.readline()
    print(map_id_name.items())
    
    connection = sqlite3.connect(db_path)
    cursor = connection.cursor()

    cursor.execute("DELETE FROM keypoints;")
    # cursor.execute("DELETE FROM descriptor;")
    cursor.execute("DELETE FROM matches;")
    connection.commit()

    print('Importing features...')

    # keypoints_path = "/media/zmc/Teclast_S20/localization_ws/project/superglue/out_put/keypoints"
    keypoints_path = "superglue/out_put/keypoints"
    keypoint_paths = os.listdir(keypoints_path)
    keypoint_paths = sorted(keypoint_paths)
    # image_id = 1
    for keypoint_path in keypoint_paths:
        kps = []
        image_id = map_id_name[keypoint_path]
        keypoint_path = osp.join(keypoints_path,keypoint_path)
        # print("keypoint path is:", keypoint_path)


        f = open(keypoint_path)
        line = f.readline()
        while line:
            x, y = line.split()
            kp = []
            kp.append(scale * (float)(x))
            kp.append(scale * (float)(y))
            kps.append(kp)

            line = f.readline()
        f.close()
        print(len(kps))
        print(len(kps[0]))
        print("image_id is: ",image_id)

        kps = np.array(kps)
        n_keypoints = kps.shape[0]
        kps = np.concatenate([kps, np.ones((n_keypoints, 1)), np.zeros((n_keypoints, 1))], axis=1).astype(np.float32)
        print(kps.shape[0])
        print(kps.shape[1])
        kps_str = kps.tostring()
        cursor.execute("INSERT INTO keypoints(image_id, rows, cols, data) VALUES(?, ?, ?, ?);", 
        (image_id, kps.shape[0],kps.shape[1],kps_str))
        print("image_id is: ",image_id)
    

    connection.commit()

    cursor.close()
    connection.close()

def image_ids_to_pair_id(image_id1, image_id2):
    if image_id1 > image_id2:
        return 2147483647 * image_id2 + image_id1
    else:
        return 2147473647 * image_id1 + image_id2


def add_match(image1_id, image2_id):
    # assert(len(matches.shape) == 2)
    # assert(matches.shape[1] == 2)

    # connection = sqlite3.connect("/media/zmc/Teclast_S20/localization_ws/project/superglue/images/write_super.db")
    connection = sqlite3.connect("superglue/images/write_super.db")
    connection.text_factory = str
    cursor = connection.cursor()

    print("Matching....")

    # matchs_path = "/media/zmc/Teclast_S20/localization_ws/project/superglue/out_put/match"
    matchs_path = "superglue/out_put/match"
    match_paths = os.listdir(matchs_path)
    match_paths = sorted(match_paths)

    for match_path in match_paths:
        kps = []
        keypoint_path = osp.join(matchs_path,match_path)
        print("keypoint path is:", keypoint_path)
        
        f = open(keypoint_path)
        matches = []
        line = f.readline()
        line_num = 1
        # pair_id = image_ids_to_pair_id(image1_id, image2_id)

        while line:
            if line_num == 1:
                image1_id, image2_id = line.split()
                # print("image1 id is ", image1_id)
                # print("image2 id is ", image2_id)
                # print("pair_id is:", pair_id)
            else:
                pt1_id, x, y, pt2_id, x1, y1 = line.split()
                match = []
                match.append(pt1_id)
                match.append(pt2_id)
                matches.append(match)
                # print("match id1", pt1_id)
                # print("match id2", pt2_id)
            line_num = line_num + 1
            line = f.readline()
        
        matches = np.array(matches)
        n_keypoints = matches.shape[0]
        matches = np.asarray(matches, np.uint32)
        print("image1 id is ", image1_id)
        print("image2 id is ", image2_id)
        print("matches shape", matches.shape[0])
        print("matches shape", matches.shape[1])
        pair_id = image_ids_to_pair_id((int)(image1_id), (int)(image2_id))
        print("pair_id is:", pair_id)
        matches_str = matches.tostring()
        cursor.execute("INSERT INTO matches(pair_id, rows, cols, data) VALUES(?,?,?,?);",
                      (pair_id, matches.shape[0], matches.shape[1], matches_str))

    connection.commit()
    cursor.close()
    connection.close()

if __name__=="__main__":
    import_feature(sys.argv[1], sys.argv[1])
    print('finish write features in db')
    add_match(1,2)
    print('finish write match in db')

