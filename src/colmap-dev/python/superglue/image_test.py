#! /usr/bin/env python3


import cv2
import time

def image_test():
    image_path1 = "/home/lxr/Downloads/Re_02/undistorsource.jpg"
    image1 = cv2.imread(image_path1)
    h, w = image1.shape[:2]
    w = int(w / 2)
    h = int(h / 2)
    image1 = image1[(3040-1710):, :, ]
    cv2.imshow('SuperGlue matches', image1)
    cv2.imwrite('/home/lxr/Downloads/Re_02/cut.jpg', image1)

if __name__ == '__main__':
    image_test()
