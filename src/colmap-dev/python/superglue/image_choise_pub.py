import rospy
import cv2
import numpy as np
import time
from vision_localization_msgs.msg import PointMatch

rospy.init_node('image_choise_pub')
single_image = rospy.Publisher(
    "/vision_localization/image_choise", PointMatch, queue_size=1)

while True:
    msg = PointMatch()
    msg.img_ids.append('01.jpg')
    msg.img_ids.append('03.jpg')
    single_image.publish(msg)
    time.sleep(0.01)
