#! /usr/bin/env python3
#-*- coding: UTF-8 -*- 
'''
2021-07-23:整理文件以发送给周明超。
功能及使用说明：本脚本实现两个功能：１．读取文件夹下的图片，生成特征点等文件保存本地。２．读取本地特征点等文件，生成匹配信息。
配置参数：class analyse_image的初始化函数中的各项参数（阈值thresh、输入尺寸等）；
　　　　　cal_save_data函数中的image_dir（功能１的路径）
　　　　　cal_match_from_local函数中的image_path0，image_path1（功能２的路径）
使用说明：main函数中注释的两行，分别取消注释即可。

2021-07-29:
更新说明：
１．重命名cal_save_image为cal_save_data；
２．新增函数，功能：实现输入pred，得到匹配图片显示并保存本地，得到txt文件保存本地；
3. 修复bug:pred中加上data0。

补充说明：最终生成的匹配信息，根据个人需要进行使用。
'''
from pathlib import Path
import argparse
import cv2
import matplotlib.cm as cm
import torch
import sys

from models.matching import Matching
from models.utils import (AverageTimer, VideoStreamer,
                          make_matching_plot_fast, frame2tensor)

torch.set_grad_enabled(False)

import time
import os, os.path as osp
import numpy as np


class analyse_image:
    def __init__(self):
        '''
        device:cuda/cpu
        '''
        self.device = 'cuda'
        config = {
            'superpoint': {
                'nms_radius': 4,
                'keypoint_threshold': 0.0005,
                'max_keypoints': -1
            },
            'superglue': {
                'weights': 'indoor',
                'sinkhorn_iterations': 20,
                # 'sinkhorn_iterations': 10,
                'match_threshold': 0.2,
            }
        }
        self.matching = Matching(config).eval().to(self.device)
        self.keys = ['keypoints', 'scores', 'descriptors']
        # self.resize_w = 1440
        # self.resize_h = 1440
        # self.resize_scale = 0.50
        # self.resize_scale_right = 0.50
        # self.resize_scale_left = 0.50

        # self.resize_w = 1280
        # self.resize_h = 720
        # self.resize_scale = 1.0
        # self.resize_scale_right = 1.0
        # self.resize_scale_left = 1.0

        self.resize_w = 720
        self.resize_h = 720
        self.resize_scale = 0.25
        self.resize_scale_right = 0.25
        self.resize_scale_left = 0.25




        # self.resize_w =  1440 #960#3680 # 2880
        # self.resize_h = 1440 #540#2456 # 1620

    def fram_to_tensor(self, frame):
        frame_tensor = frame2tensor(frame, self.device)
        return frame_tensor

    def resize_image(self, input_image):
        '''
        input_image:cv2的mat数据
        function:完成图片resize和转灰度图
        '''
        # input_image = cv2.resize(input_image, (self.resize_scale * input_image.shape[1], self.resize_scale * input_image.shape[0]),
        #                          interpolation=cv2.INTER_AREA)
        height, width, channels = input_image.shape
        input_image = cv2.resize(input_image, ((int)(width * self.resize_scale), (int)(height * self.resize_scale)),
                                 interpolation=cv2.INTER_AREA)
        input_image = cv2.cvtColor(input_image, cv2.COLOR_RGB2GRAY)
        return input_image

    def cal_save_data(self):
        '''
        遍历image_dir目录下的所有jpg图片，每一张图片（xx.jpg）会创建一个对应的xx文件夹，文件夹中保存keypoints、scores、descriptors三个npy文件。
        '''

        def get_image_paths(image_dir):
            image_paths = os.listdir(image_dir)
            image_paths = sorted(image_paths)
            return image_paths

        def cal_keypoints(input_image):
            input_image = self.resize_image(input_image)
            frame_tensor = self.fram_to_tensor(input_image)
            data = self.matching.superpoint({'image': frame_tensor})
            return data

        def save_index_name(index, faile_name):
            with open("out_put/frame_index_name.txt", "a") as f:
                input_line = str(index) + " " + str(faile_name) + "\n"
                f.write(input_line)


        def save_keypoints(data, image_path, file_name):
            '''
            {
            'keypoints': keypoints,
            'scores': scores,
            'descriptors': descriptors,
            }
            '''
            # save_path = image_path.replace('.jpg', '/')
            save_path = 'out_put/image_messages/'
            path_name = file_name.replace('.jpg', '/')
            save_path = osp.join(save_path, path_name)
            keypoint_txt_path = file_name.replace('.jpg','.txt')
            if not osp.exists(save_path):
                os.makedirs(save_path)
            keypoints = data['keypoints'][0].cpu().numpy()
            np.save(osp.join(save_path, 'keypoint.npy'), keypoints)
            np.savetxt(osp.join(save_path, 'keypoint.txt'), keypoints)
            scores = data['scores'][0].cpu().numpy()
            np.save(osp.join(save_path, 'scores.npy'), scores)
            np.savetxt(osp.join(save_path, 'scores.txt'), scores)
            descriptors = data['descriptors'][0].cpu().numpy()
            np.save(osp.join(save_path, 'descriptors.npy'), descriptors)
            np.savetxt(osp.join(save_path, 'descriptors.txt'), descriptors)
            #save keypoints txt
            keypoints_size = len(keypoints)
            # with open(self.test_out_dir + ".txt", "w") as f:
            # print("out_put/keypoints/" + keypoint_txt_path)
            with open("out_put/keypoints/" + keypoint_txt_path, "w") as f:
                for i in range(keypoints_size):
                    input_line = str(keypoints[i][0] / self.resize_scale_left) + " " + str(keypoints[i][1] / self.resize_scale_left) + "\n"
                    f.write(input_line)


        # image_dir = '/media/zmc/zmc/workspace/other/to_zhoumingchao/Re_02'
        # image_dir = '/media/zmc/84FE-CF74/0817garden/map_images'
        # image_dir = '/media/zmc/Teclast_S20/bag/zoulang/map/jpg'
        # image_dir = '/media/zmc/zmc/workspace/other/to_zhoumingchao/Re_02'
        image_dir = 'images'
        image_paths = get_image_paths(image_dir)
        frame_index = 0
        relation_path = "out_put/frame_index_name.txt"
        if os.path.exists(relation_path):    
            os.remove(relation_path)
        
        if len(image_paths) == 0:
            print(image_path, "not find jpg files")

        for image_path in image_paths:
            if not image_path.endswith('jpg'):
                continue
            image_path_long = osp.join(image_dir, image_path)
            image = cv2.imread(image_path_long)
            if image is None:
                print('image path error or image error')
                continue
            data = cal_keypoints(image)
            save_keypoints(data, image_path_long, image_path)
            image_path = image_path.replace('.jpg','')
            save_index_name(frame_index, image_path)
            print(image_path)
            frame_index = frame_index + 1

        # image_dir = 'images/front'
        # image_paths = get_image_paths(image_dir)
        # for image_path in image_paths:
        #     if not image_path.endswith('jpg'):
        #         continue
        #     image_path_long = osp.join(image_dir, image_path)
        #     image = cv2.imread(image_path_long)
        #     if image is None:
        #         print('image path error or image error')
        #         continue
        #     data = cal_keypoints(image)
        #     save_keypoints(data, image_path_long, image_path)
        #     image_path = image_path.replace('.jpg','')
        #     save_index_name(frame_index, image_path)
        #     frame_index = frame_index + 1

    def cal_match_from_local(self, image_path0, image_path1):
        '''
        输入两张图片的路径（xx.jpg）会寻找对应的xx文件夹，
        文件夹中保存keypoints、scores、descriptors三个npy文件。
        读取并进行特征点匹配。
        输入也可以是文件夹的路径。
        '''

        def get_image_paths(image_dir):
            image_paths = os.listdir(image_dir)
            image_paths = sorted(image_paths)
            return image_paths

        def read_npy(image_path):
            '''
            {
            'keypoints': keypoints,
            'scores': scores,
            'descriptors': descriptors,
            }
            '''
            # np_path = image_path.replace('.jpg', '/')
            np_path = osp.join("out_put/image_messages/", image_path)
            # print(np_path)
            np_keypoints = np.load(osp.join(np_path, 'keypoint.npy'))
            print("image name = ", image_path)
            print('keypoints size is ', len(np_keypoints))
            # with torch.no_grad():
            keypoints = torch.from_numpy(np_keypoints).to('cuda')
            np_scores = np.load(osp.join(np_path, 'scores.npy'))
            # with torch.no_grad():
            scores = torch.from_numpy(np_scores).to('cuda')
            np_descriptors = np.load(osp.join(np_path, 'descriptors.npy'))
            # with torch.no_grad():
            descriptors = torch.from_numpy(np_descriptors).to('cuda')
            data = {
                'keypoints': [keypoints],
                'scores': (scores,),
                'descriptors': [descriptors],
                'shape': (0, 0, self.resize_w, self.resize_h)
            }
            return data
        image_path0 = image_path0.replace('.jpg', '/')
        data0 = read_npy(image_path0)
        data0 = {**{}, **{k + '0': v for k, v in data0.items()}}

        image_path1 = image_path1.replace('.jpg', '/')
        data1 = read_npy(image_path1)
        data1 = {**{}, **{k + '1': v for k, v in data1.items()}}

        data = {**data0, **data1}

        for k in data:
            if isinstance(data[k], (list, tuple)):
                if k.__contains__('shape'):
                    continue
                # with torch.no_grad():
                data[k] = torch.stack(data[k])

        # Perform the matching
        pred = {**data0, **data1, **self.matching.superglue(data)}

        return pred

    def analyse_pred(self, pred, image_path00, image_path11, pt1_id, pt2_id):
        image_path0 = osp.join('images/', image_path00)
        image_path1 = osp.join('images/', image_path11)
        def show_images(confidence, valid, last_frame, frame, kpts0, kpts1, mkpts0, mkpts1):
            color = cm.jet(confidence[valid])
            text = [
                'SuperGlue',
                'Keypoints: {}:{}'.format(len(kpts0), len(kpts1)),
                'Matches: {}'.format(len(mkpts0))
            ]
            k_thresh = self.matching.superpoint.config['keypoint_threshold']
            m_thresh = self.matching.superglue.config['match_threshold']
            small_text = [
                'Keypoint Threshold: {:.4f}'.format(k_thresh),
                'Match Threshold: {:.2f}'.format(m_thresh),
                'Image Pair: {:06}:{:06}'.format(0, 1),
            ]
            out = make_matching_plot_fast(
                last_frame, frame, kpts0, kpts1, mkpts0, mkpts1, color, text,
                path=None, show_keypoints=True, small_text=small_text, show_line=True)
            # cv2.namedWindow('SuperGlue matches', cv2.WINDOW_NORMAL)
            # cv2.resizeWindow('SuperGlue matches', (1920 * 2, 1080))
            match_size = len(mkpts0)


            #显示照片
            # cv2.imshow('SuperGlue matches', out)
            # cv2.waitKey(500)
            # cv2.destroyWindow('SuperGlue matches')
            # cv2.imwrite(self.test_out_dir + '_00.jpg', out)

            image_match_path = osp.join("out_put/match/",file_name1)
            #if not osp.exists(image_match_path):
                #os.makedirs(image_match_path)
            image_match_path = osp.join(image_match_path,file_name0 + ".jpg")
            # print("image path is %s" %image_match_path)
            if match_size == 0:
                return
            # cv2.imwrite(image_match_path, out)
            out = make_matching_plot_fast(
                last_frame, frame, kpts0, kpts1, mkpts0, mkpts1, color, text,
                path=None, show_keypoints=True, small_text=small_text, show_line=False)
            # cv2.imshow('SuperGlue matches', out)
            # cv2.waitKey(0)
            # cv2.destroyWindow('SuperGlue matches')
            # cv2.imwrite('/media/zmc/zmc/workspace/other/to_zhoumingchao/Re_02/match2.jpg', out)

        def get_image(input_path, size):
            '''
            input_path:图片路径
            function:完成读图，并图片resize和转灰度图
            '''
            if not osp.exists(input_path):
                print('input image path error,input image path:%s' % input_path)
                return None
            input_image = cv2.imread(input_path)
            # input_image = cv2.resize(input_image, (self.resize_scale * input_image.shape[1], self.resize_scale * input_image.shape[0]),
            
            # if size == 0:
            # input_image = cv2.resize(input_image, (self.resize_scale * input_image.shape[1], self.resize_scale * input_image.shape[0]),
            #                          interpolation=cv2.INTER_AREA)
            # else:
            height, width, channels = input_image.shape
            if size == 0:
                input_image = cv2.resize(input_image, ((int)(width * self.resize_scale_left), (int)(height * self.resize_scale_left)),
                                     interpolation=cv2.INTER_AREA)
            else:
                input_image = cv2.resize(input_image, ((int)(width * self.resize_scale_right), (int)(height * self.resize_scale_right)),
                            interpolation=cv2.INTER_AREA)
            input_image = cv2.cvtColor(input_image, cv2.COLOR_RGB2GRAY)
            return input_image

        import traceback
        try:
            t0 = time.time()
            file_name1 = image_path11.replace(".jpg","/")
            file_name0 = image_path00.replace(".jpg","")
            input_image1 = get_image(image_path0, 0)
            input_image2 = get_image(image_path1, 1)
            formattime = time.strftime(
                "%Y-%m-%d %H:%M:%S", time.localtime())
            self.test_out_dir = osp.join(osp.dirname(image_path0), str(formattime))

            kpts0 = pred['keypoints0'][0].cpu().numpy()
            kpts1 = pred['keypoints1'][0].cpu().numpy()
            matches = pred['matches0'][0].cpu().numpy()
            valid = matches > -1
            mkpts0 = kpts0[valid]
            mkpts1 = kpts1[matches[valid]]

            match_size = len(mkpts0)
            # with open(self.test_out_dir + ".txt", "w") as f:
            print('cost time: %s' % str(time.time() - t0))

            confidence = pred['matching_scores0'][0].cpu().numpy()
            show_images(confidence, valid, input_image1, input_image2, kpts0, kpts1, mkpts0, mkpts1)
            
            i = 0
            pairs = []
            for match in matches:
                if match >= 0:
                    pairs.append((i,match))
                i = i + 1    

            # path = osp.join("/media/zmc/zmc/workspace/other/to_zhoumingchao/Re_02/match/",file_name1)#yuan
            path = "out_put/match/"
            if not osp.exists(path):
                os.makedirs(path)
            # path = osp.join(path,file_name0 + ".txt")#yuan
            path = osp.join(path,(str)(pt1_id) + "_" + (str)(pt2_id) + ".txt")
            
            print("match save",path)
            # with open("/media/zmc/zmc/workspace/other/to_zhoumingchao/Re_02/match/match.txt", "w") as f:
            if match_size == 0:
                return
            
            print('match left last id = ',pairs[match_size-1][0])
            print('match right last id = ',pairs[match_size-1][1])
            with open(path,"w") as f:
                # input_line = image_path00.replace('.jpg','') + "\n"
                input_line = (str)(pt1_id) + " " + (str)(pt2_id) + "\n"
                # print("file first line is %s" %input_line)
                f.write(input_line)
                for i in range(match_size):
                    # print(valid[i])
                    input_line = str(pairs[i][0]) + " " + str(mkpts0[i][0]) + " " + str(mkpts0[i][1]) + \
                                  " " + str(pairs[i][1]) + " " + str(mkpts1[i][0]) + " " + str(mkpts1[i][1]) + "\n"
                    # input_line = str(pairs[i][0]) + " " + str(mkpts1[i][0]) + " " + str(mkpts1[i][1]) + "\n"
                    # print(str(mkpts1[i][1]))
                    #input_line = str(pairs[i][0]) + " " + str(mkpts1[i][0]) + " " + str(mkpts1[i][1]) + "\n"
                    f.write(input_line)

        except Exception:
            traceback.print_exc()

def get_image_paths(image_dir):
    image_paths = os.listdir(image_dir)
    image_paths = sorted(image_paths)
    return image_paths

# def save_map(modle):
#     _analyse_image = analyse_image()
#     # _analyse_image.cal_save_data()
#     print ("./demo_superglue_tozhoumingchao.py 后面跟的参数大于0是提取特征点,小于等于0是匹配.")
#     if (int)(modle) > 0:
#         print("------------------save image point describer .------------------")
#         # _analyse_image.cal_save_data()
#     return 5

#yuan dai mai
# if __name__ == '__main__':
#     _analyse_image = analyse_image()
#     # _analyse_image.cal_save_data()
#     print ("./demo_superglue_tozhoumingchao.py 后面跟的参数大于0是提取特征点,小于等于0是匹配.")
#     if (int)(sys.argv[1]) > 0:
#         print("------------------save image point describer .------------------")
#         _analyse_image.cal_save_data()
#     else:
#         print("--------------match pictures .--------------")
#         image_dir = '/media/zmc/zmc/workspace/other/to_zhoumingchao/Re_02/'
#         image_localization_dir = '/media/zmc/zmc/workspace/other/to_zhoumingchao/Re_02/localization_images/'
#         # image_paths = get_image_paths(image_dir)su
#         image_paths = os.listdir(image_dir)
#         image_paths = sorted(image_paths)

#         image_localization_paths = os.listdir(image_localization_dir)
#         image_localization_paths = sorted(image_localization_paths)

#         for image_localization_path in image_localization_paths:
#             if not image_localization_path.endswith('jpg'):
#                     continue
#             for image_path in image_paths:
#                 if not image_path.endswith('jpg'):
#                     continue
#                 # image_path = osp.join(image_dir, image_path)
#                 if image_path == image_localization_path:
#                     continue
#                 print('image 0 is %s' %image_path) #0.5 
#                 print('image 1 is %s' %image_localization_path) # 0.3
#                 print('----------------fengexian-------------------------')
#                 pred = _analyse_image.cal_match_from_local(image_path, image_localization_path)
#                 _analyse_image.analyse_pred(pred, image_path, image_localization_path)
#         cv2.destroyWindow('SuperGlue matches')

#为colmap更改superpoint准备数据
if __name__ == '__main__':
    _analyse_image = analyse_image()
    # _analyse_image.cal_save_data()
    print ("./demo_superglue_tozhoumingchao.py 后面跟的参数大于0是提取特征点,小于等于0是匹配.")
    if (int)(sys.argv[1]) > 0:
        print("------------------save image point describer .------------------")
        _analyse_image.cal_save_data()
    else:
        print("--------------match pictures .--------------")
        
        f = open("out_put/frame_index_name.txt")
        line = f.readline()
        map_id_name = {}
        while line:
            # int image_id
            image_id, image_name = line.split()
            map_id_name[(int)(image_id)+1] = image_name
            line = f.readline()
        # print(map_id_name)

        # images_num = len(map_id_name)
        # match_overlap_frames = 5
        # for i in range(1,images_num):
        #     if (i + match_overlap_frames) <= images_num:
        #         for j in range(i, i+match_overlap_frames-1):
        #             pred = _analyse_image.cal_match_from_local(map_id_name[i] + ".jpg", map_id_name[j+1] + ".jpg")
        #             _analyse_image.analyse_pred(pred, map_id_name[i] + ".jpg", map_id_name[j+1] + ".jpg", i, j+1)
        #             # print("image", i)
        #             # print("VS image ", j+1)
        #     else:
        #         for j in range(i, images_num):
        #             pred = _analyse_image.cal_match_from_local(map_id_name[i] + ".jpg", map_id_name[j+1] + ".jpg")
        #             _analyse_image.analyse_pred(pred, map_id_name[i] + ".jpg", map_id_name[j+1] + ".jpg", i, j+1)
        #             # print("image",i)
        #             # print("VS image ",j+1)

        images_num = len(map_id_name)
        match_overlap_frames = 8
        i = (int)(sys.argv[2])

        if (i + match_overlap_frames) <= images_num:
            for j in range(i+1, i+match_overlap_frames+1):
                pred = _analyse_image.cal_match_from_local(map_id_name[i] + ".jpg", map_id_name[j] + ".jpg")
                # pred = _analyse_image.cal_match_from_local(map_id_name[i] + ".jpg", map_id_name[j+1] + ".jpg")
                _analyse_image.analyse_pred(pred, map_id_name[i] + ".jpg", map_id_name[j] + ".jpg", i, j)
                # _analyse_image.analyse_pred(pred, map_id_name[i] + ".jpg", map_id_name[j+1] + ".jpg", i, j+1)
                # print("image", i)
                # print("VS image ", j+1)
        else:
            for j in range(i+1, images_num+1):
                # pred = _analyse_image.cal_match_from_local(map_id_name[i] + ".jpg", map_id_name[j+1] + ".jpg")
                pred = _analyse_image.cal_match_from_local(map_id_name[i] + ".jpg", map_id_name[j] + ".jpg")
                # _analyse_image.analyse_pred(pred, map_id_name[i] + ".jpg", map_id_name[j+1] + ".jpg", i, j+1)
                _analyse_image.analyse_pred(pred, map_id_name[i] + ".jpg", map_id_name[j] + ".jpg", i, j)
                # print("image",i)
                # print("VS image ",j+1)








