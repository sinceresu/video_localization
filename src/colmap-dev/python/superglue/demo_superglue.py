#! /usr/bin/env python3
#
# %BANNER_BEGIN%
# ---------------------------------------------------------------------
# %COPYRIGHT_BEGIN%
#
#  Magic Leap, Inc. ("COMPANY") CONFIDENTIAL
#
#  Unpublished Copyright (c) 2020
#  Magic Leap, Inc., All Rights Reserved.
#
# NOTICE:  All information contained herein is, and remains the property
# of COMPANY. The intellectual and technical concepts contained herein
# are proprietary to COMPANY and may be covered by U.S. and Foreign
# Patents, patents in process, and are protected by trade secret or
# copyright law.  Dissemination of this information or reproduction of
# this material is strictly forbidden unless prior written permission is
# obtained from COMPANY.  Access to the source code contained herein is
# hereby forbidden to anyone except current COMPANY employees, managers
# or contractors who have executed Confidentiality and Non-disclosure
# agreements explicitly covering such access.
#
# The copyright notice above does not evidence any actual or intended
# publication or disclosure  of  this source code, which includes
# information that is confidential and/or proprietary, and is a trade
# secret, of  COMPANY.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION,
# PUBLIC  PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS
# SOURCE CODE  WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS
# STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND
# INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE
# CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
# TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE,
# USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
#
# %COPYRIGHT_END%
# ----------------------------------------------------------------------
# %AUTHORS_BEGIN%
#
#  Originating Authors: Paul-Edouard Sarlin
#                       Daniel DeTone
#                       Tomasz Malisiewicz
#
# %AUTHORS_END%
# --------------------------------------------------------------------*/
# %BANNER_END%

from pathlib import Path
import argparse
import cv2
import matplotlib.cm as cm
import torch

from models.matching import Matching
from models.utils import (AverageTimer, VideoStreamer,
                          make_matching_plot_fast, frame2tensor)

torch.set_grad_enabled(False)

import time
import os, os.path as osp
import numpy as np


class analyse_image:
    def __init__(self):
        '''
        device:cuda/cpu

        '''
        self.device = 'cuda'
        config = {
            'superpoint': {
                'nms_radius': 4,
                'keypoint_threshold': 0.005,
                'max_keypoints': -1
            },
            'superglue': {
                'weights': 'indoor',
                'sinkhorn_iterations': 20,
                'match_threshold': 0.2,
            }
        }
        self.matching = Matching(config).eval().to(self.device)
        self.keys = ['keypoints', 'scores', 'descriptors']
        # self.resize_w = 320
        # self.resize_h = 240
        self.resize_w = 2880
        self.resize_h = 1620

    def fram_to_tensor(self, frame):
        frame_tensor = frame2tensor(frame, self.device)
        return frame_tensor

    def resize_image(self, input_image):
        '''
        input_image:cv2的mat数据
        function:完成图片resize和转灰度图
        '''
        input_image = cv2.resize(input_image, (self.resize_w, self.resize_h),
                                 interpolation=cv2.INTER_AREA)
        input_image = cv2.cvtColor(input_image, cv2.COLOR_RGB2GRAY)
        return input_image

    def compare_images(self, input_image1, input_image2):
        t0 = time.time()
        input_image1 = self.resize_image(input_image1)
        input_image2 = self.resize_image(input_image2)
        frame_tensor = self.fram_to_tensor(input_image1)
        last_data = self.matching.superpoint({'image': frame_tensor})
        last_data = {k + '0': last_data[k] for k in self.keys}
        last_data['image0'] = frame_tensor

        frame_tensor = self.fram_to_tensor(input_image2)
        pred = self.matching({**last_data, 'image1': frame_tensor})
        kpts0 = last_data['keypoints0'][0].cpu().numpy()
        kpts1 = pred['keypoints1'][0].cpu().numpy()
        matches = pred['matches0'][0].cpu().numpy()

        valid = matches > -1
        mkpts0 = kpts0[valid]
        mkpts1 = kpts1[matches[valid]]

        match_size = len(mkpts0)
        with open(self.test_out_dir + ".txt", "w") as f:
            for i in range(match_size):
                input_line = str(mkpts0[i][0]) + " " + str(mkpts0[i][1]) + \
                             " " + str(mkpts1[i][0]) + " " + str(mkpts1[i][1]) + "\n"
                f.write(input_line)

        print('cost time: %s' % str(time.time() - t0))

        confidence = pred['matching_scores0'][0].cpu().numpy()
        self.show_images(confidence, valid, input_image1, input_image2, kpts0, kpts1, mkpts0, mkpts1)

    def show_images(self, confidence, valid, last_frame, frame, kpts0, kpts1, mkpts0, mkpts1):
        cv2.namedWindow('SuperGlue matches', cv2.WINDOW_NORMAL)
        cv2.resizeWindow('SuperGlue matches', (1920 * 2, 1080))
        color = cm.jet(confidence[valid])
        text = [
            'SuperGlue',
            'Keypoints: {}:{}'.format(len(kpts0), len(kpts1)),
            'Matches: {}'.format(len(mkpts0))
        ]
        k_thresh = self.matching.superpoint.config['keypoint_threshold']
        m_thresh = self.matching.superglue.config['match_threshold']
        small_text = [
            'Keypoint Threshold: {:.4f}'.format(k_thresh),
            'Match Threshold: {:.2f}'.format(m_thresh),
            'Image Pair: {:06}:{:06}'.format(0, 1),
        ]
        out = make_matching_plot_fast(
            last_frame, frame, kpts0, kpts1, mkpts0, mkpts1, color, text,
            path=None, show_keypoints=True, small_text=small_text)
        # cv2.imshow('SuperGlue matches', out)
        cv2.imwrite(self.test_out_dir + '_00.jpg', out)
        out = make_matching_plot_fast(
            last_frame, frame, kpts0, kpts1, mkpts0, mkpts1, color, text,
            path=None, show_keypoints=True, small_text=small_text, show_line=False)
        cv2.imwrite(self.test_out_dir + '_01.jpg', out)

    def get_images(self):
        # video_path = "/home/lxr/workdir/reid/openpose/sh3.mp4"
        # cap = cv2.VideoCapture(video_path)
        # ret, frame = cap.read()
        # self.compare_images(cap.read()[1],cap.read()[1])

        image_path1 = "/home/lxr/Downloads/Re_06/rectsource.jpg"
        image_path2 = "/home/lxr/Downloads/Re_06/undistortarget.jpg"
        image1 = cv2.imread(image_path1)
        image2 = cv2.imread(image_path2)
        self.compare_images(image1, image2)

    def test_office(self):
        root_dir = '/home/lxr/Downloads/officetest_46029'
        office_dirs = os.listdir(root_dir)
        office_dirs = sorted(office_dirs)

        test_resize_scales = [(2048, 2048),(2880, 1620), (2560, 2560)]
        for test_resize_scale in test_resize_scales:
            self.resize_w, self.resize_h = test_resize_scale
            # self.resize_w = 2880
            # self.resize_h = 1620
            test_threshes = [0, 0.2, 0.8]
            for test_thresh in test_threshes:
                self.matching.superglue.config['match_threshold'] = test_thresh
                for office_dir in office_dirs:
                    image_names = os.listdir(osp.join(root_dir, office_dir))
                    target_name = filter(lambda x: x.__contains__('target'), image_names)
                    target_name = list(target_name)
                    target_name = target_name[0]
                    for source_name in image_names:
                        if source_name == target_name or (not source_name.endswith('.jpg') and not source_name.endswith('.png')):
                            continue
                        source_path = osp.join(root_dir, office_dir, source_name)
                        self.test_out_dir = source_path[:-4]
                        if not osp.exists(self.test_out_dir):
                            os.makedirs(self.test_out_dir)
                        self.test_out_dir = osp.join(self.test_out_dir,
                                                     '%04d_%04d_%s' % (self.resize_w, self.resize_h, str(test_thresh)))
                        image1 = cv2.imread(osp.join(root_dir, office_dir, target_name))
                        image2 = cv2.imread(source_path)
                        self.compare_images(image1, image2)

    def cal_save_image(self):
        def get_image_paths(image_dir):
            image_paths = os.listdir(image_dir)
            image_paths = sorted(image_paths)
            return image_paths

        def cal_keypoints(input_image):
            input_image = self.resize_image(input_image)
            frame_tensor = self.fram_to_tensor(input_image)
            data = self.matching.superpoint({'image': frame_tensor})
            return data

        def save_keypoints(data, image_path):
            '''
            {
            'keypoints': keypoints,
            'scores': scores,
            'descriptors': descriptors,
            }
            '''
            save_path = image_path.replace('.jpg', '/')
            if not osp.exists(save_path):
                os.makedirs(save_path)
            keypoints = data['keypoints'][0].cpu().numpy()
            np.save(osp.join(save_path, 'keypoint.npy'), keypoints)
            scores = data['scores'][0].cpu().numpy()
            np.save(osp.join(save_path, 'scores.npy'), scores)
            descriptors = data['descriptors'][0].cpu().numpy()
            np.save(osp.join(save_path, 'descriptors.npy'), descriptors)

        image_dir = '/home/lxr/Downloads/Re_02'
        image_paths = get_image_paths(image_dir)
        for image_path in image_paths:
            if not image_path.endswith('jpg'):
                continue
            image_path = osp.join(image_dir, image_path)
            image = cv2.imread(image_path)
            if image is None:
                print('image path error or image error')
                continue
            data = cal_keypoints(image)
            save_keypoints(data, image_path)

    def cal_match_from_local(self):
        def get_image_paths(image_dir):
            image_paths = os.listdir(image_dir)
            image_paths = sorted(image_paths)
            return image_paths

        def read_npy(image_path):
            '''
            {
            'keypoints': keypoints,
            'scores': scores,
            'descriptors': descriptors,
            }
            '''
            np_path = image_path.replace('.jpg', '/')
            np_keypoints = np.load(osp.join(np_path, 'keypoint.npy'))
            keypoints = torch.from_numpy(np_keypoints).to('cuda')
            np_scores = np.load(osp.join(np_path, 'scores.npy'))
            scores = torch.from_numpy(np_scores).to('cuda')
            np_descriptors = np.load(osp.join(np_path, 'descriptors.npy'))
            descriptors = torch.from_numpy(np_descriptors).to('cuda')
            data = {
                'keypoints': [keypoints],
                'scores': (scores,),
                'descriptors': [descriptors],
                'shape': (0, 0, self.resize_w, self.resize_h)
            }
            return data

        image_path0 = '/home/lxr/Downloads/Re_02/undistorsource.jpg'
        data0 = read_npy(image_path0)
        data0 = {**{}, **{k + '0': v for k, v in data0.items()}}

        image_path1 = '/home/lxr/Downloads/Re_02/undistortarget.jpg'
        data1 = read_npy(image_path1)
        data1 = {**{}, **{k + '1': v for k, v in data1.items()}}

        data = {**data0, **data1}

        for k in data:
            if isinstance(data[k], (list, tuple)):
                if k.__contains__('shape'):
                    continue
                data[k] = torch.stack(data[k])

        # Perform the matching
        pred = {**data1, **self.matching.superglue(data)}

        return pred


if __name__ == '__main__':
    _analyse_image = analyse_image()
    _analyse_image.test_office()
    # _analyse_image.cal_save_image()
    # _analyse_image.cal_match_from_local()

    # import image_test
    # image_test.image_test()

'''
if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='SuperGlue demo',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        '--input', type=str, default='0',
        help='ID of a USB webcam, URL of an IP camera, '
             'or path to an image directory or movie file')
    parser.add_argument(
        '--output_dir', type=str, default=None,
        help='Directory where to write output frames (If None, no output)')

    parser.add_argument(
        '--image_glob', type=str, nargs='+', default=['*.png', '*.jpg', '*.jpeg'],
        help='Glob if a directory of images is specified')
    parser.add_argument(
        '--skip', type=int, default=1,
        help='Images to skip if input is a movie or directory')
    parser.add_argument(
        '--max_length', type=int, default=1000000,
        help='Maximum length if input is a movie or directory')
    parser.add_argument(
        '--resize', type=int, nargs='+', default=[640, 480],
        help='Resize the input image before running inference. If two numbers, '
             'resize to the exact dimensions, if one number, resize the max '
             'dimension, if -1, do not resize')

    parser.add_argument(
        '--superglue', choices={'indoor', 'outdoor'}, default='indoor',
        help='SuperGlue weights')
    parser.add_argument(
        '--max_keypoints', type=int, default=-1,
        help='Maximum number of keypoints detected by Superpoint'
             ' (\'-1\' keeps all keypoints)')
    parser.add_argument(
        '--keypoint_threshold', type=float, default=0.005,
        help='SuperPoint keypoint detector confidence threshold')
    parser.add_argument(
        '--nms_radius', type=int, default=4,
        help='SuperPoint Non Maximum Suppression (NMS) radius'
        ' (Must be positive)')
    parser.add_argument(
        '--sinkhorn_iterations', type=int, default=20,
        help='Number of Sinkhorn iterations performed by SuperGlue')
    parser.add_argument(
        '--match_threshold', type=float, default=0.2,
        help='SuperGlue match threshold')

    parser.add_argument(
        '--show_keypoints', action='store_true',
        help='Show the detected keypoints')
    parser.add_argument(
        '--no_display', action='store_true',
        help='Do not display images to screen. Useful if running remotely')
    parser.add_argument(
        '--force_cpu', action='store_true',
        help='Force pytorch to run in CPU mode.')

    opt = parser.parse_args()
    opt.no_display = False
    print(opt)

    if len(opt.resize) == 2 and opt.resize[1] == -1:
        opt.resize = opt.resize[0:1]
    if len(opt.resize) == 2:
        print('Will resize to {}x{} (WxH)'.format(
            opt.resize[0], opt.resize[1]))
    elif len(opt.resize) == 1 and opt.resize[0] > 0:
        print('Will resize max dimension to {}'.format(opt.resize[0]))
    elif len(opt.resize) == 1:
        print('Will not resize images')
    else:
        raise ValueError('Cannot specify more than two integers for --resize')

    device = 'cuda' if torch.cuda.is_available() and not opt.force_cpu else 'cpu'
    print('Running inference on device \"{}\"'.format(device))
    config = {
        'superpoint': {
            'nms_radius': opt.nms_radius,
            'keypoint_threshold': opt.keypoint_threshold,
            'max_keypoints': opt.max_keypoints
        },
        'superglue': {
            'weights': opt.superglue,
            'sinkhorn_iterations': opt.sinkhorn_iterations,
            'match_threshold': opt.match_threshold,
        }
    }
    matching = Matching(config).eval().to(device)
    keys = ['keypoints', 'scores', 'descriptors']

    vs = VideoStreamer(opt.input, opt.resize, opt.skip,
                       opt.image_glob, opt.max_length)
    frame, ret = vs.next_frame()
    assert ret, 'Error when reading the first frame (try different --input?)'

    frame_tensor = frame2tensor(frame, device)
    last_data = matching.superpoint({'image': frame_tensor})
    last_data = {k + '0': last_data[k] for k in keys}
    last_data['image0'] = frame_tensor
    last_frame = frame
    last_image_id = 0

    if opt.output_dir is not None:
        print('==> Will write outputs to {}'.format(opt.output_dir))
        Path(opt.output_dir).mkdir(exist_ok=True)

    # Create a window to display the demo.
    if not opt.no_display:
        cv2.namedWindow('SuperGlue matches', cv2.WINDOW_NORMAL)
        cv2.resizeWindow('SuperGlue matches', (640 * 2, 480))
    else:
        print('Skipping visualization, will not show a GUI.')

    # Print the keyboard help menu.
    print('==> Keyboard control:\n'
          '\tn: select the current frame as the anchor\n'
          '\te/r: increase/decrease the keypoint confidence threshold\n'
          '\td/f: increase/decrease the match filtering threshold\n'
          '\tk: toggle the visualization of keypoints\n'
          '\tq: quit')

    timer = AverageTimer()

    while True:
        frame, ret = vs.next_frame()
        if not ret:
            print('Finished demo_superglue.py')
            break
        timer.update('data')
        stem0, stem1 = last_image_id, vs.i - 1

        frame_tensor = frame2tensor(frame, device)
        pred = matching({**last_data, 'image1': frame_tensor})
        kpts0 = last_data['keypoints0'][0].cpu().numpy()
        kpts1 = pred['keypoints1'][0].cpu().numpy()
        matches = pred['matches0'][0].cpu().numpy()
        confidence = pred['matching_scores0'][0].cpu().numpy()
        timer.update('forward')

        valid = matches > -1
        mkpts0 = kpts0[valid]
        mkpts1 = kpts1[matches[valid]]
        color = cm.jet(confidence[valid])
        text = [
            'SuperGlue',
            'Keypoints: {}:{}'.format(len(kpts0), len(kpts1)),
            'Matches: {}'.format(len(mkpts0))
        ]
        k_thresh = matching.superpoint.config['keypoint_threshold']
        m_thresh = matching.superglue.config['match_threshold']
        small_text = [
            'Keypoint Threshold: {:.4f}'.format(k_thresh),
            'Match Threshold: {:.2f}'.format(m_thresh),
            'Image Pair: {:06}:{:06}'.format(stem0, stem1),
        ]
        out = make_matching_plot_fast(
            last_frame, frame, kpts0, kpts1, mkpts0, mkpts1, color, text,
            path=None, show_keypoints=opt.show_keypoints, small_text=small_text)

        if not opt.no_display:
            cv2.imshow('SuperGlue matches', out)
            key = chr(cv2.waitKey(1) & 0xFF)
            key = 'n'
            if key == 'q':
                vs.cleanup()
                print('Exiting (via q) demo_superglue.py')
                break
            elif key == 'n':  # set the current frame as anchor
                last_data = {k + '0': pred[k + '1'] for k in keys}
                last_data['image0'] = frame_tensor
                last_frame = frame
                last_image_id = (vs.i - 1)
            elif key in ['e', 'r']:
                # Increase/decrease keypoint threshold by 10% each keypress.
                d = 0.1 * (-1 if key == 'e' else 1)
                matching.superpoint.config['keypoint_threshold'] = min(max(
                    0.0001, matching.superpoint.config['keypoint_threshold'] * (1 + d)), 1)
                print('\nChanged the keypoint threshold to {:.4f}'.format(
                    matching.superpoint.config['keypoint_threshold']))
            elif key in ['d', 'f']:
                # Increase/decrease match threshold by 0.05 each keypress.
                d = 0.05 * (-1 if key == 'd' else 1)
                matching.superglue.config['match_threshold'] = min(max(
                    0.05, matching.superglue.config['match_threshold'] + d), .95)
                print('\nChanged the match threshold to {:.2f}'.format(
                    matching.superglue.config['match_threshold']))
            elif key == 'k':
                opt.show_keypoints = not opt.show_keypoints

        timer.update('viz')
        timer.print()

        if opt.output_dir is not None:
            # stem = 'matches_{:06}_{:06}'.format(last_image_id, vs.i-1)
            stem = 'matches_{:06}_{:06}'.format(stem0, stem1)
            out_file = str(Path(opt.output_dir, stem + '.png'))
            print('\nWriting image to {}'.format(out_file))
            cv2.imwrite(out_file, out)

    cv2.destroyAllWindows()
    vs.cleanup()
'''
