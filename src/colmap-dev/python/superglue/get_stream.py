import rospy
import cv2
import numpy as np
import time
from sensor_msgs.msg import Image


def cv2_to_imgmsg(cv_image):
    img_msg = Image()
    img_msg.height = cv_image.shape[0]
    img_msg.width = cv_image.shape[1]
    img_msg.encoding = "bgr8"
    img_msg.is_bigendian = 0
    img_msg.data = cv_image.tostring()
    # That double line is actually integer division, not a comment
    img_msg.step = len(img_msg.data) // img_msg.height
    return img_msg


rospy.init_node('rtsp_stream')
single_image = rospy.Publisher(
    "/camera", Image, queue_size=1)

url = "rtsp://admin:123qweasd@192.168.5.231:554/h264/ch1/main/av_stream"

gst_str = ('rtspsrc location={} latency={} ! rtph264depay ! h264parse ! omxh264dec ! nvvidconv ! video/x-raw,format=(string)BGRx ! videoconvert ! appsink').format(url, 100)
#gst_str = "rtsp://admin:123qweasd@192.168.5.231:554/h264/ch1/main/av_stream"
gst_str = "/media/zmc/84FE-CF74/0817garden/HwVideoEditor_2021_08_25_172426014.mp4"
cap = cv2.VideoCapture(gst_str)
ret, frame = cap.read()
while ret:
    ret, frame = cap.read()
    if frame is None:
        continue
    msg = cv2_to_imgmsg(frame)
    single_image.publish(msg)
    print('single_image publish')
    time.sleep(0.1)
    cv2.namedWindow("frame", cv2.WINDOW_NORMAL)
    cv2.resizeWindow("frame", 640, 480)
    cv2.imshow("frame", frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
cv2.destroyAllWindows()
cap.release()
