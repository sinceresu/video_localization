#! /usr/bin/env python3
'''
2021-07-23:整理文件以发送给周明超。
功能及使用说明：本脚本实现两个功能：１．读取文件夹下的图片，生成特征点等文件保存本地。２．读取本地特征点等文件，生成匹配信息。
配置参数：class analyse_image的初始化函数中的各项参数（阈值thresh、输入尺寸等）；
　　　　　cal_save_data函数中的image_dir（功能１的路径）
　　　　　cal_match_from_local函数中的image_path0，image_path1（功能２的路径）
使用说明：main函数中注释的两行，分别取消注释即可。

2021-07-29:
更新说明：
１．重命名cal_save_image为cal_save_data；
２．新增函数，功能：实现输入pred，得到匹配图片显示并保存本地，得到txt文件保存本地；
3. 修复bug:pred中加上data0。

2021-08-23:
更新说明：
１．新增功能解析视频、匹配本地图片、输出等，通过vision_localization类实现；
２．相关详细说明见文档。

补充说明：最终生成的匹配信息，根据个人需要进行使用。
'''
from pathlib import Path
import argparse
import sys, time, os, cv2
import os.path as osp
import numpy as np
import matplotlib.cm as cm
import matplotlib
matplotlib.use('Agg')
import torch
from models.matching import Matching
from models.utils import (AverageTimer, VideoStreamer,
                          make_matching_plot_fast, frame2tensor)

torch.set_grad_enabled(False)
from utils.logger import setup_logger
import rospy
from sensor_msgs.msg import Image
from sensor_msgs.msg import CompressedImage
from geometry_msgs.msg import Point32
from geometry_msgs.msg import Polygon
from vision_localization_msgs.msg import PointMatch

from std_msgs.msg import Header
from sensor_msgs.msg import Image

if sys.version_info < (3, 0):
    import Queue
    import ConfigParser
    import thread
else:
    import queue as Queue
    import configparser as ConfigParser
    import _thread as thread

# import sqlite3
# conn = sqlite3.connect("/home/lxr/watch_data/data_manager_sys.db")

dirname = osp.dirname(osp.abspath(__file__))
logger = None


class analyse_image:
    def __init__(self):
        '''
        device:cuda/cpu
        '''
        self.device = 'cuda'
        config = {
            'superpoint': {
                'nms_radius': 4,
                'keypoint_threshold': 0.0005,
                'max_keypoints': -1
            },
            'superglue': {
                'weights': 'indoor',
                'sinkhorn_iterations': 20,
                'match_threshold': 0.2,
            }
        }
        self.matching = Matching(config).eval().to(self.device)
        self.keys = ['keypoints', 'scores', 'descriptors']
        # self.resize_w = 2048
        # self.resize_h = 1536
        # self.resize_w = 1440
        # self.resize_h = 1440
        self.resize_w = 720
        self.resize_h = 720

        self.local_image_w = 848
        self.local_image_h = 800
    
    
    


    def fram_to_tensor(self, frame):
        # print(frame.shape[:])
        frame_tensor = frame2tensor(frame, self.device)
        return frame_tensor

    def resize_image(self, input_image):
        '''
        input_image:cv2的mat数据
        function:完成图片resize和转灰度图
        '''
        input_image = cv2.resize(input_image, (self.resize_w, self.resize_h),
                                 interpolation=cv2.INTER_AREA)
        input_image = cv2.cvtColor(input_image, cv2.COLOR_RGB2GRAY)
        return input_image

    def cal_save_data(self):
        '''
        遍历image_dir目录下的所有jpg图片，每一张图片（xx.jpg）会创建一个对应的xx文件夹，文件夹中保存keypoints、scores、descriptors三个npy文件。
        '''

        def get_image_paths(image_dir):
            image_paths = os.listdir(image_dir)
            image_paths = sorted(image_paths)
            return image_paths

        def cal_keypoints(input_image):
            t0 = time.time()
            input_image = self.resize_image(input_image)
            frame_tensor = self.fram_to_tensor(input_image)
            data = self.matching.superpoint({'image': frame_tensor})
            print('cal key points cost time:%s' % str(time.time() - t0))
            return data

        def save_keypoints(data, image_path, file_name):
            '''
            {
            'keypoints': keypoints,
            'scores': scores,
            'descriptors': descriptors,
            }
            '''
            # save_path = '/media/zmc/zmc/duwuqidata/superglue/image_messages/'
            save_path = 'out_put/image_messages/'
            path_name = file_name.replace('.jpg', '/')
            save_path = osp.join(save_path, path_name)
            if not osp.exists(save_path):
                print("save_path 127 is:", save_path)
                os.makedirs(save_path)
            keypoints = data['keypoints'][0].cpu().numpy()
            np.save(osp.join(save_path, 'keypoint.npy'), keypoints)
            scores = data['scores'][0].cpu().numpy()
            np.save(osp.join(save_path, 'scores.npy'), scores)
            descriptors = data['descriptors'][0].cpu().numpy()
            np.save(osp.join(save_path, 'descriptors.npy'), descriptors)

            keypoint_txt_path = file_name.replace('.jpg','.txt')
            keypoints_size = len(keypoints)
            print("save keypoints 148")
            with open("out_put/keypoints/" + keypoint_txt_path, "w") as f:
                for i in range(keypoints_size):
                    input_line = str(keypoints[i][0]) + " " + str(keypoints[i][1]) + "\n"
                    f.write(input_line)

        image_dir = '/home/lxr/Downloads/Re_02'
        image_paths = get_image_paths(image_dir)
        for image_path in image_paths:
            if not image_path.endswith('jpg'):
                continue
            image_path = osp.join(image_dir, image_path)
            image = cv2.imread(image_path)
            if image is None:
                print('image path error or image error')
                continue
            data = cal_keypoints(image)
            # save_keypoints(data, image_path, image_path)

    def cal_match_from_local(self, image_path0, image_path1):
        '''
        输入两张图片的路径（xx.jpg）会寻找对应的xx文件夹，
        文件夹中保存keypoints、scores、descriptors三个npy文件。
        读取并进行特征点匹配。
        输入也可以是文件夹的路径。
        '''

        def get_image_paths(image_dir):
            image_paths = os.listdir(image_dir)
            image_paths = sorted(image_paths)
            return image_paths

        def read_npy(image_path):
            '''
            {
            'keypoints': keypoints,
            'scores': scores,
            'descriptors': descriptors,
            }
            '''
            np_path = image_path.replace('.jpg', '/')
            np_keypoints = np.load(osp.join(np_path, 'keypoint.npy'))
            keypoints = torch.from_numpy(np_keypoints).to('cuda')
            np_scores = np.load(osp.join(np_path, 'scores.npy'))
            scores = torch.from_numpy(np_scores).to('cuda')
            np_descriptors = np.load(osp.join(np_path, 'descriptors.npy'))
            descriptors = torch.from_numpy(np_descriptors).to('cuda')
            data = {
                'keypoints': [keypoints],
                'scores': (scores,),
                'descriptors': [descriptors],
                'shape': (0, 0, self.resize_w, self.resize_h)
            }
            return data

        data0 = read_npy(image_path0)
        data0 = {**{}, **{k + '0': v for k, v in data0.items()}}

        data1 = read_npy(image_path1)
        data1 = {**{}, **{k + '1': v for k, v in data1.items()}}

        data = {**data0, **data1}

        t0 = time.time()
        for k in data:
            if isinstance(data[k], (list, tuple)):
                if k.__contains__('shape'):
                    continue
                data[k] = torch.stack(data[k])

        # Perform the matching
        # print(data['shape0'])
        # print(data1['shape1'])
        pred = {**data0, **data1, **self.matching.superglue(data)}
        print('cal matches cost time:%s' % str(time.time() - t0))

        return pred

    def analyse_pred(self, pred, image_path0, image_path1):
        def show_images(confidence, valid, last_frame, frame, kpts0, kpts1, mkpts0, mkpts1):
            color = cm.jet(confidence[valid])
            text = [
                'SuperGlue',
                'Keypoints: {}:{}'.format(len(kpts0), len(kpts1)),
                'Matches: {}'.format(len(mkpts0))
            ]
            k_thresh = self.matching.superpoint.config['keypoint_threshold']
            m_thresh = self.matching.superglue.config['match_threshold']
            small_text = [
                'Keypoint Threshold: {:.4f}'.format(k_thresh),
                'Match Threshold: {:.2f}'.format(m_thresh),
                'Image Pair: {:06}:{:06}'.format(0, 1),
            ]
            out = make_matching_plot_fast(
                last_frame, frame, kpts0, kpts1, mkpts0, mkpts1, color, text,
                path=None, show_keypoints=True, small_text=small_text, show_line=True)
            cv2.namedWindow('SuperGlue matches', cv2.WINDOW_NORMAL)
            cv2.resizeWindow('SuperGlue matches', (960, 540))
            cv2.imshow('SuperGlue matches', out)
            cv2.waitKey(500)
            # cv2.destroyWindow('SuperGlue matches')
            cv2.imwrite(self.test_out_dir + '_00.jpg', out)
            out = make_matching_plot_fast(
                last_frame, frame, kpts0, kpts1, mkpts0, mkpts1, color, text,
                path=None, show_keypoints=True, small_text=small_text, show_line=False)
            cv2.imshow('SuperGlue matches', out)
            cv2.waitKey(500)
            # cv2.destroyWindow('SuperGlue matches')
            cv2.imwrite(self.test_out_dir + '_01.jpg', out)

        def get_image(input_path):
            '''
            input_path:图片路径
            function:完成读图，并图片resize和转灰度图
            '''
            if not osp.exists(input_path):
                print('input image path error,input image path:%s' % input_path)
                return None
            input_image = cv2.imread(input_path)
            input_image = cv2.resize(input_image, (self.resize_w, self.resize_h),
                                     interpolation=cv2.INTER_AREA)
            input_image = cv2.cvtColor(input_image, cv2.COLOR_RGB2GRAY)
            return input_image

        import traceback
        try:
            t0 = time.time()
            input_image1 = get_image(image_path0)
            input_image2 = get_image(image_path1)
            formattime = time.strftime(
                "%Y-%m-%d %H:%M:%S", time.localtime())
            self.test_out_dir = osp.join(osp.dirname(image_path0), str(formattime))

            kpts0 = pred['keypoints0'][0].cpu().numpy()
            kpts1 = pred['keypoints1'][0].cpu().numpy()
            matches = pred['matches0'][0].cpu().numpy()
            valid = matches > -1
            mkpts0 = kpts0[valid]
            mkpts1 = kpts1[matches[valid]]

            match_size = len(mkpts0)
            with open(self.test_out_dir + ".txt", "w") as f:
                for i in range(match_size):
                    input_line = str(mkpts0[i][0]) + " " + str(mkpts0[i][1]) + \
                                 " " + str(mkpts1[i][0]) + " " + str(mkpts1[i][1]) + "\n"
                    f.write(input_line)

            print('cost time: %s' % str(time.time() - t0))

            confidence = pred['matching_scores0'][0].cpu().numpy()
            show_images(confidence, valid, input_image1, input_image2, kpts0, kpts1, mkpts0, mkpts1)
        except Exception:
            # traceback.print_exc()
            print('print_exc')


    def cal_save_image(self):
        '''
        遍历image_dir目录下的所有jpg图片，每一张图片（xx.jpg）会创建一个对应的xx文件夹，文件夹中保存keypoints、scores、descriptors三个npy文件。
        '''

        def get_image_paths(image_dir):
            image_paths = os.listdir(image_dir)
            image_paths = sorted(image_paths)
            return image_paths

        def cal_keypoints(input_image):
            t0 = time.time()
            input_image = self.resize_image(input_image)
            frame_tensor = self.fram_to_tensor(input_image)
            data = self.matching.superpoint({'image': frame_tensor})
            print('cal key points cost time:%s' % str(time.time() - t0))
            return data

        def cal_keypoints2(input_image):
            t0 = time.time()
            y0, x0 = input_image.shape[:2]
            y0 = int(y0 / 2)
            x0 = int(x0 / 2)
            input_image = input_image[y0 - 1024:y0 + 1024, x0 - 1024:x0 + 1024, :]
            input_image = cv2.cvtColor(input_image, cv2.COLOR_RGB2GRAY)
            frame_tensor = self.fram_to_tensor(input_image)
            data = self.matching.superpoint({'image': frame_tensor})
            print('cal key points cost time:%s' % str(time.time() - t0))
            return data

        def save_keypoints(data, image_path, image):
            save_path = image_path.replace('.jpg', '/')
            if not osp.exists(save_path):
                print("save_path 322 is:", save_path)
                os.makedirs(save_path)
            save_path = osp.join(save_path, '%d_%d.jpg' % (self.resize_w, self.resize_h))
            keypoints = data['keypoints'][0].cpu().numpy()
            keypoints = np.round(keypoints).astype(int)
            white = (255, 255, 255)
            black = (0, 0, 0)
            for x, y in keypoints:
                x = int(x * image.shape[1] / self.resize_w)
                y = int(y * image.shape[0] / self.resize_h)
                cv2.circle(image, (x, y), 2, black, -1, lineType=cv2.LINE_AA)
                cv2.circle(image, (x, y), 1, white, -1, lineType=cv2.LINE_AA)
            cv2.imwrite(save_path, image)

        def save_keypoints2(data, image_path, image):
            save_path = image_path.replace('.jpg', '/')
            if not osp.exists(save_path):
                print("save_path 338 is:", save_path)
                os.makedirs(save_path)
            save_path = osp.join(save_path, 'center.jpg')
            keypoints = data['keypoints'][0].cpu().numpy()
            keypoints = np.round(keypoints).astype(int)
            white = (255, 255, 255)
            black = (0, 0, 0)
            for x, y in keypoints:
                y0, x0 = image.shape[:2]
                y0 = int(y0 / 2)
                x0 = int(x0 / 2)
                x = x + x0 - 1024
                y = y + y0 - 1024
                cv2.circle(image, (x, y), 2, black, -1, lineType=cv2.LINE_AA)
                cv2.circle(image, (x, y), 1, white, -1, lineType=cv2.LINE_AA)
            cv2.imwrite(save_path, image)

        image_dir = '/home/lxr/database/test_data'
        image_paths = get_image_paths(image_dir)
        for image_path in image_paths:
            if not image_path.endswith('jpg'):
                continue
            image_path = osp.join(image_dir, image_path)
            image = cv2.imread(image_path)
            if image is None:
                print('image path error or image error')
                continue
            data = cal_keypoints(image)
            # save_keypoints(data, image_path, image)


def analyse_local_file():
    _analyse_image = analyse_image()
    _analyse_image.cal_save_data()

    image_path0 = '/home/lxr/Downloads/Re_02/img4417.jpg'
    image_path1 = '/home/lxr/Downloads/Re_02/img4418.jpg'
    pred = _analyse_image.cal_match_from_local(image_path0, image_path1)
    _analyse_image.analyse_pred(pred, image_path0, image_path1)


def main():
    # _analyse_image = analyse_image()
    # _analyse_image.cal_save_image()
    visionLocalization = VisionLocalization()
    visionLocalization.receive_image_choise()
    visionLocalization.receive_frame()


class VisionLocalization:
    '''
    used for visual positioning flight in Beijing office
    '''

    def __init__(self):
        self.init_log()
        self.init_config()
        rospy.init_node('vision_localization')
        self.q = Queue.Queue()
        self.img_index = 0

        self.device = 'cuda'
        config = {
            'superpoint': {
                'nms_radius': 4,
                'keypoint_threshold': self.superpoint_thresh,
                'max_keypoints': -1
            },
            'superglue': {
                'weights': self.weights,
                'sinkhorn_iterations': 20,
                'match_threshold': self.superglue_thresh,
            }
        }
        self.matching = Matching(config).eval().to(self.device)
        self.pre_datas = []
        self.pre_images = []
        self.choise_ids = []
        self.areas = []
        self.init_localpoints()

        pass

    def init_log(self):
        log_dir_path = osp.join(dirname, 'log')
        if not osp.exists(log_dir_path):
            print("log_dir_path 423 is:", log_dir_path)
            os.makedirs(log_dir_path)
        log_path = osp.join(log_dir_path, "%s.txt" % time.strftime("%Y%m%d", time.localtime()))
        global logger
        logger = setup_logger(output=log_path, name='vision_localization')

    def test_log(self):
        '''
        test log record function
        '''
        logger.info('#####$$$$$$$$%%%%%%%%')
        pass

    def init_config(self):
        '''
        initialize super point thresh,super glue thresh,model choise,image cut area setting.
        '''
        try:
            configpath = osp.join(dirname, "config/config.ini")
            if not os.path.exists(configpath):
                logger.error('config file not exist')
                return False
            config = ConfigParser.ConfigParser()
            config.read_file(open(configpath))
            self.superpoint_thresh = float(config.get("section0", "superpoint_thresh"))
            self.superglue_thresh = float(config.get("section0", "superglue_thresh"))
            self.weights = config.get("section0", "weights")

            image_size = config.get("section0", "image_size")
            self.image_size = None
            if not image_size == '0':
                # try:
                #     image_size = float(image_size)
                #     self.image_size = image_size
                # except ValueError:
                image_size = image_size.split(',')
                self.resize_w, self.resize_h = image_size
                self.resize_w = int(self.resize_w)
                self.resize_h = int(self.resize_h)
                print("self.resize_w",  self.resize_w)
                print("self.resize_h",  self.resize_h)
            else:
                self.image_size = image_size

            image_area = config.get("section0", "image_area")
            self.image_area = None
            if not image_area == '0':
                image_area = image_area.split(',')
                self.xmin, self.ymin, self.xmax, self.ymax = image_area
                self.xmin = int(self.xmin)
                self.ymin = int(self.ymin)
                self.xmax = int(self.xmax)
                self.ymax = int(self.ymax)
            else:
                self.image_area = image_area
            return True
        except Exception:
            logger.error("error", exc_info=True)
            return False

    def init_localpoints(self):
        '''
        initialize local pre-images superpoints,descriptions,image sizes.
        '''
        print("============Loading images npy===================")
        def save_keypoints(data, image_path, file_name):
            '''
            {
            'keypoints': keypoints,
            'scores': scores,
            'descriptors': descriptors,
            }
            '''
            
            # save_path = '/media/zmc/zmc/duwuqidata/superglue/image_messages/'
            save_path = 'out_put/image_messages/'
            path_name = file_name.replace('.jpg', '/')
            save_path = osp.join(save_path, path_name)
            # save_path = image_path.replace('.jpg', '/')
            if not osp.exists(save_path):
                print("save_path 500 is:", save_path)
                os.makedirs(save_path)

            keypoints = data['keypoints'][0].cpu().numpy()
            np.save(osp.join(save_path, 'keypoint.npy'), keypoints)
            scores = data['scores'][0].cpu().numpy()
            np.save(osp.join(save_path, 'scores.npy'), scores)
            descriptors = data['descriptors'][0].cpu().numpy()
            np.save(osp.join(save_path, 'descriptors.npy'), descriptors)
            areas = np.asarray(self.areas)
            np.save(osp.join(save_path, 'areas.npy'), areas)

            keypoint_txt_path = file_name.replace('.jpg','.txt')
            keypoints_size = len(keypoints)
            print("save keypoint 530 line")
            with open("out_put/keypoints/" + keypoint_txt_path, "w") as f:
                for i in range(keypoints_size):
                    # input_line = str(keypoints[i][0] + self.xmin) + " " + str(keypoints[i][1] + self.ymin) + "\n"
                    input_line = str(keypoints[i][0]) + " " + str(keypoints[i][1]) + "\n"
                    f.write(input_line)
            print("==============finish save keypoint===========================")

        def read_npy(image_path, input_image, file_name):
            '''
            {
            'keypoints': keypoints,
            'scores': scores,
            'descriptors': descriptors,
            }
            '''
            try:
                # np_path = image_path.replace('.jpg', '/')
                np_path = 'out_put/image_messages/'
                # np_path = '/media/zmc/Teclast_S20/localization_ws/project/superglue/image_messages/'
                path_name = file_name.replace('.jpg', '/')
                np_path = osp.join(np_path, path_name)
                if not osp.exists(osp.join(np_path, 'keypoint.npy')) or not osp.exists(
                        osp.join(np_path, 'scores.npy')) or not osp.exists(osp.join(np_path, 'descriptors.npy')):
                    return input_image, None
                np_keypoints = np.load(osp.join(np_path, 'keypoint.npy'))
                keypoints = torch.from_numpy(np_keypoints).to('cuda')
                np_scores = np.load(osp.join(np_path, 'scores.npy'))
                scores = torch.from_numpy(np_scores).to('cuda')
                np_descriptors = np.load(osp.join(np_path, 'descriptors.npy'))
                descriptors = torch.from_numpy(np_descriptors).to('cuda')
                np_areas = np.load(osp.join(np_path, 'areas.npy'),allow_pickle=True)

                input_image = cv2.cvtColor(input_image, cv2.COLOR_RGB2GRAY)
                # print("np_areas[0] != 0", np_areas[0])
                # print("np_areas[1] != 0", np_areas[1])


                if np_areas[0] != 0:
                    # print("np_areas[0] != 0", np_areas[0])
                    if len(np_areas[0]) > 1:
                        input_image = cv2.resize(input_image, (np_areas[0][0], np_areas[0][1]),
                                                 interpolation=cv2.INTER_AREA)
                    else:
                        input_image = cv2.resize(input_image, (
                            int(input_image.shape[1] * np_areas[0][0]),
                            int(input_image.shape[0] * np_areas[0][0])),
                                                 interpolation=cv2.INTER_AREA)
                # if np_areas[1] != 0:
                    # print("np_areas[1] != 0", np_areas[1])
                    # input_image = input_image[int(np_areas[1][0]):int(np_areas[1][1]), int(np_areas[1][2]):int(np_areas[1][3])]

                data = {
                    'keypoints': [keypoints],
                    'scores': (scores,),
                    'descriptors': [descriptors]
                }
                data['shape'] = (0, 0, input_image.shape[1], input_image.shape[0])
                data['size'] = np_areas[0]
                data['area']=np_areas[1] 
                return input_image, data
            except Exception:
                logger.error("error", exc_info=True)
                return input_image, None

        image_dir_path = osp.join(dirname, 'images')
        # image_dir_path = osp.join(dirname, 'images_0307')
        image_names = os.listdir(image_dir_path)
        image_names = sorted(image_names)
        for image_name in image_names:
            if not (image_name.endswith('.jpg') or image_name.endswith('.png') or image_name.endswith('JPG')):
                continue
            input_path = osp.join(image_dir_path, image_name)
            input_image = cv2.imread(input_path)
            input_image, data = read_npy(input_path, input_image, image_name)
            if data is None:
                input_image, data = self.cal_superpoint(input_image)
                save_keypoints(data, input_path, image_name)
            self.pre_images.append(input_image)
            data['image_name'] = image_name
            print(data)

            # data = {**data, 'image_name': image_name,
            #         'shape': (0, 0, self.input_image.shape[1], self.input_image.shape[0])}
            data = {**{}, **{k + '0': v for k, v in data.items()}}
            self.pre_datas.append(data)
        print("=============images npy read finish==================")
        pass

    def receive_frame(self):
        '''
        receive video stream through ros from camera or local video files
        '''

        # def imgmsg_to_cv2(img_msg):
        #     if img_msg.encoding != "bgr8":
        #         logger.info(
        #             "This Coral detect node has been hardcoded to the 'bgr8' encoding.  Come change the code if you're actually trying to implement a new camera")
        #     dtype = np.dtype("uint8")  # Hardcode to 8 bits...
        #     dtype = dtype.newbyteorder('>' if img_msg.is_bigendian else '<')
        #     image_opencv = np.ndarray(shape=(img_msg.height, img_msg.width, 3),
        #                               # and three channels of data. Since OpenCV works with bgr natively, we don't need to reorder the channels.
        #                               dtype=dtype, buffer=img_msg.data)
        #     # If the byt order is different between the message and the system.
        #     if img_msg.is_bigendian == (sys.byteorder == 'little'):
        #         image_opencv = image_opencv.byteswap().newbyteorder()
        #     return image_opencv
        

        def imgmsg_to_cv2(img_msg):
            np_arr = np.fromstring(img_msg.data, np.uint8)
            img = cv2.imdecode(np_arr, cv2.COLOR_RGB2GRAY)
            return img

        def callback_receive_img(data):
            try:
                # print('callback_receive_img')
                image = imgmsg_to_cv2(data)
                # image =  cv2.resize(image, (1440, 1440),interpolation=cv2.INTER_AREA)
                #图像旋转
                # (h, w) = image.shape[:2]
                # (cX, cY) = (w // 2, h // 2)
                # # M = cv2.getRotationMatrix2D((cX, cY), -90, 1.0)
                # M = cv2.getRotationMatrix2D((cX, cY), 0, 1.0)

                # cos = np.abs(M[0, 0])
                # sin = np.abs(M[0, 1])
                # nW = int((h * sin) + (w * cos))
                # nH = int((h * cos) + (w * sin))
                # M[0, 2] += (nW / 2) - cX
                # M[1, 2] += (nH / 2) - cY
                # image = cv2.warpAffine(image, M, (nW, nH))

                # image = cv2.warpAffine(image, M, (w, h))
                # image = image[0:1080, 420:1500]
                #图像旋转
                # cv2.imwrite("image.jpg", image)

                # self.q.put((0, image, self.img_index, rospy.Time.now()))
                self.q.put((0, image, self.img_index, data.header.stamp))
                self.img_index += 1
                self.q.get() if self.q.qsize() > 1 else time.sleep(0.001)
            except Exception:
                logger.error("error", exc_info=True)

        def sub_receive_img():
            # rospy.Subscriber("/vision_localization/rtsp_stream",
            #                  Image, callback_receive_img)
            # rospy.Subscriber("/camera/fisheye2/image_raw/compressed",
            #                  Image, callback_receive_img,queue_size = 10000)
            rospy.Subscriber("/camera/fisheye2/image_raw/compressed",
                             CompressedImage, callback_receive_img,queue_size = 10000)
            rospy.spin()

        thread.start_new_thread(sub_receive_img, ())
        match_result = rospy.Publisher(
            "/vision_localization/match_result", PointMatch, queue_size=1)
        while not rospy.is_shutdown():
            try:
                time.sleep(0.1)
                t0 = time.time()
                if self.q.empty():
                    time.sleep(0.01)
                    continue
                while not self.q.empty():
                    camera_id, frame, img_index, t = self.q.get()
                input_image, data1 = self.cal_superpoint(frame)
                data1['image_name'] = 'stream'

                data1 = {**{}, **{k + '1': v for k, v in data1.items()}}
                _choise_ids = []
                match_msg = PointMatch()
                match_msg.header.frame_id = str(img_index)
                # match_msg.header.stamp = rospy.Time.now()
                match_msg.header.stamp = t
                image_index = -1
                
                if len(self.choise_ids) > 0:
                    print ('size is  %s' % str(len(self.choise_ids)))
                    # print('cal matches cost time:%s' % str(time.time() - t0))
                    print('init pose success .')
                    init_pose = 1
                else:
                    print ('size is  %s' % str(len(self.choise_ids)))
                    print('pose Initializing .')
                    init_pose = 0

                for pre_data in self.pre_datas:
                    if rospy.is_shutdown():
                        break
                    image_index += 1
                    pre_image_name = pre_data['image_name0']
                    # print("loop %s " % pre_image_name)
                    if len(self.choise_ids) > 0 and pre_image_name not in self.choise_ids :
                        # print('continue')
                        continue
                    match_msg.img_ids.append(pre_image_name)
                    data = {**pre_data, **data1}

                    t0 = time.time()
                    for k in data:
                        if isinstance(data[k], (list, tuple)):
                            if k.__contains__('shape') or k.__contains__('size') or k.__contains__('area'):
                                continue
                            data[k] = torch.stack(data[k])

                    # Perform the matching
                    print(data['shape0'])
                    print(data1['shape1'])
                    pred = {**pre_data, **data1, **self.matching.superglue(data)}
                    print('cal matches cost time:%s' % str(time.time() - t0))
                    kpts0 = pred['keypoints0'][0].cpu().numpy()
                    kpts1 = pred['keypoints1'][0].cpu().numpy()
                    matches = pred['matches0'][0].cpu().numpy()
                    valid = matches > -1
                    mkpts0 = kpts0[valid]
                    mkpts1 = kpts1[matches[valid]]
                    match_indexs = np.argwhere(valid == True)

                    match_size = len(mkpts0)
                    print("match size is  %s " % str(match_size))

                    polygon = Polygon()
                    # scale_x = 1080 / 1440
                    # scale_y = 1920 / 1440
                    scale_x = 848 / self.resize_w
                    scale_y = 800 / self.resize_h
                    # print("scale_x", scale_x)
                    # print("scale_y", scale_y)
                    # print("data1['size1'][0]", data1['size1'][0])
                    # print("data1['size1'][1]", data1['size1'][1])
                    for i in range(match_size):
                        point = Point32()
                        point.x = mkpts1[i][0]
                        point.y = mkpts1[i][1]
                        if data1['area1'][0] != 0:
                            point.x += data1['area1'][2]
                            point.y += data1['area1'][0]
                        if data1['size1'][0] !=0:
                            if len(data1['size1']) > 1:
                                # print("pt xy is:", point.x)
                                # print(point.y)
                                point.x = point.x * scale_x
                                point.y = (point.y * scale_y)
                                # point.y = 1920 - (point.y * scale_y)
                                # point.x = point.x / data1['size1'][0]
                                # point.y = point.y / data1['size1'][1]
                            else:
                                # point.x = point.x  / data1['size1'][0]
                                # point.y = point.y  / data1['size1'][0]
                                point.x = point.x * scale_x
                                point.y = (point.y * scale_y)
                                # point.y = 1920 - (point.y * scale_y)
                                # print("else pt xy is:", point.x)
                                # print(point.y)
                        point.z = match_indexs[i]
                        polygon.points.append(point)
                    match_msg.polygons.append(polygon)

                    if match_size > 400 and init_pose == 0:
                        match_msg.img_ids = []
                        match_msg.polygons = []
                        match_msg.polygons.append(polygon)
                        match_msg.img_ids.append(pre_image_name)
                        match_msg.header.stamp = t
                        match_result.publish(match_msg)
                        print("80 publish data size is ", len(match_msg.polygons))
                        torch.cuda.empty_cache()
                        # pub images
                        confidence = pred['matching_scores0'][0].cpu().numpy()
                        last_frame = self.pre_images[image_index]
                        self.show_images(confidence, valid, last_frame,
                                        input_image, kpts0, kpts1, mkpts0, mkpts1)
                        break
                        # print("init publish size %s" % str(match_msg.polygons.points.size))
                    torch.cuda.empty_cache()
                    # pub images
                    confidence = pred['matching_scores0'][0].cpu().numpy()
                    last_frame = self.pre_images[image_index]
                    self.show_images(confidence, valid, last_frame,
                                    input_image, kpts0, kpts1, mkpts0, mkpts1)
                print("match size is: ", len(match_msg.polygons))
                print("init pose isL ", init_pose)
                # if len(match_msg.polygons) > 0 and init_pose == 1:
                if len(match_msg.polygons) > 0:
                    # print("loop match size is  %s " % str(match_msg.polygons[].points.size))
                    print("publish data size is ", len(match_msg.polygons))
                    print("loop match image is  %s " % str(match_msg.img_ids[0]))
                    match_result.publish(match_msg)
            except Exception:
                logger.error("error", exc_info=True)
        pass

    def receive_image_choise(self):
        def callback_receive_imgchoise(data):
            try:
                # print('callback_receive_img')
                self.choise_ids = data.img_ids
            except Exception:
                logger.error("error", exc_info=True)

        def sub_receive_img():
            rospy.Subscriber("/loop_index",
                             PointMatch, callback_receive_imgchoise)
            rospy.spin()

        thread.start_new_thread(sub_receive_img, ())

    def cal_superpoint(self, input_image):
        if self.image_size != '0':
            if self.image_size is None:
                input_image = cv2.resize(input_image, (self.resize_w, self.resize_h),
                                         interpolation=cv2.INTER_AREA)
                self.areas.append([self.resize_w, self.resize_h])
            else:
                input_image = cv2.resize(input_image, (
                    int(input_image.shape[1] * self.image_size), int(input_image.shape[0] * self.image_size)),
                                         interpolation=cv2.INTER_AREA)
                #change
                # input_image = cv2.resize(input_image, (
                #     int(input_image.shape[1] * 1.0), int(input_image.shape[0] * 1.0)),
                #                          interpolation=cv2.INTER_AREA)
                self.areas.append([self.image_size])
        else:
            self.areas.append([0])
        # print("self.image_area", self.image_area)
        if self.image_area != '0':
            input_image = input_image[int(self.ymin):int(self.ymax), int(self.xmin):int(self.xmax)]
            self.areas.append([self.ymin, self.ymax, self.xmin, self.xmax])
        else:
            self.areas.append([0])
        input_image = cv2.cvtColor(input_image, cv2.COLOR_RGB2GRAY)
        # print(input_image.shape[:])
        frame_tensor = frame2tensor(input_image, self.device)
        data = self.matching.superpoint({'image': frame_tensor})
        # data['shape'] = (0, 0, input_image.shape[1],~ input_image.shape[0])
        data['shape'] = (0, 0, input_image.shape[1], input_image.shape[0])
        data['size'] = self.areas[0]
        data['area']=self.areas[1]
        return input_image, data



    def show_images(self, confidence, valid, last_frame, frame, kpts0, kpts1, mkpts0, mkpts1):
        image_pubulish = rospy.Publisher('/camera/image_raw',Image,queue_size=1)
        def publish_image(imgdata):
            image_temp=Image()
            header = Header(stamp=rospy.Time.now())
            header.frame_id = 'map'
            image_temp.height=imgdata.shape[0]
            image_temp.width=imgdata.shape[1]
            image_temp.encoding='bgr8'
            image_temp.data=np.array(imgdata).tostring()
            #print(imgdata)
            #image_temp.is_bigendian=True
            image_temp.header=header
            image_temp.step=imgdata.shape[2]*imgdata.shape[1]
            image_pubulish.publish(image_temp)

        
        color = cm.jet(confidence[valid])
        text = [
            'SuperGlue',
            'Keypoints: {}:{}'.format(len(kpts0), len(kpts1)),
            'Matches: {}'.format(len(mkpts0))
        ]
        k_thresh = self.matching.superpoint.config['keypoint_threshold']
        m_thresh = self.matching.superglue.config['match_threshold']
        small_text = [
            'Keypoint Threshold: {:.4f}'.format(k_thresh),
            'Match Threshold: {:.2f}'.format(m_thresh),
            'Image Pair: {:06}:{:06}'.format(0, 1),
        ]
        #有连线的匹配图
        out = make_matching_plot_fast(
            last_frame, frame, kpts0, kpts1, mkpts0, mkpts1, color, text,
            path=None, show_keypoints=True, small_text=small_text, show_line=True)
        # cv2.namedWindow('SuperGlue matches', cv2.WINDOW_NORMAL)
        publish_image(out)
        # cv2.resizeWindow('SuperGlue matches', (960, 540))
        # cv2.imshow('SuperGlue matches', out)
        # cv2.waitKey(500)
        # cv2.destroyWindow('SuperGlue matches')
        # cv2.imwrite(self.test_out_dir + '_00.jpg', out)
        #没有有连线的匹配图
        out = make_matching_plot_fast(
            last_frame, frame, kpts0, kpts1, mkpts0, mkpts1, color, text,
            path=None, show_keypoints=True, small_text=small_text, show_line=False)
        
        # cv2.imshow('SuperGlue matches', out)
        # cv2.waitKey(500)
        # cv2.destroyWindow('SuperGlue matches')
        # cv2.imwrite(self.test_out_dir + '_01.jpg', out)


if __name__ == '__main__':
    main()
