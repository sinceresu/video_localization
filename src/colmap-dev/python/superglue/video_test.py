#! /usr/bin/env python3

import os
import vlc
import time
def get_player(url='rtsp://192.168.2.134:8554/live'):
    Instance = vlc.Instance()
    player = Instance.media_player_new()
    Media = Instance.media_new(url)
    Media.get_mrl()
    player.set_media(Media)
    player.play()
    return player

def get_player_state(player):
      is_playing = player.is_playing()#playing:1 ;other:0
      will_play = player.will_play()
      get_state = player.get_state()
      print 'is_playing: ',is_playing
      print 'will_play: ', will_play
      print 'get_state: ', get_state

def get_player_para(player):
    length = player.get_length()
    width = player.video_get_width()
    size = player.video_get_size()
    title = player.video_get_title_description()
    video_track = player.video_get_track_description()
    audio_track = player.audio_get_track_description()
    fps = player.get_fps()
    rate = player.get_rate()
    track_count = player.video_get_track_count()
    track = player.video_get_track()
    print 'length: ',length
    print 'width: ',width
    print 'size: ', size
    print 'title: ',title
    print 'video_track: ',video_track
    print 'audio_track: ',audio_track
    print 'fps: ',fps
    print 'rate: ', rate
    print 'track_count: ',track_count
    print 'track: ',track

def vlc_main():
    timestamp = time.time()
    while time.time()-timestamp < 400:
        player = get_player()
        time.sleep(1)  # must delay at least one second
        if player.is_playing():
            time.sleep(10)
            get_player_para(player)
            player.video_take_snapshot(num=0, psz_filepath=os.getcwd(), i_width=1080, i_height=360)
            #time.sleep(10)
            player.release()
            return
        player.release()
    player.release()
    print '###########################'
    print 'Fail: cannot find any video'
    print '###########################'

if __name__ == '__main__':
    vlc_main()