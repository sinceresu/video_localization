#COLMAP
视觉反定位建图功能及Block上传下载

#执行程序说明
|名称|功能|使用|缺省
|--------------------- |-----------|----------------------|------------------|
load_colmap_map_node|读取blocks中的地图信息，将定位需要信息保存为二进制地图，将匹配需要信息保存为txt|./load_colmap_map_node block地址 图像地址 block_key|./load_colmap_map_node /home/zmc/Desktop/match0225 "2022_4_19" /media/zmc/Teclast_S20/localization_ws/project/colmap-dev/python/superglue/out_put/test|
save_colmap_map_node|读取colmap建图结果和super point相关信息，上传至block|./save_colmap_map_node colmap地图地址 superpoint信息地址 block存储地址  las文件 要保存的图像地址|./save_colmap_map_node /media/zmc/Teclast_S20/localization_ws/project/colmap-dev/python/superglue/images/sparse /media/zmc/Teclast_S20/localization_ws/project/colmap-dev python/superglue/out_put/image_messages /home/zmc/Desktop/match0225 /home/zmc/Desktop/map.las /media/zmc/Teclast_S20/localization_ws/project/colmap-dev/python/superglue/images/|
write_match_node|读取通过super glue产生的匹配对，把匹配对信息写入到db文件中。|./write_match_node superglue地址|./write_match_node /media/zmc/Teclast_S20/localization_ws/project/colmap-dev/python|

======

About
-----

COLMAP is a general-purpose Structure-from-Motion (SfM) and Multi-View Stereo
(MVS) pipeline with a graphical and command-line interface. It offers a wide
range of features for reconstruction of ordered and unordered image collections.
The software is licensed under the new BSD license. If you use this project for
your research, please cite:

    @inproceedings{schoenberger2016sfm,
        author={Sch\"{o}nberger, Johannes Lutz and Frahm, Jan-Michael},
        title={Structure-from-Motion Revisited},
        booktitle={Conference on Computer Vision and Pattern Recognition (CVPR)},
        year={2016},
    }

    @inproceedings{schoenberger2016mvs,
        author={Sch\"{o}nberger, Johannes Lutz and Zheng, Enliang and Pollefeys, Marc and Frahm, Jan-Michael},
        title={Pixelwise View Selection for Unstructured Multi-View Stereo},
        booktitle={European Conference on Computer Vision (ECCV)},
        year={2016},
    }

If you use the image retrieval / vocabulary tree engine, please also cite:

    @inproceedings{schoenberger2016vote,
        author={Sch\"{o}nberger, Johannes Lutz and Price, True and Sattler, Torsten and Frahm, Jan-Michael and Pollefeys, Marc},
        title={A Vote-and-Verify Strategy for Fast Spatial Verification in Image Retrieval},
        booktitle={Asian Conference on Computer Vision (ACCV)},
        year={2016},
    }

The latest source code is available at https://github.com/colmap/colmap. COLMAP
builds on top of existing works and when using specific algorithms within
COLMAP, please also cite the original authors, as specified in the source code.


Download
--------

Executables for Windows and Mac and other resources can be downloaded from
https://demuc.de/colmap/. Executables for Linux/Unix/BSD are available at
https://repology.org/metapackage/colmap/versions. To build COLMAP from source,
please see https://colmap.github.io/install.html.

Getting Started
---------------

1. Download the pre-built binaries from https://demuc.de/colmap/ or build the
   library manually as described in the documentation.
2. Download one of the provided datasets at https://demuc.de/colmap/datasets/
   or use your own images.
3. Use the **automatic reconstruction** to easily build models
   with a single click or command.


Documentation
-------------

The documentation is available at https://colmap.github.io/.


Support
-------

Please, use the COLMAP Google Group at
https://groups.google.com/forum/#!forum/colmap (colmap@googlegroups.com) for
questions and the GitHub issue tracker at https://github.com/colmap/colmap for
bug reports, feature requests/additions, etc.


Acknowledgments
---------------

The library was written by Johannes L. Schönberger (https://demuc.de/). Funding
was provided by his PhD advisors Jan-Michael Frahm (http://frahm.web.unc.edu/)
and Marc Pollefeys (https://people.inf.ethz.ch/pomarc/).


Contribution
------------

Contributions (bug reports, bug fixes, improvements, etc.) are very welcome and
should be submitted in the form of new issues and/or pull requests on GitHub.


License
-------

The COLMAP library is licensed under the new BSD license. Note that this text
refers only to the license for COLMAP itself, independent of its dependencies,
which are separately licensed. Building COLMAP with these dependencies may
affect the resulting COLMAP license.

    Copyright (c) 2018, ETH Zurich and UNC Chapel Hill.
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        * Redistributions of source code must retain the above copyright
          notice, this list of conditions and the following disclaimer.

        * Redistributions in binary form must reproduce the above copyright
          notice, this list of conditions and the following disclaimer in the
          documentation and/or other materials provided with the distribution.

        * Neither the name of ETH Zurich and UNC Chapel Hill nor the names of
          its contributors may be used to endorse or promote products derived
          from this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    Author: Johannes L. Schoenberger (jsch-at-demuc-dot-de)
