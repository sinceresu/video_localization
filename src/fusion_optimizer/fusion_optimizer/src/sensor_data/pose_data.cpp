/*
 * @Description: POSE DATA TYPE
 * @Author: Zhou Mingchao
 * @Date: 2022-01-10 
 */
#include "fusion_optimizer/sensor_data/pose_data.hpp"

namespace fusion_optimizer {
Eigen::Quaternionf MPoseData::GetQuaternion() {
    Eigen::Quaternionf q;
    q = pose.block<3,3>(0,0);

    return q;
}

bool OPoseData::SyncData(std::deque<OPoseData>& UnsyncedData, std::deque<OPoseData>& SyncedData, double sync_time)
{
    while(UnsyncedData.size() > 2)
    {
        if (UnsyncedData.front().stamp > sync_time)
        {
            return false;
        }

        if (UnsyncedData.at(1).stamp < sync_time)
        {
            UnsyncedData.pop_front();
            continue;
        }
        if ((sync_time - UnsyncedData.front().stamp) > 0.2)
        {
            UnsyncedData.pop_front();
            return false;
        }
        if ((UnsyncedData.at(1).stamp - sync_time) > 0.2)
        {
            UnsyncedData.pop_front();
            return false;
        }
    }
    if (UnsyncedData.size() < 2)
        return false;
    
    OPoseData pre_pose = UnsyncedData.front();
    OPoseData cur_pose = UnsyncedData.at(1);
    OPoseData sync_pose;

    double front_scale = (cur_pose.stamp - sync_time) / (cur_pose.stamp - pre_pose.stamp);
    double back_scale = (sync_time - pre_pose.stamp) / (cur_pose.stamp - pre_pose.stamp);
    sync_pose.stamp = sync_time;


    sync_pose.orientation.x() = pre_pose.orientation.x() * front_scale + cur_pose.orientation.x() * back_scale;
    sync_pose.orientation.y() = pre_pose.orientation.y() * front_scale + cur_pose.orientation.y() * back_scale;
    sync_pose.orientation.z() = pre_pose.orientation.z() * front_scale + cur_pose.orientation.z() * back_scale;
    sync_pose.orientation.w() = pre_pose.orientation.w() * front_scale + cur_pose.orientation.w() * back_scale;


    sync_pose.position[0] = pre_pose.position[0] * front_scale + cur_pose.position[0] * back_scale;
    sync_pose.position[1] = pre_pose.position[1] * front_scale + cur_pose.position[1] * back_scale;
    sync_pose.position[2] = pre_pose.position[2] * front_scale + cur_pose.position[2] * back_scale;

    SyncedData.push_back(sync_pose);
}
}