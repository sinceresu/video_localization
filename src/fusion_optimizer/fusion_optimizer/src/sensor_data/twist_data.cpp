/*
 * @Description: Twist DATA TYPE
 * @Author: Zhou Mingchao
 * @Date: 2022-04-11 
 */
#include "fusion_optimizer/sensor_data/twist_data.hpp"

#include <cmath>
#include "glog/logging.h"

namespace fusion_optimizer {

TwistData::TwistData()
{
    // Clear();
}
TwistData::TwistData(const TwistData &data)
:vel(data.vel), gyro(data.gyro)
{
}
TwistData &TwistData::operator=(const TwistData &rhs)
{
    vel = rhs.vel;
    gyro = rhs.gyro;
    return *this;
}

Eigen::Matrix3f TwistData::GetOrientationMatrix() {
/*     Eigen::Quaterniond q(orientation.w, orientation.x, orientation.y, orientation.z);
    Eigen::Matrix3f matrix = q.matrix().cast<float>();

    return matrix; */
}

bool TwistData::SyncData(std::deque<TwistData>& UnsyncedData, std::deque<TwistData>& SyncedData, double sync_time) {
    // 传感器数据按时间序列排列，在传感器数据中为同步的时间点找到合适的时间位置
    // 即找到与同步时间相邻的左右两个数据
    // 需要注意的是，如果左右相邻数据有一个离同步时间差值比较大，则说明数据有丢失，时间离得太远不适合做差值
    while (UnsyncedData.size() >= 2) {
        if (UnsyncedData.front().time > sync_time) 
            return false;
        if (UnsyncedData.at(1).time < sync_time) {
            UnsyncedData.pop_front();
            continue;
        }
        if (sync_time - UnsyncedData.front().time > 0.2) {
            UnsyncedData.pop_front();
            return false;
        }
        
        if (UnsyncedData.at(1).time - sync_time > 0.2) {
            UnsyncedData.pop_front();
            return false;
        }
        break;
    }
    if (UnsyncedData.size() < 2)
        return false;

    TwistData front_data = UnsyncedData.at(0);
    TwistData back_data = UnsyncedData.at(1);
    TwistData synced_data;

    double front_scale = (back_data.time - sync_time) / (back_data.time - front_data.time);
    double back_scale = (sync_time - front_data.time) / (back_data.time - front_data.time);
    synced_data.time = sync_time;

    synced_data.vel = front_data.vel * front_scale + back_data.vel * back_scale;
    synced_data.gyro = front_data.gyro * front_scale + back_data.gyro * back_scale;
    SyncedData.push_back(synced_data);

    return true;
}

}