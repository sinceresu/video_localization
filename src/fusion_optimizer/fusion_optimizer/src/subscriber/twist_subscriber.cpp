#include "fusion_optimizer/subscriber/twist_subscriber.hpp"
#include "glog/logging.h"

namespace fusion_optimizer{
TwistSubscriber::TwistSubscriber(ros::NodeHandle& nh, std::string topic_name, size_t buff_size)
    :nh_(nh) {
    subscriber_ = nh_.subscribe(topic_name, buff_size, &TwistSubscriber::msg_callback, this);
}

void TwistSubscriber::msg_callback(const geometry_msgs::TwistStampedConstPtr& twist_msg_ptr) {
    buff_mutex_.lock();
    TwistData twist_data;
    twist_data.time = twist_msg_ptr->header.stamp.toSec();

    twist_data.vel[0] = twist_msg_ptr->twist.linear.x;
    twist_data.vel[1] = twist_msg_ptr->twist.linear.y;
    twist_data.vel[2] = twist_msg_ptr->twist.linear.z;

    twist_data.gyro[0] = twist_msg_ptr->twist.angular.x;
    twist_data.gyro[1] = twist_msg_ptr->twist.angular.y;
    twist_data.gyro[2] = twist_msg_ptr->twist.angular.z;

    new_twist_data_.push_back(twist_data);
    buff_mutex_.unlock();
}

void TwistSubscriber::ParseData(std::deque<TwistData>& twist_data_buff) {
    buff_mutex_.lock();
    if (new_twist_data_.size() > 0) {
        twist_data_buff.insert(twist_data_buff.end(), new_twist_data_.begin(), new_twist_data_.end());
        new_twist_data_.clear();
    }
    buff_mutex_.unlock();
}

}