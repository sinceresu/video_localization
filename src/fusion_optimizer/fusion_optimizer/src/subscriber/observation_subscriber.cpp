/*
 * @Description: 订阅imu数据
 * @Author: Zhou Mingchao
 * @Date: 2022-01-10 
 */
#include "fusion_optimizer/subscriber/observation_subscriber.hpp"
#include "glog/logging.h"

namespace fusion_optimizer{
ObservationSubscriber::ObservationSubscriber(ros::NodeHandle& nh, std::string topic_name, size_t buff_size)
    :nh_(nh) {
    subscriber_ = nh_.subscribe(topic_name, buff_size, &ObservationSubscriber::msg_callback, this);
    topic_name_ = topic_name;
}

void ObservationSubscriber::msg_callback(const nav_msgs::OdometryConstPtr& odom_msg_ptr) {
    buff_mutex_.lock();
    State pose_data;
    pose_data.stamp = odom_msg_ptr->header.stamp.toSec();
    pose_data.time = odom_msg_ptr->header.stamp.toSec();

    //set the position
    pose_data.p(0,0) = odom_msg_ptr->pose.pose.position.x;
    pose_data.p(1,0) = odom_msg_ptr->pose.pose.position.y;
    pose_data.p(2,0) = odom_msg_ptr->pose.pose.position.z;

    pose_data.q.x() = odom_msg_ptr->pose.pose.orientation.x;
    pose_data.q.y() = odom_msg_ptr->pose.pose.orientation.y;
    pose_data.q.z() = odom_msg_ptr->pose.pose.orientation.z;
    pose_data.q.w() = odom_msg_ptr->pose.pose.orientation.w;

    new_pose_data_.push_back(pose_data);
    buff_mutex_.unlock();
}

/* void ObservationSubscriber::msg_callback(const nav_msgs::OdometryConstPtr& odom_msg_ptr) {
    buff_mutex_.lock();
    MPoseData pose_data;
    pose_data.time = odom_msg_ptr->header.stamp.toSec();

    //set the position
    pose_data.pose(0,3) = odom_msg_ptr->pose.pose.position.x;
    pose_data.pose(1,3) = odom_msg_ptr->pose.pose.position.y;
    pose_data.pose(2,3) = odom_msg_ptr->pose.pose.position.z;

    Eigen::Quaternionf q;
    q.x() = odom_msg_ptr->pose.pose.orientation.x;
    q.y() = odom_msg_ptr->pose.pose.orientation.y;
    q.z() = odom_msg_ptr->pose.pose.orientation.z;
    q.w() = odom_msg_ptr->pose.pose.orientation.w;
    pose_data.pose.block<3,3>(0,0) = q.matrix();

    new_pose_data_.push_back(pose_data);
    buff_mutex_.unlock();
} */

void ObservationSubscriber::ParseData(std::deque<State>& pose_data_buff) {
    buff_mutex_.lock();
    if (new_pose_data_.size() > 0) {
        pose_data_buff.insert(pose_data_buff.end(), new_pose_data_.begin(), new_pose_data_.end());
        new_pose_data_.clear();
    }
    buff_mutex_.unlock();
}

/* void ObservationSubscriber::ValidData(const double start_time, const double end_time, std::deque<State>& deque_imu_data)
{
    // Subscriber::ValidData<State>(start_time, end_time, new_pose_data_, deque_imu_data);
} */

}