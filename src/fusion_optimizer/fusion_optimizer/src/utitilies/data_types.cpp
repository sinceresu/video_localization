/**
 * @file data_types.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-2-6
 * @brief data types
 */

#include "fusion_optimizer/utitilies/data_types.hpp"


namespace fusion_optimizer
{
    State::State()
        : type(StateType::None),
          index(-1),
          stamp(-1.0),
          time(-1.0),
          p(Eigen::Vector3d::Zero()),
          q(1, 0, 0, 0),
          v(Eigen::Vector3d::Zero()),
          ba(Eigen::Vector3d::Zero()),
          bg(Eigen::Vector3d::Zero())
    {
    }
    State::State(const State &data)
        : type(data.type),
          index(data.index),
          stamp(data.stamp),
          time(data.stamp),
          p(data.p),
          q(data.q),
          v(data.v),
          ba(data.ba),
          bg(data.bg),
          covariance(data.covariance)
    {
    }
    State &State::operator=(const State &rhs)
    {
        type = rhs.type;
        index = rhs.index;
        stamp = rhs.stamp;
        time = rhs.stamp;
        p = rhs.p;
        q = rhs.q;
        v = rhs.v;
        ba = rhs.ba;
        bg = rhs.bg;
        covariance = rhs.covariance;
        return *this;
    }
    void State::Print()
    {
        LOG(INFO) << "State information: ";
        LOG(INFO) << "time is: " << std::setprecision(20)<< stamp;
        LOG(INFO) << "p is: " << p(0) << ", "<< p(1) << ", " << p(2) << ". ";
        LOG(INFO) << "q is: " << q.w() << ", "<< q.x() << ", " << q.y() << ", " << q.z() << ". ";
        LOG(INFO) << "v is: " << v(0) << ", "<< v(1) << ", " << v(2) << ". ";
        LOG(INFO) << "ba is: " << ba(0) << ", "<< ba(1) << ", " << ba(2) << ". ";
        LOG(INFO) << "bg is: " << bg(0) << ", "<< bg(1) << ", " << bg(2) << ". ";
        if (covariance.data()){
            LOG(INFO) << "state covariance = ";
            for (int j = 0; j < 6; ++j)
            {
                LOG(INFO) << covariance(j, j) << std::endl;
            }
        }
        LOG(INFO) << "---------------";
    }

    int State::SyncData(std::deque<State>& UnsyncedData, std::deque<State>& SyncedData, double sync_time)
    {
        if (!SyncedData.empty() && sync_time == SyncedData.back().stamp)
        {
            // std::cout << std::setprecision(20) << "synv_time : "  << sync_time << std::endl
            //           << "SyncedData.back().stamp : "  << SyncedData.back().stamp << std::endl;
            return 1;
        }

        while(UnsyncedData.size() >= 2)
        {
            if (UnsyncedData.front().stamp > sync_time)
            {
                return 1;
            }

            if (UnsyncedData.at(1).stamp < sync_time)
            {
                UnsyncedData.pop_front();
                continue;
            }
            if ((sync_time - UnsyncedData.front().stamp) > 0.2)
            {
                UnsyncedData.pop_front();
                return 0;
            }
            if ((UnsyncedData.at(1).stamp - sync_time) > 0.2)
            {
                UnsyncedData.pop_front();
                return 0;
            }
            break;
        }
        if (UnsyncedData.size() < 2)
            return 0;
        
        State pre_pose = UnsyncedData.front();
        State cur_pose = UnsyncedData.at(1);
        State sync_pose;

        double front_scale = (cur_pose.stamp - sync_time) / (cur_pose.stamp - pre_pose.stamp);
        double back_scale = (sync_time - pre_pose.stamp) / (cur_pose.stamp - pre_pose.stamp);
        sync_pose.stamp = sync_time;


        sync_pose.q.x() = pre_pose.q.x() * front_scale + cur_pose.q.x() * back_scale;
        sync_pose.q.y() = pre_pose.q.y() * front_scale + cur_pose.q.y() * back_scale;
        sync_pose.q.z() = pre_pose.q.z() * front_scale + cur_pose.q.z() * back_scale;
        sync_pose.q.w() = pre_pose.q.w() * front_scale + cur_pose.q.w() * back_scale;


        sync_pose.p[0] = pre_pose.p[0] * front_scale + cur_pose.p[0] * back_scale;
        sync_pose.p[1] = pre_pose.p[1] * front_scale + cur_pose.p[1] * back_scale;
        sync_pose.p[2] = pre_pose.p[2] * front_scale + cur_pose.p[2] * back_scale;

        SyncedData.push_back(sync_pose);
        return 2;
    }

    // BState::BState()
    //     : stamp(-1.0),
    //       linear_velocity(Eigen::Vector3f::Zero()),
    //       angular_velocity(Eigen::Vector3f::Zero())
    // {
    // }
    // BState::BState(const BState &data)
    //     : stamp(data.stamp),
    //       linear_velocity(data.linear_velocity),
    //       angular_velocity(data.angular_velocity)
    // {
    // }
    // BState &BState::operator=(const BState &rhs)
    // {
    //     stamp = rhs.stamp;
    //     linear_velocity = rhs.linear_velocity;
    //     angular_velocity = rhs.angular_velocity;
    //     return *this;
    // }

    // IState::IState()
    //     : stamp(-1.0)
    // {
    // }
    // IState::IState(const IState &data)
    //     : stamp(data.stamp),
    //       pose(data.pose)
    // {
    // }
    // IState &IState::operator=(const IState &rhs)
    // {
    //     stamp = rhs.stamp;
    //     pose = rhs.pose;
    //     return *this;
    // }

    // MState::MState()
    //     : stamp(-1.0),
    //       linear_velocity(Eigen::Vector3d::Zero()),
    //       angular_velocity(Eigen::Vector3d::Zero())
    // {
    // }
    // MState::MState(const MState &data)
    //     : stamp(data.stamp),
    //       pose(data.pose),
    //       linear_velocity(data.linear_velocity),
    //       angular_velocity(data.angular_velocity)
    // {
    // }
    // MState &MState::operator=(const MState &rhs)
    // {
    //     stamp = rhs.stamp;
    //     pose = rhs.pose;
    //     linear_velocity = rhs.linear_velocity;
    //     angular_velocity = rhs.angular_velocity;
    //     return *this;
    // }

    // PredictData::PredictData()
    // {
    //     stamps.clear();
    // }
    // PredictData::PredictData(const PredictData &data)
    //     : sensor_to_robot(data.sensor_to_robot),
    //       noise(data.noise),
    //       stamps(data.stamps)
    // {
    // }
    // PredictData &PredictData::operator=(const PredictData &rhs)
    // {
    //     sensor_to_robot = rhs.sensor_to_robot;
    //     noise = rhs.noise;
    //     stamps = rhs.stamps;
    //     return *this;
    // }

    // ImuData::ImuData() : PredictData()
    // {
    //     Clear();
    // }
    // ImuData::ImuData(const ImuData &data)
    //     : PredictData(data),
    //       gravity(data.gravity),
    //       acc(data.acc),
    //       gyro(data.gyro)
    // {
    // }
    // ImuData &ImuData::operator=(const ImuData &rhs)
    // {
    //     PredictData::operator=(rhs);
    //     gravity = rhs.gravity;
    //     acc = rhs.acc;
    //     gyro = rhs.gyro;
    //     return *this;
    // }
    // PredictType ImuData::GetType() const
    // {
    //     return PredictType::Imu;
    // }
    // void ImuData::Clear()
    // {
    //     stamps.clear();
    //     acc.clear();
    //     gyro.clear();
    // }

    // TwistData::TwistData() : PredictData()
    // {
    //     Clear();
    // }
    // TwistData::TwistData(const TwistData &data)
    //     : PredictData(data),
    //       vel(data.vel),
    //       gyro(data.gyro)
    // {
    // }
    // TwistData &TwistData::operator=(const TwistData &rhs)
    // {
    //     PredictData::operator=(rhs);
    //     vel = rhs.vel;
    //     gyro = rhs.gyro;
    //     return *this;
    // }
    // PredictType TwistData::GetType() const
    // {
    //     return PredictType::Twist;
    // }
    // void TwistData::Clear()
    // {
    //     stamps.clear();
    //     vel.clear();
    //     gyro.clear();
    // }

    // OdometryData::OdometryData() : PredictData()
    // {
    //     Clear();
    // }
    // OdometryData::OdometryData(const OdometryData &data)
    //     : PredictData(data),
    //       pose(data.pose)
    // {
    // }
    // OdometryData &OdometryData::operator=(const OdometryData &rhs)
    // {
    //     PredictData::operator=(rhs);
    //     pose = rhs.pose;
    //     return *this;
    // }
    // PredictType OdometryData::GetType() const
    // {
    //     return PredictType::Odometry;
    // }
    // void OdometryData::Clear()
    // {
    //     stamps.clear();
    //     pose.clear();
    // }

    // VgData::VgData() : PredictData()
    // {
    //     Clear();
    // }
    // VgData::VgData(const VgData &data)
    //     : PredictData(data),
    //       sensor_to_robot_gyro(data.sensor_to_robot_gyro),
    //       vel(data.vel),
    //       gyro(data.gyro)
    // {
    // }
    // VgData &VgData::operator=(const VgData &rhs)
    // {
    //     PredictData::operator=(rhs);
    //     sensor_to_robot_gyro = rhs.sensor_to_robot_gyro;
    //     vel = rhs.vel;
    //     gyro = rhs.gyro;
    //     return *this;
    // }
    // PredictType VgData::GetType() const
    // {
    //     return PredictType::Vg;
    // }
    // void VgData::Clear()
    // {
    //     stamps.clear();
    //     vel.clear();
    //     gyro.clear();
    // }

    // MeasureData::MeasureData()
    //     : stamp(-1.0)
    // {
    // }
    // MeasureData::MeasureData(const MeasureData &data)
    //     : sensor_to_robot(data.sensor_to_robot),
    //       noise(data.noise),
    //       stamp(data.stamp)
    // {
    // }
    // MeasureData &MeasureData::operator=(const MeasureData &rhs)
    // {
    //     sensor_to_robot = rhs.sensor_to_robot;
    //     noise = rhs.noise;
    //     stamp = rhs.stamp;
    // }

    // CloudData::CloudData()
    //     : MeasureData(),
    //       xyz_ptr(nullptr),
    //       xyzt_ptr(nullptr),
    //       xyzi_ptr(nullptr),
    //       xyzirt_ptr(nullptr)
    // {
    // }
    // CloudData::CloudData(const CloudData &data)
    //     : MeasureData(data),
    //       point_type(data.point_type),
    //       xyz_ptr(data.xyz_ptr),
    //       xyzt_ptr(data.xyzt_ptr),
    //       xyzi_ptr(data.xyzi_ptr),
    //       xyzirt_ptr(data.xyzirt_ptr)
    // {
    // }
    // CloudData &CloudData::operator=(const CloudData &rhs)
    // {
    //     MeasureData::operator=(rhs);
    //     point_type = rhs.point_type;
    //     xyz_ptr = rhs.xyz_ptr;
    //     xyzt_ptr = rhs.xyzt_ptr;
    //     xyzi_ptr = rhs.xyzi_ptr;
    //     xyzirt_ptr = rhs.xyzirt_ptr;
    //     return *this;
    // }
    // MeasureType CloudData::GetType() const
    // {
    //     return MeasureType::CloudM;
    // }

    // GNSSData::GNSSData() : MeasureData() {}
    // GNSSData::GNSSData(const GNSSData &data)
    //     : MeasureData(data),
    //       latitude(data.latitude),
    //       longitude(data.longitude),
    //       altitude(data.altitude),
    //       status(data.status),
    //       service(data.service)
    // {
    // }
    // GNSSData &GNSSData::operator=(const GNSSData &rhs)
    // {
    //     MeasureData::operator=(rhs);
    //     latitude = rhs.latitude;
    //     longitude = rhs.longitude;
    //     altitude = rhs.altitude;
    //     status = rhs.status;
    //     service = rhs.service;
    //     return *this;
    // }
    // MeasureType GNSSData::GetType() const
    // {
    //     return MeasureType::GNSSM;
    // }

    // ImageData::ImageData()
    //     : MeasureData(),
    //       image_ptr(nullptr)
    // {
    // }
    // ImageData::ImageData(const ImageData &data)
    //     : MeasureData(data),
    //       image_ptr(data.image_ptr)
    // {
    // }
    // ImageData &ImageData::operator=(const ImageData &rhs)
    // {
    //     MeasureData::operator=(rhs);
    //     image_ptr = rhs.image_ptr;
    //     return *this;
    // }
    // MeasureType ImageData::GetType() const
    // {
    //     return MeasureType::ImageM;
    // }

    // PoseData::PoseData()
    //     : MeasureData()
    // {
    // }
    // PoseData::PoseData(const PoseData &data)
    //     : MeasureData(data),
    //       pose(data.pose)
    // {
    // }
    // PoseData &PoseData::operator=(const PoseData &rhs)
    // {
    //     MeasureData::operator=(rhs);
    //     pose = rhs.pose;
    //     return *this;
    // }
    // MeasureType PoseData::GetType() const
    // {
    //     return MeasureType::PoseM;
    // }

    // PData::PData()
    //     : MeasureData()
    // {
    // }
    // PData::PData(const PData &data)
    //     : MeasureData(data),
    //       p(data.p)
    // {
    // }
    // PData &PData::operator=(const PData &rhs)
    // {
    //     MeasureData::operator=(rhs);
    //     p = rhs.p;
    //     return *this;
    // }
    // MeasureType PData::GetType() const
    // {
    //     return MeasureType::PM;
    // }

    // QData::QData()
    //     : MeasureData()
    // {
    // }
    // QData::QData(const QData &data)
    //     : MeasureData(data),
    //       q(data.q)
    // {
    // }
    // QData &QData::operator=(const QData &rhs)
    // {
    //     MeasureData::operator=(rhs);
    //     q = rhs.q;
    //     return *this;
    // }
    // MeasureType QData::GetType() const
    // {
    //     return MeasureType::QM;
    // }

    // PreprocessedData::PreprocessedData()
    //     : stamp(-1.0)
    // {
    // }
    // PreprocessedData::PreprocessedData(const PreprocessedData &data)
    //     : sensor_to_robot(data.sensor_to_robot),
    //       noise(data.noise),
    //       stamp(data.stamp)
    // {
    // }
    // PreprocessedData &PreprocessedData::operator=(const PreprocessedData &rhs)
    // {
    //     sensor_to_robot = rhs.sensor_to_robot;
    //     noise = rhs.noise;
    //     stamp = rhs.stamp;
    // }

    // CloudXYZData::CloudXYZData()
    //     : PreprocessedData(),
    //       xyz_ptr(nullptr)
    // {
    // }
    // CloudXYZData::CloudXYZData(const CloudXYZData &data)
    //     : PreprocessedData(data),
    //       xyz_ptr(data.xyz_ptr)
    // {
    // }
    // CloudXYZData &CloudXYZData::operator=(const CloudXYZData &rhs)
    // {
    //     PreprocessedData::operator=(rhs);
    //     xyz_ptr = rhs.xyz_ptr;
    //     return *this;
    // }
    // PreprocessedType CloudXYZData::GetType() const
    // {
    //     return PreprocessedType::CloudXYZ;
    // }

    // CloudXYZIData::CloudXYZIData()
    //     : PreprocessedData(),
    //       xyzi_ptr(nullptr)
    // {
    // }
    // CloudXYZIData::CloudXYZIData(const CloudXYZIData &data)
    //     : PreprocessedData(data),
    //       xyzi_ptr(data.xyzi_ptr)
    // {
    // }
    // CloudXYZIData &CloudXYZIData::operator=(const CloudXYZIData &rhs)
    // {
    //     PreprocessedData::operator=(rhs);
    //     xyzi_ptr = rhs.xyzi_ptr;
    //     return *this;
    // }
    // PreprocessedType CloudXYZIData::GetType() const
    // {
    //     return PreprocessedType::CloudXYZI;
    // }

    // CloudImageData::CloudImageData()
    //     : PreprocessedData(),
    //       xyzi_ptr(nullptr),
    //       range_matrix(),
    //       ang_res_x(0),
    //       ang_res_y(0),
    //       rows(0),
    //       cols(0)
    // {
    // }
    // CloudImageData::CloudImageData(const CloudImageData &data)
    //     : PreprocessedData(data),
    //       xyzi_ptr(data.xyzi_ptr),
    //       range_matrix(data.range_matrix),
    //       ang_res_x(data.ang_res_x),
    //       ang_res_y(data.ang_res_y),
    //       rows(data.rows),
    //       cols(data.cols)
    // {
    // }
    // CloudImageData &CloudImageData::operator=(const CloudImageData &rhs)
    // {
    //     PreprocessedData::operator=(rhs);
    //     xyzi_ptr = rhs.xyzi_ptr;
    //     range_matrix = rhs.range_matrix;
    //     ang_res_x = rhs.ang_res_x;
    //     ang_res_y = rhs.ang_res_y;
    //     rows = rhs.rows;
    //     cols = rhs.cols;
    //     return *this;
    // }
    // PreprocessedType CloudImageData::GetType() const
    // {
    //     return PreprocessedType::CloudImage;
    // }

    // CloudFeatureData::CloudFeatureData()
    //     : PreprocessedData(),
    //       startRingIndex(0),
    //       endRingIndex(0),
    //       pointColInd(0),
    //       pointRange(0),
    //       xyzi_ptr(nullptr),
    //       corner_ptr(nullptr),
    //       surface_ptr(nullptr)
    // {
    // }
    // CloudFeatureData::CloudFeatureData(const CloudFeatureData &data)
    //     : PreprocessedData(data),
    //       startRingIndex(data.startRingIndex),
    //       endRingIndex(data.endRingIndex),
    //       pointColInd(data.pointColInd),
    //       pointRange(data.pointRange),
    //       xyzi_ptr(data.xyzi_ptr),
    //       corner_ptr(data.corner_ptr),
    //       surface_ptr(data.surface_ptr)
    // {
    // }
    // CloudFeatureData &CloudFeatureData::operator=(const CloudFeatureData &rhs)
    // {
    //     PreprocessedData::operator=(rhs);
    //     startRingIndex = rhs.startRingIndex;
    //     endRingIndex = rhs.endRingIndex;
    //     pointColInd = rhs.pointColInd;
    //     pointRange = rhs.pointRange;
    //     xyzi_ptr = rhs.xyzi_ptr;
    //     corner_ptr = rhs.corner_ptr;
    //     surface_ptr = rhs.surface_ptr;
    //     return *this;
    // }
    // PreprocessedType CloudFeatureData::GetType() const
    // {
    //     return PreprocessedType::CloudFeature;
    // }

    // CloudPlaneData::CloudPlaneData()
    //     : PreprocessedData(),
    //       xyzi_ptr(nullptr),
    //       coeffs(Eigen::Vector4f::Zero())
    // {
    // }
    // CloudPlaneData::CloudPlaneData(const CloudPlaneData &data)
    //     : PreprocessedData(data),
    //       xyzi_ptr(data.xyzi_ptr),
    //       coeffs(data.coeffs)
    // {
    // }
    // CloudPlaneData &CloudPlaneData::operator=(const CloudPlaneData &rhs)
    // {
    //     PreprocessedData::operator=(rhs);
    //     xyzi_ptr = rhs.xyzi_ptr;
    //     coeffs = rhs.coeffs;
    //     return *this;
    // }
    // PreprocessedType CloudPlaneData::GetType() const
    // {
    //     return PreprocessedType::CloudPlane;
    // }

    // LandmarkDataInFrame::LandmarkDataInFrame()
    //     : index(-1)
    // {
    // }
    // LandmarkDataInFrame::LandmarkDataInFrame(const LandmarkDataInFrame &data)
    //     : index(data.index),
    //       noise(data.noise)
    // {
    // }
    // LandmarkDataInFrame &LandmarkDataInFrame::operator=(const LandmarkDataInFrame &rhs)
    // {
    //     index = rhs.index;
    //     noise = rhs.noise;
    // }

    // PlaneLandmarkDataInFrame::PlaneLandmarkDataInFrame() : LandmarkDataInFrame() {}
    // PlaneLandmarkDataInFrame::PlaneLandmarkDataInFrame(const PlaneLandmarkDataInFrame &data)
    //     : LandmarkDataInFrame(data),
    //       coeff(data.coeff)
    // {
    // }
    // PlaneLandmarkDataInFrame &PlaneLandmarkDataInFrame::operator=(const PlaneLandmarkDataInFrame &rhs)
    // {
    //     LandmarkDataInFrame::operator=(rhs);
    //     coeff = rhs.coeff;
    //     return *this;
    // }
    // LandmarkType PlaneLandmarkDataInFrame::GetType() const
    // {
    //     return LandmarkType::PlaneM;
    // }

    // FrameData::FrameData()
    //     : stamp(-1.0)
    // {
    // }
    // FrameData::FrameData(const FrameData &data)
    //     : sensor_to_robot(data.sensor_to_robot),
    //       noise(data.noise),
    //       stamp(data.stamp),
    //       index(data.index),
    //       odometry(data.odometry)
    // {
    // }
    // FrameData &FrameData::operator=(const FrameData &rhs)
    // {
    //     sensor_to_robot = rhs.sensor_to_robot;
    //     noise = rhs.noise;
    //     stamp = rhs.stamp;
    //     index = rhs.index;
    //     odometry = rhs.odometry;
    // }

    // LidarFrameData::LidarFrameData()
    //     : FrameData(),
    //       xyzi_ptr(nullptr),
    //       corner_ptr(nullptr),
    //       surface_ptr(nullptr),
    //       plane()
    // {
    // }
    // LidarFrameData::LidarFrameData(const LidarFrameData &data)
    //     : FrameData(data),
    //       xyzi_ptr(data.xyzi_ptr),
    //       corner_ptr(data.corner_ptr),
    //       surface_ptr(data.surface_ptr),
    //       plane(data.plane)
    // {
    // }
    // LidarFrameData &LidarFrameData::operator=(const LidarFrameData &rhs)
    // {
    //     FrameData::operator=(rhs);
    //     xyzi_ptr = rhs.xyzi_ptr;
    //     corner_ptr = rhs.corner_ptr;
    //     surface_ptr = rhs.surface_ptr;
    //     plane = rhs.plane;
    //     return *this;
    // }
    // FrameDataType LidarFrameData::GetType() const
    // {
    //     return FrameDataType::Lidar;
    // }

    // TIOFrameData::TIOFrameData()
    //     : FrameData(),
    //       type(StateType::Basic),
    //       v(),
    //       ba(),
    //       bg()
    // {
    // }
    // TIOFrameData::TIOFrameData(const TIOFrameData &data)
    //     : FrameData(data),
    //       type(data.type),
    //       v(data.v),
    //       ba(data.ba),
    //       bg(data.bg)
    // {
    // }
    // TIOFrameData &TIOFrameData::operator=(const TIOFrameData &rhs)
    // {
    //     FrameData::operator=(rhs);
    //     type = rhs.type;
    //     v = rhs.v;
    //     ba = rhs.ba;
    //     bg = rhs.bg;
    //     return *this;
    // }
    // FrameDataType TIOFrameData::GetType() const
    // {
    //     return FrameDataType::TIO;
    // }

    // LandmarkData::LandmarkData()
    //     : index(-1),
    //       frame_index(-1),
    //       stamp(-1.0),
    //       sensor_to_robot(),
    //       noise()
    // {
    // }
    // LandmarkData::LandmarkData(const LandmarkData &data)
    //     : index(data.index),
    //       frame_index(data.frame_index),
    //       stamp(data.stamp),
    //       sensor_to_robot(data.sensor_to_robot),
    //       noise(data.noise)
    // {
    // }
    // LandmarkData &LandmarkData::operator=(const LandmarkData &rhs)
    // {
    //     index = rhs.index;
    //     frame_index = rhs.frame_index;
    //     stamp = rhs.stamp;
    //     sensor_to_robot = rhs.sensor_to_robot;
    //     noise = rhs.noise;
    // }

    // PlaneLandmarkData::PlaneLandmarkData() : LandmarkData() {}
    // PlaneLandmarkData::PlaneLandmarkData(const PlaneLandmarkData &data)
    //     : LandmarkData(data),
    //       coeff(data.coeff)
    // {
    // }
    // PlaneLandmarkData &PlaneLandmarkData::operator=(const PlaneLandmarkData &rhs)
    // {
    //     LandmarkData::operator=(rhs);
    //     coeff = rhs.coeff;
    //     return *this;
    // }
    // LandmarkType PlaneLandmarkData::GetType() const
    // {
    //     return LandmarkType::PlaneM;
    // }

    // LoopData::LoopData()
    //     : stamp_prev(-1.0),
    //       stamp_next(-1.0),
    //       index_prev(-1),
    //       index_next(-1)
    // {
    // }
    // LoopData::LoopData(const LoopData &data)
    //     : sensor_to_robot(data.sensor_to_robot),
    //       noise(data.noise),
    //       stamp_prev(data.stamp_prev),
    //       stamp_next(data.stamp_next),
    //       index_prev(data.index_prev),
    //       index_next(data.index_next),
    //       relative_pose(data.relative_pose)
    // {
    // }
    // LoopData &LoopData::operator=(const LoopData &rhs)
    // {
    //     sensor_to_robot = rhs.sensor_to_robot;
    //     noise = rhs.noise;
    //     stamp_prev = rhs.stamp_prev;
    //     stamp_next = rhs.stamp_next;
    //     index_prev = rhs.index_prev;
    //     index_next = rhs.index_next;
    //     relative_pose = rhs.relative_pose;
    // }

    // LocalizerData::LocalizerData()
    //     : stamp(-1.0)
    // {
    // }
    // LocalizerData::LocalizerData(const LocalizerData &data)
    //     : sensor_to_robot(data.sensor_to_robot),
    //       local_to_map(data.local_to_map),
    //       noise(data.noise),
    //       stamp(data.stamp)
    // {
    // }
    // LocalizerData &LocalizerData::operator=(const LocalizerData &rhs)
    // {
    //     stamp = rhs.stamp;
    //     noise = rhs.noise;
    //     sensor_to_robot = rhs.sensor_to_robot;
    //     local_to_map = rhs.local_to_map;
    // }

    // PLocalizerData::PLocalizerData() : LocalizerData() {}
    // PLocalizerData::PLocalizerData(const PLocalizerData &data)
    //     : LocalizerData(data),
    //       p(data.p)
    // {
    // }
    // PLocalizerData &PLocalizerData::operator=(const PLocalizerData &rhs)
    // {
    //     LocalizerData::operator=(rhs);
    //     p = rhs.p;
    //     return *this;
    // }
    // LocalizerType PLocalizerData::GetType() const
    // {
    //     return LocalizerType::PL;
    // }

    // QLocalizerData::QLocalizerData() : LocalizerData() {}
    // QLocalizerData::QLocalizerData(const QLocalizerData &data)
    //     : LocalizerData(data),
    //       q(data.q)
    // {
    // }
    // QLocalizerData &QLocalizerData::operator=(const QLocalizerData &rhs)
    // {
    //     LocalizerData::operator=(rhs);
    //     q = rhs.q;
    //     return *this;
    // }
    // LocalizerType QLocalizerData::GetType() const
    // {
    //     return LocalizerType::QL;
    // }

    // PoseLocalizerData::PoseLocalizerData() : LocalizerData() {}
    // PoseLocalizerData::PoseLocalizerData(const PoseLocalizerData &data)
    //     : LocalizerData(data),
    //       pose(data.pose)
    // {
    // }
    // PoseLocalizerData &PoseLocalizerData::operator=(const PoseLocalizerData &rhs)
    // {
    //     LocalizerData::operator=(rhs);
    //     pose = rhs.pose;
    //     return *this;
    // }
    // LocalizerType PoseLocalizerData::GetType() const
    // {
    //     return LocalizerType::PoseL;
    // }

    // PublishData::PublishData() : stamp(-1.0) {}
    // PublishData::PublishData(const PublishData &data)
    //     : stamp(data.stamp)
    // {
    // }

    // CloudPublishData::CloudPublishData() : PublishData() {}
    // CloudPublishData::CloudPublishData(const CloudPublishData &data)
    //     : PublishData(data),
    //       cloud_ptr(data.cloud_ptr)
    // {
    // }
    // PublishType CloudPublishData::GetType() const
    // {
    //     return PublishType::CloudP;
    // }

    // PosePublishData::PosePublishData() : PublishData() {}
    // PosePublishData::PosePublishData(const PosePublishData &data)
    //     : PublishData(data),
    //       pose(data.pose),
    //       covariance(data.covariance)
    // {
    // }
    // PublishType PosePublishData::GetType() const
    // {
    //     return PublishType::PoseP;
    // }

    // OdometryPublishData::OdometryPublishData() : PublishData() {}
    // OdometryPublishData::OdometryPublishData(const OdometryPublishData &data)
    //     : PublishData(data),
    //       pose(data.pose),
    //       twist(data.twist),
    //       posecov(data.posecov),
    //       twistcov(data.twistcov)
    // {
    // }
    // PublishType OdometryPublishData::GetType() const
    // {
    //     return PublishType::OdometryP;
    // }

    // PathPublishData::PathPublishData() : PublishData() {}
    // PathPublishData::PathPublishData(const PathPublishData &data)
    //     : PublishData(data),
    //       poses(data.poses)
    // {
    // }
    // PublishType PathPublishData::GetType() const
    // {
    //     return PublishType::PathP;
    // }

    // CloudXYZIPublishData::CloudXYZIPublishData() : PublishData() {}
    // CloudXYZIPublishData::CloudXYZIPublishData(const CloudXYZIPublishData &data)
    //     : PublishData(data),
    //       xyzi_ptr(data.xyzi_ptr)
    // {
    // }
    // PublishType CloudXYZIPublishData::GetType() const
    // {
    //     return PublishType::CloudXYZIP;
    // }

    // LoopPublishData::LoopPublishData() : PublishData() {}
    // LoopPublishData::LoopPublishData(const LoopPublishData &data)
    //     : PublishData(data),
    //       key_pose_prev(data.key_pose_prev),
    //       key_pose_next(data.key_pose_next)
    // {
    // }
    // PublishType LoopPublishData::GetType() const
    // {
    //     return PublishType::LoopP;
    // }
} // namespace fusion_optimizer
