#include "fusion_optimizer/utitilies/data_conversions.hpp"

namespace fusion_optimizer{

void TransIMUData2Measure(State& imu_state, const Eigen::Matrix4d& imu_2_measure_sensor_T)
{
    Eigen::Matrix4d imu_2_map, measure_2_map;
    imu_2_map.setIdentity();
    imu_2_map.block<3, 3>(0, 0) = imu_state.q.matrix();
    imu_2_map.block<3, 1>(0, 3) = imu_state.p;
    measure_2_map = imu_2_map * imu_2_measure_sensor_T.inverse();

    imu_state.p = measure_2_map.block<3, 1>(0, 3);
    imu_state.q = Eigen::Quaterniond(measure_2_map.block<3, 3>(0, 0));
}

void TransMeasureData2IMU(State& measure_state, const Eigen::Matrix4d& imu_2_measure_sensor_T)
{
    Eigen::Matrix4d measure_2_map, imu_2_map;
    measure_2_map.setIdentity();
    measure_2_map.block<3, 3>(0, 0) = measure_state.q.matrix();
    measure_2_map.block<3, 1>(0, 3) = measure_state.p;
    imu_2_map = measure_2_map * imu_2_measure_sensor_T;

    measure_state.p = imu_2_map.block<3, 1>(0, 3);
    measure_state.q = Eigen::Quaterniond(imu_2_map.block<3, 3>(0, 0));
    measure_state.v = (measure_state.v.transpose() * imu_2_measure_sensor_T.block<3,3>(0,0)).transpose();
}

void TransTwistData2IMU(TwistData& twist_data, const Eigen::Matrix3d& imu_2_twist_R)
{
    twist_data.vel = imu_2_twist_R.inverse() * twist_data.vel;
    twist_data.gyro = imu_2_twist_R.inverse() * twist_data.gyro;
}


void JacPosition(const int index_i, const int index_j, int& rows, int& cols, int& vertex_i_dimension, int& vertex_j_dimension)
{
    int zs_i = index_i / 4;
    int ys_i = index_i % 4;

    int zs_j = index_j / 4;
    int ys_j = index_j % 4;
    
    if(ys_i == 0)
    {
        vertex_i_dimension = 6;
    }else{
        vertex_i_dimension = 3;
    }

    if(ys_j == 0)
    {
        vertex_j_dimension = 6;
    }else{
        vertex_j_dimension = 3;
    }

    if(ys_i >= 1)
    {
        rows = zs_i * 15 + (ys_i - 1) * 3 + 6;
    }else{
        rows = zs_i * 15;
    }

    if(ys_j >= 1)
    {
        cols = zs_j * 15 + (ys_j - 1) * 3 + 6;
    }else{
        cols = zs_j * 15;
    }
}
//根据顶点的序列号，获取其在H矩阵或者b中的起始位置。与JacPosition功能相同，本函数只处理一个index
void JacPositionI(const int index, int& rows, int& vertex_dimension)
{
    int zs = index / 4;
    int ys = index % 4;

    if(ys == 0)
    {
        vertex_dimension = 6;
    }else{
        vertex_dimension = 3;
    }

    if(ys >= 1)
    {
        rows = zs * 15 + (ys - 1) * 3 + 6;
    }else{
        rows = zs * 15;
    }
}

}