#include "fusion_optimizer/localization/graph_optimization_flow.hpp"

namespace fusion_optimizer
{
    GraphOptimizerLocalizationFlow::GraphOptimizerLocalizationFlow(ros::NodeHandle& nh, const std::string& imu_topic_name, 
                              const std::string& observation_topic_name, size_t buff_size) 
    : Localization(nh, imu_topic_name, observation_topic_name, buff_size)
    {
        graph_slam_ptr_ = std::make_shared<GraphSlam>();
        vis_information_matrix_ = Eigen::MatrixXd::Zero(6, 6);
        odo_information_matrix_ = Eigen::MatrixXd::Zero(6, 6);
        //滑窗中包含的状态量个数
        window_kfs_ = 0;
        init_window_ = false;


        Eigen::Matrix3d measure_p, measure_q, odom_measure_p, odom_measure_q;
        measure_p.setIdentity();
        measure_q.setIdentity();
        odom_measure_p.setIdentity();
        odom_measure_q.setIdentity();
        for (int i = 0; i < 3; i++)
        {
            measure_p(i, i) = 1.0 / (measurement_p_n_[i] * measurement_p_n_[i]);
            measure_q(i, i) = 1.0 / (measurement_q_n_[i] * measurement_q_n_[i]);
        //     odom_measure_p(i, i) = 1.0 / (odom_measurement_p_n_[i] * odom_measurement_p_n_[i]);
        //     odom_measure_q(i, i) = 1.0 / (odom_measurement_q_n_[i] * odom_measurement_q_n_[i]);
        }
        vis_information_matrix_.block(0,0,3,3) = measure_p;
        vis_information_matrix_.block(3,3,3,3) = measure_q;

        // odo_information_matrix_.block(0,0,3,3) = odom_measure_p;
        // odo_information_matrix_.block(3,3,3,3) = odom_measure_q;

/*         std::cout << "vis information = " << std::endl
                  << vis_information_matrix_ << std::endl; */

        // vis_information_matrix_.block(0,0,3,3) = Eigen::Matrix3d::Identity() * 1.0 / (measurement_p_n_ * measurement_p_n_);
        // vis_information_matrix_.block(3,3,3,3) = Eigen::Matrix3d::Identity() * 1.0 / (measurement_q_n_ * measurement_q_n_);

        noise_.setZero();
        noise_.block<3,3>(0,0) = acc_n_ * acc_n_ * Eigen::Matrix3d::Identity();
        noise_.block<3,3>(3,3) = noise_.block<3,3>(0,0);

        noise_.block<3,3>(6,6) = gyr_n_ * gyr_n_ * Eigen::Matrix3d::Identity();
        noise_.block<3,3>(9,9) = noise_.block<3,3>(6,6);

        noise_.block<3,3>(12,12) = acc_w_ * acc_w_ * Eigen::Matrix3d::Identity();
        noise_.block<3,3>(15,15) = gyr_w_ * gyr_w_ * Eigen::Matrix3d::Identity();

        std::thread *t = new std::thread(&GraphOptimizerLocalizationFlow::RunDeadReckoning, this);
        t->join();
    }
    
/*     //向g2o添加边和顶点，如果是第一次添加，只添加顶点
    void GraphOptimizerLocalizationFlow::Addstate(State& state, bool if_cur, std::shared_ptr<PreIntergration> pre_intergration_ptr)
    {
        //添加的是否为当前帧，如果当前帧需要添加imu边。
        pre_intergration_ptr->TransMeasureData2IMU(state);
        if (!if_cur)
        {
            g2o::SE3Quat T_pre = g2o::SE3Quat(state.q.matrix(), state.p);
            vertex_T_pre_ = graph_slam_ptr_->AddVertexSE3(T_pre);
            vertex_v_pre_ = graph_slam_ptr_->AddVertex3D(state.v);
            vertex_ba_pre_ = graph_slam_ptr_->AddVertex3D(state.ba);
            vertex_bg_pre_ = graph_slam_ptr_->AddVertex3D(state.bg);
            pre_time_ = state.stamp;

            graph_slam_ptr_->AddVisEdge(T_pre, *vertex_T_pre_, vis_information_matrix_);
            window_kfs_++;
            return;
        }else{
            g2o::SE3Quat T = g2o::SE3Quat(state.q.matrix(), state.p);
            vertex_T_cur_ = graph_slam_ptr_->AddVertexSE3(T);
            vertex_v_cur_ = graph_slam_ptr_->AddVertex3D(state.v);
            vertex_ba_cur_ = graph_slam_ptr_->AddVertex3D(state.ba);
            vertex_bg_cur_ = graph_slam_ptr_->AddVertex3D(state.bg);

            // LOG(INFO) << "pre vertex estimati T is: " << vertex_T_pre_->estimate();
            // LOG(INFO) << "pre vertex estimati v is: " << vertex_v_pre_->estimate();
            // LOG(INFO) << "pre vertex estimati ba is: " << vertex_ba_pre_->estimate();
            // LOG(INFO) << "pre vertex estimati bg is: " << vertex_bg_pre_->estimate();

            graph_slam_ptr_->AddImuEdge(*vertex_T_pre_, *vertex_v_pre_, *vertex_ba_pre_, *vertex_bg_pre_,
                                        *vertex_T_cur_, *vertex_v_cur_, *vertex_ba_cur_, *vertex_bg_cur_,
                                        pre_intergration_ptr, pre_time_, state.stamp);
            graph_slam_ptr_->AddVisEdge(T, *vertex_T_cur_, vis_information_matrix_);
            
            window_kfs_++;
        }
        vertex_T_pre_ = vertex_T_cur_;
        vertex_v_pre_ = vertex_v_cur_;
        vertex_ba_pre_ = vertex_ba_cur_;
        vertex_bg_pre_ = vertex_bg_cur_;

        pre_time_ = state.stamp;
    } */


    //向g2o添加边和顶点，如果是第一次添加，只添加顶点
    void GraphOptimizerLocalizationFlow::Addstate(State& state, bool if_cur)
    {
        TransMeasureData2IMU(state, imu_2_measure_sensor_T_);
        graph_slam_ptr_->AddVertexVec(state.p);
        graph_slam_ptr_->AddVertexQ(state.q);
        graph_slam_ptr_->AddVertexVec(state.v);
        graph_slam_ptr_->AddVertexVec(state.ba);
        graph_slam_ptr_->AddVertexVec(state.bg);
        std::pair<Eigen::Vector3d, Eigen::Quaterniond> T_measurement;
        T_measurement.first = state.p;
        T_measurement.second = state.q;
        graph_slam_ptr_->AddMeasureEdge(T_measurement, vis_information_matrix_);
        window_kfs_++;

        if (!if_cur){
            pre_time_ = state.stamp;
            return;
        }else{
            for (const auto& predictor : predictors_)
            {
                if (predictor->type_ == "Imu")
                {
                    std::shared_ptr<PreIntergration> pre_intergration_ptr = std::make_shared<PreIntergration>(pre_intergration_imu_buff_, pre_state_, measure_state_);
                    graph_slam_ptr_->AddImuEdge(pre_intergration_ptr, pre_time_, state.stamp);
                }else if (predictor->type_ == "Odometry"){
                    std::shared_ptr<OdomData> odom_data_ptr_ = std::make_shared<OdomData>(pre_odom_.p, pre_odom_.q, cur_odom_.p, cur_odom_.q);
                    graph_slam_ptr_->AddOdomEdge(odom_data_ptr_, odo_information_matrix_);
                }else if (predictor->type_ == "Twist"){
                    std::shared_ptr<PreTwistIntergration> pre_twist_intergration_ptr = std::make_shared<PreTwistIntergration>(pre_twist_intergration_imu_buff_, pre_state_, measure_state_);
                    graph_slam_ptr_->AddTwistEdge(pre_twist_intergration_ptr);
                }else{
                    LOG(ERROR) << "predic didn't have " << predictor->type_ << " type!";
                }
            }
        }
        pre_time_ = state.stamp;
    }

    void GraphOptimizerLocalizationFlow::Reset()
    {
        // first_input_window = true;
        init_window_ = false;
        graph_slam_ptr_->Reset();
    }

    bool GraphOptimizerLocalizationFlow::InitWindows()
    {
        static bool first_input_window = true;
        if (first_input_window)
        {
            Addstate(pre_state_, false);
            first_input_window = false;
        }

        Addstate(measure_state_, true);
        // if(graph_slam_ptr_->GetVertexSize() >= windows_max_size_)
        if(window_kfs_ >= windows_max_size_)
        {
             if(graph_slam_ptr_->OptimizerInterface(optimizer_state_))
            {
                LOG(INFO) << "Window initialization complete !!!";
                init_window_ = true;
                return true;
            }
            return false;
        }
        return false;
    }

/*     bool GraphOptimizerLocalizationFlow::LocalizationProcess(std::shared_ptr<PreIntergration> pre_intergration_ptr)
    {
        if(!init_window_)
        {
            InitWindows(pre_intergration_ptr);
            return false;
        }
        Addstate(measure_state_, true, pre_intergration_ptr);
        if(graph_slam_ptr_->OptimizerInterface(optimizer_state_))
        {
            return true;
        }
    } */

    bool GraphOptimizerLocalizationFlow::LocalizationProcess()
    {
        if(!init_window_)
        {
            InitWindows();
            return false;
        }
        Addstate(measure_state_, true);
        if(graph_slam_ptr_->OptimizerInterface(optimizer_state_))
        {
            return true;
        }
        return false;
    }

    void GraphOptimizerLocalizationFlow::PublishOdom(std::shared_ptr<PosePublisher<nav_msgs::Odometry, nav_msgs::Odometry>> pose_pub_ptr, const State& state)
    {
        nav_msgs::Odometry curr_pub_pose;
        curr_pub_pose.header.stamp = ros::Time().fromSec(state.stamp);
        curr_pub_pose.pose.pose.position.x = state.p(0);
        curr_pub_pose.pose.pose.position.y = state.p(1);
        curr_pub_pose.pose.pose.position.z = state.p(2);

        curr_pub_pose.pose.pose.orientation.w = state.q.w();
        curr_pub_pose.pose.pose.orientation.x = state.q.x();
        curr_pub_pose.pose.pose.orientation.y = state.q.y();
        curr_pub_pose.pose.pose.orientation.z = state.q.z();

        pose_pub_ptr->Publish(curr_pub_pose);
    }

    void GraphOptimizerLocalizationFlow::PublishPath(std::shared_ptr<PosePublisher<nav_msgs::Path, nav_msgs::Path>> path_pub_ptr_, const std::deque<State>& state_buff)
    {
        nav_msgs::Path pub_path;
        pub_path.header.stamp = ros::Time().fromSec(state_buff.back().stamp);
        pub_path.header.frame_id = "map";

        for(const auto& state : state_buff)
        {
            geometry_msgs::PoseStamped pub;
            geometry_msgs::Pose pp;
            tf::pointEigenToMsg(state.p, pp.position);
            tf::quaternionEigenToMsg(state.q, pp.orientation);
            pub.pose = pp;
            pub_path.poses.emplace_back(pub);
        }
        path_pub_ptr_->Publish(pub_path);

    }

    void GraphOptimizerLocalizationFlow::RunDeadReckoning()
    {
        static double last_time = -1.0;
        // ros::Rate rate(100);
        while(ros::ok()){
            // ros::spinOnce();
            imu_sub_dead_reckoning_ptr_->ParseData(imu_buff_copy_);
            if (imu_buff_copy_.size() <= 0)
                continue;
            if(init_window_)
            {
                State optimizer_state = optimizer_state_;
                while(!imu_buff_copy_.empty() && imu_buff_copy_.front().time < optimizer_state.stamp)
                {
                    imu_buff_copy_.pop_front();
                }

                while(!dead_reckoning_imu_data_buff_.empty() && dead_reckoning_imu_data_buff_.front().time < optimizer_state.stamp)
                {
                    dead_reckoning_imu_data_buff_.pop_front();
                }
                

                while(imu_buff_copy_.size() && (imu_buff_copy_.front().time - optimizer_state.stamp) < lead_time_)
                {
                    dead_reckoning_imu_data_buff_.push_back(imu_buff_copy_.front());
                    imu_buff_copy_.pop_front();
                }
                if(!dead_reckoning_imu_data_buff_.empty() && dead_reckoning_imu_data_buff_.back().time > (last_time + dead_recking_update_time_))
                {
                    auto tmp_pre_imu_state = last_imu_state_;
                    last_imu_state_ = PreIntergration::DeadReckoning(optimizer_state, dead_reckoning_imu_data_buff_, noise_, gn_);

                    //將定位信息轉到相機坐標系，last_imu_state_爲相機在世界坐標系下的坐標
                    TransIMUData2Measure(last_imu_state_, imu_2_measure_sensor_T_);
/*                     if ((abs(last_imu_state_.p(0) - tmp_pre_imu_state.p(0)) 
                    + abs(last_imu_state_.p(1) - tmp_pre_imu_state.p(1)) 
                    + abs(last_imu_state_.p(2) - tmp_pre_imu_state.p(2))) > 50)
                    {
                        last_imu_state_ = PreIntergration::DeadReckoning(optimizer_state_, dead_reckoning_imu_data_buff_, noise_);
                        LOG(ERROR) << "DeadReckoning fail !";
                        last_imu_state_ = tmp_pre_imu_state;
                        continue;
                    } */

                    last_time = last_imu_state_.stamp;
                    LOG(INFO) << "-----------------imu state-------------------------imu buff size = " << dead_reckoning_imu_data_buff_.size();
                    last_imu_state_.Print();
                    PublishOdom(imu_pose_pub_ptr_, last_imu_state_);
                    imu_state_buff_.push_back(last_imu_state_);
                    PublishPath(path_pub_ptr_, imu_state_buff_);  
                }
            }else{
                continue;
            }
 
            // rate.sleep();

        }
    }
}