#include "fusion_optimizer/localization/eskf_localization_flow.hpp"

namespace fusion_optimizer
{
EskfLocalizationFlow::EskfLocalizationFlow(ros::NodeHandle& nh, const std::string& imu_topic_name, 
                                    const std::string& observation_topic_name, size_t buff_size)
{
    imu_sub_ptr_ = std::make_shared<IMUSubscriber>(nh, imu_topic_name, buff_size);
    obs_sub_ptr_ = std::make_shared<ObservationSubscriber>(nh, observation_topic_name, buff_size);
    // pre_intergration_ptr_ = std::make_shared<PreIntergration>();
    eskf_flow_init_ = false;
    init_pose_ = false;
}
bool EskfLocalizationFlow::HaveData()
{
    imu_sub_ptr_->ParseData(imu_buff_);
    obs_sub_ptr_->ParseData(state_buff_);
    if (imu_buff_.size() > 0 && state_buff_.size() > 0)
    {
        return true;
    }
    return false;
}


bool EskfLocalizationFlow::ValidData()
{   
    if(!eskf_flow_init_){
        while(state_buff_.front().stamp < imu_buff_.front().time)
        {
            pre_state_ = state_buff_.front();
            state_buff_.pop_front();
        }

        while(imu_buff_.front().time < state_buff_.front().stamp)
        {
            imu_buff_.pop_front();
        }
        eskf_flow_init_ = true;
    }

    if (state_buff_.size() < 2 || imu_buff_.size() == 0)
    {
        return false;
    }

    while(imu_buff_.front().time < state_buff_.at(1).stamp)
    {
        pre_intergration_imu_buff_.push_back(imu_buff_.front());
        imu_buff_.pop_front();
    }
    measure_state_ = state_buff_.at(1);
    return true;

}

bool EskfLocalizationFlow::InitPose()
{
    // pre_intergration_ptr_->InitESKFSystem(pre_state_);
    init_pose_ = true;
}

void EskfLocalizationFlow::UpdataBuffData()
{
    pre_state_ = measure_state_;
    state_buff_.pop_front();
    measure_state_ = state_buff_.front();
}

void EskfLocalizationFlow::Run()
{
    if (!HaveData())
    {
        return;
    }
    if(!ValidData())
    {
        return;
    }
    if (!init_pose_)
    {
        InitPose();
    }
/*     pre_intergration_ptr_->SetInputImuDatas(pre_intergration_imu_buff_, pre_state_);
    pre_intergration_ptr_->Propagate(); */
    pre_intergration_ptr_ = std::make_shared<PreIntergration>(pre_intergration_imu_buff_, pre_state_, measure_state_);
    pre_intergration_ptr_->InitESKFSystem(pre_state_);

    pre_intergration_ptr_->ErrorEKFCorrect(measure_state_);
}



}// namespace fusion_optimizer