#include "fusion_optimizer/localization/localization_flow.hpp"

namespace fusion_optimizer
{
Localization::Localization(ros::NodeHandle& nh, const std::string& imu_topic_name, 
                     const std::string& observation_topic_name, size_t buff_size) : Params()
{
    imu_sub_ptr_ = std::make_shared<IMUSubscriber>(nh, imu_topic_name, buff_size);
    imu_sub_dead_reckoning_ptr_ = std::make_shared<IMUSubscriber>(nh, imu_topic_name, buff_size);
    obs_sub_ptr_ = std::make_shared<ObservationSubscriber>(nh, observation_topic_name, buff_size);
    //里程计定位
    // odom_sub_ptr_ = std::make_shared<ObservationSubscriber>(nh, odom_sub_topic_name_, buff_size);
    pose_pub_ptr_ = std::make_shared<PosePublisher<nav_msgs::Odometry, nav_msgs::Odometry>>(nh, pose_publisher_topic_, pose_pub_buff_);
    imu_pose_pub_ptr_ = std::make_shared<PosePublisher<nav_msgs::Odometry, nav_msgs::Odometry>>(nh, imu_pose_publisher_topic_, pose_pub_buff_);
    path_pub_ptr_ = std::make_shared<PosePublisher<nav_msgs::Path, nav_msgs::Path>>(nh, imu_path_publisher_topic_, pose_pub_buff_);

    for (auto predictor_param : predictor_params_)
    {
        std::shared_ptr<Predictor> predictor = std::make_shared<Predictor>(nh, predictor_param);
        predictors_.push_back(predictor);
    }
    
    eskf_flow_init_ = false;
    init_pose_ = false;
}

bool Localization::HaveData()
{
/*     for (const auto& predictor : predictors_)
    {
        if (!predictor->HasData())
        {
            LOG(INFO) << predictor->type_ << "didn't have data!";
            return false;
        }
    } */
    
    obs_sub_ptr_->ParseData(state_buff_);
    // use_for_sync_state_buff_.insert(use_for_sync_state_buff_.end(), state_buff_.begin(), state_buff_.end());
    // imu_sub_ptr_->ParseData(imu_buff_);

    // odom_sub_ptr_->ParseData(unsync_odom_buff_);
    // if (imu_buff_.size() > 0 && state_buff_.size() > 0 && unsync_odom_buff_.size() > 0)
    if (state_buff_.size() > 0)
    {
        return true;
    }
    return false;
}

bool Localization::ValidData()
{   
    if (state_buff_.size() >= 2)
    {
        measure_state_ = state_buff_.at(1);
        pre_state_ = state_buff_.front();
    }else{
        return false;
    }

    double dt = measure_state_.stamp - pre_state_.stamp;
    if(dt <= 0)
    {
        state_buff_.pop_front();
        LOG(ERROR) << "measure data stamp is wrong!!!";
        return false;
    }

    //如果边缘化掉的是最老帧，则进行正常的数据更新，如果删除掉的是最新帧，pre_state不做更新，只更新measure_state_
    if(marge_old_)
    {
        pre_intergration_imu_buff_.clear();
        pre_state_ = state_buff_.front();
    }else{
        LOG(INFO) << "marge new ";
    }

    for (const auto& predictor : predictors_)
    {
        if (predictor->type_ == "Imu")
        {
            if (!predictor->ValidImuData(pre_state_.stamp, measure_state_.stamp, pre_intergration_imu_buff_))
            {
                LOG(INFO) << predictor->type_ << " data is not valid!";
                UpdataBuffData();
                return false;
            }
            if (pre_intergration_imu_buff_.size() == 0)
            {
                return false;
            }
            std::deque<IMUData> SyncedData;
            auto un_sync_imu_buff = pre_intergration_imu_buff_;
            IMUData::SyncData(un_sync_imu_buff, SyncedData, pre_state_.stamp);
            IMUData::SyncData(un_sync_imu_buff, SyncedData, measure_state_.stamp);
            pre_intergration_imu_buff_.pop_front();
            pre_intergration_imu_buff_.pop_back();
            pre_intergration_imu_buff_.push_front(SyncedData.front());
            pre_intergration_imu_buff_.push_back(SyncedData.back());
        }else if (predictor->type_ == "Odometry"){
            if (!predictor->ValidOdomData(pre_state_.stamp, measure_state_.stamp, unsync_odom_buff_))
            {
                LOG(INFO) << predictor->type_ << " data is not valid!";
                UpdataBuffData();
                return false;
            }
            //第一次同步两个数据，之后每次只同步当前帧
            if (sync_odom_buff_.size() == 0)
            {
                State::SyncData(unsync_odom_buff_, sync_odom_buff_, pre_state_.stamp);
                State::SyncData(unsync_odom_buff_, sync_odom_buff_, measure_state_.stamp);
            }else{
                State::SyncData(unsync_odom_buff_, sync_odom_buff_, measure_state_.stamp);
                sync_odom_buff_.pop_front();
            }
            pre_odom_ = sync_odom_buff_.front();
            cur_odom_ = sync_odom_buff_.back();
        }else if(predictor->type_ == "Twist"){
            if (!predictor->ValidTwistData(pre_state_.stamp, measure_state_.stamp, pre_twist_intergration_imu_buff_))
            {
                LOG(INFO) << predictor->type_ << " data is not valid!";
                UpdataBuffData();
                return false;
            }
            if (pre_twist_intergration_imu_buff_.size() == 0)
            {
                return false;
            }
            std::deque<TwistData> SyncedData;
            TwistData::SyncData(pre_twist_intergration_imu_buff_, SyncedData, pre_state_.stamp);
            TwistData::SyncData(pre_twist_intergration_imu_buff_, SyncedData, measure_state_.stamp);
            pre_twist_intergration_imu_buff_.pop_front();
            pre_twist_intergration_imu_buff_.pop_back();
            pre_twist_intergration_imu_buff_.push_front(SyncedData.front());
            pre_twist_intergration_imu_buff_.push_back(SyncedData.back());
        }else{
            LOG(ERROR) << "Don't have predictor type " << predictor->type_;
        }
    }

    Eigen::Vector3d curr_v = (measure_state_.p - pre_state_.p) / dt;
    measure_state_.v = curr_v;
    
    if (!optimize_z_)
    {
        pre_state_.p(2) = init_z_; 
        measure_state_.p(2) = init_z_; 
        measure_state_.v(2) = 0;
    }
    if (init_window_)
    {
        measure_state_.ba = optimizer_state_.ba;
        measure_state_.bg = optimizer_state_.bg;
    }
    return true;
}

bool Localization::InitPose(std::shared_ptr<PreIntergration> pre_intergration_ptr)
{
    pre_intergration_ptr->InitESKFSystem(pre_state_);
    init_pose_ = true;
}

void Localization::UpdataBuffData()
{
    if (state_buff_.size() > 1){
        state_buff_.pop_front();
    }
/*     if (sync_odom_buff_.size() > 1){
        sync_odom_buff_.pop_back();
    } */
}

void Localization::Run()
{
    if (!HaveData())
    {
        return;
    }
    if(!ValidData())
    {
        return;
    }
    if(LocalizationProcess())
    {
        auto camera_state =  optimizer_state_;
        TransIMUData2Measure(camera_state, imu_2_measure_sensor_T_);
        PublishOdom(pose_pub_ptr_, camera_state);
    }else{
        LOG(ERROR) << "localization process fail !";
    }
    UpdataBuffData();
}

}


