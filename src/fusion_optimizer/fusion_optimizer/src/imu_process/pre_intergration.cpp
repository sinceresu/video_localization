#include "fusion_optimizer/imu_process/pre_integration.hpp"

/* 
pose信息15维，顺序为P,Q,V,ba,bg
 */
namespace fusion_optimizer
{

PreIntergration::PreIntergration(const std::deque<IMUData>& imu_datas, const State& pre_state, const State& curr_state)
:Params()
{
    F_init_.setIdentity();
    G_init_.setZero();
    G_eskf_.setZero();
    system_init_ = false;
    init_v_ = false;
    noise_.setZero();

    // for (int i = 0; i < 3; ++i)
    // {
    noise_.block<3,3>(0,0) = acc_n_ * acc_n_ * Eigen::Matrix3d::Identity();
    noise_.block<3,3>(3,3) = noise_.block<3,3>(0,0);

    noise_.block<3,3>(6,6) = gyr_n_ * gyr_n_ * Eigen::Matrix3d::Identity();
    noise_.block<3,3>(9,9) = noise_.block<3,3>(6,6);

    noise_.block<3,3>(12,12) = acc_w_ * acc_w_ * Eigen::Matrix3d::Identity();
    noise_.block<3,3>(15,15) = gyr_w_ * gyr_w_ * Eigen::Matrix3d::Identity();
    // }

    curr_ba_ = curr_state.ba;
    curr_bg_ = curr_state.bg;

    imu_buff_.clear();
    imu_buff_ = imu_datas;
    pre_state_ = pre_state;
    sum_dt_ = imu_buff_.back().time - imu_buff_.front().time;
    jacobians_.setIdentity();
    covariance_.setZero();
    Propagate();
}

void PreIntergration::InitESKFSystem(State& init_measure_state)
{
    // TransMeasureData2IMU(init_measure_state);
    // state_.q = init_measure_state.q;
    // state_.q = Eigen::Quaterniond(1,0,0,0);
    // state_.p = init_measure_state.p;
    state_.ba = Eigen::Vector3d(0,0,0);
    state_.bg = Eigen::Vector3d(0,0,0);
}

void PreIntergration::SetInputImuDatas(const std::deque<IMUData>& imu_datas,  State pre_state)
{
    imu_buff_.clear();
    imu_buff_ = imu_datas;
    pre_state_ = pre_state;
}

void PreIntergration::Propagate()
{
    //第一次进入数据
    bool first = true;
    IMUData pre_imu_data, curr_imu_data;
    Eigen::Quaterniond pre_Q, curr_Q;
    Eigen::Vector3d pre_alpha, curr_alpha;
    Eigen::Vector3d curr_beta, pre_beta;
    Eigen::Matrix<double, 15, 15> pre_jacobians, cur_jacobians;
    Eigen::Matrix<double, 15, 15> pre_covariance, cur_covariance;

    //每次预积分这些变量相对与i都不变，从初始开始积分。
    pre_Q.setIdentity();
    pre_alpha.setZero();
    pre_beta.setZero();

    pre_jacobians.setIdentity();
    pre_covariance.setZero();
    for (const auto& imudata : imu_buff_)
    {
        if(first)
        {
            pre_imu_data = imudata;
            first = false;
            init_v_ = true;
            continue;
        }
         curr_imu_data = imudata;
        
        midPointIntegration(pre_imu_data, curr_imu_data, pre_Q, curr_Q, pre_beta, curr_beta, pre_alpha, curr_alpha,  
                            curr_ba_, curr_bg_, pre_jacobians, pre_covariance, cur_jacobians, cur_covariance, noise_, gn_, true, false);
        
        // MidPointIntegration(pre_imu_data, curr_imu_data, pre_Q, curr_Q, pre_alpha, curr_alpha, pre_beta, curr_beta, 
        //                     curr_ba_, curr_bg_, pre_jacobians, cur_jacobians, pre_covariance, cur_covariance, noise_);
        pre_imu_data = curr_imu_data;
        pre_Q = curr_Q;
        pre_alpha = curr_alpha;
        pre_beta = curr_beta;
        pre_jacobians = cur_jacobians;
        pre_covariance = cur_covariance;
    }
    jacobians_ = cur_jacobians;
    covariance_ = cur_covariance;
    curr_Q_ = curr_Q;
    curr_P_ = curr_alpha;
    curr_V_ = curr_beta;
    
    information_ = covariance_.inverse();
    // std::cout << "imu informatrix is " << std::endl << information_ << std::endl;
    // std::cout << "imu covariance_ is " << std::endl << covariance_ << std::endl;
    
}


/* 
void PreIntergration::MidPointIntegration(const IMUData& pre_imu_data, 
                                          const IMUData& curr_imu_data, 
                                          const Eigen::Quaterniond& pre_Q, 
                                          Eigen::Quaterniond& cur_Q,
                                          const Eigen::Vector3d& pre_alpha,
                                          Eigen::Vector3d& cur_alpha,
                                          const Eigen::Vector3d pre_beta,
                                          Eigen::Vector3d& cur_beta,
                                          const Eigen::Vector3d& acc_ba,
                                          const Eigen::Vector3d& omega_ba,
                                          const Eigen::Matrix<double, 15, 15>& pre_jacobians,
                                          Eigen::Matrix<double, 15, 15>& cur_jacobians,
                                          const Eigen::Matrix<double, 15, 15>& pre_covariance,
                                          Eigen::Matrix<double, 15, 15>& cur_covariance,
                                          const Eigen::Matrix<double, 18, 18>& noise)
{
    Eigen::Matrix3d R_ik0, R_ik1;
    R_ik0 = pre_Q.matrix();

    Eigen::Vector3d acc_ik0, acc_ik1, omega_ik0, omega_ik1;
    acc_ik0 = Eigen::Vector3d(pre_imu_data.linear_acceleration.x, pre_imu_data.linear_acceleration.y, pre_imu_data.linear_acceleration.z);
    acc_ik1 = Eigen::Vector3d(curr_imu_data.linear_acceleration.x, curr_imu_data.linear_acceleration.y, curr_imu_data.linear_acceleration.z);
    omega_ik0 = Eigen::Vector3d(pre_imu_data.angular_velocity.x, pre_imu_data.angular_velocity.y, pre_imu_data.angular_velocity.z);
    omega_ik1 = Eigen::Vector3d(curr_imu_data.angular_velocity.x, curr_imu_data.angular_velocity.y, curr_imu_data.angular_velocity.z);
    Eigen::Matrix3d I = Eigen::Matrix3d::Identity();
    double dt = curr_imu_data.time - pre_imu_data.time;

    Eigen::Matrix3d skewS_acc0, skewS_acc1, skewS_omega0;
    skewS_acc0 = skewSymmetric(acc_ik0 - acc_ba);
    skewS_acc1 = skewSymmetric(acc_ik1 - acc_ba);
    skewS_omega0 = skewSymmetric(omega_ik0);

    Eigen::Vector3d omega_mid = 0.5 * (omega_ik0 + omega_ik1) - omega_ba;
    Eigen::Quaterniond tmp_dq = Eigen::Quaterniond(1, 0.5 * omega_mid(0) * dt, 0.5 * omega_mid(1) * dt, 0.5 * omega_mid(2) * dt);
    tmp_dq.normalize();
    cur_Q = pre_Q * tmp_dq;
    cur_Q.normalize();
    R_ik1 = cur_Q.matrix();
    
    Eigen::Vector3d acc_mid = 0.5 * (R_ik0 * (acc_ik0 - acc_ba) + R_ik1 * (acc_ik1 - acc_ba));
    cur_alpha = pre_alpha + dt * pre_beta + 0.5 * (acc_mid + gn_) * dt * dt;

    // cur_beta = pre_beta + dt * (acc_mid + gn_);
    cur_beta = pre_beta + dt * (acc_mid);

    auto F = F_init_;
    //F32
    // F.block<3,3>(3,6) = -0.5 * (R_ik0 * skewS_acc0 * dt + R_ik1 * skewS_acc1 * (I - skewS_omega0 * dt)* dt);
    F.block<3,3>(6,3) = -0.5 * (R_ik0 * skewS_acc0 * dt + R_ik1 * skewS_acc1 * (I - skewS_omega0 * dt)* dt);
    //F12
    F.block<3,3>(0,3) = 0.5 * dt * F.block<3,3>(6,3);
    // F.block<3,3>(3,3) = I - skewS_omega0;
    F.block<3,3>(3,3) = I - skewS_omega0 * dt;
    F.block<3,3>(0,6) = I * dt;
    F.block<3,3>(6,9) = -0.5 * (R_ik0 + R_ik1) * dt;
    F.block<3,3>(0,9) = 0.5 * dt * F.block<3,3>(6,9);
    //F35 不同
    F.block<3,3>(6,12) = 0.5 * (R_ik1 * skewS_acc1 * dt * dt);
    //F15
    F.block<3,3>(0, 12) = 0.5 * dt * F.block<3,3>(6,12);
    F.block<3,3>(3, 12) = -(I * dt);

    auto G = G_init_;
    G.block<3,3>(6,0) = 0.5 * R_ik0 * dt;
    G.block<3,3>(0,0) = 0.5 * dt * G.block<3,3>(6,0);
    //G32
    // G.block<3,3>(3,6) = -0.5 * (R_ik1 * skewS_acc1 * dt) * (0.5 * dt);
    G.block<3,3>(6,3) = -0.5 * (R_ik1 * skewS_acc1 * dt) * (0.5 * dt);
    //G12
    G.block<3,3>(0,3) = 0.5 * dt * G.block<3,3>(6,3);
    G.block<3,3>(6,6) = 0.5 * R_ik1 * dt;
    G.block<3,3>(0,6) = 0.5 * dt * G.block<3,3>(6,6);
    //G34
    G.block<3,3>(6,9) = G.block<3,3>(6,3);
    //G14
    G.block<3,3>(0,9) = G.block<3,3>(0,3);
    G.block<3,3>(9,12) = I * dt;
    G.block<3,3>(12,15) = G.block<3,3>(9,12);
    G.block<3,3>(3,3) = 0.5 * G.block<3,3>(9,12);
    G.block<3,3>(3,9) = G.block<3,3>(3,3);

    cur_jacobians = F * pre_jacobians;
    cur_covariance = F * pre_covariance * F.transpose() + G * noise * G.transpose();
    G_eskf_ =  G;
}
 */
//将观测数据转换到imu坐标系下
void PreIntergration::TransMeasureData2IMU(State& measure_state)
{
    //视觉反定位，定位的是旋转之后的图像，所以外参要添加一个图像旋转。
    Eigen::Matrix4d image_T;
    image_T.setIdentity();
/*     Eigen::Matrix3d images_rotation;
    images_rotation << 0, -1, 0,
                       1, 0, 0,
                       0, 0, 1;
    image_T.block<3,3>(0,0) = images_rotation; */

    imu_2_measure_sensor_T_ =  image_T * imu_2_measure_sensor_T_;
    
    Eigen::Matrix4d measure_2_map, imu_2_map;
    measure_2_map.setIdentity();
    measure_2_map.block<3, 3>(0, 0) = measure_state.q.matrix();
    measure_2_map.block<3, 1>(0, 3) = measure_state.p;
    imu_2_map = measure_2_map * imu_2_measure_sensor_T_;

    measure_state.p = imu_2_map.block<3, 1>(0, 3);
    measure_state.q = Eigen::Quaterniond(imu_2_map.block<3, 3>(0, 0));


    // measure_state.p = measure_state.q * imu_2_measure_sensor_t_ + measure_state.p;
    measure_state.v = (measure_state.v.transpose() * imu_2_measure_sensor_R_).transpose();

    // measure_state.q = measure_state.q * Eigen::Quaterniond(imu_2_measure_sensor_R_);
}

Eigen::Matrix<double, 15, 1> PreIntergration::ComputerError(Eigen::Vector3d p1, Eigen::Quaterniond q1, Eigen::Vector3d vec1, Eigen::Vector3d ba1, Eigen::Vector3d bg1,
                                    Eigen::Vector3d p2, Eigen::Quaterniond q2, Eigen::Vector3d vec2, Eigen::Vector3d ba2, Eigen::Vector3d bg2)
{
     Eigen::Matrix<double, 15, 1> error;
    error.setZero();
    Eigen::Vector3d delta_ba = ba1 - curr_ba_;
    Eigen::Vector3d delta_bg = bg1 - curr_bg_;

    Eigen::Vector3d update_P = curr_P_ + jacobians_.block<3,3>(0,9) * delta_ba + jacobians_.block<3,3>(0,12) * delta_bg;
    Eigen::Vector3d updata_V = curr_V_ + jacobians_.block<3,3>(6,9) * delta_ba + jacobians_.block<3,3>(6,12) * delta_bg;
    Eigen::Quaterniond update_Q = curr_Q_ * deltaQ(jacobians_.block<3,3>(3,12) * delta_bg);

    // error.block<3,1>(0,0) = q1.inverse().matrix() * (p2 - p1) - (vec1 * sum_dt_ + 0.5 * gn_ * sum_dt_ * sum_dt_) - update_P;
/*     auto out = q1.inverse() * (p2 - p1 - vec1 * sum_dt_ - 0.5 * gn_ * sum_dt_ * sum_dt_);
    LOG(INFO) <<"P: " << std::endl
              << "观测：" << out(0) << ", " << out(1) << ", " << out(2) << std::endl
              << "预积分： " << update_P(0) << ", " << update_P(1) << ", " << update_P(2);
    auto out2 = q1.inverse() * (vec2 - vec1 - gn_ * sum_dt_) - updata_V;
    auto out1 = q1.inverse() * (vec2 - vec1 - gn_ * sum_dt_);
    LOG(INFO) <<"V: " << std::endl
              <<"vec2: " << vec2(0) << ", " << vec2(1)<< ", " << vec2(2) << std::endl
              <<"error: " << out2(0) << ", " << out2(1)<< ", " << out2(2) << std::endl
              << "观测：" << out1(0) << ", " << out1(1)<< ", " << out1(2) << std::endl
              << "预积分： " << updata_V(0) << ", " << updata_V(1) << ", " << updata_V(2); */

    error.block<3,1>(0,0) = q1.inverse() * (p2 - p1 - vec1 * sum_dt_ - 0.5 * gn_ * sum_dt_ * sum_dt_) - update_P;
    error.block<3,1>(3,0) = 2 *(update_Q.inverse() * (q1.inverse() * q2)).vec();
    error.block<3,1>(6,0) = q1.inverse() * (vec2 - vec1 - gn_ * sum_dt_) - updata_V;
    error.block<3,1>(9,0) = ba2 - ba1;
    error.block<3,1>(12,0) = bg2 - bg1;
    return error;

}

/* Eigen::Matrix<double, 15, 30> PreIntergration::GetJacobian(Eigen::Vector3d p1, Eigen::Quaterniond q1, Eigen::Vector3d vec1, Eigen::Vector3d ba1, Eigen::Vector3d bg1,
                                          Eigen::Vector3d p2, Eigen::Quaterniond q2, Eigen::Vector3d vec2, Eigen::Vector3d ba2, Eigen::Vector3d bg2)
{
    Eigen::Matrix<double, 15, 30> error_jacobian;
    error_jacobian.setZero();

    Eigen::Vector3d delta_bg = bg1 - curr_bg_;


    Eigen::Matrix3d dp_ba = jacobians_.block<3,3>(0,9);
    Eigen::Matrix3d dp_bg = jacobians_.block<3,3>(0,12);

    Eigen::Matrix3d dq_dbg = jacobians_.block<3,3>(3,12);
    Eigen::Matrix3d dv_dba = jacobians_.block<3,3>(6,9);
    Eigen::Matrix3d dv_dbg = jacobians_.block<3,3>(6,12);
    Eigen::Quaterniond update_q = curr_Q_ * deltaQ(dq_dbg * delta_bg);


    error_jacobian.block<3,3>(0,0) = -q1.inverse().matrix();
    error_jacobian.block<3,3>(0,3) = skewSymmetric(q1.inverse() * (p2 - p1 - vec1 * sum_dt_ + 0.5 * gn_ * sum_dt_ * sum_dt_));
    error_jacobian.block<3,3>(0,6) = -sum_dt_ * q1.inverse().matrix();
    error_jacobian.block<3,3>(0,9) = -dp_ba;
    error_jacobian.block<3,3>(0,12) = -dp_bg;

    error_jacobian.block<3,3>(0,15) = q1.inverse().matrix();

    error_jacobian.block<3,3>(3,3) = -(Qleft(q2.inverse() * q1) * Qright(update_q)).bottomRightCorner<3,3>();
    error_jacobian.block<3,3>(3,12) = -(Qleft(q2.inverse() *q1 * update_q)).bottomRightCorner<3,3>() * dq_dbg;

    error_jacobian.block<3,3>(3,18) = (Qleft(update_q.inverse() * q1.inverse() * q2)).bottomRightCorner<3,3>();

    // error_jacobian.block<3,3>(6,3) = skewSymmetric(q1.inverse() * (v2 - v1 - gn_ * sum_dt_));
    error_jacobian.block<3,3>(6,3) = skewSymmetric((vec2 - vec1 - gn_ * sum_dt_));
    error_jacobian.block<3,3>(6,6) = -q1.inverse().matrix();
    error_jacobian.block<3,3>(6,9) = -dv_dba;
    error_jacobian.block<3,3>(6,12) = -dv_dbg;

    error_jacobian.block<3,3>(6,21) = q1.inverse().matrix();
    
    error_jacobian.block<3,3>(9,9) = -Eigen::Matrix3d::Identity();

    error_jacobian.block<3,3>(9,24) = Eigen::Matrix3d::Identity();

    error_jacobian.block<3,3>(12,12) = -Eigen::Matrix3d::Identity();
    
    error_jacobian.block<3,3>(12,27) = Eigen::Matrix3d::Identity();

    return error_jacobian;
} */

bool PreIntergration::RePropagate(const Eigen::Vector3d &Bai, const Eigen::Vector3d &Bgi)
{
    if ((Bai - curr_ba_).norm() < 0.10 && (Bgi - curr_bg_).norm() < 0.01)
    {
        return false;
    }
    curr_ba_ = Bai;
    curr_bg_ = Bgi;
    Propagate();
    return true;
}

void PreIntergration::midPointIntegration(const IMUData& pre_imu_data, 
                        const IMUData& curr_imu_data,
                        const Eigen::Quaterniond &dq,
                        Eigen::Quaterniond &result_dq,
                        const Eigen::Vector3d &beta,
                        Eigen::Vector3d &result_beta,
                        const Eigen::Vector3d &alpha,
                        Eigen::Vector3d &result_alpha,
                        const Eigen::Vector3d &linearized_ba,
                        const Eigen::Vector3d &linearized_bg,
                        const Eigen::Matrix<double, 15, 15> &jacobian,
                        const Eigen::Matrix<double, 15, 15> &covariance,
                        Eigen::Matrix<double, 15, 15> &result_jacobian,
                        Eigen::Matrix<double, 15, 15> &result_covariance,
                        const Eigen::Matrix<double, 18, 18>& noise,
                        const Eigen::Vector3d& gn,
                        bool update_jacobian,
                        bool gravity_included)
{
    Eigen::Vector3d acc0, acc1, gyr0, gyr1;
    acc0 = Eigen::Vector3d(pre_imu_data.linear_acceleration.x, pre_imu_data.linear_acceleration.y, pre_imu_data.linear_acceleration.z);
    acc1 = Eigen::Vector3d(curr_imu_data.linear_acceleration.x, curr_imu_data.linear_acceleration.y, curr_imu_data.linear_acceleration.z);
    gyr0 = Eigen::Vector3d(pre_imu_data.angular_velocity.x, pre_imu_data.angular_velocity.y, pre_imu_data.angular_velocity.z);
    gyr1 = Eigen::Vector3d(curr_imu_data.angular_velocity.x, curr_imu_data.angular_velocity.y, curr_imu_data.angular_velocity.z);
    
    double dt = curr_imu_data.time - pre_imu_data.time;
    Eigen::Vector3d gyro_av = 0.5 * (gyr0 + gyr1) - linearized_bg;
    Eigen::Quaterniond temp = Eigen::Quaterniond(1, gyro_av(0) * dt / 2, gyro_av(1) * dt / 2, gyro_av(2) * dt / 2);
    temp.normalize();
    result_dq = dq * temp;

    Eigen::Vector3d acc_av = 0.5 * ((dq * (acc0 - linearized_ba) + result_dq * (acc1 - linearized_ba)));
    if (gravity_included)
    {
        acc_av += gn;
    }
     
    result_alpha = alpha + beta * dt + 0.5 * acc_av * dt * dt;
    result_beta = beta + acc_av * dt;
    // result_linearized_ba = linearized_ba;
    // result_linearized_bg = linearized_bg;

    if (update_jacobian)
    {
        Eigen::Vector3d a0_x = acc0 - linearized_ba;
        Eigen::Vector3d a1_x = acc1 - linearized_ba;
        Eigen::Matrix3d R_w_x, R_a0_x, R_a1_x;

        R_w_x << 0, -gyro_av(2), gyro_av(1),
            gyro_av(2), 0, -gyro_av(0),
            -gyro_av(1), gyro_av(0), 0;
        R_a0_x << 0, -a0_x(2), a0_x(1),
            a0_x(2), 0, -a0_x(0),
            -a0_x(1), a0_x(0), 0;
        R_a1_x << 0, -a1_x(2), a1_x(1),
            a1_x(2), 0, -a1_x(0),
            -a1_x(1), a1_x(0), 0;

        Eigen::Matrix<double, 15, 15> F = Eigen::Matrix<double, 15, 15>::Zero();
        F.block<3, 3>(0, 0) = Eigen::Matrix3d::Identity();
        F.block<3, 3>(0, 3) = -0.25 * dt * dt * (dq.toRotationMatrix() * R_a0_x + result_dq.toRotationMatrix() * R_a1_x * (Eigen::Matrix3d::Identity() - R_w_x * dt));
        F.block<3, 3>(0, 6) = Eigen::Matrix3d::Identity() * dt;
        F.block<3, 3>(0, 9) = -0.25 * dt * dt * (dq.toRotationMatrix() + result_dq.toRotationMatrix());
        F.block<3, 3>(0, 12) = 0.25 * dt * dt * dt * result_dq.toRotationMatrix() * R_a1_x;

        F.block<3, 3>(3, 3) = Eigen::Matrix3d::Identity() - R_w_x * dt;
        F.block<3, 3>(3, 12) = -1.0 * dt * Eigen::Matrix3d::Identity(3, 3);

        F.block<3, 3>(6, 3) = -0.5 * dt * (dq.toRotationMatrix() * R_a0_x + result_dq.toRotationMatrix() * R_a1_x * (Eigen::Matrix3d::Identity() - R_w_x * dt));
        F.block<3, 3>(6, 6) = Eigen::Matrix3d::Identity();
        F.block<3, 3>(6, 9) = -0.5 * dt * (dq.toRotationMatrix() + result_dq.toRotationMatrix());
        F.block<3, 3>(6, 12) = 0.5 * dt * dt * result_dq.toRotationMatrix() * R_a1_x;

        F.block<3, 3>(9, 9) = Eigen::Matrix3d::Identity();

        F.block<3, 3>(12, 12) = Eigen::Matrix3d::Identity();

        Eigen::Matrix<double, 15, 18> G = Eigen::Matrix<double, 15, 18>::Zero();
        G.block<3, 3>(0, 0) = 0.25 * dt * dt * dq.toRotationMatrix();
        G.block<3, 3>(0, 3) = -0.125 * dt * dt * dt * result_dq.toRotationMatrix() * R_a1_x;
        G.block<3, 3>(0, 6) = 0.25 * dt * dt * result_dq.toRotationMatrix();
        G.block<3, 3>(0, 9) = G.block<3, 3>(0, 3);

        G.block<3, 3>(3, 3) = 0.5 * dt * Eigen::Matrix3d::Identity(3, 3);
        G.block<3, 3>(3, 9) = 0.5 * dt * Eigen::Matrix3d::Identity(3, 3);

        G.block<3, 3>(6, 0) = 0.5 * dt * dq.toRotationMatrix();
        G.block<3, 3>(6, 3) = -0.25 * dt * dt * result_dq.toRotationMatrix() * R_a1_x;
        G.block<3, 3>(6, 6) = 0.5 * dt * result_dq.toRotationMatrix();
        G.block<3, 3>(6, 9) = G.block<3, 3>(6, 3);

        G.block<3, 3>(9, 12) = Eigen::Matrix3d::Identity() * dt;

        G.block<3, 3>(12, 15) = Eigen::Matrix3d::Identity() * dt;

        result_jacobian = F * jacobian;
        result_covariance = F * covariance * F.transpose() + G * noise * G.transpose();
    }
}


//将观测信息转到imu坐标系，进行修正预测。
void PreIntergration::ErrorEKFCorrect(State& measure_state)
{
    //将观测数据转到imu坐标系下
    TransMeasureData2IMU(measure_state);

    //6*1
    Eigen::Matrix<double,6,1> m_state;
    sum_dt_ = measure_state.stamp - pre_state_.stamp;
    State preintergration_state;
    preintergration_state.p = pre_state_.q.matrix() * (pre_state_.q.inverse().matrix() * (pre_state_.p + pre_state_.v * sum_dt_ - 0.5 * gn_ * sum_dt_ * sum_dt_) + curr_P_);
    preintergration_state.q = pre_state_.q * curr_Q_;
    preintergration_state.v = pre_state_.q.matrix() * (pre_state_.q.inverse().matrix() * (pre_state_.v - sum_dt_ * gn_) + curr_V_);
    
    m_state.head(3) = preintergration_state.p - measure_state.p;
    Eigen::Matrix3d dr = preintergration_state.q.matrix() * measure_state.q.matrix().inverse();
    dr = dr - Eigen::Matrix3d::Identity();
    Eigen::Vector3d dphi(dr(1, 2), dr(2, 0), dr(0, 1));
    m_state.tail(3) = dphi;

    //定义的state_其中x是error state的15维数据，pvq是imu在世界坐标系下的数据
    // state_.x = jacobians_ * Eigen::Vector3d::Zero() + G_eskf_ * noise_;
    Eigen::Matrix<double, 18, 1> noise;
    // Eigen::Matrix<double, 15, 6> _G_eskf_;
    // state_.x = _G_eskf_ * noise;
    double dp_noise, dphi_noise, gyro_noise_, acc_noise_;
    Eigen::Vector3d I_vec = Eigen::Vector3d(1,1,1);
    noise.block<3,1>(0,0) = acc_n_ * I_vec;
    noise.block<3,1>(0,3) = gyr_n_ * I_vec;
    noise.block<3,1>(0,6) = acc_n_ * I_vec;
    noise.block<3,1>(0,9) = gyr_n_ * I_vec;

    noise.block<3,1>(0,12) = acc_w_ * I_vec;
    noise.block<3,1>(0,15) = gyr_w_ * I_vec;

    state_.x = G_eskf_ * noise;
    Eigen::Matrix<double,6,15> Gk;
    Eigen::Matrix<double,6,6> Ck;
    Eigen::Matrix<double,6,6> Rk;

    measure_p_n_ = 0.1;
    measure_q_n_ = 0.02;
    Rk.block<3,3>(0,0) = measure_p_n_ * measure_p_n_ * Eigen::Matrix3d::Identity();
    Rk.block<3,3>(3,3) = measure_q_n_ * measure_q_n_ * Eigen::Matrix3d::Identity();

    Gk.block<3,3>(0,0) = Eigen::Matrix3d::Identity();
    Gk.block<3,3>(3,6) = Eigen::Matrix3d::Identity();

    Ck.setIdentity();
    eskf_K_ = covariance_ * Gk.transpose() * (Gk * covariance_ * Gk.transpose() + Ck * Rk * Ck.transpose()).inverse();

    covariance_ = (Eigen::Matrix<double, 15, 15>::Identity() - eskf_K_ * Gk) * covariance_;
    Eigen::Matrix<double, 15, 1> Xk = state_.x + eskf_K_ * (m_state - Gk * state_.x);
    
    state_.p = preintergration_state.p - Xk.head(3);
    Eigen::Vector3d dphi_dir = Xk.block<3, 1>(3, 0);
    double dphi_norm = dphi_dir.norm();
    if (dphi_norm != 0)
    {
        dphi_dir = dphi_dir / dphi_norm;
        dphi_dir = dphi_dir * std::sin(dphi_norm / 2);
    }
    Eigen::Quaterniond temp2(std::cos(dphi_norm / 2), dphi_dir[0], dphi_dir[1], dphi_dir[2]);
    state_.q = temp2 * preintergration_state.q;

    state_.v = preintergration_state.v - Xk.block<3,1>(6,0);
    state_.ba = curr_ba_ - Xk.block<3,1>(9,0);
    state_.bg = curr_bg_ - Xk.block<3,1>(12,0);
    // state_.x.block<3,1>(6,0) -= Xk.tail(6);

}

State PreIntergration::DeadReckoning(const State& opt_state, std::deque<IMUData>& imu_datas, const Eigen::Matrix<double, 18, 18>& noise, const Eigen::Vector3d& gn)
{
    Eigen::Quaterniond curr_Q;
    Eigen::Vector3d curr_beta, curr_alpha, curr_ba, curr_bg;

    Eigen::Vector3d pre_alpha = opt_state.p;
    Eigen::Quaterniond pre_Q = opt_state.q;
    Eigen::Vector3d pre_beta = opt_state.v;
    curr_ba = opt_state.ba;
    curr_bg = opt_state.bg;

    IMUData pre_imu_data, curr_imu_data, first_imu_data;

    Eigen::Matrix<double, 15, 15> jacobian, result_jacobian;
    jacobian.setIdentity();
    Eigen::Matrix<double, 15, 15> result_covariance;
    Eigen::Matrix<double, 15, 15> covariance;
    covariance.setZero();

    if ((imu_datas.front().time - opt_state.stamp) > 1e-5)
    {
        first_imu_data = imu_datas.front();
        first_imu_data.time = opt_state.stamp;
        imu_datas.push_front(first_imu_data);
    }
    
    bool first = true;


    for (const auto& imudata : imu_datas)
    {
        if(first)
        {
            pre_imu_data = imudata;
            first = false;
            continue;
        }
        curr_imu_data = imudata;
        midPointIntegration(pre_imu_data, curr_imu_data, pre_Q, curr_Q, pre_beta, curr_beta, pre_alpha, curr_alpha, curr_ba, curr_bg, jacobian, covariance, result_jacobian, result_covariance, noise, gn, false, true);
        pre_Q = curr_Q;
        pre_beta = curr_beta;
        pre_alpha = curr_alpha;
        jacobian = result_jacobian;
        covariance = result_covariance;
        pre_imu_data = curr_imu_data;
    }
    State last_state;
    last_state.p = curr_alpha;
    last_state.q = curr_Q;
    last_state.v = curr_beta;
    last_state.ba = curr_ba;
    last_state.bg = curr_bg;
    // last_state.covariance = 
    last_state.stamp = imu_datas.back().time;
    return last_state;
}

Eigen::Matrix<double, 15, 1> PreIntergration::evaluate(const Eigen::Vector3d &Pi,
                                            const Eigen::Quaterniond &Qi,
                                            const Eigen::Vector3d &Vi,
                                            const Eigen::Vector3d &Bai,
                                            const Eigen::Vector3d &Bgi,
                                            const Eigen::Vector3d &Pj,
                                            const Eigen::Quaterniond &Qj,
                                            const Eigen::Vector3d &Vj,
                                            const Eigen::Vector3d &Baj,
                                            const Eigen::Vector3d &Bgj)
{
    Eigen::Matrix<double, 15, 1> residuals;

    Eigen::Vector3d dba = Bai - curr_ba_;
    Eigen::Vector3d dbg = Bgi - curr_bg_;
    Eigen::Matrix3d dalpha_dba = jacobians_.block<3, 3>(0, 9);
    // Eigen::Matrix3d dq_dba = jacobians_.block<3, 3>(3, 9);
    Eigen::Matrix3d dbeta_dba = jacobians_.block<3, 3>(6, 9);
    Eigen::Matrix3d dalpha_dbg = jacobians_.block<3, 3>(0, 12);
    Eigen::Matrix3d dq_dbg = jacobians_.block<3, 3>(3, 12);
    Eigen::Matrix3d dbeta_dbg = jacobians_.block<3, 3>(6, 12);

    Eigen::Quaterniond corrected_dq = curr_Q_ * deltaQ(dq_dbg * dbg);
    Eigen::Vector3d corrected_alpha = curr_P_ + dalpha_dba * dba + dalpha_dbg * dbg;
    Eigen::Vector3d corrected_beta = curr_V_ + dbeta_dba * dba + dbeta_dbg * dbg;


/*         auto out = Qi.inverse() * (Pj - Pi - Vi * sum_dt_ - 0.5 * gn_ * sum_dt_ * sum_dt_);
    LOG(INFO) <<"P: " << std::endl
            << "观测：" << out(0) << ", " << out(1) << ", " << out(2) << std::endl
            << "预积分： " << corrected_alpha(0) << ", " << corrected_alpha(1) << ", " << corrected_alpha(2);
    auto out2 = Qi.inverse() * (Vj - Vi - gn_ * sum_dt_) - corrected_beta;
    auto out1 = Qi.inverse() * (Vj - Vi - gn_ * sum_dt_);
    LOG(INFO) <<"V: " << std::endl
            <<"vec2: " << Vj(0) << ", " << Vj(1)<< ", " << Vj(2) << std::endl
            <<"error: " << out2(0) << ", " << out2(1)<< ", " << out2(2) << std::endl
            << "观测：" << out1(0) << ", " << out1(1)<< ", " << out1(2) << std::endl
            << "预积分： " << corrected_beta(0) << ", " << corrected_beta(1) << ", " << corrected_beta(2)
            <<"------------------------------------------------------------------------------------------"; */


    residuals.block<3, 1>(0, 0) = Qi.inverse() * (Pj - Pi - Vi * sum_dt_ - 0.5 * gn_ * sum_dt_ * sum_dt_) - corrected_alpha;
    residuals.block<3, 1>(3, 0) = 2 * (corrected_dq.inverse() * (Qi.inverse() * Qj)).vec();
    residuals.block<3, 1>(6, 0) = Qi.inverse() * (Vj - Vi - gn_ * sum_dt_) - corrected_beta;
    residuals.block<3, 1>(9, 0) = Baj - Bai;
    residuals.block<3, 1>(12, 0) = Bgj - Bgi;
        return residuals;
}

Eigen::Matrix<double, 15, 30> PreIntergration::jacobian(const Eigen::Vector3d &Pi,
                                        const Eigen::Quaterniond &Qi,
                                        const Eigen::Vector3d &Vi,
                                        const Eigen::Vector3d &Bai,
                                        const Eigen::Vector3d &Bgi,
                                        const Eigen::Vector3d &Pj,
                                        const Eigen::Quaterniond &Qj,
                                        const Eigen::Vector3d &Vj,
                                        const Eigen::Vector3d &Baj,
                                        const Eigen::Vector3d &Bgj)
{
    Eigen::Matrix<double, 15, 30> jacobians;
    jacobians.setZero();

    // Eigen::Vector3d dba = Bai - linearized_ba_;
    Eigen::Vector3d dbg = Bgi - curr_bg_;
    Eigen::Matrix3d dalpha_dba = jacobians_.block<3, 3>(0, 9);
    // Eigen::Matrix3d dq_dba = jacobians_.block<3, 3>(3, 9);
    Eigen::Matrix3d dbeta_dba = jacobians_.block<3, 3>(6, 9);
    Eigen::Matrix3d dalpha_dbg = jacobians_.block<3, 3>(0, 12);
    Eigen::Matrix3d dq_dbg = jacobians_.block<3, 3>(3, 12);
    Eigen::Matrix3d dbeta_dbg = jacobians_.block<3, 3>(6, 12);
    Eigen::Quaterniond corrected_dq = curr_Q_ * deltaQ(dq_dbg * dbg);
    // Eigen::Vector3d corrected_alpha = alpha_ + dalpha_dba * dba + dalpha_dbg * dbg;
    // Eigen::Vector3d corrected_beta = beta_ + dbeta_dba * dba + dbeta_dbg * dbg;

    jacobians.block<3, 3>(0, 0) = -Qi.inverse().toRotationMatrix();
    jacobians.block<3, 3>(0, 3) = skewSymmetric(Qi.inverse() * (Pj - Pi - Vi * sum_dt_ - 0.5 * gn_ * sum_dt_ * sum_dt_));
    jacobians.block<3, 3>(0, 6) = -Qi.inverse().toRotationMatrix() * sum_dt_;
    jacobians.block<3, 3>(0, 9) = -dalpha_dba;
    jacobians.block<3, 3>(0, 12) = -dalpha_dbg;

    jacobians.block<3, 3>(0, 15) = Qi.inverse().toRotationMatrix();

    jacobians.block<3, 3>(3, 3) = -(Qleft(Qj.inverse() * Qi) * Qright(corrected_dq)).bottomRightCorner<3, 3>();
    jacobians.block<3, 3>(3, 12) = -(Qleft(Qj.inverse() * Qi * corrected_dq)).bottomRightCorner<3, 3>() * dq_dbg;

    jacobians.block<3, 3>(3, 18) = (Qleft(corrected_dq.inverse() * Qi.inverse() * Qj)).bottomRightCorner<3, 3>();

    jacobians.block<3, 3>(6, 3) = skewSymmetric(Qi.inverse() * (Vj - Vi - gn_ * sum_dt_));
    jacobians.block<3, 3>(6, 6) = -Qi.inverse().toRotationMatrix();
    jacobians.block<3, 3>(6, 9) = -dbeta_dba;
    jacobians.block<3, 3>(6, 12) = -dbeta_dbg;

    jacobians.block<3, 3>(6, 21) = Qi.inverse().toRotationMatrix();

    jacobians.block<3, 3>(9, 9) = -Eigen::Matrix3d::Identity();

    jacobians.block<3, 3>(9, 24) = Eigen::Matrix3d::Identity();

    jacobians.block<3, 3>(12, 12) = -Eigen::Matrix3d::Identity();

    jacobians.block<3, 3>(12, 27) = Eigen::Matrix3d::Identity();

    return jacobians;
}

}// fusion_optimizer