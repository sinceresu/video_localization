#include "fusion_optimizer/imu_process/pre_twist_integration.hpp"

namespace fusion_optimizer{

PreTwistIntergration::PreTwistIntergration(const std::deque<TwistData>& twist_datas, const State& pre_state, const State& curr_state)
:Params()
{   
    twist_buff_.clear();
    twist_buff_ = twist_datas;
    for(int i = 0; i < 3; i++)
    {
        noise_(i, i) = twist_acc_n_[i] * twist_acc_n_[i]; 
        int j = i + 3; 
        noise_(j, j) = twist_gyr_n_[j] * twist_gyr_n_[j]; 
    }
    noise_.block<6, 6>(6, 6) = noise_.block<6, 6>(0, 0);
    Propagate();
}

void PreTwistIntergration::Propagate()
{
    Eigen::Quaterniond pre_Q, curr_Q;
    Eigen::Vector3d pre_alpha, curr_alpha;
    Eigen::Matrix<double, 6, 6> pre_jacobians, cur_jacobians;
    Eigen::Matrix<double, 6, 6> pre_covariance, cur_covariance;

    pre_Q.setIdentity();
    pre_alpha.setZero();
    pre_jacobians.setIdentity();
    pre_covariance.setZero();
    
    int data_length = twist_buff_.size(); 
    for (int i = 0; i < (data_length-1); i++)
    {
        Eigen::Matrix3d twist_2_imu;
        twist_2_imu << 1, 0, 0,
                       0, 0, 1,
                       0, 1, 0;
        TransTwistData2IMU(twist_buff_[i], twist_2_imu.inverse());
        TransTwistData2IMU(twist_buff_[i+1], twist_2_imu.inverse());
        midPointIntegration(twist_buff_[i], twist_buff_[i+1], pre_Q, curr_Q, pre_alpha, curr_alpha, pre_jacobians, pre_covariance, cur_jacobians, cur_covariance, noise_, true);
        pre_Q = curr_Q;
        pre_alpha = curr_alpha;
        pre_jacobians = cur_jacobians;
        pre_covariance = cur_covariance;

    }

    curr_Q_ = curr_Q;
    curr_alpha_ = curr_alpha;
    covariance_ = cur_covariance;
    jacobians_ = cur_jacobians;
    information_ = cur_covariance.inverse();
}

void PreTwistIntergration::midPointIntegration(const TwistData& pre_twist_data, 
                                    const TwistData& curr_twist_data,
                                    const Eigen::Quaterniond &dq,
                                    Eigen::Quaterniond &result_dq,
                                    const Eigen::Vector3d &alpha,
                                    Eigen::Vector3d &result_alpha,
                                    const Eigen::Matrix<double, 6, 6> &jacobian,
                                    const Eigen::Matrix<double, 6, 6> &covariance,
                                    Eigen::Matrix<double, 6, 6> &result_jacobian,
                                    Eigen::Matrix<double, 6, 6> &result_covariance,
                                    const Eigen::Matrix<double, 12, 12>& noise,
                                    bool update_jacobian)
{
    Eigen::Vector3d vel0, vel1, q1, q0, q_mid, vel_mid;
    vel0 = pre_twist_data.vel;
    vel1 = curr_twist_data.vel;
    q0 = pre_twist_data.gyro;
    q1 = curr_twist_data.gyro;
    double dt = curr_twist_data.time - pre_twist_data.time;
    q_mid = (q0 + q1) * 0.5 * dt;
    Eigen::Quaterniond temp = Eigen::Quaterniond(1, 0.5 * q_mid[0], 0.5 * q_mid[1], 0.5 * q_mid[2]);
    temp.normalize();
    result_dq = dq * temp;

    vel_mid = dq.matrix() * vel0 + result_dq.matrix() * vel1;
    result_alpha = alpha + vel_mid * dt;

    if(update_jacobian)
    {
        Eigen::Matrix3d R_w_x, R_v0_x, R_v1_x;
        R_w_x << 0, -q_mid(2), q_mid(1),
                q_mid(2), 0, -q_mid(0),
                -q_mid(1), q_mid(0), 0;

        R_v0_x << 0, -vel0(2), vel0(1),
                vel0(2), 0, -vel0(0),
                -vel0(1), vel0(0), 0;
    
        R_v1_x << 0, -vel1(2), vel1(1),
                vel1(2), 0, -vel1(0),
                -vel1(1), vel1(0), 0;

        Eigen::Matrix<double, 6, 6> F = Eigen::Matrix<double, 6, 6>::Zero();
        F.block<3, 3>(0, 0) = Eigen::Matrix3d::Identity();
        F.block<3, 3>(3, 3) = Eigen::Matrix3d::Identity() - R_w_x * dt;
        F.block<3, 3>(0, 3) = -0.5 * dt * (dq.matrix() * R_v0_x + result_dq.matrix() * R_v1_x  * F.block<3, 3>(3, 3));

        Eigen::Matrix<double, 6, 12> G = Eigen::Matrix<double, 6, 12>::Zero();
        G.block<3, 3>(0, 0) = -0.5 * dt * dq.matrix();
        G.block<3, 3>(0, 3) = -0.5 * dt * result_dq.matrix();
        G.block<3, 3>(0, 6) = 0.25 * dt * dt * result_dq.matrix() * R_v1_x;
        G.block<3, 3>(0, 9) = G.block<3, 3>(0, 6);
        G.block<3, 3>(0, 6) = G.block<3, 3>(0, 6) - 0.5 * dt * dq.matrix();
        G.block<3, 3>(0, 9) = G.block<3, 3>(0, 9) - 0.5 * dt * result_dq.matrix();

        result_jacobian = F * jacobian;
        result_covariance = F * covariance * F.transpose() + G * noise * G.transpose();
    }
}

Eigen::Matrix<double, 6, 1> PreTwistIntergration::evaluate(const Eigen::Vector3d &Pi,
                                        const Eigen::Quaterniond &Qi,
                                        const Eigen::Vector3d &Pj,
                                        const Eigen::Quaterniond &Qj)
{
    Eigen::Matrix<double, 6, 1> residuals;
    residuals.setZero();
    residuals.head(3) = Qi.inverse().matrix() * (Pj - Pi) - curr_alpha_;
    residuals.tail(3) = 2.0 * (curr_Q_.inverse() * (Qi.inverse() * Qj).vec());
    return residuals;
}

Eigen::Matrix<double, 6, 12> PreTwistIntergration::jacobian(const Eigen::Vector3d &Pi,
                                        const Eigen::Quaterniond &Qi,
                                        const Eigen::Vector3d &Pj,
                                        const Eigen::Quaterniond &Qj)
{
    Eigen::Matrix<double, 6, 12> jacobians;
    jacobians.setZero();

    jacobians.block<3, 3>(0, 0) = -Qi.inverse().matrix();
    jacobians.block<3, 3>(0, 3) = skewSymmetric(Qi.inverse().matrix() * (Pj - Pi));
    jacobians.block<3, 3>(0, 9) = Qi.inverse().matrix();

    jacobians.block<3, 3>(3, 3) = -(Qleft((Qi.inverse() * Qj)) * Qright(curr_Q_)).bottomRightCorner<3, 3>();
    jacobians.block<3, 3>(3, 9) = Qleft(curr_Q_.inverse() * Qi.inverse() * Qj).bottomRightCorner<3, 3>();

}

State PreTwistIntergration::DeadReckoning(const State& opt_state, std::deque<TwistData>& twist_datas, const Eigen::Matrix<double, 12, 12>& noise)
{
    int data_length = twist_datas.size();
    State res_state;
    if(data_length == 0)
    {
        return res_state;
    }

    if ((twist_datas.front().time - opt_state.stamp) > 1e-5)
    {
        TwistData first_imu_data = twist_datas.front();
        first_imu_data.time = opt_state.stamp;
        twist_datas.push_front(first_imu_data);
    }

    Eigen::Quaterniond pre_Q, curr_Q;
    Eigen::Vector3d pre_alpha, curr_alpha;
    Eigen::Matrix<double, 6, 6> pre_jacobians, cur_jacobians;
    Eigen::Matrix<double, 6, 6> pre_covariance, cur_covariance;

    pre_Q = opt_state.q;
    pre_alpha = opt_state.p;
    pre_jacobians.setIdentity();
    pre_covariance.setZero();

    for (int i = 0;  i < (data_length - 1); i++)
    {
        // twist_datas[i]
        midPointIntegration(twist_datas[i], twist_datas[i + 1], pre_Q, curr_Q, pre_alpha, curr_alpha, pre_jacobians, pre_covariance, cur_jacobians, cur_covariance, noise, false);
        pre_alpha = curr_alpha;
        pre_Q = curr_Q;
    }
    State last_state;
    last_state.p = curr_alpha;
    last_state.q = curr_Q;
    last_state.stamp = twist_datas.back().time;
    return last_state;
}

}