#include "fusion_optimizer/sensor_bridge/predictor.hpp"

namespace fusion_optimizer{
    Predictor::Predictor(ros::NodeHandle& nh, const PredictorParam& predic_param)
    {
        if (predic_param.type == "Imu")
        {
            subscriber_ = std::make_shared<IMUSubscriber>(nh, predic_param.topic, predic_param.buffer_size);
            type_ = "Imu";
        }else if (predic_param.type == "Odometry"){
            subscriber_ = std::make_shared<ObservationSubscriber>(nh, predic_param.topic, predic_param.buffer_size);
            type_ = "Odometry";
        }else if (predic_param.type == "Twist"){
            subscriber_ = std::make_shared<TwistSubscriber>(nh, predic_param.topic, predic_param.buffer_size);
            type_ = "Twist";
        }else{
            LOG(ERROR) << "Don't have predictor type " << predic_param.type;
        }
        //ToDO
    }

    bool Predictor::HasData(){

    }

}