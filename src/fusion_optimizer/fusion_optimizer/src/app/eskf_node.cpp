#include "fusion_optimizer/localization/eskf_localization_flow.hpp"
#include <ros/ros.h>

using namespace fusion_optimizer;
int main(int argc, char** argv)
{
    google::InitGoogleLogging(argv[0]);
    FLAGS_log_dir =  "/home/Log";
    FLAGS_stop_logging_if_full_disk = true;
    FLAGS_logtostderr = true;
    ros::init(argc, argv, "eskf_node");
    ros::NodeHandle nh;
    std::string sub_imu_topic, sub_obvervation_topic;
    int buff_size = 1000;
    nh.param<std::string>("sub_imu_topic", sub_imu_topic, "/livox/imu");
    nh.param<std::string>("sub_obvervation_topic", sub_obvervation_topic, "/aft_mapped_to_init");
    std::unique_ptr<EskfLocalizationFlow> eskf_localization_ptr = std::unique_ptr<EskfLocalizationFlow>(new EskfLocalizationFlow(nh, sub_imu_topic, sub_obvervation_topic, buff_size));

    ros::Rate rate(100);
    while(ros::ok())
    {
        ros::spinOnce();
        eskf_localization_ptr->Run();
        rate.sleep();
    }
    return 1;
}