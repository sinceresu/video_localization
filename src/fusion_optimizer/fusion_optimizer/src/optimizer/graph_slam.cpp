#include "fusion_optimizer/optimizer/graph_slam.hpp"
#include "fusion_optimizer/optimizer/g2o/vertex_3_demensional.hpp"
#include "fusion_optimizer/optimizer/g2o/measure_edge.hpp"

namespace fusion_optimizer{
using namespace g2o;
GraphSlam::GraphSlam()
: Params()
{
    window_size_ = windows_max_size_;
    edge_num_ = 0;
    //所有添加到优化器的状态组数
    state_size_ = 0;
    //一组状态量包含顶点个数，pq为一个，v,ba,bg各为一个
    // state_has_vertex_num_ = 4;
    state_has_vertex_num_ = 5;
    //因为优化器中顶点个数不包括已经marge掉的顶点，但是id却是一直累加，marge_vertex_num_ + 目前优化器中顶点个数为下一个顶点的id
    marge_vertex_num_ = 0;

    robust_kernel_factory_ = g2o::RobustKernelFactory::instance();
    robust_kernel_name_ = "Cauchy";
    robust_kernel_delta_ = 1.0;
    // last_pose_valid_ = true;
    graph_ptr_.reset(new g2o::SparseOptimizer());
    
    g2o::OptimizationAlgorithmFactory* solver_factory = g2o::OptimizationAlgorithmFactory::instance();
    g2o::OptimizationAlgorithmProperty solver_property;
    g2o::OptimizationAlgorithm* solver = solver_factory->construct("lm_var_cholmod", solver_property);

    graph_ptr_->setAlgorithm(solver);
    if(!graph_ptr_->solver())
    {
        LOG(ERROR) << " g2o 优化器创建失败";
        solver_factory->listSolvers(std::cout);
    }else{
        LOG(INFO) << " g2o 优化器创建成功";
    }
}


void GraphSlam::AddPositionEdge(Eigen::Vector3d& m, Eigen::Matrix3d& information)
{
    int vertex_size = graph_ptr_->vertices().size() + marge_vertex_num_;
    int start_index = vertex_size - 5;
    VertexVec* p = dynamic_cast<VertexVec* >(graph_ptr_->vertex(start_index));
    PositionEdge* edge(new PositionEdge());
    edge->vertices()[0] = p;

    edge->setId(edge_num_);
    edge->setMeasurement(m);

    edge->setInformation(information);
    edge_num_++;
    AddRobustKernel(edge, robust_kernel_name_, robust_kernel_delta_);
    graph_ptr_->addEdge(edge);
}


MeasureEdge* GraphSlam::AddMeasureEdge(std::pair<Eigen::Vector3d, Eigen::Quaterniond>& m, Eigen::MatrixXd& information)
{
    int vertex_size = graph_ptr_->vertices().size() + marge_vertex_num_;
    int start_index = vertex_size - 5;
    VertexVec* p = dynamic_cast<VertexVec* >(graph_ptr_->vertex(start_index));
    VertexQ* q = dynamic_cast<VertexQ* >(graph_ptr_->vertex(start_index + 1));
    MeasureEdge* edge(new MeasureEdge());
    edge->vertices()[0] = p;
    edge->vertices()[1] = q;

    edge->setId(edge_num_);
    edge->setMeasurement(m);
    edge->setInformation(information);
    edge_num_++;
    AddRobustKernel(edge, robust_kernel_name_, robust_kernel_delta_);
    graph_ptr_->addEdge(edge);
    return edge;
}

ImuEdge* GraphSlam::AddImuEdge(const std::shared_ptr<PreIntergration> pre_intergration_ptr_,
                                const double& pre_time,
                                const double& last_time)
{
    int vertex_size = graph_ptr_->vertices().size() + marge_vertex_num_;
    int indexi = vertex_size - 10;
    int indexj = vertex_size - 5;
    VertexVec* p1 = dynamic_cast<VertexVec* >(graph_ptr_->vertex(indexi));
    VertexQ* q1 = dynamic_cast<VertexQ* >(graph_ptr_->vertex(indexi + 1));
    VertexVec* vv1 = dynamic_cast<VertexVec* >(graph_ptr_->vertex(indexi + 2));
    VertexVec* baa1 = dynamic_cast<VertexVec* >(graph_ptr_->vertex(indexi + 3));
    VertexVec* bgg1 = dynamic_cast<VertexVec* >(graph_ptr_->vertex(indexi + 4));

    VertexVec* p2 = dynamic_cast<VertexVec* >(graph_ptr_->vertex(indexj));
    VertexQ* q2 = dynamic_cast<VertexQ* >(graph_ptr_->vertex(indexj + 1));
    VertexVec* vv2 = dynamic_cast<VertexVec* >(graph_ptr_->vertex(indexj + 2));
    VertexVec* baa2 = dynamic_cast<VertexVec* >(graph_ptr_->vertex(indexj + 3));
    VertexVec* bgg2 = dynamic_cast<VertexVec* >(graph_ptr_->vertex(indexj + 4));

    EdgeImu* edge(new EdgeImu());
    edge->vertices()[0] = p1;
    edge->vertices()[1] = q1;
    edge->vertices()[2] = vv1;
    edge->vertices()[3] = baa1;
    edge->vertices()[4] = bgg1;

    edge->vertices()[5] = p2;
    edge->vertices()[6] = q2;
    edge->vertices()[7] = vv2;
    edge->vertices()[8] = baa2;
    edge->vertices()[9] = bgg2;
    
    edge->setId(edge_num_);
    edge->setMeasurement(pre_intergration_ptr_);
    edge_num_++;
    AddRobustKernel(edge, robust_kernel_name_, robust_kernel_delta_);
    graph_ptr_->addEdge(edge);
    last_time_ = last_time;
    pre_time_ = pre_time;
    
    if (window_times_.size() < 2)
    {
        window_times_.push_back(last_time);
        window_times_.push_back(pre_time);
    }else{
        window_times_.push_back(last_time);
        if(window_times_.size() > windows_max_size_)
        {
            window_times_.pop_front();
        }
    }
}

void GraphSlam::AddTwistEdge(const std::shared_ptr<PreTwistIntergration> pre_twist_intergration_ptr_)
{
    int vertex_size = graph_ptr_->vertices().size() + marge_vertex_num_;
    int indexi = vertex_size - 10;
    int indexj = vertex_size - 5;

    VertexVec* p1 = dynamic_cast<VertexVec* >(graph_ptr_->vertex(indexi));
    VertexQ* q1 = dynamic_cast<VertexQ* >(graph_ptr_->vertex(indexi + 1));

    VertexVec* p2 = dynamic_cast<VertexVec* >(graph_ptr_->vertex(indexj));
    VertexQ* q2 = dynamic_cast<VertexQ* >(graph_ptr_->vertex(indexj + 1));

    TwistEdge* edge(new TwistEdge());
    edge->vertices()[0] = p1;
    edge->vertices()[1] = q1;

    edge->vertices()[2] = p2;
    edge->vertices()[3] = q2;
    edge->setId(edge_num_);
    edge->setMeasurement(pre_twist_intergration_ptr_);
    edge_num_++;
    AddRobustKernel(edge, robust_kernel_name_, robust_kernel_delta_);
    graph_ptr_->addEdge(edge);
}

OdomeEdge* GraphSlam::AddOdomEdge(const std::shared_ptr<OdomData> m, const Eigen::MatrixXd& information_matrix)
{
    int vertex_size = graph_ptr_->vertices().size() + marge_vertex_num_;
    int indexi = vertex_size - 10;
    int indexj = vertex_size - 5;
    VertexVec* p1 = dynamic_cast<VertexVec* >(graph_ptr_->vertex(indexi));
    VertexQ* q1 = dynamic_cast<VertexQ* >(graph_ptr_->vertex(indexi + 1));

    VertexVec* p2 = dynamic_cast<VertexVec* >(graph_ptr_->vertex(indexj));
    VertexQ* q2 = dynamic_cast<VertexQ* >(graph_ptr_->vertex(indexj + 1));

    OdomeEdge* edge(new OdomeEdge());
    edge->vertices()[0] = p1;
    edge->vertices()[1] = q1;
    edge->vertices()[2] = p2;
    edge->vertices()[3] = q2;

    edge->setId(edge_num_);
    edge->setMeasurement(m);
    edge->setInformation(information_matrix);
    edge_num_++;
    AddRobustKernel(edge, robust_kernel_name_, robust_kernel_delta_);
    graph_ptr_->addEdge(edge);
    return edge;
}

VisUnitEdge* GraphSlam::AddVisEdge(g2o::SE3Quat& m, g2o::VertexSE3Expmap& v1, const Eigen::MatrixXd& information_matrix)
{
    int vertex_size = graph_ptr_->vertices().size();
    int indexj = vertex_size - 4;
    g2o::VertexSE3Expmap* T2 = dynamic_cast<g2o::VertexSE3Expmap* >(graph_ptr_->vertex(indexj));

    
    VisUnitEdge* edge(new VisUnitEdge());
    edge->setInformation(information_matrix);
    edge->setMeasurement(m);
    edge->vertices()[0] = T2;
    // edge->vertices()[0] = &v1;
    // edge->vertices()[0] = dynamic_cast<g2o::VertexSE3Expmap *>(&v1);
    edge->setId(edge_num_);
    AddRobustKernel(edge, robust_kernel_name_, robust_kernel_delta_);
    edge_num_++;
    graph_ptr_->addEdge(edge);
    return edge;
}
        
EdgeMarginalization* GraphSlam::AddMarEdge(const Eigen::VectorXd& minus_mu, 
                                const Eigen::MatrixXd& A,
                                const std::vector<g2o::HyperGraph::Vertex *>& margvertices,
                                const int vertexs_size)
{
    EdgeMarginalization* edge(new EdgeMarginalization());
    edge->setMeasurement(minus_mu);
    edge->setInformation(A);
    edge->setSize(vertexs_size);
    edge->setId(edge_num_);
    edge_num_++;
    for (size_t i = state_has_vertex_num_; i < margvertices.size(); i++)
    {
        if(dynamic_cast<VertexVec *>(margvertices[i]))
        {
            edge->push_back(i - state_has_vertex_num_, (dynamic_cast<VertexVec *>(margvertices[i]))->estimate());
        }else if(dynamic_cast<VertexQ *>(margvertices[i]))
        {
            edge->push_back(i - state_has_vertex_num_, (dynamic_cast<VertexQ *>(margvertices[i]))->estimate());
        }else{
            std::cerr << "Vertex Error!!!" << std::endl;
            return edge;
        }
        edge->vertices()[i - state_has_vertex_num_] = margvertices[i];
    }
    // AddRobustKernel(edge, robust_kernel_name_, robust_kernel_delta_);
    graph_ptr_->addEdge(edge);
    return edge;
}

g2o::VertexSE3Expmap* GraphSlam::AddVertexSE3(const g2o::SE3Quat& m)
{
    g2o::VertexSE3Expmap* vertex(new g2o::VertexSE3Expmap());
    vertex->setId(static_cast<int>(graph_ptr_->vertices().size()) + marge_vertex_num_);
    vertex->setEstimate(m);
    graph_ptr_->addVertex(vertex);
    state_size_++;
    return vertex;
}

Vertex3D* GraphSlam::AddVertex3D(const Eigen::Vector3d& m)
{
    Vertex3D* vertex(new Vertex3D());
    vertex->setId(static_cast<int>(graph_ptr_->vertices().size()) + marge_vertex_num_);
    vertex->setEstimate(m);
    graph_ptr_->addVertex(vertex);
    return vertex;
}

VertexVec* GraphSlam::AddVertexVec(const Eigen::Vector3d& m)
{
    VertexVec* vertex(new VertexVec());
    vertex->setId(static_cast<int>(graph_ptr_->vertices().size()) + marge_vertex_num_);
    vertex->setEstimate(m);
    graph_ptr_->addVertex(vertex);
    return vertex;
}

VertexQ* GraphSlam::AddVertexQ(const Eigen::Quaterniond& m)
{
    state_size_++;
    VertexQ* vertex(new VertexQ());
    vertex->setId(static_cast<int>(graph_ptr_->vertices().size()) + marge_vertex_num_);
    vertex->setEstimate(m);
    graph_ptr_->addVertex(vertex);
    return vertex;
}

void GraphSlam::RemoveLastFrame()
{
    int end_index = graph_ptr_->vertices().size() + marge_vertex_num_;
    int start_index = end_index - state_has_vertex_num_;
    for (int i = start_index; i < end_index; i++)
    {
        g2o::OptimizableGraph::Vertex *v = graph_ptr_->vertex(i);
        // std::cout << v->id() << std::endl;
        graph_ptr_->removeVertex(v, false);
    }
}  

bool GraphSlam::CheckStateCovariance(const State& window_state)
{
    for (int j = 0; j < 6; ++j)
    {
        // LOG(INFO) << "state covariance = " <<  window_state.covariance(j, j) << std::endl;
        if (window_state.covariance(j, j) > max_std_deviation_)
        {
            LOG(INFO) << "state covariance is big = " << window_state.covariance(j, j);
            return false;
        }
    }
    return true;
}

bool GraphSlam::OptimizerInterface(State &result_state)
{
    if(!Optimizer())
    {
        return false;
    }
    std::deque<State> window_states;
    GetOptimizedState(window_states);
    if(CheckStateCovariance(window_states.back()))
    {
        Marginalize();
        marge_old_ = true;
    }else{
        RemoveLastFrame();
        window_states.pop_back();
        LOG(ERROR) << "RemoveLastFrame " << std::endl;
        state_size_--;
        marge_old_ = false;
    }
    // LOG(INFO) << "次新帧-------------";
    // window_states[window_states.size()-2].Print();
    LOG(INFO) << "最新帧-------------";
    bool covar_cheak = true;
    for (int i = (window_states.size() - 1); i >= 0; i--)
    {
        covar_cheak = true;
        if(!CheckStateCovariance(window_states[i]))
        {   
            covar_cheak = false;
        }
/*         for (int j = 0; j < 6; ++j)
        {
            std::cout << window_states[i].covariance(j, j) << std::endl;
            if (window_states[i].covariance(j, j) > max_std_deviation_)
            {
                LOG(INFO) << "state covariance is big = " << window_states[i].covariance(j, j);
                covar_cheak = false;
                break;
            }
        } */
        if (!covar_cheak && i == 0)
        {
            LOG(ERROR) << "window state did not have well covariance";
            return false;
        }
        if (!covar_cheak)
        {
            continue;
        }
        result_state = window_states[i];
        result_state.stamp = window_times_[i];
        break;
    }
    // result_state = window_states.back();
    result_state.Print();
    // result_state.stamp = last_time_;
    return true;
}
// (dynamic_cast<VertexVec *>(graph_ptr_->vertices()[start_index]))
bool GraphSlam::GetCovariance(const int index, State& state)
{
    std::vector<std::pair<int, int> > indices;
    std::vector<int> inds;
    int size = 0;
    std::vector<g2o::OptimizableGraph::Vertex* > vertices;
    for(int i = index; i < (index + state_has_vertex_num_); i++)
    {
        g2o::OptimizableGraph::Vertex *v = graph_ptr_->vertex(i);
        int hi = v->hessianIndex();
        if (hi != -1)
        {
            inds.push_back(v->hessianIndex());
            vertices.push_back(v);
            size += v->dimension();
        }
    }

    for (int i = 0; i < inds.size(); i++)
    {
        for (int j = 0; j < inds.size(); j++)
        {
            indices.push_back(std::make_pair(inds[i], inds[j]));
            if(j != i)
            {
                indices.push_back(std::make_pair(inds[j], inds[i]));
            }
        }
    }
    state.covariance = Eigen::MatrixXd::Zero(size, size);
    
    g2o::SparseBlockMatrix<g2o::MatrixX> cov;

    if (!graph_ptr_->computeMarginals(cov, indices))
    {
        LOG(ERROR) << "GraphOptimizerG2o computeMarginals False!";
        state.covariance = Eigen::MatrixXd::Identity(6, 6) * 0.5;
        return true;
    }

    if (vertices.size() == 0)
    {
        return false;
    }

    for (int i = 0; i < vertices.size(); ++i)
    {
        for (int j = 0; j < vertices.size(); ++j)
        {
            int row = vertices[i]->hessianIndex();
            int col = vertices[j]->hessianIndex();
            state.covariance.block(i * 3, j * 3, 3, 3) = *(cov.block(row, col));
        }
    }
    return true;
}

bool GraphSlam::Optimizer()
{
     if(graph_ptr_->edges().size() < 1)
    {
        return false;
    }
    // LOG(INFO) << "优化器含有边条ddddd数为： " << graph_ptr_->edges().size() 
    //           << ", 顶点个数为： " << graph_ptr_->vertices().size();

    graph_ptr_->initializeOptimization();
    graph_ptr_->computeInitialGuess();
    graph_ptr_->computeActiveErrors();
    graph_ptr_->setVerbose(false);
    graph_ptr_->optimize(100);

    std::vector<g2o::OptimizableGraph::Edge *> outlier_edges;

    for(auto edge : graph_ptr_->edges())
    {
        // std::cout << "edge id is: " <<dynamic_cast<g2o::OptimizableGraph::Edge *>(edge)->id() << ", chi2 is= " << dynamic_cast<g2o::OptimizableGraph::Edge *>(edge)->chi2() << std::endl;
         /* if (dynamic_cast<ImuEdge *>(edge))
        {
            std::cout << "imu edge id is: " <<dynamic_cast<g2o::OptimizableGraph::Edge *>(edge)->id() << ", chi2 is= " << dynamic_cast<g2o::OptimizableGraph::Edge *>(edge)->chi2() << std::endl;            
            LOG(INFO) << "imu edge " << (dynamic_cast<ImuEdge *>(edge))->error() << std::endl;
            // std::cout << "information matrix is: " << std::endl << (dynamic_cast<ImuEdge *>(edge))->information() << std::endl;
        }else if(dynamic_cast<MarginalizationEdge *>(edge))
        {
            std::cout << "MarginalizationEdge edge id is: " <<dynamic_cast<g2o::OptimizableGraph::Edge *>(edge)->id() << ", chi2 is= " << dynamic_cast<g2o::OptimizableGraph::Edge *>(edge)->chi2() << std::endl;
            LOG(INFO) << "marge edge " << (dynamic_cast<MarginalizationEdge *>(edge))->error() << std::endl;
            // std::cout << "information matrix is: " << std::endl << (dynamic_cast<MarginalizationEdge *>(edge))->information() << std::endl;

        }else if(dynamic_cast<VisUnitEdge *>(edge))
        {
            std::cout << "VisUnitEdge edge id is: " <<dynamic_cast<g2o::OptimizableGraph::Edge *>(edge)->id() << ", chi2 is= " << dynamic_cast<g2o::OptimizableGraph::Edge *>(edge)->chi2() << std::endl;
            LOG(INFO) << "vis edge " << (dynamic_cast<VisUnitEdge *>(edge))->error() << std::endl;
            // std::cout << "information matrix is: " << std::endl << (dynamic_cast<VisUnitEdge *>(edge))->information() << std::endl;
        }else if(dynamic_cast<EdgeImu *>(edge))        
        {
            std::cout << "EdgeImu edge id is: " <<dynamic_cast<g2o::OptimizableGraph::Edge *>(edge)->id() << ", chi2 is= " << dynamic_cast<g2o::OptimizableGraph::Edge *>(edge)->chi2() << std::endl;
            LOG(INFO) << "imu edge " << (dynamic_cast<EdgeImu *>(edge))->error() << std::endl;
            // std::cout << "information matrix is: " << std::endl << (dynamic_cast<EdgeImu *>(edge))->information() << std::endl;
        }else if (dynamic_cast<MeasureEdge *>(edge)){
            
            std::cout << "measure edge p input =" <<dynamic_cast<MeasureEdge *>(edge)->getMeasure().first << std::endl;
            // std::cout << "measure edge Q input =" <<dynamic_cast<MeasureEdge *>(edge)->getMeasure().second.coeffs() << std::endl;
            for (auto m_vertex : dynamic_cast<MeasureEdge *>(edge)->vertices())
            {
                std::cout << "after optimizer p state = " <<(dynamic_cast<VertexVec *>(graph_ptr_->vertices()[m_vertex->id()]))->estimate() << std::endl;
                // std::cout << "after optimizer Q state = " <<(dynamic_cast<VertexQ *>(graph_ptr_->vertices()[m_vertex->id()+1]))->estimate().coeffs() << std::endl;
                std::cout << "after optimizer v state = " <<(dynamic_cast<VertexVec *>(graph_ptr_->vertices()[m_vertex->id()+2]))->estimate() << std::endl;
                std::cout << "after optimizer ba state = " <<(dynamic_cast<VertexVec *>(graph_ptr_->vertices()[m_vertex->id()+3]))->estimate() << std::endl;
                std::cout << "after optimizer bg state = " <<(dynamic_cast<VertexVec *>(graph_ptr_->vertices()[m_vertex->id()+4]))->estimate() << std::endl;
                break;

            }
            std::cout << "MeasureEdge edge id is: " <<dynamic_cast<g2o::OptimizableGraph::Edge *>(edge)->id() << ", chi2 is= " << dynamic_cast<g2o::OptimizableGraph::Edge *>(edge)->chi2() << std::endl;
            LOG(INFO) << "measure edge " << (dynamic_cast<MeasureEdge *>(edge))->error() << std::endl;
            // std::cout << "information matrix is: " << std::endl << (dynamic_cast<MeasureEdge *>(edge))->information() << std::endl;
        }else if (dynamic_cast<OdomeEdge *>(edge))
        {
            std::cout << "OdomeEdge edge id is: " <<dynamic_cast<g2o::OptimizableGraph::Edge *>(edge)->id() << ", chi2 is= " << dynamic_cast<g2o::OptimizableGraph::Edge *>(edge)->chi2() << std::endl;
            LOG(INFO) << "imu edge " << (dynamic_cast<OdomeEdge *>(edge))->error() << std::endl;
        }else{
            
            std::cout << "edge id is: " <<dynamic_cast<g2o::OptimizableGraph::Edge *>(edge)->id() << ", chi2 is= " << dynamic_cast<g2o::OptimizableGraph::Edge *>(edge)->chi2() << std::endl;
        }
 */
        if (dynamic_cast<g2o::OptimizableGraph::Edge *>(edge)->chi2() > outlier_chi2_)
        {
            // graph_ptr_->removeEdge(dynamic_cast<g2o::OptimizableGraph::Edge *>(edge));
            outlier_edges.push_back(dynamic_cast<g2o::OptimizableGraph::Edge *>(edge));
            LOG(ERROR) << "edge chi2 is too big = " << dynamic_cast<g2o::OptimizableGraph::Edge *>(edge)->chi2();
        }else{
            dynamic_cast<g2o::OptimizableGraph::Edge *>(edge)->setRobustKernel(nullptr);
        }
    }
    if (!outlier_edges.empty())
    {
        for (auto& edge : outlier_edges)
        {
           graph_ptr_->removeEdge(edge);
           edge = nullptr;
        }
        LOG(INFO) << "outlier_edges size is " << outlier_edges.size() << std::endl;
    }
    LOG(INFO) <<"------------------------第二次优化--------------------";
    graph_ptr_->initializeOptimization();
    graph_ptr_->computeInitialGuess();
    graph_ptr_->computeActiveErrors();
    graph_ptr_->setVerbose(false);
    graph_ptr_->optimize(100);
    for(auto edge : graph_ptr_->edges())
    {
        // std::cout << "edge id is: " <<dynamic_cast<g2o::OptimizableGraph::Edge *>(edge)->id() << ", chi2 is= " << dynamic_cast<g2o::OptimizableGraph::Edge *>(edge)->chi2() << std::endl;
        /* if (dynamic_cast<ImuEdge *>(edge))
        {
            std::cout << "imu edge id is: " <<dynamic_cast<g2o::OptimizableGraph::Edge *>(edge)->id() << ", chi2 is= " << dynamic_cast<g2o::OptimizableGraph::Edge *>(edge)->chi2() << std::endl;            
            LOG(INFO) << "imu edge " << (dynamic_cast<ImuEdge *>(edge))->error() << std::endl;
            // std::cout << "information matrix is: " << std::endl << (dynamic_cast<ImuEdge *>(edge))->information() << std::endl;
        }else if(dynamic_cast<MarginalizationEdge *>(edge))
        {
            std::cout << "MarginalizationEdge edge id is: " <<dynamic_cast<g2o::OptimizableGraph::Edge *>(edge)->id() << ", chi2 is= " << dynamic_cast<g2o::OptimizableGraph::Edge *>(edge)->chi2() << std::endl;
            LOG(INFO) << "marge edge " << (dynamic_cast<MarginalizationEdge *>(edge))->error() << std::endl;
            // std::cout << "information matrix is: " << std::endl << (dynamic_cast<MarginalizationEdge *>(edge))->information() << std::endl;

        }else if(dynamic_cast<VisUnitEdge *>(edge))
        {
            std::cout << "VisUnitEdge edge id is: " <<dynamic_cast<g2o::OptimizableGraph::Edge *>(edge)->id() << ", chi2 is= " << dynamic_cast<g2o::OptimizableGraph::Edge *>(edge)->chi2() << std::endl;
            LOG(INFO) << "vis edge " << (dynamic_cast<VisUnitEdge *>(edge))->error() << std::endl;
            // std::cout << "information matrix is: " << std::endl << (dynamic_cast<VisUnitEdge *>(edge))->information() << std::endl;
        }else if(dynamic_cast<EdgeImu *>(edge))        
        {
            std::cout << "EdgeImu edge id is: " <<dynamic_cast<g2o::OptimizableGraph::Edge *>(edge)->id() << ", chi2 is= " << dynamic_cast<g2o::OptimizableGraph::Edge *>(edge)->chi2() << std::endl;
            LOG(INFO) << "imu edge " << (dynamic_cast<EdgeImu *>(edge))->error() << std::endl;
            // std::cout << "information matrix is: " << std::endl << (dynamic_cast<EdgeImu *>(edge))->information() << std::endl;
        }else if (dynamic_cast<MeasureEdge *>(edge)){
            
            std::cout << "measure edge p input =" <<dynamic_cast<MeasureEdge *>(edge)->getMeasure().first << std::endl;
            // std::cout << "measure edge Q input =" <<dynamic_cast<MeasureEdge *>(edge)->getMeasure().second.coeffs() << std::endl;
            for (auto m_vertex : dynamic_cast<MeasureEdge *>(edge)->vertices())
            {
                std::cout << "after optimizer p state = " <<(dynamic_cast<VertexVec *>(graph_ptr_->vertices()[m_vertex->id()]))->estimate() << std::endl;
                // std::cout << "after optimizer Q state = " <<(dynamic_cast<VertexQ *>(graph_ptr_->vertices()[m_vertex->id()+1]))->estimate().coeffs() << std::endl;
                std::cout << "after optimizer v state = " <<(dynamic_cast<VertexVec *>(graph_ptr_->vertices()[m_vertex->id()+2]))->estimate() << std::endl;
                std::cout << "after optimizer ba state = " <<(dynamic_cast<VertexVec *>(graph_ptr_->vertices()[m_vertex->id()+3]))->estimate() << std::endl;
                std::cout << "after optimizer bg state = " <<(dynamic_cast<VertexVec *>(graph_ptr_->vertices()[m_vertex->id()+4]))->estimate() << std::endl;
                break;

            }
            std::cout << "MeasureEdge edge id is: " <<dynamic_cast<g2o::OptimizableGraph::Edge *>(edge)->id() << ", chi2 is= " << dynamic_cast<g2o::OptimizableGraph::Edge *>(edge)->chi2() << std::endl;
            LOG(INFO) << "measure edge " << (dynamic_cast<MeasureEdge *>(edge))->error() << std::endl;
            // std::cout << "information matrix is: " << std::endl << (dynamic_cast<MeasureEdge *>(edge))->information() << std::endl;
        }else if (dynamic_cast<OdomeEdge *>(edge))
        {
            std::cout << "EdgeImu edge id is: " <<dynamic_cast<g2o::OptimizableGraph::Edge *>(edge)->id() << ", chi2 is= " << dynamic_cast<g2o::OptimizableGraph::Edge *>(edge)->chi2() << std::endl;
            LOG(INFO) << "imu edge " << (dynamic_cast<OdomeEdge *>(edge))->error() << std::endl;
        }else{
            
            std::cout << "edge id is: " <<dynamic_cast<g2o::OptimizableGraph::Edge *>(edge)->id() << ", chi2 is= " << dynamic_cast<g2o::OptimizableGraph::Edge *>(edge)->chi2() << std::endl;
        } */

/*         if (dynamic_cast<g2o::OptimizableGraph::Edge *>(edge)->chi2() > outlier_chi2_)
        {
            // graph_ptr_->removeEdge(dynamic_cast<g2o::OptimizableGraph::Edge *>(edge));
            outlier_edges.push_back(dynamic_cast<g2o::OptimizableGraph::Edge *>(edge));
            LOG(ERROR) << "edge chi2 is too big = " << dynamic_cast<g2o::OptimizableGraph::Edge *>(edge)->chi2();
        }else{
            dynamic_cast<g2o::OptimizableGraph::Edge *>(edge)->setRobustKernel(nullptr);
        } */
    }
    return true;
}

void GraphSlam::AddRobustKernel(g2o::OptimizableGraph::Edge *edge, const std::string &kernel_type, double kernel_delta)
{
    if (kernel_type == "NONE")
        return;
    g2o::RobustKernel* kernel = robust_kernel_factory_->construct(kernel_type);
    if (kernel == nullptr)
    {
        std::cerr << "warning : invalid robust kernel type: " << kernel_type << std::endl;
        return;
    }
    kernel->setDelta(kernel_delta);
    edge->setRobustKernel(kernel);
}

bool GraphSlam::GetOptimizedState(std::deque<State>& optimized_state)
{
    if(state_size_ < window_size_)
    {
        LOG(ERROR) << "Initialization of sliding window is not completed ";
        return false;
    }
    State state;
    for(size_t i = (state_size_ - window_size_); i < state_size_; i++)
    {
        int start_index = i * state_has_vertex_num_;
        
        state.p = (dynamic_cast<VertexVec *>(graph_ptr_->vertices()[start_index]))->estimate();
        state.q = (dynamic_cast<VertexQ *>(graph_ptr_->vertices()[start_index + 1]))->estimate();
        state.v = (dynamic_cast<VertexVec *>(graph_ptr_->vertex(start_index + 2)))->estimate();
        state.ba = (dynamic_cast<VertexVec *>(graph_ptr_->vertex(start_index + 3)))->estimate();
        state.bg = (dynamic_cast<VertexVec *>(graph_ptr_->vertex(start_index + 4)))->estimate();

        if(GetCovariance(start_index, state))
        {
            // std::cout << "GraphOptimizerG2o::GetCovariance " << state.covariance(0, 0) << std::endl;
            // std::cout << "GraphOptimizerG2o::GetCovariance " << std::endl << state.covariance << std::endl;
        }else{
            LOG(ERROR) << "get covariance wrong!";
        }
        optimized_state.push_back(state);
    }
    return true;
}

void GraphSlam::Reset()
{
    graph_ptr_->clear();
    edge_num_ = 0;
    state_size_ = 0;
    marge_vertex_num_ = 0;
    window_times_.clear();

}

bool GraphSlam::Marginalize()
{
    if(state_size_ < window_size_)
    {
        LOG(ERROR) << "state size is not enough, size is " << state_size_ << " .";
        return false;
    }
    std::vector<g2o::HyperGraph::Vertex*> marge_vertexs;
    std::vector<g2o::HyperGraph::Edge*> marge_edges;
    
    //优化中顶点个数，是所有添加进来的顶点 - marge掉的顶点
    int opm_vertex_num = graph_ptr_->vertices().size() + marge_vertex_num_;
    //滑窗起始顶点id
    int marge_start_id = opm_vertex_num - state_has_vertex_num_ * window_size_;
    // std::cout << "marge_start_id = " << marge_start_id << std::endl;
    for(size_t i = marge_start_id; i < (marge_start_id + state_has_vertex_num_); i++)
    {
        g2o::HyperGraph::Vertex* v = graph_ptr_->vertex(i);
        marge_vertexs.push_back(v);
        for (auto &it : v->edges())
        {
            auto ei = std::find_if(marge_edges.begin(), marge_edges.end(), [it](const std::vector<g2o::HyperGraph::Edge *>::value_type &e) { return e->id() == it->id(); });
            if (ei == marge_edges.end())
            {
                marge_edges.push_back(it);
            }
        }
    }
    //找到所有跟要边缘化的顶点相关的顶点
    for (auto &eit : marge_edges)
    {
        for (auto vit : eit->vertices())
        {
            // std::cout << "vertex id= " << vit->id() << std::endl;
            auto vi = std::find_if(marge_vertexs.begin(), marge_vertexs.end(), [vit](const std::vector<g2o::HyperGraph::Vertex *>::value_type &v) { return v->id() == vit->id(); });
            if (vi == marge_vertexs.end())
            {
                marge_vertexs.push_back(vit);
            }
        }
    }

    //要边缘化的维度
    int m = 15;
    //边缘化顶点所有关联的维度
    int pos = (marge_vertexs.size() / state_has_vertex_num_) * 15;
    //保留的维度
    int n = pos - m;
    if(n > 0)
    {
        Eigen::MatrixXd A = Eigen::MatrixXd::Zero(pos, pos);
        Eigen::VectorXd b = Eigen::VectorXd::Zero(pos);
        for(auto it = marge_edges.begin(); it != marge_edges.end(); it++)
        {
            std::vector<g2o::MatrixX::MapType, Eigen::aligned_allocator<g2o::MatrixX::MapType>> jacs;
            Eigen::MatrixXd info;
            Eigen::VectorXd error;
            if(dynamic_cast<ImuEdge*>(*it))
            {
                auto edge = (dynamic_cast<ImuEdge*>(*it));
                edge->computeError();
                edge->linearizeOplus();
                jacs = edge->GetJacobian();
                info = edge->information();
                error = edge->error();
                // LOG(INFO) << "ImuEdge";
            }else if(dynamic_cast<VisUnitEdge*>(*it)){
                auto edge = (dynamic_cast<VisUnitEdge*>(*it));
                edge->computeError();
                edge->linearizeOplus();
                jacs = edge->GetJacobian();
                info = edge->information();
                error = edge->error();
                // LOG(INFO) << "VisUnitEdge";
            }else if((dynamic_cast<MarginalizationEdge*>(*it))){
                auto edge = (dynamic_cast<MarginalizationEdge*>(*it));
                edge->computeError();
                edge->linearizeOplus();
                jacs = edge->GetJacobian();
                info = edge->information();
                error = edge->error();
                // LOG(INFO) << "MarginalizationEdge";
            }else if((dynamic_cast<EdgeImu*>(*it))){
                auto edge = (dynamic_cast<EdgeImu*>(*it));
                edge->computeError();
                edge->linearizeOplus();
                jacs = edge->GetJacobian();
                info = edge->information();
                error = edge->error();
                // LOG(INFO) << "EdgeImu";
            }else if((dynamic_cast<MeasureEdge*>(*it)))
            {
                auto edge = (dynamic_cast<MeasureEdge*>(*it));
                edge->computeError();
                edge->linearizeOplus();
                jacs = edge->GetJacobian();
                info = edge->information();
                error = edge->error();
                // LOG(INFO) << "MeasureEdge";
            }else if((dynamic_cast<EdgeMarginalization*>(*it))){
                auto edge = (dynamic_cast<EdgeMarginalization*>(*it));
                edge->computeError();
                edge->linearizeOplus();
                jacs = edge->GetJacobian();
                info = edge->information();
                error = edge->error();
                // LOG(INFO) << "EdgeMarginalization";
            }else if ((dynamic_cast<OdomeEdge*>(*it))){
                auto edge = (dynamic_cast<OdomeEdge*>(*it));
                edge->computeError();
                edge->linearizeOplus();
                jacs = edge->GetJacobian();
                info = edge->information();
                error = edge->error();
                // LOG(INFO) << "OdomeEdge";
            }else if ((dynamic_cast<TwistEdge*>(*it))){
                auto edge = (dynamic_cast<TwistEdge*>(*it));
                edge->computeError();
                edge->linearizeOplus();
                jacs = edge->GetJacobian();
                info = edge->information();
                error = edge->error();
                // LOG(INFO) << "TwistEdge";
            }else{
                LOG(ERROR) << "g2o 边类型错误 ！！！！";
                return false;
            }
            std::vector<g2o::HyperGraph::Vertex* > vertices((*it)->vertices());
            for (size_t i = 0; i < vertices.size(); ++i)
            {
                int vertex_id = vertices[i]->id();
                auto vit = std::find_if(marge_vertexs.begin(), marge_vertexs.end(), [vertex_id](const std::vector<g2o::HyperGraph::Vertex *>::value_type &v) { return v->id() == vertex_id; });
                if (vit == marge_vertexs.end())
                {
                    std::cerr << "vit error!!!" << std::endl;
                    return false;
                }
                // index_i and index_j 表示顶点在边缘化矩阵中的顶点位置
                int index_i = std::distance(marge_vertexs.begin(), vit);
                int rows, cols, vertex_i_dimension, vertex_j_dimension;
                for(size_t j = i; j < vertices.size(); j++)
                {
                    vertex_id = vertices[j]->id();
                    vit = std::find_if(marge_vertexs.begin(), marge_vertexs.end(), [vertex_id](const std::vector<g2o::HyperGraph::Vertex *>::value_type &v) { return v->id() == vertex_id; });
                    if (vit == marge_vertexs.end())
                    {
                        std::cerr << "vit error!!!" << std::endl;
                        return false;
                    }
                    int index_j = std::distance(marge_vertexs.begin(), vit);
                    if (index_j == marge_vertexs.size())
                    {   
                        std::cout << "index = marge_vertexs.size()" << std::endl;
                    }   
                    if (i == j)
                    {
                        A.block(index_i * 3, index_j * 3, 3, 3) += jacs[i].transpose() * info * jacs[j];
                    }
                    else
                    {
                        A.block(index_i * 3, index_j * 3, 3, 3) += jacs[i].transpose() * info * jacs[j];
                        A.block(index_j * 3, index_i * 3, 3, 3) = A.block(index_i * 3, index_j * 3, 3, 3).transpose();
                    }
                }
                b.segment(index_i * 3, 3) += jacs[i].transpose() * info * error;
            }
        }
        //A 是一个对称矩阵, transpose 之后理应不变
        Eigen::MatrixXd Amm = 0.5 * (A.block(0,0,m,m) + A.block(0,0,m,m).transpose()); 
        Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> saes(Amm);
        double eps = 1e-8;
        Eigen::MatrixXd Amm_inv = saes.eigenvectors() * Eigen::VectorXd((saes.eigenvalues().array() > eps).select(saes.eigenvalues().array().inverse(), 0)).asDiagonal() * saes.eigenvectors().transpose();

        Eigen::VectorXd bmm = b.segment(0,m);
        Eigen::MatrixXd Amr = A.block(0,m,m,n);
        Eigen::MatrixXd Arm = A.block(m,0,n,m);
        Eigen::MatrixXd Arr = A.block(m,m,n,n);
        Eigen::VectorXd brr = b.segment(m,n);
        A = Arr - Arm * Amm_inv * Amr;
        b = brr - Arm * Amm_inv * bmm;

        Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> sae2(A);
        Eigen::MatrixXd A_inv = sae2.eigenvectors() * Eigen::VectorXd((sae2.eigenvalues().array() > eps).select(sae2.eigenvalues().array().inverse(), 0)).asDiagonal() * sae2.eigenvectors().transpose();
        Eigen::VectorXd minus_mu = A_inv * b;

        AddMarEdge(minus_mu, A, marge_vertexs, n / 15 * state_has_vertex_num_);
    }
    //去除掉滑窗内最老的一帧，
    for (size_t i = 0; i < state_has_vertex_num_; i++)
    {
        graph_ptr_->removeVertex(marge_vertexs[i], false);
        marge_vertex_num_++;
    }
}


/* bool GraphSlam::Marginalize()
{
    if(state_size_ < window_size_)
    {
        LOG(ERROR) << "state size is not enough, size is " << state_size_ << " .";
        return false;
    }
    std::vector<g2o::HyperGraph::Vertex*> marge_vertexs;
    std::vector<g2o::HyperGraph::Edge*> marge_edges;
    
    //优化中顶点个数，是所有添加进来的顶点 - marge掉的顶点
    int opm_vertex_num = graph_ptr_->vertices().size() + marge_vertex_num_;
    //滑窗起始顶点id
    int marge_start_id = opm_vertex_num - state_has_vertex_num_ * window_size_;
    for(size_t i = marge_start_id; i < (marge_start_id + state_has_vertex_num_); i++)
    {
        g2o::HyperGraph::Vertex* v = graph_ptr_->vertex(i);
        marge_vertexs.push_back(v);
        for (auto &it : v->edges())
        {
            auto ei = std::find_if(marge_edges.begin(), marge_edges.end(), [it](const std::vector<g2o::HyperGraph::Edge *>::value_type &e) { return e->id() == it->id(); });
            if (ei == marge_edges.end())
            {
                marge_edges.push_back(it);
            }
        }
    }
    //找到所有跟要边缘化的顶点相关的顶点
    for (auto &eit : marge_edges)
    {
        for (auto vit : eit->vertices())
        {
            auto vi = std::find_if(marge_vertexs.begin(), marge_vertexs.end(), [vit](const std::vector<g2o::HyperGraph::Vertex *>::value_type &v) { return v->id() == vit->id(); });
            if (vi == marge_vertexs.end())
            {
                marge_vertexs.push_back(vit);
            }
        }
    }
    //要边缘化的维度
    int m = 15;
    //边缘化顶点所有关联的维度
    int pos = (marge_vertexs.size() / state_has_vertex_num_) * 15;
    //保留的维度
    int n = pos - m;
    if(n > 0)
    {
        Eigen::MatrixXd A = Eigen::MatrixXd::Zero(pos, pos);
        Eigen::VectorXd b = Eigen::VectorXd::Zero(pos);
        for(auto it = marge_edges.begin(); it != marge_edges.end(); it++)
        {
            std::vector<g2o::MatrixX::MapType, Eigen::aligned_allocator<g2o::MatrixX::MapType>> jacs;
            Eigen::MatrixXd info;
            Eigen::VectorXd error;
            if(dynamic_cast<ImuEdge*>(*it))
            {
                auto edge = (dynamic_cast<ImuEdge*>(*it));
                edge->computeError();
                edge->linearizeOplus();
                jacs = edge->GetJacobian();
                info = edge->information();
                error = edge->error();
                // LOG(INFO) << "ImuEdge";
            }else if(dynamic_cast<VisUnitEdge*>(*it)){
                auto edge = (dynamic_cast<VisUnitEdge*>(*it));
                edge->computeError();
                edge->linearizeOplus();
                jacs = edge->GetJacobian();
                info = edge->information();
                error = edge->error();
                // LOG(INFO) << "VisUnitEdge";
            }else if((dynamic_cast<MarginalizationEdge*>(*it))){
                auto edge = (dynamic_cast<MarginalizationEdge*>(*it));
                edge->computeError();
                edge->linearizeOplus();
                jacs = edge->GetJacobian();
                info = edge->information();
                error = edge->error();
                // LOG(INFO) << "MarginalizationEdge";
            }else{
                LOG(ERROR) << "g2o 边类型错误 ！！！！";
                return false;
            }
            std::vector<g2o::HyperGraph::Vertex* > vertices((*it)->vertices());
            for (size_t i = 0; i < vertices.size(); ++i)
            {
                int vertex_id = vertices[i]->id();
                auto vit = std::find_if(marge_vertexs.begin(), marge_vertexs.end(), [vertex_id](const std::vector<g2o::HyperGraph::Vertex *>::value_type &v) { return v->id() == vertex_id; });
                if (vit == marge_vertexs.end())
                {
                    std::cerr << "vit error!!!" << std::endl;
                    return false;
                }
                // index_i and index_j 表示顶点在边缘化矩阵中的顶点位置
                int index_i = std::distance(marge_vertexs.begin(), vit);
                int rows, cols, vertex_i_dimension, vertex_j_dimension;
                for(size_t j = i; j < vertices.size(); j++)
                {
                    vertex_id = vertices[j]->id();
                    vit = std::find_if(marge_vertexs.begin(), marge_vertexs.end(), [vertex_id](const std::vector<g2o::HyperGraph::Vertex *>::value_type &v) { return v->id() == vertex_id; });
                    if (vit == marge_vertexs.end())
                    {
                        std::cerr << "vit error!!!" << std::endl;
                        return false;
                    }
                    int index_j = std::distance(marge_vertexs.begin(), vit);
                    
                    JacPosition(index_i, index_j, rows, cols, vertex_i_dimension, vertex_j_dimension);
                    if(i == j)
                    {
                        A.block(rows, cols, vertex_i_dimension, vertex_j_dimension) += jacs[i].transpose() * info * jacs[j];
                    }else{
                        //TODO: dimension
                        A.block(rows, cols, vertex_i_dimension, vertex_j_dimension) += jacs[i].transpose() * info * jacs[j];
                        A.block(cols, rows, vertex_j_dimension, vertex_i_dimension) = A.block(rows, cols, vertex_i_dimension, vertex_j_dimension).transpose();
                    }

                }
                JacPositionI(index_i, rows, vertex_i_dimension);
                b.segment(rows, vertex_i_dimension) += jacs[i].transpose() * info * error;
            }
        }
        //A 是一个对称矩阵, transpose 之后理应不变
        Eigen::MatrixXd Amm = 0.5 * (A.block(0,0,m,m) + A.block(0,0,m,m).transpose()); 
        Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> saes(Amm);
        double eps = 1e-8;
        Eigen::MatrixXd Amm_inv = saes.eigenvectors() * Eigen::VectorXd((saes.eigenvalues().array() > eps).select(saes.eigenvalues().array().inverse(), 0)).asDiagonal() * saes.eigenvectors().transpose();

        Eigen::VectorXd bmm = b.segment(0,m);
        Eigen::MatrixXd Amr = A.block(0,m,m,n);
        Eigen::MatrixXd Arm = A.block(m,0,n,m);
        Eigen::MatrixXd Arr = A.block(m,m,n,n);
        Eigen::VectorXd brr = b.segment(m,n);
        A = Arr - Arm * Amm_inv * Amr;
        b = brr - Arm * Amm_inv * bmm;

        Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> sae2(A);
        Eigen::MatrixXd A_inv = sae2.eigenvectors() * Eigen::VectorXd((sae2.eigenvalues().array() > eps).select(sae2.eigenvalues().array().inverse(), 0)).asDiagonal() * sae2.eigenvectors().transpose();
        Eigen::VectorXd minus_mu = A_inv * b;

        AddMarEdge(minus_mu, A, marge_vertexs, n/15*4);
    }
} */


}


