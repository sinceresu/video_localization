#include "fusion_optimizer/optimizer/g2o/marginalization_edge.hpp"

namespace fusion_optimizer
{
MarginalizationEdge::MarginalizationEdge()
{
    Ts_.clear();
    Ms_.clear();
}
void MarginalizationEdge::setDimension(int d)
{
    _information.resize(d, d);
    _error.resize(d, 1);
    _measurement.resize(d, 1);
    _dimension = d;
}

void MarginalizationEdge::setSize(int vertices)
{
    int d = vertices / 4 * 15;
    setDimension(d);
    resize(vertices);
}

void MarginalizationEdge::computeError()
{
    Eigen::VectorXd dx = Eigen::VectorXd::Zero(_error.rows());
    int rows;
    int vertex_dimension;

    for(auto it = Ts_.begin(); it != Ts_.end(); it++)
    {
        int index = it->first;
        JacPositionI(index, rows, vertex_dimension);
        // dx.segment(rows, 6) = ((*it).second).inverse() * (dynamic_cast<g2o::SE3Quat *>(_vertices[index])->estimate())).log();
        dx.segment(rows, 3) = (dynamic_cast<g2o::VertexSE3Expmap *>(_vertices[index])->estimate()).translation() - ((*it).second).translation();
        dx.segment(rows+3, 3) = (((*it).second).rotation().inverse() * (dynamic_cast<g2o::VertexSE3Expmap *>(_vertices[index])->estimate()).rotation()).vec();
    }
    for(auto it = Ms_.begin(); it != Ms_.end(); it++)
    {
        int index = it->first;
        JacPositionI(index, rows, vertex_dimension);
        dx.segment(rows, 3) = (dynamic_cast<Vertex3D *>(_vertices[index])->estimate()) - (*it).second;
    }
    _error = dx + _measurement;
}

void MarginalizationEdge::linearizeOplus()
{   
    int rows, vertex_dimension;
    for(size_t i = 0; i < _vertices.size(); i++)
    {
        // int index = (_vertices[i])->id();
        // JacPositionI(index, rows, vertex_dimension);
        JacPositionI(i, rows, vertex_dimension);
        _jacobianOplus[i] = Eigen::MatrixXd::Zero(_dimension, vertex_dimension);
        _jacobianOplus[i].block(rows, 0, vertex_dimension, vertex_dimension) = Eigen::MatrixXd::Identity(vertex_dimension, vertex_dimension);
    }

}

void MarginalizationEdge::push_back(const int index, const g2o::SE3Quat& T)
{
    Ts_[index] = T;
}

void MarginalizationEdge::push_back(const int index, const Eigen::Vector3d& M)
{
    Ms_[index] = M;
}

void MarginalizationEdge::setMeasurement(const Eigen::VectorXd &m)
{
    _measurement = m;
}

}