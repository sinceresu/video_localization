#include "fusion_optimizer/optimizer/g2o/imu_edge.hpp"


namespace fusion_optimizer
{
ImuEdge::ImuEdge()
{
    resize(8);
}
//目前顶点为imu到map的定位
/* void ImuEdge::computeError()
{
    g2o::SE3Quat T1 = (dynamic_cast<g2o::VertexSE3Expmap *>(_vertices[0]))->estimate();
    Eigen::Vector3d vec1 = (dynamic_cast<Vertex3D *>(_vertices[1]))->estimate();
    Eigen::Vector3d ba1 = (dynamic_cast<Vertex3D *>(_vertices[2]))->estimate();
    Eigen::Vector3d bg1 = (dynamic_cast<Vertex3D *>(_vertices[3]))->estimate();

    g2o::SE3Quat T2 = (dynamic_cast<g2o::VertexSE3Expmap *>(_vertices[4]))->estimate();
    Eigen::Vector3d vec2 = (dynamic_cast<Vertex3D *>(_vertices[5]))->estimate();
    Eigen::Vector3d ba2 = (dynamic_cast<Vertex3D *>(_vertices[6]))->estimate();
    Eigen::Vector3d bg2 = (dynamic_cast<Vertex3D *>(_vertices[7]))->estimate();
    
    LOG(INFO) << "vec1 = " << vec1(0) << ", "<< vec1(1) << ", "<< vec1(2);
    LOG(INFO) << "vec2 = " << vec2(0) << ", "<< vec2(1) << ", "<< vec2(2);
    //TODO
    _information = _measurement->information_;
    if (_measurement->RePropagate(ba1, bg1))
    {
        _information = _measurement->information_;
    }
    _error = _measurement->ComputerError(T1.translation(), T1.rotation(), vec1, ba1, bg1, 
                                         T2.translation(), T2.rotation(), vec2, ba2, bg2);
}

void ImuEdge::linearizeOplus()
{
    g2o::SE3Quat T1 = (dynamic_cast<g2o::VertexSE3Expmap *>(_vertices[0]))->estimate();
    Eigen::Vector3d vec1 = (dynamic_cast<Vertex3D *>(_vertices[1]))->estimate();
    Eigen::Vector3d ba1 = (dynamic_cast<Vertex3D *>(_vertices[2]))->estimate();
    Eigen::Vector3d bg1 = (dynamic_cast<Vertex3D *>(_vertices[3]))->estimate();

    g2o::SE3Quat T2 = (dynamic_cast<g2o::VertexSE3Expmap *>(_vertices[4]))->estimate();
    Eigen::Vector3d vec2 = (dynamic_cast<Vertex3D *>(_vertices[5]))->estimate();
    Eigen::Vector3d ba2 = (dynamic_cast<Vertex3D *>(_vertices[6]))->estimate();
    Eigen::Vector3d bg2 = (dynamic_cast<Vertex3D *>(_vertices[7]))->estimate();

    Eigen::Matrix<double, 15, 30> jacs = _measurement->GetJacobian(T1.translation(), T1.rotation(), vec1, ba1, bg1, 
                                                                   T2.translation(), T2.rotation(), vec2, ba2, bg2);
    int rows, vertex_dimension;
    for (size_t i = 0; i < _vertices.size(); i++)
    {
        JacPositionI(i, rows, vertex_dimension);
        const int vd = vertex_dimension;
        _jacobianOplus[i] = jacs.block(0, rows, 15, vd);
        // _jacobianOplus[i] = jacs.block(15, vd, 0, rows);
    }
} */


void ImuEdge::computeError()
{
    g2o::VertexSE3Expmap* T1 = dynamic_cast<g2o::VertexSE3Expmap *>(_vertices[0]);
    Vertex3D *Vi = dynamic_cast<Vertex3D *>(_vertices[1]);
    Vertex3D *Bai = dynamic_cast<Vertex3D *>(_vertices[2]);
    Vertex3D *Bgi = dynamic_cast<Vertex3D *>(_vertices[3]);

    g2o::VertexSE3Expmap* T2 = dynamic_cast<g2o::VertexSE3Expmap *>(_vertices[4]);
    Vertex3D *Vj = dynamic_cast<Vertex3D *>(_vertices[5]);
    Vertex3D *Baj = dynamic_cast<Vertex3D *>(_vertices[6]);
    Vertex3D *Bgj = dynamic_cast<Vertex3D *>(_vertices[7]);
    _information = _measurement->information_;

    // _error = _measurement->ComputerError(T1->estimate().translation(), T1->estimate().rotation(), Vi->estimate(), Bai->estimate(), Bgi->estimate(),
    //                                 T2->estimate().translation(), T2->estimate().rotation(), Vj->estimate(), Baj->estimate(), Bgj->estimate());

    _error = _measurement->evaluate(T1->estimate().translation(), T1->estimate().rotation(), Vi->estimate(), Bai->estimate(), Bgi->estimate(),
                                    T2->estimate().translation(), T2->estimate().rotation(), Vj->estimate(), Baj->estimate(), Bgj->estimate());
 }
void ImuEdge::linearizeOplus()
{
    g2o::VertexSE3Expmap* T1 = dynamic_cast<g2o::VertexSE3Expmap *>(_vertices[0]);
    Vertex3D *Vi = dynamic_cast<Vertex3D *>(_vertices[1]);
    Vertex3D *Bai = dynamic_cast<Vertex3D *>(_vertices[2]);
    Vertex3D *Bgi = dynamic_cast<Vertex3D *>(_vertices[3]);

    g2o::VertexSE3Expmap* T2 = dynamic_cast<g2o::VertexSE3Expmap *>(_vertices[4]);
    Vertex3D *Vj = dynamic_cast<Vertex3D *>(_vertices[5]);
    Vertex3D *Baj = dynamic_cast<Vertex3D *>(_vertices[6]);
    Vertex3D *Bgj = dynamic_cast<Vertex3D *>(_vertices[7]);

    Eigen::Matrix<double, 15, 30> jacs = _measurement->jacobian(T1->estimate().translation(), T1->estimate().rotation(), Vi->estimate(), Bai->estimate(), Bgi->estimate(),
                                                              T2->estimate().translation(), T2->estimate().rotation(), Vj->estimate(), Baj->estimate(), Bgj->estimate());
    
    int rows, vertex_dimension;
    for (unsigned int i = 0; i < _vertices.size(); i++)
    {
        JacPositionI(i, rows, vertex_dimension);
        const int vd = vertex_dimension;
        _jacobianOplus[i] = jacs.block(0, rows, 15, vd);
    }
}

void ImuEdge::setMeasurement(const std::shared_ptr<PreIntergration>& m)
{
    _measurement = m;
    _information = _measurement->jacobians_;
}
}