# fusion_optimizer (融合定位)
融合视觉反定位和imu等数据

## 安装编译环境
第三方库中的g2o.tar.gz

解压缩后执行：

````shell
mkdir build
cd build
cmake ..
make -j3
sudo make install
````

##  订阅话题
```
/yd_camera_pose [nav_msgs::Odometry] #纯视觉定位camera的定位
/camera/imu [sensor_msgs::ImuConstPtr] #Imu数据
/orb_slam3/pose [nav_msgs::Odometry] #Orbslam3 定位数据
```


## 发布话题
```
/camera_pose [nav_msgs::Odometry] #融合后camera的定位
/camera_imu_path [nav_msgs::Path] #航迹推算相机的定位轨迹
/camera_imu_pose [nav_msgs::Odometry] #航迹推算相机的定位
```

## 配置参数

|名称|类型|描述|缺省
|--------------------- |-----------|----------------------|------------------|
measurement_p_n|数组|相机定位position误差(xyz)|
measurement_q_n|数组|相机定位姿态相机定位position误差(rpy)||
odom_measurement_p_n|数组|里程计position误差(xyz)|
odom_measurement_p_n|数组|里程计姿态相机定位position误差(rpy)|
imu_2_measure_sensor_R|数组|Imu到相机的外参旋转|
imu_2_measure_sensor_t|数组|Imu到相机的外参平移|


## 启动
```
roslaunch fusion_optimizer fusion_optimizer.launch
```