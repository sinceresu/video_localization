cmake_minimum_required(VERSION 3.0.2)
project(fusion_optimizer)
# set(CMAKE_CXX_STANDARD 14)
# add_compile_options(-std=c++14)
# add_definitions(-std=c++14)

SET(CMAKE_BUILD_TYPE "Release")
# SET(CMAKE_BUILD_TYPE "Release")



find_package(catkin REQUIRED COMPONENTS
  geometry_msgs
  message_generation
  nav_msgs
  rosbag
  roscpp
  rospy
  sensor_msgs
  std_msgs
  std_msgs
  tf
  tf_conversions
  eigen_conversions
  # eigen_conversions
)

set(ALL_TARGET_LIBRARIES "")

include(cmake/glog.cmake)
include(cmake/YAML.cmake)
include(cmake/PCL.cmake)
include(cmake/eigen.cmake)
# include(cmake/geographic.cmake)
include(cmake/g2o.cmake)
include(cmake/opencv.cmake)
# include(cmake/liblas.cmake)
# include(cmake/global_defination.cmake)
include(cmake/boost.cmake)
# include(cmake/csparse.cmake)
include(cmake/Sophus.cmake)

catkin_package(
)

include_directories(
  include
  ${catkin_INCLUDE_DIRS}
)

file(GLOB_RECURSE ALL_SRCS "src/*.cpp")
file(GLOB_RECURSE NODE_SRCS "src/app/*_node.cpp")
file(GLOB_RECURSE THIRD_PARTY_SRCS "third_party/*.cpp")


list(REMOVE_ITEM ALL_SRCS ${NODE_SRCS})
list(REMOVE_ITEM ALL_SRCS ${THIRD_PARTY_SRCS})


add_executable(eskf_localization_node src/app/eskf_node.cpp ${ALL_SRCS} src/utitilies/data_types.cpp)
target_link_libraries(eskf_localization_node  ${ALL_TARGET_LIBRARIES} ${catkin_LIBRARIES})

add_executable(graph_localization_node src/app/graph_localization_node.cpp ${ALL_SRCS} src/utitilies/data_conversions.cpp)
target_link_libraries(graph_localization_node  ${ALL_TARGET_LIBRARIES} ${catkin_LIBRARIES} )
