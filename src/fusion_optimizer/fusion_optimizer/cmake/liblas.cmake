find_package(libLAS REQUIRED)
include_directories(
    ${LIBLAS_INCLUDE_DIR}
)
list(APPEND ALL_TARGET_LIBRARIES ${LIBLAS_LIBRARIES})