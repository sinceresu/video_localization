find_package(CSparse REQUIRED)
include_directories(${CSPARSE_INCLUDE_DIR})
list(APPEND ALL_TARGET_LIBRARIES  ${CSPARSE_LIBRARY})