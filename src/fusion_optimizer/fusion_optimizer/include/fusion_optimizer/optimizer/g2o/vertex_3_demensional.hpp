#ifndef FUSION_OPTIMIZER_VERTEX_3_DEMENSIONAL_HPP_
#define FUSION_OPTIMIZER_VERTEX_3_DEMENSIONAL_HPP_

#include <Eigen/Core>
#include <g2o/core/base_vertex.h>
#include <g2o/types/sba/types_six_dof_expmap.h>
#include <g2o/core/base_vertex.h>

namespace fusion_optimizer{
class Vertex3D : public g2o::BaseVertex<3, Eigen::Vector3d>
{
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
    Vertex3D() : BaseVertex<3, Eigen::Vector3d>() { setToOriginImpl(); };

    virtual bool read(std::istream &is) { return false; }

    virtual bool write(std::ostream &os) const { return false; }

    virtual void setToOriginImpl() { _estimate.fill(0.); }

    virtual void oplusImpl(const double* update_)
    {
        //Eigen::Map 把数组指针转换为eigen 矩阵或者向量
        Eigen::Map<const Eigen::Vector3d> update(update_);
        _estimate += update;
    }
};
}





#endif