#ifndef FUSION_OPTIMIZER_EDGE_TWIST_
#define FUSION_OPTIMIZER_EDGE_TWIST_

#include <g2o/core/base_multi_edge.h>
#include  "fusion_optimizer/optimizer/g2o/vertex_q.hpp"
#include  "fusion_optimizer/optimizer/g2o/vertex_vec.hpp"
#include "fusion_optimizer/imu_process/pre_twist_integration.hpp"

namespace fusion_optimizer{
class TwistEdge : public g2o::BaseMultiEdge<6, std::shared_ptr<fusion_optimizer::PreTwistIntergration> >
{ 
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW

        TwistEdge(){
            resize(4);
        }

        void computeError()
        {
            Eigen::Vector3d p1 = (dynamic_cast<VertexVec* >(_vertices[0]))->estimate();
            Eigen::Quaterniond q1 = (dynamic_cast<VertexQ* >(_vertices[1]))->estimate();
            Eigen::Vector3d p2 = (dynamic_cast<VertexVec* >(_vertices[2]))->estimate();
            Eigen::Quaterniond q2 = (dynamic_cast<VertexQ* >(_vertices[3]))->estimate();

            _error = _measurement->evaluate(p1, q1, p2, q2);
        }

        void linearizeOplus()
        {
            Eigen::Vector3d p1 = (dynamic_cast<VertexVec* >(_vertices[0]))->estimate();
            Eigen::Quaterniond q1 = (dynamic_cast<VertexQ* >(_vertices[1]))->estimate();
            Eigen::Vector3d p2 = (dynamic_cast<VertexVec* >(_vertices[2]))->estimate();
            Eigen::Quaterniond q2 = (dynamic_cast<VertexQ* >(_vertices[3]))->estimate();


            Eigen::Matrix<double, 6, 12> jacs = _measurement->jacobian(p1, q1, p2, q2);
            for (unsigned int i = 0; i < 4; i++)
            {
                _jacobianOplus[i] = jacs.block<6, 3>(0, 3 * i);
            }
        }

        virtual void setMeasurement(const std::shared_ptr<fusion_optimizer::PreTwistIntergration> &m)
        {
            _measurement = m;
        }

        std::vector<g2o::MatrixX::MapType, Eigen::aligned_allocator<g2o::MatrixX::MapType>> GetJacobian() const
        {
            return _jacobianOplus;
        }

        virtual bool read(std::istream &is) { return false; }
        virtual bool write(std::ostream &os) const { return false; }
};
}
#endif