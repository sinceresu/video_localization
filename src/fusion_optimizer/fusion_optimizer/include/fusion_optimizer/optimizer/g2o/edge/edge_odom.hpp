#ifndef FUSION_OPTIMIZER_EDGE_ODOM_
#define FUSION_OPTIMIZER_EDGE_ODOM_

#include <Eigen/Core>
#include "fusion_optimizer/optimizer/g2o/vertex_vec.hpp"
#include "fusion_optimizer/optimizer/g2o/vertex_q.hpp"
#include "fusion_optimizer/utitilies/data_conversions.hpp"


namespace fusion_optimizer
{
    struct OdomData
    {
        OdomData();
        OdomData(Eigen::Vector3d pi, Eigen::Quaterniond Qi, Eigen::Vector3d pj, Eigen::Quaterniond Qj)
       :p1(pi), Q1(Qi), p2(pj), Q2(Qj)
       {
            Eigen::Isometry3d pre_pose, cur_pose, relate_pose;
            pre_pose.setIdentity();
            pre_pose.translation() = p1;
            pre_pose.linear() = Q1.matrix();

            cur_pose.setIdentity();
            cur_pose.translation() = p2;
            cur_pose.linear() = Q2.matrix();

            relate_pose = pre_pose.inverse() * cur_pose;
            
            alpha = relate_pose.translation();
            Eigen::Matrix3d R_relate = relate_pose.linear();
            gamma = Eigen::Quaterniond(R_relate);
       }
       
       Eigen::Vector3d p1;
       Eigen::Quaterniond Q1;
       Eigen::Vector3d p2;
       Eigen::Quaterniond Q2;
       Eigen::Vector3d alpha;
       Eigen::Quaterniond gamma;
    };

    class OdomeEdge : public g2o::BaseMultiEdge<6, std::shared_ptr<OdomData> >
    {
        public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        OdomeEdge()
        {
            resize(4);
        }
        void computeError()
        {
            Eigen::Vector3d p1 = (dynamic_cast<VertexVec* >(_vertices[0]))->estimate();
            Eigen::Quaterniond q1 = (dynamic_cast<VertexQ* >(_vertices[1]))->estimate();
            Eigen::Vector3d p2 = (dynamic_cast<VertexVec* >(_vertices[2]))->estimate();
            Eigen::Quaterniond q2 = (dynamic_cast<VertexQ* >(_vertices[3]))->estimate();

            _error.head(3) = q1.inverse() * (p2 - p1) - _measurement->alpha;
            _error.tail(3) = 2 * (_measurement->gamma.inverse() * (q1.inverse() * q2)).vec();

        }

        void linearizeOplus()
        {
            Eigen::Vector3d p1 = (dynamic_cast<VertexVec* >(_vertices[0]))->estimate();
            Eigen::Quaterniond q1 = (dynamic_cast<VertexQ* >(_vertices[1]))->estimate();
            Eigen::Vector3d p2 = (dynamic_cast<VertexVec* >(_vertices[2]))->estimate();
            Eigen::Quaterniond q2 = (dynamic_cast<VertexQ* >(_vertices[3]))->estimate();


            Eigen::Matrix<double, 6, 12> jacobians;
            jacobians.setZero();
            jacobians.block<3,3>(0,0) = -q1.inverse().matrix();
            jacobians.block<3,3>(0,3) = skewSymmetric(q1.inverse() * (p2 - p1));
            jacobians.block<3,3>(0,6) = q1.inverse().toRotationMatrix();

            jacobians.block<3,3>(3,3) = -(Qleft(q2.inverse() * q1) * Qright(_measurement->gamma)).bottomRightCorner<3, 3>(); 
            jacobians.block<3,3>(3,9) = (Qleft(_measurement->gamma.inverse() * q1.inverse() * q2)).bottomRightCorner<3, 3>();

            for (unsigned int i = 0; i < 4; i++)
            {
                _jacobianOplus[i] = jacobians.block<6, 3>(0, 3 * i);
            }
        }

        virtual bool read(std::istream &is) { return false; }
        virtual bool write(std::ostream &os) const { return false; }

        virtual void setMeasurement(const std::shared_ptr<OdomData> &m)
        {
            _measurement = m;
        }

        std::vector<g2o::MatrixX::MapType, Eigen::aligned_allocator<g2o::MatrixX::MapType>> GetJacobian() const
        {
            return _jacobianOplus;
        }




    };
}























#endif