#ifndef FUSION_OPTIMIZER_IMU_EDGE_HPP_
#define FUSION_OPTIMIZER_IMU_EDGE_HPP_
#include <g2o/core/base_binary_edge.h>
#include <g2o/core/base_multi_edge.h>
#include <g2o/types/slam3d/se3quat.h>
#include <g2o/types/sba/types_six_dof_expmap.h>

#include "fusion_optimizer/imu_process/pre_integration.hpp"
#include "fusion_optimizer/optimizer/g2o/vertex_3_demensional.hpp"
#include "fusion_optimizer/utitilies/data_conversions.hpp"

namespace fusion_optimizer{

class ImuEdge : public g2o::BaseMultiEdge<15, std::shared_ptr<PreIntergration>>
{
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    ImuEdge();
    void computeError();
    virtual void linearizeOplus();
    virtual void setMeasurement(const std::shared_ptr<PreIntergration>& m);
    std::vector<g2o::MatrixX::MapType, Eigen::aligned_allocator<g2o::MatrixX::MapType>> GetJacobian() const
    {
        return _jacobianOplus;
    }
    virtual bool read(std::istream &is) { return false; }
    virtual bool write(std::ostream &os) const { return false; }
};
}










#endif