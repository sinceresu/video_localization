#ifndef FUSION_OPTIMIZER_EDGE_MARGE_
#define FUSION_OPTIMIZER_EDGE_MARGE_

#include <g2o/core/base_multi_edge.h>
#include "vertex_vec.hpp"
#include "vertex_q.hpp"

namespace fusion_optimizer
{
    class EdgeMarginalization : public g2o::BaseMultiEdge<-1, Eigen::VectorXd>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    EdgeMarginalization()
    {
        vecs_.clear();
        qs_.clear();
    }
    void setDimension(int d)
    {
        _dimension = d;
        _information.resize(d, d);
        _error.resize(d, 1);
        _measurement.resize(d, 1);
    }

    void setSize(int vertices)
    {
        resize(vertices);
        setDimension(vertices * 3);
    }

    void computeError()
    {
        Eigen::VectorXd dx = Eigen::VectorXd::Zero(_error.rows());
        for (auto it = vecs_.begin(); it != vecs_.end(); it++)
        {
            int index = (*it).first;
            dx.segment(index * 3, 3) = (dynamic_cast<VertexVec *>(_vertices[index]))->estimate() - (*it).second;
        }
        for (auto it = qs_.begin(); it != qs_.end(); it++)
        {
            int index = (*it).first;
            dx.segment(index * 3, 3) = 2 * ((*it).second.inverse() * (dynamic_cast<VertexQ *>(_vertices[index]))->estimate()).vec();
        }
        _error = dx + _measurement;
    }

    void setMeasurement(const Eigen::VectorXd &m)
    {
        _measurement = m;
    }

    void linearizeOplus()
    {
        for (unsigned int i = 0; i < _vertices.size(); i++)
        {
            _jacobianOplus[i] = Eigen::MatrixXd::Zero(_dimension, 3);
            _jacobianOplus[i].block(3 * i, 0, 3, 3) = Eigen::Matrix3d::Identity();
        }
    }

    void push_back(int index, const Eigen::Vector3d &v)
    {
        vecs_[index] = v;
    }
    
    void push_back(int index, const Eigen::Quaterniond &q)
    {
        qs_[index] = q;
    }
        // EdgeMarginalization();
        // virtual void setDimension(int dimension_);
        // virtual void setSize(int vertices);
        virtual bool read(std::istream &is) { return false; }
        virtual bool write(std::ostream &os) const { return false; }
        // void computeError();
        // virtual void linearizeOplus();
        // virtual void setMeasurement(const Eigen::VectorXd &m);
        std::vector<g2o::MatrixX::MapType, Eigen::aligned_allocator<g2o::MatrixX::MapType>> GetJacobian() const
        {
            return _jacobianOplus;
        }
        // void push_back(int index, const Eigen::Vector3d &v);
        // void push_back(int index, const Eigen::Quaterniond &q);
        std::unordered_map<int, Eigen::Vector3d> vecs_;
        std::unordered_map<int, Eigen::Quaterniond> qs_;
    };
} 

#endif