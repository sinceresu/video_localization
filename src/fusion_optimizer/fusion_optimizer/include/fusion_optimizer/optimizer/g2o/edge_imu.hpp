#pragma once

// #include "g2o/core/base_multi_edge.h"
#include <g2o/core/base_multi_edge.h>
#include "vertex_vec.hpp"
#include "vertex_q.hpp"
#include "fusion_optimizer/imu_process/pre_integration.hpp"
// #include "fusion_optimizer/models/graph_optimizer/preintegration/preintegration.hpp"
using namespace g2o;

namespace fusion_optimizer
{   

    class EdgeImu : public BaseMultiEdge<15, std::shared_ptr<fusion_optimizer::PreIntergration>>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        EdgeImu() : BaseMultiEdge<15, std::shared_ptr<fusion_optimizer::PreIntergration>>()
    {
        resize(10);
        // information().setIdentity();
        // setMeasurement(nullptr);
    }
    void computeError()
    {
        VertexVec *Pi = dynamic_cast<VertexVec *>(_vertices[0]);
        VertexQ *Qi = dynamic_cast<VertexQ *>(_vertices[1]);
        VertexVec *Vi = dynamic_cast<VertexVec *>(_vertices[2]);
        VertexVec *Bai = dynamic_cast<VertexVec *>(_vertices[3]);
        VertexVec *Bgi = dynamic_cast<VertexVec *>(_vertices[4]);
        VertexVec *Pj = dynamic_cast<VertexVec *>(_vertices[5]);
        VertexQ *Qj = dynamic_cast<VertexQ *>(_vertices[6]);
        VertexVec *Vj = dynamic_cast<VertexVec *>(_vertices[7]);
        VertexVec *Baj = dynamic_cast<VertexVec *>(_vertices[8]);
        VertexVec *Bgj = dynamic_cast<VertexVec *>(_vertices[9]);
        // if ((Bai->estimate() - _measurement->linearized_ba_).norm() > 0.1 ||(Bgi->estimate() - _measurement->linearized_bg_).norm() > 0.01)
        // {
        //     _measurement->repropagate(Bai->estimate(), Bgi->estimate());
        //     _information = _measurement->covariance_.inverse();
        // }
        _error = _measurement->evaluate(Pi->estimate(), Qi->estimate(), Vi->estimate(), Bai->estimate(), Bgi->estimate(), Pj->estimate(), Qj->estimate(), Vj->estimate(), Baj->estimate(), Bgj->estimate());
    }
    void linearizeOplus()
    {
        VertexVec *Pi = dynamic_cast<VertexVec *>(_vertices[0]);
        VertexQ *Qi = dynamic_cast<VertexQ *>(_vertices[1]);
        VertexVec *Vi = dynamic_cast<VertexVec *>(_vertices[2]);
        VertexVec *Bai = dynamic_cast<VertexVec *>(_vertices[3]);
        VertexVec *Bgi = dynamic_cast<VertexVec *>(_vertices[4]);
        VertexVec *Pj = dynamic_cast<VertexVec *>(_vertices[5]);
        VertexQ *Qj = dynamic_cast<VertexQ *>(_vertices[6]);
        VertexVec *Vj = dynamic_cast<VertexVec *>(_vertices[7]);
        VertexVec *Baj = dynamic_cast<VertexVec *>(_vertices[8]);
        VertexVec *Bgj = dynamic_cast<VertexVec *>(_vertices[9]);
        // if ((Bai->estimate() - _measurement->linearized_ba_).norm() > 0.1 ||(Bgi->estimate() - _measurement->linearized_bg_).norm() > 0.01)
        // {
        //     _measurement->repropagate(Bai->estimate(), Bgi->estimate());
        //     _information = _measurement->covariance_.inverse();
        // }
        Eigen::Matrix<double, 15, 30> jacs = _measurement->jacobian(Pi->estimate(), Qi->estimate(), Vi->estimate(), Bai->estimate(), Bgi->estimate(), Pj->estimate(), Qj->estimate(), Vj->estimate(), Baj->estimate(), Bgj->estimate());
        for (unsigned int i = 0; i < _vertices.size(); i++)
        {
            _jacobianOplus[i] = jacs.block<15, 3>(0, 3 * i);
        }
    }
    void setMeasurement(const std::shared_ptr<fusion_optimizer::PreIntergration> &m)
    {
        _measurement = m;
        _information = m->information_;
        // Eigen::SelfAdjointEigenSolver<Eigen::Matrix<double, 15, 15>> saes(_measurement->covariance_);
        // double eps = 1e-8;
        // _information = saes.eigenvectors() * Eigen::VectorXd((saes.eigenvalues().array() > eps).select(saes.eigenvalues().array().inverse(), 0)).asDiagonal() * saes.eigenvectors().transpose();
    }
        // EdgeImu();
        virtual bool read(std::istream &is) { return false; }
        virtual bool write(std::ostream &os) const { return false; }
        // void computeError();
        // virtual void linearizeOplus();
        // virtual void setMeasurement(const std::shared_ptr<fusion_optimizer::PreIntergration> &m);
        std::vector<g2o::MatrixX::MapType, Eigen::aligned_allocator<g2o::MatrixX::MapType>> GetJacobian() const
        {
            return _jacobianOplus;
        }
    };
} // namespace g2o