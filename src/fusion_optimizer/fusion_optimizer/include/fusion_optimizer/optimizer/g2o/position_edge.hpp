#ifndef FUSION_OPTIMIZER_POSITION_EDGE_
#define FUSION_OPTIMIZER_POSITION_EDGE_

#include <Eigen/Core>
#include "fusion_optimizer/utitilies/data_conversions.hpp"
#include "fusion_optimizer/optimizer/g2o/vertex_vec.hpp"
#include "fusion_optimizer/optimizer/g2o/vertex_q.hpp"

namespace fusion_optimizer
{
    class PositionEdge : public g2o::BaseMultiEdge<3, Eigen::Vector3d>
    {
        public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        PositionEdge()
        {
            resize(1);
        }
        void computeError()
        {
            Eigen::Vector3d p = (dynamic_cast<VertexVec* >(_vertices[0]))->estimate();
            _error = p - _measurement;
           }
        void linearizeOplus()
        {
            _jacobianOplus[0] = Eigen::Matrix3d::Identity();
        }
        virtual bool read(std::istream &is) { return false; }
        virtual bool write(std::ostream &os) const { return false; }
        virtual void setMeasurement(const Eigen::Vector3d &m)
        {
            _measurement = m;
        }
        std::vector<g2o::MatrixX::MapType, Eigen::aligned_allocator<g2o::MatrixX::MapType>> GetJacobian() const
        {
            return _jacobianOplus;
        }


    };
}





















#endif