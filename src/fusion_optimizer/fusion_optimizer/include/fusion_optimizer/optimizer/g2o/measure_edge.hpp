#ifndef FUSION_OPTIMIZER_MEASURE_EDGE_
#define FUSION_OPTIMIZER_MEASURE_EDGE_

#include <Eigen/Core>
#include "fusion_optimizer/utitilies/data_conversions.hpp"
#include "fusion_optimizer/optimizer/g2o/vertex_vec.hpp"
#include "fusion_optimizer/optimizer/g2o/vertex_q.hpp"

namespace fusion_optimizer
{
    class MeasureEdge : public g2o::BaseMultiEdge<6, std::pair<Eigen::Vector3d, Eigen::Quaterniond>>
    {
        public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        MeasureEdge()
        {
            resize(2);
        }
        void computeError()
        {
            Eigen::Vector3d p = (dynamic_cast<VertexVec* >(_vertices[0]))->estimate();
            Eigen::Quaterniond q = (dynamic_cast<VertexQ* >(_vertices[1]))->estimate();
            _error.head(3) = p - _measurement.first;
            _error.tail(3) = 2 * (_measurement.second.inverse() * q).vec();
           }
        void linearizeOplus()
        {
            Eigen::MatrixXd J = Eigen::MatrixXd(6, 6);
            J.setZero();

            Eigen::Quaterniond Q = (dynamic_cast<VertexQ *>(_vertices[1]))->estimate();
            
            J.block<3, 3>(0, 0) = Eigen::Matrix3d::Identity();
            J.block<3, 3>(3, 3) = Qleft(_measurement.second.inverse() * Q).bottomRightCorner<3, 3>();
            // J.block<3, 3>(3, 3) = (_measurement.second.inverse() * Q).matrix();
            for (unsigned int i = 0; i < _vertices.size(); i++)
            {
                _jacobianOplus[i] = J.block<6, 3>(0, 3 * i);
            }
        }
        virtual bool read(std::istream &is) { return false; }
        virtual bool write(std::ostream &os) const { return false; }
        virtual void setMeasurement(const std::pair<Eigen::Vector3d, Eigen::Quaterniond> &m)
        {
            _measurement = m;
        }
        std::vector<g2o::MatrixX::MapType, Eigen::aligned_allocator<g2o::MatrixX::MapType>> GetJacobian() const
        {
            return _jacobianOplus;
        }

        std::pair<Eigen::Vector3d, Eigen::Quaterniond> getMeasure()
        {
            return _measurement;
        }



    };
}





















#endif