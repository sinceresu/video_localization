#ifndef FUSION_OPTIMIZER_VIS_UNIT_EDGE_HPP_
#define FUSION_OPTIMIZER_VIS_UNIT_EDGE_HPP_

#include <g2o/core/base_unary_edge.h>
#include <g2o/types/sba/types_six_dof_expmap.h>
#include <g2o/types/slam3d/se3quat.h>
// #include <g2o/core/base_vertex.h>
#include <g2o/core/base_multi_edge.h>

namespace fusion_optimizer{


class VisUnitEdge : public g2o::BaseMultiEdge<6, g2o::SE3Quat>
{
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
    VisUnitEdge()
    { 
        resize(1); 
    }
    void computeError()
    {
        const g2o::VertexSE3Expmap* v1 = static_cast<const g2o::VertexSE3Expmap*>(_vertices[0]);
        const g2o::SE3Quat est1 = v1->estimate();
        _error = (est1 * _measurement.inverse()).log();
    }
    void linearizeOplus()
    {
        _jacobianOplus[0] = Eigen::MatrixXd::Identity(6, 6);
    }
    virtual void setMeasurement(const g2o::SE3Quat &m)
    {
        _measurement = m;
    }

    

    std::vector<g2o::MatrixX::MapType, Eigen::aligned_allocator<g2o::MatrixX::MapType>> GetJacobian() const
    {
        return _jacobianOplus;
    }
    virtual bool read(std::istream &is) {return false;}
    virtual bool write(std::ostream &os) const {return false;}
};
}





#endif