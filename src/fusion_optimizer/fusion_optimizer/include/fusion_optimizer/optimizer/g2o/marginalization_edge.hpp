#ifndef FUSION_OPTIMIZER_MARGINALIZATION_EDGE_
#define FUSION_OPTIMIZER_MARGINALIZATION_EDGE_

#include <g2o/types/slam3d/se3quat.h>
// #include <g2o/core/base_multi_edge.h>

#include <g2o/core/optimizable_graph.h>
#include "g2o/core/base_multi_edge.h"
#include <g2o/types/sba/types_six_dof_expmap.h>
#include <unordered_map>
#include "fusion_optimizer/utitilies/data_conversions.hpp"
#include "fusion_optimizer/optimizer/g2o/vertex_3_demensional.hpp"
namespace fusion_optimizer
{
class MarginalizationEdge : public g2o::BaseMultiEdge<-1, Eigen::VectorXd>
{
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    MarginalizationEdge();
    // {
    //     Ts_.clear();
    //     Ms_.clear();
    // }
    virtual void setDimension(int d);
    virtual void setSize(int vertices);
    void computeError();
    virtual void linearizeOplus();
    virtual bool read(std::istream &is) { return false; }
    virtual bool write(std::ostream &os) const { return false; }
    void push_back(const int index, const g2o::SE3Quat& T);
    void push_back(const int index, const Eigen::Vector3d& M);
    virtual void setMeasurement(const Eigen::VectorXd &m);
    std::vector<g2o::MatrixX::MapType, Eigen::aligned_allocator<g2o::MatrixX::MapType>> GetJacobian() const
    {
        return _jacobianOplus;
    }

    std::unordered_map<int, g2o::SE3Quat> Ts_;
    std::unordered_map<int, Eigen::Vector3d> Ms_;

};



}

















#endif