#ifndef FUSION_OPTIMIZER_GRAPH_SLAM_HPP_
#define FUSION_OPTIMIZER_GRAPH_SLAM_HPP_

#include "fusion_optimizer/optimizer/g2o/marginalization_edge.hpp"
#include "fusion_optimizer/optimizer/g2o/imu_edge.hpp"
#include "fusion_optimizer/optimizer/g2o/vertex_3_demensional.hpp"
#include "fusion_optimizer/optimizer/g2o/vis_unit_edge.hpp"
#include "fusion_optimizer/params/params.hpp"
#include "fusion_optimizer/optimizer/g2o/vertex_q.hpp"
#include "fusion_optimizer/optimizer/g2o/vertex_vec.hpp"
#include "fusion_optimizer/optimizer/g2o/edge_imu.hpp"
#include "fusion_optimizer/optimizer/g2o/measure_edge.hpp"
#include "fusion_optimizer/optimizer/g2o/edge_marginalization.hpp"
#include "fusion_optimizer/optimizer/g2o/position_edge.hpp"
#include "fusion_optimizer/optimizer/g2o/edge/edge_odom.hpp"
#include "fusion_optimizer/optimizer/g2o/edge/edge_twist.hpp"

#include <g2o/core/sparse_optimizer.h>
// #include <g2o/core/base_vertex.hpp>
#include <g2o/stuff/macros.h>
#include <g2o/core/factory.h>
#include <g2o/core/block_solver.h>
#include <g2o/core/linear_solver.h>
#include <g2o/core/sparse_optimizer.h>
#include <g2o/core/robust_kernel_factory.h>
#include <g2o/core/optimization_algorithm_factory.h>
#include <g2o/solvers/pcg/linear_solver_pcg.h>
#include <g2o/solvers/eigen/linear_solver_eigen.h>
// #include <g2o/solvers/cholmod/linear_solver_cholmod.h>
#include <g2o/core/optimization_algorithm_levenberg.h>
#include <g2o/core/optimization_algorithm_factory.h>
#include <g2o/core/sparse_optimizer.h>
#include <g2o/types/slam3d/se3quat.h>
#include <g2o/types/sba/types_six_dof_expmap.h>
#include <glog/logging.h>

G2O_USE_OPTIMIZATION_LIBRARY(pcg)
G2O_USE_OPTIMIZATION_LIBRARY(cholmod)
G2O_USE_OPTIMIZATION_LIBRARY(csparse)

using namespace g2o;

namespace fusion_optimizer{
class GraphSlam : public Params
{
public:
    GraphSlam();
    MeasureEdge* AddMeasureEdge(std::pair<Eigen::Vector3d, Eigen::Quaterniond>& m, Eigen::MatrixXd& information);
    ImuEdge* AddImuEdge(const std::shared_ptr<PreIntergration> pre_intergration_ptr_,
                        const double& pre_time,
                        const double& last_time);
    void AddTwistEdge(const std::shared_ptr<PreTwistIntergration> pre_twist_intergration_ptr_);
    OdomeEdge* AddOdomEdge(const std::shared_ptr<OdomData> m, const Eigen::MatrixXd& information_matrix);
    VisUnitEdge* AddVisEdge(g2o::SE3Quat& m, g2o::VertexSE3Expmap& v1, const Eigen::MatrixXd& information_matrix);

    void AddPositionEdge(Eigen::Vector3d& m, Eigen::Matrix3d& information);
/*     MarginalizationEdge* AddMarEdge(const Eigen::VectorXd& minus_mu, 
                                    const Eigen::MatrixXd& A,
                                    const std::vector<g2o::HyperGraph::Vertex *>& margvertices,
                                    const int vertexs_size); */
    EdgeMarginalization* AddMarEdge(const Eigen::VectorXd& minus_mu, 
                                const Eigen::MatrixXd& A,
                                const std::vector<g2o::HyperGraph::Vertex *>& margvertices,
                                const int vertexs_size);

    g2o::VertexSE3Expmap* AddVertexSE3(const g2o::SE3Quat& m);
    Vertex3D* AddVertex3D(const Eigen::Vector3d& m);
    bool OptimizerInterface(State &result_state);



    VertexVec* AddVertexVec(const Eigen::Vector3d& m);
    VertexQ* AddVertexQ(const Eigen::Quaterniond& m);

    bool Optimizer();
    void AddRobustKernel(g2o::OptimizableGraph::Edge *edge, const std::string &kernel_type, double kernel_delta);
    bool GetOptimizedState(std::deque<State>& optimized_state);
    bool Marginalize();
    bool GetCovariance(const int index, State& state);
    void Reset();
    bool CheckStateCovariance(const State& window_state);
    void RemoveLastFrame();

private:
    int window_size_;
    //优化器中状态量的个数，每个状态量包含4个顶点
    int state_size_;
    int state_has_vertex_num_;
    int marge_vertex_num_;
    std::unique_ptr<g2o::SparseOptimizer> graph_ptr_;

    g2o::RobustKernelFactory *robust_kernel_factory_;
    std::string robust_kernel_name_;
    double robust_kernel_delta_;

    double last_time_;
    double pre_time_;
    int edge_num_;
    //优化中最新帧数的边误差较大的情况下，不使用其定位进行行距推算。
    bool last_pose_valid_;
    std::deque<double> window_times_;
};
}





#endif