#ifndef FUSION_OPTIMIZER_POSE_PUBLISHER_
#define FUSION_OPTIMIZER_POSE_PUBLISHER_

#include "publisher.hpp"
namespace fusion_optimizer{
    template <class pub_type, class sensor_type>
    class PosePublisher : public Publisher<pub_type, sensor_type>
    {
    public:
        PosePublisher(ros::NodeHandle &nh, const std::string topic_name, int buff_size)
        : Publisher<pub_type, sensor_type>(nh, topic_name, buff_size)
        {

        }

        virtual void Publish(const sensor_type& pub_data)
        {
            this->publisher_.publish(pub_data);
        }
    };

























}
#endif