#ifndef FUSION_OPTIMIZER_PATH_PUBLISHER_
#define FUSION_OPTIMIZER_PATH_PUBLISHER_

#include <ros/ros.h>
#include <nav_msgs/Path.h>

#include "publisher.hpp"


namespace fusion_optimizer
{
    template <class pub_type, class sensor_type>
    class PathPublisher : public Publisher<pub_type, sensor_type>
    {
    public:
        PathPublisher(ros::NodeHandle &nh, const std::string topic_name, int buff_size)
        : Publisher<pub_type, sensor_type>(nh, topic_name, buff_size)
        {

        }

        virtual void Publish(const sensor_type& pub_data)
        {
            this->publisher_.publish(pub_data);
        }
    };
}

#endif