#ifndef FUSION_OPTIMIZER_PUBLISHER_
#define FUSION_OPTIMIZER_PUBLISHER_


#include <ros/ros.h>

namespace fusion_optimizer
{

template <class pub_type, class sensor_type>
class Publisher
{
    public:
    Publisher(ros::NodeHandle &nh, const std::string topic_name, int buff_size)
    :nh_(nh), topic_name_(topic_name), buff_size_(buff_size)
    {
        publisher_ = nh.advertise<pub_type>(topic_name, buff_size);
    }


    virtual void Publish(const sensor_type& pub_data) = 0;
    
    ros::Publisher publisher_;
    std::string topic_name_;
    int buff_size_;
    ros::NodeHandle nh_;
};























}



















#endif