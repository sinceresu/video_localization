/*
 * @Description: IMU DATA TYPE
 * @Author: Zhou Mingchao
 * @Date: 2022-01-10 
 */
#ifndef FUSION_OPTIMIZER_SENSOR_DATA_TWIST_DATA_HPP_
#define FUSION_OPTIMIZER_SENSOR_DATA_TWIST_DATA_HPP_

#include <deque>
#include <cmath>
#include <Eigen/Dense>
#include "sophus/sim3.hpp"

namespace fusion_optimizer {
class TwistData {
  public:
    TwistData();
    TwistData(const TwistData &data);
    ~TwistData() {}
    TwistData &operator=(const TwistData &rhs);

    Eigen::Vector3d vel;
    Eigen::Vector3d gyro;

    // Sophus::SE3d sensor_to_robot;
    Eigen::VectorXd noise;
    // std::deque<double> stamps;

    double time = 0.0;
  
  public:
    // 把四元数转换成旋转矩阵送出去
    Eigen::Matrix3f GetOrientationMatrix();
    static bool SyncData(std::deque<TwistData>& UnsyncedData, std::deque<TwistData>& SyncedData, double sync_time);
};
}
#endif