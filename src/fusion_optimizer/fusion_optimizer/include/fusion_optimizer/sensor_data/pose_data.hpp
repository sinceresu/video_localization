/*
 * @Description: POSE DATA TYPE
 * @Author: Zhou Mingchao
 * @Date: 2022-01-10 
 */
#ifndef FUSION_OPTIMIZER_SENSOR_DATA_POSE_DATA_HPP_
#define FUSION_OPTIMIZER_SENSOR_DATA_POSE_DATA_HPP_

#include <Eigen/Dense>
#include <deque>

namespace fusion_optimizer {
class MPoseData {
  public:
    Eigen::Matrix4f pose = Eigen::Matrix4f::Identity();
    double time = 0.0;

  public:
    Eigen::Quaternionf GetQuaternion();
};

class OPoseData
{
  public:
  
  static bool SyncData(std::deque<OPoseData>& UnsyncedData, std::deque<OPoseData>& SyncedData, double sync_time);
  double stamp;
  Eigen::Quaterniond orientation;
  Eigen::Vector3d position;

};


}

#endif