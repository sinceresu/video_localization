/*
 * @Description: 订阅twist数据
 * @Author: Zhou Mingchao
 * @Date: 2022-04-11 
 */
#ifndef FUSION_OPTIMIZER_SUBSCRIBER_TWIST_SUBSCRIBER_HPP_
#define FUSION_OPTIMIZER_SUBSCRIBER_TWIST_SUBSCRIBER_HPP_

#include <deque>
#include <mutex>
#include <thread>

#include <ros/ros.h>
#include <geometry_msgs/TwistStamped.h>

#include "fusion_optimizer/sensor_data/twist_data.hpp"
#include "subscriber.hpp"

namespace fusion_optimizer {
class TwistSubscriber : public Subscriber {
  public:
    TwistSubscriber() = default;
    TwistSubscriber(ros::NodeHandle& nh, std::string topic_name, size_t buff_size);
    void ParseData(std::deque<TwistData>& deque_twist_data);

    template <typename datatype>
    bool ValidData(const double start_time, const double end_time, std::deque<datatype>& deque_twist_data)
    {
      if(Subscriber::ValidData<datatype>(start_time, end_time, new_twist_data_, deque_twist_data)){
        return true;
      }
      return false;
    }

  private:
    void msg_callback(const geometry_msgs::TwistStampedConstPtr& twist_msg_ptr);

  private:
    ros::NodeHandle nh_;
    ros::Subscriber subscriber_;
    std::deque<TwistData> new_twist_data_;

    std::mutex buff_mutex_; 
};
}
#endif