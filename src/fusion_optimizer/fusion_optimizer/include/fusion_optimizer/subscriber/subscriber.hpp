#ifndef FUSION_OPTIMIZER_SUBSCRIBER_HPP_
#define FUSION_OPTIMIZER_SUBSCRIBER_HPP_

#include <deque>
#include <mutex>

namespace fusion_optimizer
{
class Subscriber{
    public:
        Subscriber() = default;
        virtual void ClearData(){};
    protected:
        int start_index_;
        int end_index_;
        std::mutex buff_mutex_;
        
        template<typename datatype>
        bool ValidData(const double start_time, const double end_time, std::deque<datatype> &data_buffer, std::deque<datatype> &out_data_buffer)
        {
            assert(end_time > start_time);
            // if (data_buffer.empty() || data_buffer.back().time < start_time || start_time > data_buffer.front().time)
            if (data_buffer.empty())
            {
                return false;
            }
            if (data_buffer.back().time < start_time - 1e-5){
                return false;
            }
            if (start_time < data_buffer.front().time + 1e-5){
                return false;
            }
            if (end_time > data_buffer.back().time + 1e-5)
            {
                return false;
            }
            buff_mutex_.lock();
            start_index_ = 1;
            bool found = false;
            while(start_time > data_buffer.at(start_index_).time + 1e-5 && data_buffer.size() > (start_index_+1))
            {
                start_index_++;
            }
            if (data_buffer.size() > start_index_)
            {
                found = true;
            }
            start_index_--;
            if (found)
            {
                if(data_buffer.size() > 1 && data_buffer.at(start_index_).time < start_time - 1e-5 && data_buffer.back().time > end_time + 1e-5)
                {
                    end_index_ = data_buffer.size() - 1;
                    for (int i = start_index_; i < data_buffer.size(); i++)
                    {
                        if ((i+1) < data_buffer.size())
                        {
                            if((data_buffer.at(i + 1).time - data_buffer.at(i).time) > 5e-1)
                            {
                                buff_mutex_.unlock();
                                return false;
                            }
                        }

                        if(data_buffer.at(i).time > end_time)
                        {
                             end_index_ = i;
                            //????index
                            // std::cout << std::setprecision(20) <<  start_time << std::endl;
                            // std::cout << std::setprecision(20) <<  end_time << std::endl;

                            out_data_buffer.insert(out_data_buffer.end(), data_buffer.begin() + start_index_, data_buffer.begin() + end_index_ + 1);

                            // std::cout << std::setprecision(20) <<  data_buffer.at(i).time << std::endl;
                            // std::cout << std::setprecision(20) <<  out_data_buffer.front().time << std::endl;
                            // std::cout << std::setprecision(20) <<  out_data_buffer.back().time << std::endl;

                            break;
                        }
                    }
                    buff_mutex_.unlock();
                    return true;
                }else{
                    buff_mutex_.unlock();
                    return false;
                }
            }else{
                buff_mutex_.unlock();
                return false;
            }
        }

};

}














#endif