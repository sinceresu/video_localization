/*
 * @Description: 订阅观测pose数据
 * @Author: Zhou Mingchao
 * @Date: 2022-01-10 
 */
#ifndef FUSION_OPTIMIZER_OBSERVATION_SUBSCRIBER_HPP_
#define FUSION_OPTIMIZER_OBSERVATION_SUBSCRIBER_HPP_

#include <deque>
#include <mutex>
#include <thread>

#include <ros/ros.h>
#include <nav_msgs/Odometry.h>

#include "fusion_optimizer/sensor_data/pose_data.hpp"
#include "fusion_optimizer/utitilies/data_conversions.hpp"
#include "fusion_optimizer/utitilies/data_types.hpp"
#include "subscriber.hpp"


namespace fusion_optimizer {
class ObservationSubscriber : public Subscriber {
  public:
    ObservationSubscriber(ros::NodeHandle& nh, std::string topic_name, size_t buff_size);
    ObservationSubscriber() = default;
    void ParseData(std::deque<State>& deque_pose_data);
    
    template <typename datatype>
    bool ValidData(const double start_time, const double end_time, std::deque<datatype>& deque_imu_data)
    {
      if (Subscriber::ValidData<datatype>(start_time, end_time, new_pose_data_, deque_imu_data)){
        return true;
      }
      return false;
    }


  std::string topic_name_;
  private:
    void msg_callback(const nav_msgs::OdometryConstPtr& odom_msg_ptr);

  private:
    ros::NodeHandle nh_;
    ros::Subscriber subscriber_;
    std::deque<State> new_pose_data_;

    std::mutex buff_mutex_; 
};
}
#endif