/*
 * @Description: 订阅imu数据
 * @Author: Zhou Mingchao
 * @Date: 2022-01-10 
 */
#ifndef FUSION_OPTIMIZER_SUBSCRIBER_IMU_SUBSCRIBER_HPP_
#define FUSION_OPTIMIZER_SUBSCRIBER_IMU_SUBSCRIBER_HPP_

#include <deque>
#include <mutex>
#include <thread>

#include <ros/ros.h>
#include "sensor_msgs/Imu.h"

#include "fusion_optimizer/sensor_data/imu_data.hpp"
#include "subscriber.hpp"

namespace fusion_optimizer {
class IMUSubscriber : public Subscriber {
  public:
    IMUSubscriber() = default;
    IMUSubscriber(ros::NodeHandle& nh, std::string topic_name, size_t buff_size);
    void ParseData(std::deque<IMUData>& deque_imu_data);

    template <typename datatype>
    bool ValidData(const double start_time, const double end_time, std::deque<datatype>& deque_imu_data)
    {
      if(Subscriber::ValidData<datatype>(start_time, end_time, new_imu_data_, deque_imu_data)){
        return true;
      }
      return false;
    }

  private:
    void msg_callback(const sensor_msgs::ImuConstPtr& imu_msg_ptr);

  private:
    ros::NodeHandle nh_;
    ros::Subscriber subscriber_;
    std::deque<IMUData> new_imu_data_;

    std::mutex buff_mutex_; 
};
}
#endif