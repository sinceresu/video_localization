#ifndef FUSION_OPTIMIZER_PRE_TWIST_INTERGRATION_HPP_
#define FUSION_OPTIMIZER_PRE_TWIST_INTERGRATION_HPP_

#include "fusion_optimizer/sensor_data/pose_data.hpp"
#include "fusion_optimizer/utitilies/data_types.hpp"
#include "fusion_optimizer/utitilies/data_conversions.hpp"
#include "fusion_optimizer/sensor_data/twist_data.hpp"
#include "fusion_optimizer/params/params.hpp"
namespace fusion_optimizer{
class PreTwistIntergration : public Params
{
public:
    PreTwistIntergration() = default;
    PreTwistIntergration(const std::deque<TwistData>& twist_datas, const State& pre_state, const State& curr_state);
    void Propagate();
    static void midPointIntegration(const TwistData& pre_twist_data, 
                                    const TwistData& curr_twist_data,
                                    const Eigen::Quaterniond &dq,
                                    Eigen::Quaterniond &result_dq,
                                    const Eigen::Vector3d &alpha,
                                    Eigen::Vector3d &result_alpha,
                                    const Eigen::Matrix<double, 6, 6> &jacobian,
                                    const Eigen::Matrix<double, 6, 6> &covariance,
                                    Eigen::Matrix<double, 6, 6> &result_jacobian,
                                    Eigen::Matrix<double, 6, 6> &result_covariance,
                                    const Eigen::Matrix<double, 12, 12>& noise,
                                    bool update_jacobian);
    static State DeadReckoning(const State& opt_state, std::deque<TwistData>& twist_datas, const Eigen::Matrix<double, 12, 12>& noise);
    Eigen::Matrix<double, 6, 1> evaluate(const Eigen::Vector3d &Pi,
                                            const Eigen::Quaterniond &Qi,
                                            const Eigen::Vector3d &Pj,
                                            const Eigen::Quaterniond &Qj);
    Eigen::Matrix<double, 6, 12> jacobian(const Eigen::Vector3d &Pi,
                                            const Eigen::Quaterniond &Qi,
                                            const Eigen::Vector3d &Pj,
                                            const Eigen::Quaterniond &Qj);
    public:
        Eigen::Matrix<double, 6, 6> information_;
        Eigen::Matrix<double, 6, 6> jacobians_;
        Eigen::Matrix<double, 6, 6> covariance_;
    
    // private:
        Eigen::Matrix<double, 12, 12> noise_;
        std::deque<TwistData> twist_buff_;
        Eigen::Quaterniond curr_Q_;
        Eigen::Vector3d curr_alpha_;


        

};

}
#endif