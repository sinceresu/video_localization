#ifndef FUSION_OPTIMIZER_PRE_INTERGRATION_HPP_
#define FUSION_OPTIMIZER_PRE_INTERGRATION_HPP_

#include "fusion_optimizer/sensor_data/imu_data.hpp"
#include "fusion_optimizer/utitilies/data_conversions.hpp"
#include "fusion_optimizer/utitilies/data_types.hpp"
#include "fusion_optimizer/params/params.hpp"

namespace fusion_optimizer{
class PreIntergration : public Params
{
public:
    // EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    PreIntergration() = default;
    PreIntergration(const std::deque<IMUData>& imu_datas, const State& pre_state, const State& curr_state);
    void InitESKFSystem(State& init_measure_state);
    void SetInputImuDatas(const std::deque<IMUData>& imu_datas, State pre_state);
    void Propagate();
    void TransMeasureData2IMU(State& measure_state);
    bool RePropagate(const Eigen::Vector3d &Bai, const Eigen::Vector3d &Bgi);
    void ErrorEKFCorrect(State& measure_state);

    Eigen::Matrix<double, 15, 1> ComputerError(Eigen::Vector3d p1, Eigen::Quaterniond q1, Eigen::Vector3d vec1, Eigen::Vector3d ba1, Eigen::Vector3d bg1,
                                               Eigen::Vector3d p2, Eigen::Quaterniond q2, Eigen::Vector3d vec2, Eigen::Vector3d ba2, Eigen::Vector3d bg2);

    // Eigen::Matrix<double, 15, 30> GetJacobian(Eigen::Vector3d p1, Eigen::Quaterniond q1, Eigen::Vector3d vec1, Eigen::Vector3d ba1, Eigen::Vector3d bg1,
    //                                           Eigen::Vector3d p2, Eigen::Quaterniond q2, Eigen::Vector3d vec2, Eigen::Vector3d ba2, Eigen::Vector3d bg2);

    static State DeadReckoning(const State& opt_state, std::deque<IMUData>& imu_datas, const Eigen::Matrix<double, 18, 18>& noise, const Eigen::Vector3d& gn);
    static void midPointIntegration(const IMUData& pre_imu_data, 
                        const IMUData& curr_imu_data,
                        const Eigen::Quaterniond &dq,
                        Eigen::Quaterniond &result_dq,
                        const Eigen::Vector3d &beta,
                        Eigen::Vector3d &result_beta,
                        const Eigen::Vector3d &alpha,
                        Eigen::Vector3d &result_alpha,
                        const Eigen::Vector3d &linearized_ba,
                        const Eigen::Vector3d &linearized_bg,
                        const Eigen::Matrix<double, 15, 15> &jacobian,
                        const Eigen::Matrix<double, 15, 15> &covariance,
                        Eigen::Matrix<double, 15, 15> &result_jacobian,
                        Eigen::Matrix<double, 15, 15> &result_covariance,
                        const Eigen::Matrix<double, 18, 18>& noise,
                        const Eigen::Vector3d& gn,
                        bool update_jacobian,
                        bool gravity_included);

    Eigen::Matrix<double, 15, 1> evaluate(const Eigen::Vector3d &Pi,
                                            const Eigen::Quaterniond &Qi,
                                            const Eigen::Vector3d &Vi,
                                            const Eigen::Vector3d &Bai,
                                            const Eigen::Vector3d &Bgi,
                                            const Eigen::Vector3d &Pj,
                                            const Eigen::Quaterniond &Qj,
                                            const Eigen::Vector3d &Vj,
                                            const Eigen::Vector3d &Baj,
                                            const Eigen::Vector3d &Bgj);
    Eigen::Matrix<double, 15, 30> jacobian(const Eigen::Vector3d &Pi,
                                            const Eigen::Quaterniond &Qi,
                                            const Eigen::Vector3d &Vi,
                                            const Eigen::Vector3d &Bai,
                                            const Eigen::Vector3d &Bgi,
                                            const Eigen::Vector3d &Pj,
                                            const Eigen::Quaterniond &Qj,
                                            const Eigen::Vector3d &Vj,
                                            const Eigen::Vector3d &Baj,
                                            const Eigen::Vector3d &Bgj);

public:
    Eigen::Matrix<double, 15, 15> information_;
    Eigen::Matrix<double, 15, 15> jacobians_;
    Eigen::Matrix<double, 15, 15> covariance_;

private:
    std::deque<IMUData> imu_buff_;
    double sum_dt_;
    Eigen::Matrix<double, 15, 15> F_init_;
    Eigen::Matrix<double, 15, 18> G_init_;
    Eigen::Matrix<double, 15, 18> G_eskf_;
    Eigen::Matrix<double, 18, 18> noise_;

    // Eigen::Matrix<double, 15, 15> F_propagate_;
    // Eigen::Matrix<double, 15, 18> G_propagate_;


    // Eigen::Matrix3d imu_2_measure_sensor_R_;
    // Eigen::Vector3d imu_2_measure_sensor_t_;
    // Eigen::Matrix<double, 3, 3> imu_2_measure_sensor_;
    // Eigen::Isometry3d imu_2_measure_sensor_;

    bool system_init_;
    //速度初始化，不同于pq初始化，它要用imu数据赋值
    bool init_v_;

    Eigen::Matrix<double, 15, 6> eskf_K_;
    //Imu积分后的数据
    Eigen::Quaterniond curr_Q_;
    Eigen::Vector3d curr_P_;
    Eigen::Vector3d curr_V_;
    Eigen::Vector3d curr_ba_;
    Eigen::Vector3d curr_bg_;
    State state_;



    // double acc_n_;
    // double gyr_n_;
    // double acc_w_;
    // double gyr_w_;
    // Eigen::Vector3d gn_;

    //观测噪声
    double measure_p_n_;
    double measure_q_n_;

    State pre_state_;

    // Eigen::Matrix3d imu_2_measure_sensor_R_;
    // Eigen::Vector3d imu_2_measure_sensor_t_;



};

}//fusion_optimizer





#endif