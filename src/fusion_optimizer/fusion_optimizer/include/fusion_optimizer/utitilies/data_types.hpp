#ifndef PCL_NO_PRECOMPILE
#define PCL_NO_PRECOMPILE
#endif

#ifndef FUSION_OPTIMIZER_DATA_TYPES_HPP_
#define FUSION_OPTIMIZER_DATA_TYPES_HPP_

#include <deque>
#include <vector>
#include <unordered_map>
#include <Eigen/Geometry>
#include <Eigen/Core>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include "sophus/so3.hpp"
#include "sophus/interpolate.hpp"
// #include <Sophus/so3.h>
// #include <Sophus/interpolate.h>
// #include <Sophus/so3.h>
#include "glog/logging.h"
#include <yaml-cpp/yaml.h>

#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/PointCloud2.h>
#include "sensor_msgs/NavSatFix.h"
#include <sensor_msgs/Imu.h>
#include <sensor_msgs/Image.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <nav_msgs/Odometry.h>
#include "geometry_msgs/TwistStamped.h"
#include "geometry_msgs/PoseStamped.h"
namespace fusion_optimizer
{
    struct PointXYZT
    {
        // PCL_ADD_POINT4D;
        float x;
        float y;
        float z;
        float time;
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    } EIGEN_ALIGN16;
} // namespace fusion_optimizer
POINT_CLOUD_REGISTER_POINT_STRUCT(
    fusion_optimizer::PointXYZT,
    (float, x, x)(float, y, y)(float, z, z)(float, time, time))
namespace fusion_optimizer
{
    enum DiagnosticType
    {
        UpdateOk = 0,
        Stop,
        ReceivedInitialpose,
        Relocalization,
        JumpBackInTime,
        UpdateDelayed,
        WaitingForData,
        DeadReckoningFailed,
        InitWithLoopDetection,
        InitWithHistorypose,
        LoopFailed,
        LoopDetected,
        InitializeFailed1,
        InitializeFailed2,
        InitializeSucceeded1,
        InitializeSucceeded2,
        InitWindowDelayed,
        InitWindowCheckPositionFailed,
        InitWindowAddStateFailed1,
        InitWindowSucceeded,
        InitWindowFailed,
        BackEndFinishedIfFinishTrue,
        BackEndFinished,
        BackEndRunLDelayed,
        BackEndRunPDelayed,
        BackEndIfMeasureTrue,
        GetNStateNegative,
        BackEndMeasureFusionAddStateFalse,
        BackEndMeasureFusionOptimizeFalse,
        BackEndMeasureFusionSucceeded,
        BackEndIfPredictTrue,
        BackEndPredictFusionAddStateFalse,
        BackEndPredictFusionOptimizeFalse,
        BackEndPredictFusionSucceeded,
        FrontEndStatusFalse,
        BackEndIfInitedFalse,
        BackEndIfInitedTrue,
        FrontEndUpdateFalse,
        Finish,
        Finished,
        InitWindowAddStateSucceeded,
        WindowInited,
        InitializeFailed1Succeeded1,
        InitializeFailed1Failed1,
        InitWindowFailedInitIndexError,
        BackEndIfMeasureGetNStateFalse1,
        BackEndIfMeasureGetMStampFalse,
        BackEndIfMeasureGetNStateFalse2,
        BackEndIfMeasureGetMStateFalseWait,
        BackEndIfMeasureGetMStateFalseSetZero,
        BackEndMeasureNoLData,
        BackEndMeasureNoPData,
        BackEndIfPredictGetNStateFalse,
        BackEndIfPredictNoData,
        InitializeNoMeasure1,
        InitializeNoMeasure2,
        InitWindowNoMData1,
        InitWindowNoLData1,
        InitWindowNoPData1,
        InitWindowNoMData2,
        InitWindowNoLData2,
        InitWindowNoPData2,
        InitWindowAddStateFailed2,
    };

    enum StateType
    {
        None = 0,
        Basic,
        V,
        Bg,
        VBaBg,
    };

    struct State
    {
        State();
        State(const State &data);
        ~State() {}
        State &operator=(const State &rhs);
        void Print();
        static int SyncData(std::deque<State>& UnsyncedData, std::deque<State>& SyncedData, double sync_time);

        StateType type;
        int index;
        double stamp;
        double time;
        Eigen::Vector3d p;
        Eigen::Quaterniond q;
        Eigen::Vector3d v;
        Eigen::Vector3d ba;
        Eigen::Vector3d bg;
        Eigen::MatrixXd covariance;
        Eigen::Matrix<double, 15, 1> x;
    };

    struct MState
    {
        MState();
        MState(const MState &data);
        ~MState() {}
        MState &operator=(const MState &rhs);

        double stamp;
        Sophus::SE3d pose;
        Eigen::Vector3d linear_velocity;
        Eigen::Vector3d angular_velocity;
    };

    enum PredictType
    {
        Imu = 0,
        Twist,
        Odometry,
        Vg,
    };

    struct PredictData
    {
        PredictData();
        PredictData(const PredictData &data);
        virtual ~PredictData() {}
        PredictData &operator=(const PredictData &rhs);
        virtual PredictType GetType() const = 0;
        virtual void Clear() = 0;

        Sophus::SE3d sensor_to_robot;
        Eigen::VectorXd noise;
        std::deque<double> stamps;
    };
    typedef std::shared_ptr<PredictData> PredictDataPtr;

    struct ImuData : public PredictData
    {
        ImuData();
        ImuData(const ImuData &data);
        ~ImuData() {}
        ImuData &operator=(const ImuData &rhs);
        PredictType GetType() const override;
        void Clear() override;

        Eigen::Vector3d gravity;
        std::deque<Eigen::Vector3d> acc;
        std::deque<Eigen::Vector3d> gyro;
    };
    typedef std::shared_ptr<ImuData> ImuDataPtr;

/*     struct TwistData : public PredictData
    {
        TwistData();
        TwistData(const TwistData &data);
        ~TwistData() {}
        TwistData &operator=(const TwistData &rhs);
        PredictType GetType() const override;
        void Clear() override;

        std::deque<Eigen::Vector3d> vel;
        std::deque<Eigen::Vector3d> gyro;
    };
    typedef std::shared_ptr<TwistData> TwistDataPtr; */

    struct OdometryData : public PredictData
    {
        OdometryData();
        OdometryData(const OdometryData &data);
        ~OdometryData() {}
        OdometryData &operator=(const OdometryData &rhs);
        PredictType GetType() const override;
        void Clear() override;

        std::deque<Sophus::SE3d> pose;
    };
    typedef std::shared_ptr<OdometryData> OdometryDataPtr;

    struct VgData : public PredictData
    {
        VgData();
        VgData(const VgData &data);
        ~VgData() {}
        VgData &operator=(const VgData &data);
        PredictType GetType() const override;
        void Clear() override;

        Sophus::SE3d sensor_to_robot_gyro;
        std::deque<Eigen::Vector3d> vel;
        std::deque<Eigen::Vector3d> gyro;
    };
    typedef std::shared_ptr<VgData> VgDataPtr;

    enum MeasureType
    {
        Cloud = 0,
        GNSS,
        Image,
        Pose,
        P,
        Q,
    };

    struct MeasureData
    {
        MeasureData();
        MeasureData(const MeasureData &data);
        virtual ~MeasureData() {}
        MeasureData &operator=(const MeasureData &rhs);
        virtual MeasureType GetType() const = 0;

        Sophus::SE3d sensor_to_robot;
        Eigen::VectorXd noise;
        double stamp;
    };
    typedef std::shared_ptr<MeasureData> MeasureDataPtr;

    enum CloudPointType
    {
        XYZ = 0,
        XYZT,
    };
    struct CloudData : public MeasureData
    {
        CloudData();
        CloudData(const CloudData &data);
        ~CloudData() {}
        CloudData &operator=(const CloudData &rhs);
        MeasureType GetType() const override;

        CloudPointType point_type;
        pcl::PointCloud<pcl::PointXYZ>::Ptr xyz_ptr;
        pcl::PointCloud<PointXYZT>::Ptr xyzt_ptr;
    };
    typedef std::shared_ptr<CloudData> CloudDataPtr;

    struct GNSSData : public MeasureData
    {
        GNSSData();
        GNSSData(const GNSSData &data);
        ~GNSSData() {}
        GNSSData &operator=(const GNSSData &rhs);
        MeasureType GetType() const override;

        double latitude;
        double longitude;
        double altitude;
        int status;
        int service;
    };
    typedef std::shared_ptr<GNSSData> GNSSDataPtr;

    struct ImageData : public MeasureData
    {
        ImageData();
        ImageData(const ImageData &data);
        ~ImageData() {}
        ImageData &operator=(const ImageData &rhs);
        MeasureType GetType() const override;

        cv_bridge::CvImageConstPtr image_ptr;
    };
    typedef std::shared_ptr<ImageData> ImageDataPtr;

    struct PoseData : public MeasureData
    {
        PoseData();
        PoseData(const PoseData &data);
        ~PoseData() {}
        PoseData &operator=(const PoseData &rhs);
        MeasureType GetType() const override;

        Sophus::SE3d pose;
    };
    typedef std::shared_ptr<PoseData> PoseDataPtr;

    struct PData : public MeasureData
    {
        PData();
        PData(const PData &data);
        ~PData() {}
        PData &operator=(const PData &rhs);
        MeasureType GetType() const override;

        Eigen::Vector3d p;
    };
    typedef std::shared_ptr<PData> PDataPtr;

    struct QData : public MeasureData
    {
        QData();
        QData(const QData &data);
        ~QData() {}
        QData &operator=(const QData &rhs);
        MeasureType GetType() const override;

        Eigen::Quaterniond q;
    };
    typedef std::shared_ptr<QData> QDataPtr;

    enum LocalizerType
    {
        PL = 0,
        QL,
        PoseL,
    };
    struct LocalizerData
    {
        LocalizerData();
        LocalizerData(const LocalizerData &data);
        virtual ~LocalizerData() {}
        LocalizerData &operator=(const LocalizerData &rhs);
        virtual LocalizerType GetType() const = 0;

        Sophus::SE3d sensor_to_robot;
        Sophus::SE3d local_to_map;
        Eigen::VectorXd noise;
        double stamp;
    };
    typedef std::shared_ptr<LocalizerData> LocalizerDataPtr;

    struct PLocalizerData : public LocalizerData
    {
        PLocalizerData();
        PLocalizerData(const PLocalizerData &data);
        ~PLocalizerData() {}
        PLocalizerData &operator=(const PLocalizerData &rhs);
        LocalizerType GetType() const override;

        Eigen::Vector3d p;
    };
    typedef std::shared_ptr<PLocalizerData> PLocalizerDataPtr;

    struct QLocalizerData : public LocalizerData
    {
        QLocalizerData();
        QLocalizerData(const QLocalizerData &data);
        ~QLocalizerData() {}
        QLocalizerData &operator=(const QLocalizerData &rhs);
        LocalizerType GetType() const override;

        Eigen::Quaterniond q;
    };
    typedef std::shared_ptr<QLocalizerData> QLocalizerDataPtr;

    struct PoseLocalizerData : public LocalizerData
    {
        PoseLocalizerData();
        PoseLocalizerData(const PoseLocalizerData &data);
        ~PoseLocalizerData() {}
        PoseLocalizerData &operator=(const PoseLocalizerData &rhs);
        LocalizerType GetType() const override;

        Sophus::SE3d pose;
    };
    typedef std::shared_ptr<PoseLocalizerData> PoseLocalizerDataPtr;

    enum PublishType
    {
        CloudP = 0,
        OdometryP,
        PoseP,
    };

    struct PublishData
    {
        PublishData();
        PublishData(const PublishData &data);
        virtual ~PublishData() {}
        PublishData &operator=(const PublishData &rhs) = delete;
        virtual PublishType GetType() const = 0;

        double stamp;
    };
    typedef std::shared_ptr<PublishData> PublishDataPtr;

    struct CloudPublishData : public PublishData
    {
        CloudPublishData();
        CloudPublishData(const CloudPublishData &data);
        ~CloudPublishData() {}
        CloudPublishData &operator=(const CloudPublishData &rhs) = delete;
        PublishType GetType() const override;

        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_ptr;
    };
    typedef std::shared_ptr<CloudPublishData> CloudPublishDataPtr;

    struct PosePublishData : public PublishData
    {
        PosePublishData();
        PosePublishData(const PosePublishData &data);
        ~PosePublishData() {}
        PosePublishData &operator=(const PosePublishData &rhs) = delete;
        PublishType GetType() const override;

        Sophus::SE3d pose;
        Eigen::Matrix<double, 6, 6> covariance;
    };
    typedef std::shared_ptr<PosePublishData> PosePublishDataPtr;

    struct OdometryPublishData : public PublishData
    {
        OdometryPublishData();
        OdometryPublishData(const OdometryPublishData &data);
        ~OdometryPublishData() {}
        OdometryPublishData &operator=(const OdometryPublishData &rhs) = delete;
        PublishType GetType() const override;

        Sophus::SE3d pose;
        Eigen::Matrix<double, 6, 1> twist;
        Eigen::Matrix<double, 6, 6> posecov;
        Eigen::Matrix<double, 6, 6> twistcov;
    };
    typedef std::shared_ptr<OdometryPublishData> OdometryPublishDataPtr;

    struct MessagePointer
    {
        sensor_msgs::PointCloud2::ConstPtr cloud_ptr;
        sensor_msgs::NavSatFixConstPtr gnss_ptr;
        sensor_msgs::ImuConstPtr imu_ptr;
        geometry_msgs::PoseWithCovarianceStampedConstPtr pose_ptr;
        nav_msgs::OdometryConstPtr odometry_ptr;
        geometry_msgs::TwistStampedConstPtr twist_ptr;
        sensor_msgs::ImageConstPtr image_ptr;
        geometry_msgs::PoseStampedConstPtr p_ptr;
        sensor_msgs::ImuConstPtr q_ptr;
    };
} // namespace fusion_optimizer

#endif