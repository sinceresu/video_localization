#ifndef FUSION_OPTIMIZER_PREFICTOR_HPP_
#define FUSION_OPTIMIZER_PREFICTOR_HPP_

#include <glog/logging.h>

#include "fusion_optimizer/params/params.hpp"
#include "fusion_optimizer/subscriber/subscriber.hpp"
#include "fusion_optimizer/subscriber/imu_subscriber.hpp"
#include "fusion_optimizer/subscriber/twist_subscriber.hpp"
#include "fusion_optimizer/subscriber/observation_subscriber.hpp"

namespace fusion_optimizer{

class Predictor : public Subscriber{
public:
    Predictor(ros::NodeHandle& nh, const PredictorParam& predic_param);
    bool HasData();

    bool ValidImuData(const double start_time, const double end_time,  std::deque<IMUData> &out_data_buffer){
        if (std::dynamic_pointer_cast<IMUSubscriber>(subscriber_) != NULL){
            if((std::dynamic_pointer_cast<IMUSubscriber>(subscriber_))->ValidData<IMUData>(start_time, end_time, out_data_buffer))
            {
                return true;
            }
        }
        return false;
    }
    bool ValidOdomData(const double start_time, const double end_time,  std::deque<State> &out_data_buffer){
        if (std::dynamic_pointer_cast<ObservationSubscriber>(subscriber_) != NULL){
            if ((std::dynamic_pointer_cast<ObservationSubscriber>(subscriber_))->ValidData<State>(start_time, end_time, out_data_buffer)){
                return true;
            }
        }
        return false;
    };
    bool ValidTwistData(const double start_time, const double end_time,  std::deque<TwistData> &out_data_buffer){
    if (std::dynamic_pointer_cast<TwistSubscriber>(subscriber_) != NULL){
        if ((std::dynamic_pointer_cast<TwistSubscriber>(subscriber_))->ValidData<TwistData>(start_time, end_time, out_data_buffer)){
            return true;
        }
    }
    return false;
};
    std::string type_;
private:
    std::shared_ptr<Subscriber> subscriber_;
};
}

#endif