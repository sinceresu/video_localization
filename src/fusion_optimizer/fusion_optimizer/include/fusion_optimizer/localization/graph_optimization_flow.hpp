#ifndef FUSION_OPTIMIZER_GRAPH_OPYIMIZATION_FLOW_
#define FUSION_OPTIMIZER_GRAPH_OPYIMIZATION_FLOW_
#include "fusion_optimizer/localization/localization_flow.hpp"
#include "fusion_optimizer/optimizer/graph_slam.hpp"
#include "fusion_optimizer/optimizer/g2o/vertex_3_demensional.hpp"
#include "fusion_optimizer/utitilies/data_types.hpp"
#include "fusion_optimizer/imu_process/pre_integration.hpp"
#include "fusion_optimizer/imu_process/pre_twist_integration.hpp"
#include "fusion_optimizer/utitilies/data_conversions.hpp"

#include <tf_conversions/tf_eigen.h>
#include <geometry_msgs/PoseStamped.h>
#include <eigen_conversions/eigen_msg.h>
#include <glog/logging.h>
#include <thread>



namespace fusion_optimizer
{
class GraphOptimizerLocalizationFlow : public Localization
{
public:

    GraphOptimizerLocalizationFlow(ros::NodeHandle& nh, const std::string& imu_topic_name, 
                              const std::string& observation_topic_name, size_t buff_size);
    //窗口初始化
    bool InitWindows();
    void Addstate(State& state, bool if_cur);
    virtual bool LocalizationProcess() override;
    virtual void PublishOdom(std::shared_ptr<PosePublisher<nav_msgs::Odometry, nav_msgs::Odometry>> pose_pub_ptr, const State& state) override;
    void PublishPath(std::shared_ptr<PosePublisher<nav_msgs::Path, nav_msgs::Path>> path_pub_ptr_, const std::deque<State>& state_buff);
    void RunDeadReckoning();
    void Reset();


private:
    std::shared_ptr<GraphSlam> graph_slam_ptr_;
    int window_kfs_;

    Eigen::MatrixXd vis_information_matrix_;
    // Eigen::MatrixXd odo_information_matrix_;
    Eigen::MatrixXd imu_information_matrix_;

    double pre_time_;

    std::deque<IMUData> dead_reckoning_imu_data_buff_;
    std::deque<State> imu_state_buff_;

    Eigen::Matrix<double, 18, 18> noise_;

    State pre_odom_;
    State cur_odom_;




    
};

}




#endif