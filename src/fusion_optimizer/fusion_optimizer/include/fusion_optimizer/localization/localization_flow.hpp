#ifndef FUSION_OPTIMIZER_LOCALIZATION_HPP_
#define FUSION_OPTIMIZER_LOCALIZATION_HPP_

#include <ros/ros.h>
#include <ros/time.h>

#include "fusion_optimizer/imu_process/pre_integration.hpp"
#include "fusion_optimizer/subscriber/imu_subscriber.hpp"
#include "fusion_optimizer/subscriber/observation_subscriber.hpp"
#include "fusion_optimizer/publisher/pose_publisher.hpp"
#include "fusion_optimizer/publisher/path_publisher.hpp"
#include "fusion_optimizer/imu_process/pre_integration.hpp"
#include "fusion_optimizer/params/params.hpp"
#include "fusion_optimizer/sensor_bridge/predictor.hpp"


namespace fusion_optimizer 
{
class Localization : public Params
{
public:
    Localization(ros::NodeHandle& nh, const std::string& imu_topic_name, 
                 const std::string& observation_topic_name, size_t buff_size);
    virtual bool HaveData();
    virtual bool ValidData();
    virtual bool InitPose(std::shared_ptr<PreIntergration> pre_intergration_ptr);
    virtual void UpdataBuffData();
    virtual void Run();
    virtual bool LocalizationProcess() = 0;
    virtual void PublishOdom(std::shared_ptr<PosePublisher<nav_msgs::Odometry, nav_msgs::Odometry>> pose_pub_ptr, const State& state) = 0;
    

protected:
    std::shared_ptr<IMUSubscriber> imu_sub_ptr_;
    std::shared_ptr<IMUSubscriber> imu_sub_dead_reckoning_ptr_;
    std::shared_ptr<ObservationSubscriber> obs_sub_ptr_;
    std::shared_ptr<ObservationSubscriber> odom_sub_ptr_;
    std::shared_ptr<PosePublisher<nav_msgs::Odometry, nav_msgs::Odometry>> pose_pub_ptr_;
    std::shared_ptr<PosePublisher<nav_msgs::Path, nav_msgs::Path>> path_pub_ptr_;
    std::shared_ptr<PosePublisher<nav_msgs::Odometry, nav_msgs::Odometry>> imu_pose_pub_ptr_;
    std::deque<State> state_buff_;
    //拷贝的state_buff_，用于插值sync_odom_buff_
    std::deque<State> use_for_sync_state_buff_;

    std::deque<State> unsync_odom_buff_;
    std::deque<TwistData> pre_twist_intergration_imu_buff_;
    std::deque<State> sync_odom_buff_;
    std::deque<IMUData> imu_buff_;
    std::deque<IMUData> imu_buff_copy_;
    std::shared_ptr<PreIntergration> pre_intergration_ptr_;
    State pre_state_;
    State measure_state_;
    State optimizer_state_;
    State last_imu_state_;
    State pre_odom_;
    State cur_odom_;
    std::deque<IMUData> pre_intergration_imu_buff_;
    bool eskf_flow_init_;
    bool init_pose_;
    bool init_window_;
    std::vector<std::shared_ptr<Predictor>> predictors_;

};
}

















#endif