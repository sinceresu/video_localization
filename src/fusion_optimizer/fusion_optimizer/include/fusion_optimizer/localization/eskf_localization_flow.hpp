#ifndef FUSION_OPTIMIZER_ESKF_LOCALIZATION_FLOW_HPP_
#define FUSION_OPTIMIZER_ESKF_LOCALIZATION_FLOW_HPP_

#include "fusion_optimizer/imu_process/pre_integration.hpp"
#include "fusion_optimizer/subscriber/imu_subscriber.hpp"
#include "fusion_optimizer/subscriber/observation_subscriber.hpp"
#include "fusion_optimizer/imu_process/pre_integration.hpp"

namespace fusion_optimizer
{

class EskfLocalizationFlow
{
public:
    EskfLocalizationFlow() = default;
    EskfLocalizationFlow(ros::NodeHandle& nh, const std::string& imu_topic_name, 
                         const std::string& observation_topic_name, size_t buff_size);
    bool HaveData();
    bool ValidData();
    bool InitPose();
    void UpdataBuffData();
    void Run();


private:
    std::shared_ptr<IMUSubscriber> imu_sub_ptr_;
    std::shared_ptr<ObservationSubscriber> obs_sub_ptr_;
    std::deque<State> state_buff_;
    std::deque<IMUData> imu_buff_;
    std::shared_ptr<PreIntergration> pre_intergration_ptr_;
    State pre_state_;
    State measure_state_;
    std::deque<IMUData> pre_intergration_imu_buff_;
    bool eskf_flow_init_;
    bool init_pose_;


};    



} // namespace fusion_optimizer























#endif