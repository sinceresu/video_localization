#ifndef FUSION_OPTIMIZER_PARAMS_HPP_
#define FUSION_OPTIMIZER_PARAMS_HPP_

#include <ros/ros.h>
#include <Eigen/Core>
#include <vector>

namespace fusion_optimizer{

struct PredictorParam{
    std::string topic;
    std::string type;
    int buffer_size;
    std::vector<double> noise_p;
    std::vector<double> noise_q;
    std::vector<double> noise_imu;
    std::vector<double> noise;
    std::vector<double> gn;
    double max_acc;
    double max_gyro;
    double max_time;

};

class Params
{
public:
    Params()
    {
        if (!ros::ok())
        {
            return;
        }
        ros::NodeHandle nh;
        nh.param<std::vector<double>>("measurement_p_n", measurement_p_n_, std::vector<double>());
        nh.param<std::vector<double>>("measurement_q_n", measurement_q_n_, std::vector<double>());

        // nh.param<std::vector<double>>("odom_measurement_q_n", odom_measurement_q_n_, std::vector<double>());
        // nh.param<std::vector<double>>("odom_measurement_p_n", odom_measurement_p_n_, std::vector<double>());
        nh.param<int>("windows_max_size", windows_max_size_, 5);
        std::vector<double> vec_imu_2_measure_t;
        std::vector<double> vec_imu_2_measure_R;
        nh.param<std::vector<double>>("imu_2_measure_sensor_R", vec_imu_2_measure_R, std::vector<double>());
        nh.param<std::vector<double>>("imu_2_measure_sensor_t", vec_imu_2_measure_t, std::vector<double>());
        imu_2_measure_sensor_R_ = Eigen::Map<const Eigen::Matrix<double, -1, -1, Eigen::RowMajor>>(vec_imu_2_measure_R.data(), 3, 3);
        imu_2_measure_sensor_t_ = Eigen::Map<const Eigen::Matrix<double, -1, -1, Eigen::RowMajor>>(vec_imu_2_measure_t.data(), 3, 1);
        imu_2_measure_sensor_T_.setIdentity();
        imu_2_measure_sensor_T_.block<3, 3>(0, 0) = imu_2_measure_sensor_R_;
        imu_2_measure_sensor_T_.block<3, 1>(0, 3) = imu_2_measure_sensor_t_;
        
        //这里的camera_pose topic指的是观测信息优化之后，滑窗内最新帧的pose，目前任然是imu坐标系
        nh.param<std::string>("pose_publisher_topic", pose_publisher_topic_, "/camera_pose");
        nh.param<std::string>("imu_pose_publisher_topic", imu_pose_publisher_topic_, "/camera_pose");
        nh.param<std::string>("imu_path_publisher_topic", imu_path_publisher_topic_, "/camera_path");
        nh.param<std::string>("odom_sub_topic_name", odom_sub_topic_name_, "/orbslam3/camera_pose");
        
        nh.param<int>("pose_pub_buff", pose_pub_buff_, 100);
        nh.param<int>("state_has_vertex_num", state_has_vertex_num_, 5);

        nh.param<double>("lead_time", lead_time_, 0.2);
        nh.param<double>("dead_recking_update_time", dead_recking_update_time_, 0.05);
        nh.param<double>("outlier_chi2", outlier_chi2_, 3.0);

        nh.param<bool>("optimize_z", optimize_z_, false);
        nh.param<double>("init_z", init_z_, 0.2);

        nh.param<double>("max_std_deviation", max_std_deviation_, 2.0);

        int predictor_num;
        nh.param<int>("predictors/num", predictor_num, 0);
        PredictorParam pre_param;  
        for (int i = 1; i < (predictor_num + 1); i++)
        {   
            nh.param<std::string>("predictors/no_" + std::to_string(i) + "/type", pre_param.type, "null");
            nh.param<std::string>("predictors/no_" + std::to_string(i) + "/topic", pre_param.topic, "null");
            nh.param<std::vector<double>>("predictors/no_" + std::to_string(i) + "/noise", pre_param.noise, std::vector<double>());
            nh.param<double>("predictors/no_" + std::to_string(i) + "/max_time", pre_param.max_time, 0);
            nh.param<double>("predictors/no_" + std::to_string(i) + "/max_gyro", pre_param.max_gyro, 0);
            nh.param<double>("predictors/no_" + std::to_string(i) + "/max_acc", pre_param.max_acc, 0);
            Eigen::Matrix3d odom_measure_p, odom_measure_q;
            odom_measure_p.setIdentity();
            odom_measure_q.setIdentity();
            if (pre_param.type == "Imu")
            {
                nh.param<std::vector<double>>("predictors/no_" + std::to_string(i) + "/gravity", pre_param.gn, std::vector<double>());
                gn_ = Eigen::Map<const Eigen::Matrix<double, -1, -1, Eigen::RowMajor>>(pre_param.gn.data(), 3, 1);
                acc_n_ = pre_param.noise[0];
                gyr_n_ = pre_param.noise[1];
                acc_w_ = pre_param.noise[2];
                gyr_w_ = pre_param.noise[3];
            }else if(pre_param.type == "Odometry"){
                odom_measurement_p_n_ = std::vector<double>(pre_param.noise.begin(), pre_param.noise.begin() + 3);
                odom_measurement_q_n_ = std::vector<double>(pre_param.noise.end() - 3, pre_param.noise.end());
                for (int i = 0; i < 3; i++)
                {
                    odom_measure_p(i, i) = 1.0 / (odom_measurement_p_n_[i] * odom_measurement_p_n_[i]);
                    odom_measure_q(i, i) = 1.0 / (odom_measurement_q_n_[i] * odom_measurement_q_n_[i]);
                }
                odo_information_matrix_.block(0,0,3,3) = odom_measure_p;
                odo_information_matrix_.block(3,3,3,3) = odom_measure_q;
            }else if(pre_param.type == "Twist"){
                twist_acc_n_[0] =  pre_param.noise[0];
                twist_acc_n_[1] =  pre_param.noise[1];
                twist_acc_n_[2] =  pre_param.noise[2];

                twist_gyr_n_[0] =  pre_param.noise[3];
                twist_gyr_n_[1] =  pre_param.noise[4];
                twist_gyr_n_[2] =  pre_param.noise[5];
            }
            predictor_params_.push_back(pre_param);
        }
        //twist

    }
    //predictors
    std::vector<PredictorParam> predictor_params_;

    std::vector<double> measurement_p_n_;
    std::vector<double> measurement_q_n_;

    std::vector<double> odom_measurement_p_n_;
    std::vector<double> odom_measurement_q_n_;
    int windows_max_size_;

    Eigen::MatrixXd odo_information_matrix_;


    std::vector<double> vec_imu_2_measure_sensor_t_;
    Eigen::Matrix3d imu_2_measure_sensor_R_;
    Eigen::Vector3d imu_2_measure_sensor_t_;
    Eigen::Matrix4d imu_2_measure_sensor_T_;


    double acc_n_;
    double gyr_n_;
    double acc_w_;
    double gyr_w_;
    Eigen::Vector3d gn_;

    Eigen::Vector3d twist_acc_n_;
    Eigen::Vector3d twist_gyr_n_;


    std::string pose_publisher_topic_;
    std::string imu_pose_publisher_topic_;
    std::string imu_path_publisher_topic_;
    std::string odom_sub_topic_name_;
    int pose_pub_buff_;
    int state_has_vertex_num_;

    double lead_time_;
    double dead_recking_update_time_;
    double outlier_chi2_;

    // 视觉反定位选择不去优化Z
    double init_z_;
    bool optimize_z_;

    double max_std_deviation_;
    static bool marge_old_;
};






























}
#endif