#include <opencv2/opencv.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <stdio.h>
#include <string.h>
#include <dirent.h>
using namespace std;
using namespace cv;
static void SplitString(const std::string &s, std::vector<std::string> &v, const std::string &c)
{
	std::string::size_type pos1, pos2;
	pos2 = s.find(c);
	pos1 = 0;
	while (std::string::npos != pos2)
	{
		v.push_back(s.substr(pos1, pos2 - pos1));

		pos1 = pos2 + c.size();
		pos2 = s.find(c, pos1);
	}
	if (pos1 != s.length())
		v.push_back(s.substr(pos1));
};
static void readFileList(std::string path, std::vector<std::string> &files, std::string ext)
{
	struct dirent *ptr;
	DIR *dir;
	dir = opendir(path.c_str());
	while ((ptr = readdir(dir)) != NULL)
	{
		if (ptr->d_name[0] == '.')
			continue;
		std::vector<std::string> strVec;
		SplitString(ptr->d_name, strVec, ".");

		if (strVec.size() >= 2)
		{
			std::string p;
			if (strVec[strVec.size() - 1].compare(ext) == 0)
				files.push_back(p.assign(path).append("/").append(ptr->d_name));
		}
		else
		{
			std::string p;
			readFileList(p.assign(path).append("/").append(ptr->d_name), files, ext);
		}
	}
	closedir(dir);
};
int GetIMGDataFilePath(std::string strFileDirectory, std::vector<std::string> &imageList)
{
	readFileList(strFileDirectory, imageList, "png");
	return imageList.size();
}; 


int main(int argc, char** argv)
 
{
    cv::Mat projection_matrix;
    cv::Mat distortion_matrix;

	int org_image_size = 3040;
	int out_image_size = 2880;
    float m_intri_matrix[9] = {8.5480199272600237e+02, 0., 1.5067232887074153e+03, 0.,
       8.5480730698483421e+02, 1.5138260917779573e+03, 0., 0., 1. };
	m_intri_matrix[2] = (m_intri_matrix[2] - org_image_size / 2) + (out_image_size / 2);
	m_intri_matrix[5] = (m_intri_matrix[5] - org_image_size / 2) + (out_image_size / 2);
    float m_distor_matrix[4] = {3.8996454533623423e-02, 5.8890890610124634e-03,
       -8.5922383472045467e-03, 5.4759740373530911e-04};

	// 白机头 new rear
    // float m_intri_matrix[9] = {8.5348938451228321e+02, 0., 1.5224102617426900e+03, 0.,
    //    8.5331179177287993e+02, 1.5321728349072473e+03, 0., 0., 1. };
	// for (auto& intri : m_intri_matrix)
	// {
	// 	intri = intri / 3040 * 2880;
	// }
	// m_intri_matrix[8] = 1.0;


    // float m_distor_matrix[4] = {4.2231225280884258e-02, -9.9830491123620545e-03,
    //    6.4954649175840203e-03, -3.8334670997096426e-03};

    cv::Mat(3, 3, CV_32FC1, &m_intri_matrix).copyTo(projection_matrix);
    cv::Mat(sizeof(m_distor_matrix) / sizeof(m_distor_matrix[0]), 1, CV_32FC1, &m_distor_matrix).copyTo(distortion_matrix);
	std::cout <<"Image intri matrix is :" << std::endl << projection_matrix << std::endl;

    std::vector<std::string> imageList;
    imageList.clear();
    string strFileDirectory=argv[1];
    GetIMGDataFilePath(strFileDirectory, imageList);

	int image_id = 0;


    for(int i=0;i<imageList.size();i++){
        Mat frame=cv::imread(imageList[i]);
        Mat undistortImg;
		cv::fisheye::undistortImage(frame,undistortImg, projection_matrix, distortion_matrix, projection_matrix);
		// cv::undistort(frame,undistortImg, projection_matrix, distortion_matrix, projection_matrix);
		
        // cv::undistort(frame, undistortImg, projection_matrix, distortion_matrix, projection_matrix);
        // std::string image_name = strFileDirectory + "/undistor1/" + std::to_string(image_id) + ".jpg";
		// cv::imwrite(image_name, undistortImg);
		std::string image_name = imageList[i].replace(imageList[i].find(".png"), 4, "");
		image_name = image_name + ".jpg";
		std::cout << "image_name is " << image_name << std::endl;
		cv::imwrite(image_name, undistortImg);
		// image_id++;
    }
	return 0;
 
}
